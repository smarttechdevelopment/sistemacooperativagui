package com.smartech.sistemacooperativa.gui.util;

import java.awt.Container;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Window;
import java.awt.event.KeyEvent;
import javax.swing.JInternalFrame;
import javax.swing.JWindow;

/**
 *
 * @author Smartech
 */
public class AWTUtil {

    private static KeyEventDispatcher mayusculasKeyEventDispatcher = new KeyEventDispatcher() {
        @Override
        public boolean dispatchKeyEvent(KeyEvent e) {
            if (e.getID() == KeyEvent.KEY_TYPED) {
                if (e.getKeyChar() >= 'a' && e.getKeyChar() <= 'z') {
                    e.setKeyChar((char) (((int) e.getKeyChar()) - 32));
                } else if (e.getKeyChar() == ((int) 241)) {
                    e.setKeyChar((char) 209);
                } else if (e.getKeyChar() == ((int) 164)) {
                    e.setKeyChar((char) 165);
                }
            }
            return false;
        }
    };
    private static KeyEventDispatcher minusculasKeyEventDispatcher = new KeyEventDispatcher() {
        @Override
        public boolean dispatchKeyEvent(KeyEvent e) {
            if (e.getID() == KeyEvent.KEY_TYPED) {
                if (e.getKeyChar() >= 'A' && e.getKeyChar() <= 'Z') {
                    e.setKeyChar((char) (((int) e.getKeyChar()) + 32));
                }
                if (e.getKeyChar() == ((int) 209)) {
                    e.setKeyChar((char) 241);
                } else if (e.getKeyChar() == ((int) 165)) {
                    e.setKeyChar((char) 164);
                }
            }
            return false;
        }
    };

    public static synchronized void setUpperCase() {
        KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.removeKeyEventDispatcher(minusculasKeyEventDispatcher);
        manager.addKeyEventDispatcher(mayusculasKeyEventDispatcher);
    }

    public static synchronized void setLowerCase() {
        KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.removeKeyEventDispatcher(mayusculasKeyEventDispatcher);
        manager.addKeyEventDispatcher(minusculasKeyEventDispatcher);
    }

    public static synchronized void setRegularCase() {
        KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.removeKeyEventDispatcher(mayusculasKeyEventDispatcher);
        manager.removeKeyEventDispatcher(minusculasKeyEventDispatcher);
    }

    public static synchronized void tryDispose(Container objContainer) {
        if (objContainer instanceof Window) {
            ((Window) objContainer).dispose();
        } else if (objContainer instanceof JInternalFrame) {
            ((JInternalFrame) objContainer).dispose();
        } else if (objContainer instanceof JWindow) {
            ((JWindow) objContainer).dispose();
        } else {
            System.out.println("No se puede \"disposear\": " + objContainer.getClass().getName());
        }
    }

    public static synchronized void tryFocus(Container objContainer) {
        if (objContainer instanceof Window) {
            ((Window) objContainer).requestFocus();
            ((Window) objContainer).toFront();
        }
        if (objContainer instanceof JInternalFrame) {
            ((JInternalFrame) objContainer).requestFocus();
            try {
                ((JInternalFrame) objContainer).setSelected(true);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        if (objContainer instanceof JWindow) {
            ((JWindow) objContainer).requestFocus();
            ((JWindow) objContainer).toFront();
        }

    }
}
