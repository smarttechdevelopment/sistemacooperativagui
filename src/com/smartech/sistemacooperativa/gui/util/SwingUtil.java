package com.smartech.sistemacooperativa.gui.util;

import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.TipoDocumentoIdentidad;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JInternalFrame;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.border.Border;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.JTextComponent;
import javax.swing.text.PlainDocument;

/**
 *
 * @author Smartech
 */
public class SwingUtil {

    private static Border DEFAULT_JTEXTFIELD_BORDER = new JTextField().getBorder();

    private SwingUtil() {
    }

    public static List<String> verifyNumericInputs(JTextField... textFields) {
        List<String> lstStrings = new ArrayList<>();

        for (JTextField textField : textFields) {
            if (textField.getText().trim().isEmpty()) {
                lstStrings.add("Vacio: " + textField.getName());
                continue;
            }
            try {
                BigDecimal inputValue = new BigDecimal(textField.getText().trim());
                if (inputValue.compareTo(BigDecimal.ZERO) < 0) {
                    lstStrings.add("Valor: " + textField.getName());
                }
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
                lstStrings.add("Formato: " + textField.getName());
            }
        }

        return lstStrings;
    }

    public static void clear(JTextField... textFields) {
        for (JTextField textField : textFields) {
            textField.setText("");
        }
    }

    private static List<RowFilter<Object, Object>> getRowFilterList(String text) {
        String[] arr = text.trim().split(" ");
        if (arr.length == 0) {
            return null;
        }
        ArrayList<RowFilter<Object, Object>> lstFilters = new ArrayList<>(arr.length);
        RowFilter objRowFilter = null;
        for (int i = 0; i < arr.length; i++) {
            objRowFilter = getRegexFilter(arr[i]);
            if (objRowFilter != null) {
                lstFilters.add(objRowFilter);
            }
        }
        return lstFilters;
    }

    public static RowFilter getAndRowFilter(String text) {
        return RowFilter.andFilter(getRowFilterList(text));
    }

    public static RowFilter getOrRowFilter(String text) {
        return RowFilter.andFilter(getRowFilterList(text));
    }

    public static RowFilter getRegexFilter(String text) {
        RowFilter value = null;
        if (text != null && !text.isEmpty()) {
            value = RowFilter.regexFilter(text);
        }
        return value;
    }

    public static void textFilter(JTextField txt, TableRowSorter<TableModel> objSorter) {
        int caretPosition = txt.getCaretPosition();
        objSorter.setRowFilter(SwingUtil.getAndRowFilter(txt.getText()));
        txt.setCaretPosition(caretPosition);
    }

    public static void textFilter(KeyEvent evt, JTable tabla, JTextField txt, TableRowSorter<TableModel> objSorter) {
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_DOWN) {
            tabla.requestFocusInWindow();
            tabla.setRowSelectionInterval(0, 0);
        } else {
            SwingUtil.textFilter(txt, objSorter);
        }
    }

    public static void textFilter(KeyEvent evt, JTable tabla, JInternalFrame form, JTextField txt, TableRowSorter<TableModel> objSorter) {
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ESCAPE) {
            form.dispose();
        } else {
            SwingUtil.textFilter(evt, tabla, txt, objSorter);
        }
    }

    public static void textFilter(KeyEvent evt, JTable tabla, JDialog form, JTextField txt, TableRowSorter<TableModel> objSorter) {
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ESCAPE) {
            form.dispose();
        } else {
            SwingUtil.textFilter(evt, tabla, txt, objSorter);
        }
    }

    public static synchronized void centerOnScreen(JDialog dialog) {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        dialog.setLocation((dim.width - dialog.getWidth()) / 2, (dim.height - dialog.getHeight()) / 2);
    }

    public static synchronized void centerOnScreen(JInternalFrame internalFrame) {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        internalFrame.setLocation((dim.width - internalFrame.getWidth()) / 2, (dim.height - internalFrame.getHeight()) / 2);
    }

    public static void setRUCInput(JTextField... textFields) {
        for (JTextField textField : textFields) {
            textField.setDocument(new PlainDocument() {
                @Override
                public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
                    if (getLength() + str.length() <= 10 && str.matches("\\d+")) {
                        super.insertString(offs, str, a);
                    }
                }
            });
        }
    }

    public static void setDocumentoIdentidadInput(TipoDocumentoIdentidad objTipoDocumentoIdentidad, JTextField... textFields) {
        for (JTextField textField : textFields) {
            textField.setDocument(new PlainDocument() {
                @Override
                public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
                    if (getLength() + str.length() <= objTipoDocumentoIdentidad.getLongitud()) {
                        if (objTipoDocumentoIdentidad.isPermiteLetras()) {
                            if (str.matches("[a-zA-Z0-9]+")) {
                                super.insertString(offs, str, a);
                            }
                        } else if (str.matches("\\d+")) {
                            super.insertString(offs, str, a);
                        }
                    }
                }
            });
        }
    }

    public static void setCodigoInput(int size, JTextField... textFields){
        for (JTextField textField : textFields) {
            textField.setDocument(new PlainDocument() {
                @Override
                public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
                    if (getLength() + str.length() <= size && str.matches("\\d+")) {
                        super.insertString(offs, str, a);
                    }
                }
            });
        }
    }
    
    public static void setCodigoInput(JTextField... textFields) {
        for (JTextField textField : textFields) {
            textField.setDocument(new PlainDocument() {
                @Override
                public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
                    if (getLength() + str.length() <= 4 && str.matches("\\d+")) {
                        super.insertString(offs, str, a);
                    }
                }
            });
        }
    }

    public static void setPhoneInput(JTextField... textFields) {
        for (JTextField textField : textFields) {
            textField.setDocument(new PlainDocument() {
                @Override
                public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
                    if (getLength() + str.length() <= 15 && str.matches("\\d+")) {
                        super.insertString(offs, str, a);
                    }
                }
            });
        }
    }

    public static void setAccountNumberInput(JTextField... textFields) {
        for (JTextField textField : textFields) {
            textField.setDocument(new PlainDocument() {
                @Override
                public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
                    int max = Cuenta.DIGITOS_CODIGO;
                    if (getLength() + str.length() <= max) {
                        if(str.matches("[a-zA-Z0-9]+")){
                            //str.matches("\\d+|[a-zA-Z]+")
                            super.insertString(offs, str, a);
                        }
                    }
                }
            });
        }
    }

    public static void setStringInput(int max, JTextField... textFields) {
        for (JTextField textField : textFields) {
            textField.setDocument(new PlainDocument() {
                @Override
                public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
                    if (getLength() + str.length() <= max && str.matches("[a-zA-Z]")) {

                        super.insertString(offs, str, a);
                    }
                }
            });
        }
    }

    public static void setUsernameInput(JTextField... textFields) {
        for (JTextField textField : textFields) {
            textField.setDocument(new PlainDocument() {
                @Override
                public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
                    if (str.matches("[a-zA-Z0-9]*")) {
                        super.insertString(offs, str, a);
                    }
                }
            });
        }
    }

    public static void setNonWhiteSpaceInput(JTextField... textFields) {
        for (JTextField textField : textFields) {
            textField.setDocument(new PlainDocument() {
                @Override
                public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
                    if (str.matches("^\\S*[a-zA-Z0-9]\\S*")) {

                        super.insertString(offs, str, a);
                    }
                }
            });
        }
    }

    public static boolean hasBlankInputs(JTextComponent... textFields) {
        boolean result = false;

        for (JTextComponent textField : textFields) {
            if (textField.getText().trim().length() == 0) {
                result = true;
                textField.setBorder(BorderFactory.createLineBorder(Color.RED));
            } else {
                textField.setBorder(DEFAULT_JTEXTFIELD_BORDER);
            }
        }

        return result;
    }

    public static void setPositiveNumericInput(JTextComponent... textFields) {
        for (JTextComponent textField : textFields) {
            textField.setDocument(new PlainDocument() {
                @Override
                public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
                    String text = textField.getText() + str;
                    if (text.matches("[\\d]+[\\.]{0,1}[\\d]+")) {
                        super.insertString(offs, str, a);
                    } else if (text.matches("[\\d]+[\\.]{0,1}")) {
                        super.insertString(offs, str, a);
                    } else if (str.matches("[\\d]+")) {
                        super.insertString(offs, str, a);
                    }
                }
            });
        }
    }
}
