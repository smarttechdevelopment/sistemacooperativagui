package com.smartech.sistemacooperativa.gui.util;

import com.digitalpersona.onetouch.DPFPFingerIndex;
import com.digitalpersona.onetouch.DPFPGlobal;
import com.digitalpersona.onetouch.DPFPTemplate;
import com.smartech.sistemacooperativa.dominio.Huella;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class FingerprintUtil {

    private static final EnumMap<DPFPFingerIndex, String> fingerNames;

    private FingerprintUtil() {
    }

    static {
        fingerNames = new EnumMap<>(DPFPFingerIndex.class);
        fingerNames.put(DPFPFingerIndex.LEFT_PINKY, "Meñique");
        fingerNames.put(DPFPFingerIndex.LEFT_RING, "Anular");
        fingerNames.put(DPFPFingerIndex.LEFT_MIDDLE, "Medio");
        fingerNames.put(DPFPFingerIndex.LEFT_INDEX, "Indice");
        fingerNames.put(DPFPFingerIndex.LEFT_THUMB, "Pulgar");

        fingerNames.put(DPFPFingerIndex.RIGHT_PINKY, "Meñique");
        fingerNames.put(DPFPFingerIndex.RIGHT_RING, "Anular");
        fingerNames.put(DPFPFingerIndex.RIGHT_MIDDLE, "Medio");
        fingerNames.put(DPFPFingerIndex.RIGHT_INDEX, "Indice");
        fingerNames.put(DPFPFingerIndex.RIGHT_THUMB, "Pulgar");
    }

    public static String fingerName(DPFPFingerIndex finger) {
        return fingerNames.get(finger);
    }

    public static boolean getHand(DPFPFingerIndex fingerIndex) {
        return !(fingerIndex.equals(DPFPFingerIndex.LEFT_INDEX)
                || fingerIndex.equals(DPFPFingerIndex.LEFT_MIDDLE)
                || fingerIndex.equals(DPFPFingerIndex.LEFT_PINKY)
                || fingerIndex.equals(DPFPFingerIndex.LEFT_RING)
                || fingerIndex.equals(DPFPFingerIndex.LEFT_THUMB));
    }

    public static DPFPFingerIndex getFingerIndex(Huella objHuella) {
        if (objHuella.isMano()) {
            if (objHuella.getDedo().equals("Meñique")) {
                return DPFPFingerIndex.RIGHT_PINKY;
            }
            if (objHuella.getDedo().equals("Anular")) {
                return DPFPFingerIndex.RIGHT_RING;
            }
            if (objHuella.getDedo().equals("Medio")) {
                return DPFPFingerIndex.RIGHT_MIDDLE;
            }
            if (objHuella.getDedo().equals("Indice")) {
                return DPFPFingerIndex.RIGHT_INDEX;
            }
            if (objHuella.getDedo().equals("Pulgar")) {
                return DPFPFingerIndex.RIGHT_THUMB;
            }
        } else {
            if (objHuella.getDedo().equals("Meñique")) {
                return DPFPFingerIndex.LEFT_PINKY;
            }
            if (objHuella.getDedo().equals("Anular")) {
                return DPFPFingerIndex.LEFT_RING;
            }
            if (objHuella.getDedo().equals("Medio")) {
                return DPFPFingerIndex.LEFT_MIDDLE;
            }
            if (objHuella.getDedo().equals("Indice")) {
                return DPFPFingerIndex.LEFT_INDEX;
            }
            if (objHuella.getDedo().equals("Pulgar")) {
                return DPFPFingerIndex.LEFT_THUMB;
            }
        }
        
        return null;
    }

    public static List<Huella> getHuellas(EnumMap<DPFPFingerIndex, DPFPTemplate> fingers) {
        List<Huella> lstHuellas = new ArrayList<>();

        for (Map.Entry<DPFPFingerIndex, DPFPTemplate> entry : fingers.entrySet()) {
            Huella objHuella = new Huella(
                    0,
                    entry.getValue().serialize(),
                    fingerName(entry.getKey()),
                    getHand(entry.getKey()));
            lstHuellas.add(objHuella);
        }

        return lstHuellas;
    }

    public static EnumMap<DPFPFingerIndex, DPFPTemplate> getHuellas(List<Huella> lstHuellas) {
        EnumMap<DPFPFingerIndex, DPFPTemplate> fingers = new EnumMap<>(DPFPFingerIndex.class);

        for (Huella objHuella : lstHuellas) {
            DPFPTemplate template = DPFPGlobal.getTemplateFactory().createTemplate(objHuella.getHuella());
            fingers.put(getFingerIndex(objHuella), template);
        }

        return fingers;
    }
}
