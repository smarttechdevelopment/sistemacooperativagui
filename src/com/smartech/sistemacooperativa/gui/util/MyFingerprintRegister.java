package com.smartech.sistemacooperativa.gui.util;

import com.digitalpersona.onetouch.DPFPFingerIndex;
import com.digitalpersona.onetouch.DPFPTemplate;
import com.digitalpersona.onetouch.ui.swing.DPFPEnrollmentControl;
import com.digitalpersona.onetouch.ui.swing.DPFPEnrollmentEvent;
import com.digitalpersona.onetouch.ui.swing.DPFPEnrollmentListener;
import com.digitalpersona.onetouch.ui.swing.DPFPEnrollmentVetoException;
import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EnumMap;
import java.util.EnumSet;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

/**
 *
 * @author Smartech
 */
public class MyFingerprintRegister extends JDialog{
    
    private static int MAX_COUNT = 10;
    
    private String reasonToFail = null;

    private EnumMap<DPFPFingerIndex, DPFPTemplate> fingerPrints = new EnumMap<>(DPFPFingerIndex.class);
    private final DPFPEnrollmentControl enrollmentControl;
    
    public MyFingerprintRegister(Frame parent, boolean modal) {
        super(parent, modal);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Registro de Huellas");
        
        this.enrollmentControl = new DPFPEnrollmentControl();

        EnumSet<DPFPFingerIndex> fingers = EnumSet.noneOf(DPFPFingerIndex.class);
        fingers.addAll(this.fingerPrints.keySet());
        this.enrollmentControl.setEnrolledFingers(fingers);
        this.enrollmentControl.setMaxEnrollFingerCount(MAX_COUNT);
        
        enrollmentControl.addEnrollmentListener(new DPFPEnrollmentListener() {
            @Override
            public void fingerDeleted(DPFPEnrollmentEvent e) throws DPFPEnrollmentVetoException {
                if (reasonToFail != null) {
                    throw new DPFPEnrollmentVetoException(reasonToFail);
                } else {
                    MyFingerprintRegister.this.fingerPrints.remove(e.getFingerIndex());
                }
            }

            @Override
            public void fingerEnrolled(DPFPEnrollmentEvent e) throws DPFPEnrollmentVetoException {
                if (reasonToFail != null) {
                    throw new DPFPEnrollmentVetoException(reasonToFail);
                } else {
                    byte[] fingerArray = e.getTemplate().serialize();
                    MyFingerprintRegister.this.fingerPrints.put(e.getFingerIndex(), e.getTemplate());
                }
            }
        });
        
        JButton closeButton = new JButton("Guardar");
        closeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        
        JPanel bottom = new JPanel();
        bottom.add(closeButton);
        
        this.add(this.enrollmentControl, BorderLayout.CENTER);
        this.add(bottom, BorderLayout.PAGE_END);
        
        this.pack();
        this.setLocationRelativeTo(null);
    }

    public EnumMap<DPFPFingerIndex, DPFPTemplate> getFingerPrints() {
        return fingerPrints;
    }

    public void setFingerPrints(EnumMap<DPFPFingerIndex, DPFPTemplate> fingerPrints) {
        this.fingerPrints = fingerPrints;
    }
}
