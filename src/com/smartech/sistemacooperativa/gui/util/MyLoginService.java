package com.smartech.sistemacooperativa.gui.util;

import com.smartech.sistemacooperativa.bll.facade.SecurityFacade;
import com.smartech.sistemacooperativa.dal.DAOEmpleado;
import com.smartech.sistemacooperativa.dal.DAOSocio;
import com.smartech.sistemacooperativa.dominio.Persona;
import com.smartech.sistemacooperativa.dominio.Usuario;
import com.smartech.sistemacooperativa.gui.PrincipalJRibbon;
import com.smartech.sistemacooperativa.util.generics.StringUtil;
import java.util.ArrayList;
import java.util.List;
import javax.swing.SwingUtilities;
import org.jdesktop.swingx.auth.LoginService;

/**
 *
 * @author Smartech
 */
public class MyLoginService extends LoginService {

    @Override
    public boolean authenticate(String string, char[] chars, String string1) throws Exception {
        final List<Usuario> lstUsuarios = SecurityFacade.getInstance().verifyAccess(string, StringUtil.encodeString(new String(chars)));

        if (lstUsuarios.isEmpty()) {
            return false;
        }

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                List<Persona> lstPersonas = new ArrayList<>();

                switch (lstUsuarios.get(0).getObjTipoUsuario().getTipoPersona()) {
                    case 1:
                        for (Usuario objUsuario : lstUsuarios) {
                            Persona objPersona = DAOEmpleado.getInstance().getEmpleado(objUsuario);
                            lstPersonas.add(objPersona);
                        }
                        break;

                    case 2:
                        for (Usuario objUsuario : lstUsuarios) {
                            Persona objPersona = DAOSocio.getInstance().getSocio(objUsuario, false);
                            lstPersonas.add(objPersona);
                        }
                        break;
                }

                PrincipalJRibbon principalJRibbon = PrincipalJRibbon.getInstance();
                principalJRibbon.setLstPersonas(lstPersonas);
                principalJRibbon.setLstUsuarios(lstUsuarios);
            }
        });

        return true;
    }
}
