package com.smartech.sistemacooperativa.gui.util;

import com.digitalpersona.onetouch.DPFPFingerIndex;
import com.digitalpersona.onetouch.DPFPGlobal;
import com.digitalpersona.onetouch.DPFPTemplate;
import com.digitalpersona.onetouch.ui.swing.DPFPVerificationControl;
import com.digitalpersona.onetouch.ui.swing.DPFPVerificationEvent;
import com.digitalpersona.onetouch.ui.swing.DPFPVerificationListener;
import com.digitalpersona.onetouch.ui.swing.DPFPVerificationVetoException;
import com.digitalpersona.onetouch.verification.DPFPVerification;
import com.digitalpersona.onetouch.verification.DPFPVerificationResult;
import com.smartech.sistemacooperativa.dominio.Usuario;
import java.awt.BorderLayout;
import java.awt.Frame;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

/**
 *
 * @author Smartech
 */
public class MyFingerprintVerification extends JDialog {

    private static int FAR_REQUESTED = 21474;

    private EnumMap<DPFPFingerIndex, DPFPTemplate> fingers;
    private DPFPVerificationControl verificationControl;

    private boolean matched;
    private int farAchieved;
    public static final String FAR_PROPERTY = "FAR";
    public static final String MATCHED_PROPERTY = "Matched";
    private boolean verificacionMultiple;

    private List<Usuario> lstUsuarios = new ArrayList<>();

    public MyFingerprintVerification(Frame parent, boolean modal, List<Usuario> lstUsuarios) {
        super(parent, modal);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Verificacion Biometrica");
        this.verificacionMultiple = false;

        this.lstUsuarios = lstUsuarios;
        this.verificationControl = new DPFPVerificationControl();

        this.verificationControl.addVerificationListener(new DPFPVerificationListener() {
            @Override
            public void captureCompleted(DPFPVerificationEvent e) throws DPFPVerificationVetoException {
                final DPFPVerification verification = DPFPGlobal.getVerificationFactory().createVerification(FAR_REQUESTED);
                e.setStopCapture(false);
                int bestFAR = DPFPVerification.PROBABILITY_ONE;
                boolean hasMatch = false;
                Usuario objUsuarioVerificado = null;
                for (Usuario objUsuario : MyFingerprintVerification.this.lstUsuarios) {
                    boolean userMatch = false;
                    EnumMap<DPFPFingerIndex, DPFPTemplate> userFingers = FingerprintUtil.getHuellas(objUsuario.getLstHuellas());
                    for (DPFPTemplate template : userFingers.values()) {
                        final DPFPVerificationResult result = verification.verify(e.getFeatureSet(), template);
                        e.setMatched(result.isVerified());
                        bestFAR = Math.min(bestFAR, result.getFalseAcceptRate());
                        if (e.getMatched()) {
                            userMatch = true;
                            objUsuarioVerificado = objUsuario;
                            break;
                        }
                    }
                    if (userMatch) {
                        hasMatch = true;
                        break;
                    }
                }
                setMatched(hasMatch);
                setFAR(bestFAR);
                getResult(objUsuarioVerificado, hasMatch);
            }
        });

        this.setPanel();
    }

    public MyFingerprintVerification(Frame parent, boolean modal, Usuario objUsuario) {
        super(parent, modal);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Verificacion Biometrica");
        this.verificacionMultiple = false;

        this.fingers = FingerprintUtil.getHuellas(objUsuario.getLstHuellas());

        this.verificationControl = new DPFPVerificationControl();

        this.verificationControl.addVerificationListener(new DPFPVerificationListener() {
            @Override
            public void captureCompleted(DPFPVerificationEvent e) throws DPFPVerificationVetoException {
                final DPFPVerification verification = DPFPGlobal.getVerificationFactory().createVerification(FAR_REQUESTED);
                e.setStopCapture(false);
                int bestFAR = DPFPVerification.PROBABILITY_ONE;
                boolean hasMatch = false;
                for (DPFPTemplate template : MyFingerprintVerification.this.fingers.values()) {
                    final DPFPVerificationResult result = verification.verify(e.getFeatureSet(), template);
                    e.setMatched(result.isVerified());
                    bestFAR = Math.min(bestFAR, result.getFalseAcceptRate());
                    if (e.getMatched()) {
                        hasMatch = true;
                        break;
                    }
                }
                setMatched(hasMatch);
                setFAR(bestFAR);
                getResult();
            }
        });

        this.setPanel();
    }

    private void setPanel() {
        JPanel center = new JPanel();
        center.add(this.verificationControl);
        center.add(new JLabel("Para la verificacion, coloque su huella."));
        this.add(center, BorderLayout.CENTER);
        this.pack();
        this.setLocationRelativeTo(null);
    }

    private void getResult() {
        if (this.matched) {
            JOptionPane.showMessageDialog(this, "Verificado", "Resultado", JOptionPane.INFORMATION_MESSAGE);
            this.dispose();
        } else {
            JOptionPane.showMessageDialog(this, "No hay coincidencia. Intente nuevamente.", "Resultado", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void getResult(Usuario objUsuario, boolean userMatched) {
        if (objUsuario != null) {
            if (userMatched) {
                JOptionPane.showMessageDialog(this, "Tipo de Usuario " + objUsuario.getObjTipoUsuario().getNombre() + "  Verificado!\nUsuario: " + objUsuario.getUsername(), "Resultado", JOptionPane.INFORMATION_MESSAGE);
                this.dispose();
            } else {
                JOptionPane.showMessageDialog(this, "No hay coincidencia. Intente nuevamente.", "Resultado", JOptionPane.ERROR_MESSAGE);
            }
        }else{
            JOptionPane.showMessageDialog(this, "No hay coincidencia. Intente nuevamente.", "Resultado", JOptionPane.ERROR_MESSAGE);
        }
    }

    public boolean isMatched() {
        return matched;
    }

    public int getFAR() {
        return farAchieved;
    }

    protected void setFAR(int far) {
        final int old = getFAR();
        farAchieved = far;
        firePropertyChange(FAR_PROPERTY, old, getFAR());
    }

    public boolean getMatched() {
        return matched;
    }

    protected void setMatched(boolean matched) {
        final boolean old = getMatched();
        this.matched = matched;
        firePropertyChange(MATCHED_PROPERTY, old, getMatched());
    }

    @Override
    public void setVisible(boolean b) {
        if (b) {
            matched = false;
            verificationControl.start();
        } else if (!matched) {
            verificationControl.stop();
        }
        super.setVisible(b);
    }
}
