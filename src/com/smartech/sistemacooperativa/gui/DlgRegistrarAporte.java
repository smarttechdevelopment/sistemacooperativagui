package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.process.AportesProcessLogic;
import com.smartech.sistemacooperativa.bll.process.PrestamosProcessLogic;
import com.smartech.sistemacooperativa.bll.process.RenunciaSocioProcessLogic;
import com.smartech.sistemacooperativa.dominio.Aporte;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.Tarjeta;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import com.smartech.sistemacooperativa.util.interfaces.IObservable;
import com.smartech.sistemacooperativa.util.interfaces.IObserver;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Smartech
 */
public class DlgRegistrarAporte extends javax.swing.JDialog implements IObservable {

    private List<IObserver> lstIObservers = new ArrayList<>();
    private Tarjeta objTarjeta = null;
    private Aporte objAporteReingreso = null;

    public DlgRegistrarAporte(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();

        setTitle("Registrar Aporte");

        SwingUtil.setPositiveNumericInput(this.txtMonto);
    }

    public DlgRegistrarAporte(java.awt.Frame parent, boolean modal, Aporte objAporte) {
        super(parent, modal);
        initComponents();

        this.objAporteReingreso = objAporte;

        setTitle("Registrar Aporte");

        SwingUtil.setPositiveNumericInput(this.txtMonto);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtSocio = new javax.swing.JTextField();
        btnIngresarTarjeta = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txtMonto = new javax.swing.JTextField();
        btnGuardar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("Socio:");

        txtSocio.setEditable(false);

        btnIngresarTarjeta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/credit-card.png"))); // NOI18N
        btnIngresarTarjeta.setText("Ingresar Tarjeta");
        btnIngresarTarjeta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIngresarTarjetaActionPerformed(evt);
            }
        });

        jLabel2.setText("Monto:");

        txtMonto.setEditable(false);

        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_save.png"))); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtSocio, javax.swing.GroupLayout.DEFAULT_SIZE, 313, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnIngresarTarjeta))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtMonto, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnGuardar)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnIngresarTarjeta)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtMonto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnGuardar)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnIngresarTarjetaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIngresarTarjetaActionPerformed
        DlgIngresarTarjeta dlgIngresarTarjeta = new DlgIngresarTarjeta(PrincipalJRibbon.getInstance().getFrame(), true, false, false);
        SwingUtil.centerOnScreen(dlgIngresarTarjeta);
        dlgIngresarTarjeta.setVisible(true);
        this.objTarjeta = dlgIngresarTarjeta.getObjTarjeta();

        if (this.objTarjeta != null) {
            if (this.objTarjeta.getObjSocio() != null && this.objTarjeta.getObjSocio().isEstado()) {
                this.txtSocio.setText(this.objTarjeta.getObjSocio().getNombreCompleto());

                if (PrestamosProcessLogic.getInstance().debeAportes(this.objTarjeta.getObjSocio())) {
                    JOptionPane.showMessageDialog(this, "El socio debe cancelar aportes", "Advertencia", JOptionPane.WARNING_MESSAGE);
                    BigDecimal deuda = PrestamosProcessLogic.getInstance().calcularDeudaAportes(this.objTarjeta.getObjSocio());
                    this.txtMonto.setText(deuda.toPlainString());
                    this.txtMonto.setEditable(false);
                } else {
                    this.txtMonto.setEditable(true);
                }
            }else{
                JOptionPane.showMessageDialog(this, "El socio ha sido dado de baja", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btnIngresarTarjetaActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        if (this.objTarjeta != null) {

            if (SwingUtil.hasBlankInputs(this.txtMonto)) {
                JOptionPane.showMessageDialog(this, "Error en el registro\n Revise los campos", "Error", JOptionPane.ERROR_MESSAGE);
            } else if (this.objTarjeta.getObjSocio() != null && this.objTarjeta.getObjSocio().isEstado()) {
                int respuesta = JOptionPane.showConfirmDialog(this, "Confirmar Aporte de S/" + this.txtMonto.getText(), "Cuenta", JOptionPane.WARNING_MESSAGE);

                if (respuesta == JOptionPane.YES_OPTION) {
//                    String estadoActividad = RenunciaSocioProcessLogic.getInstance().getEstadoActividadSocio(this.objTarjeta.getObjSocio());
//                    if (!estadoActividad.equals(Socio.ESTADO_ACTIVIDAD_PASIVO) && this.objTarjeta.getObjSocio().isActivo()) {
                    if (this.objTarjeta.getObjSocio().isActivo() || this.objAporteReingreso != null) {
                        Aporte objAporte;
                        if (this.objAporteReingreso != null) {
                            objAporte = this.objAporteReingreso;
                        } else {
                            objAporte = new Aporte(
                                    0,
                                    new BigDecimal(this.txtMonto.getText()),
                                    DateUtil.currentTimestamp(),
                                    null,
                                    true,
                                    PrincipalJRibbon.getInstance().getObjUsuarioLogeado(),
                                    true,
                                    false,
                                    false,
                                    false,
                                    this.objTarjeta,
                                    new ArrayList<>());
                        }

                        boolean result = AportesProcessLogic.getInstance().registrarAporte(objAporte);

                        if (result) {
                            JOptionPane.showMessageDialog(this, "Aporte registrado correctamente!", "Aporte", JOptionPane.INFORMATION_MESSAGE);
                            this.notifyObservers();
                            this.dispose();
                        } else {
                            JOptionPane.showMessageDialog(this, "Error en el registro", "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "El Socio se encuentra Inactivo", "Error", JOptionPane.ERROR_MESSAGE);
                    }
//                    }
                }

            } else {
                // estado = false
                JOptionPane.showMessageDialog(this, "El socio ha sido dado de baja", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnIngresarTarjeta;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JTextField txtMonto;
    private javax.swing.JTextField txtSocio;
    // End of variables declaration//GEN-END:variables

    @Override
    public void addObserver(IObserver objIObserver) {
        this.lstIObservers.add(objIObserver);
    }

    @Override
    public List<IObserver> getObserverList() {
        return this.lstIObservers;
    }

    @Override
    public void notifyObservers() {
        for (IObserver objIObserver : this.lstIObservers) {
            objIObserver.update(this);
        }
    }

}
