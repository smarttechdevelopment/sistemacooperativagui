package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.process.AdmisionSociosProcessLogic;
import com.smartech.sistemacooperativa.dominio.Departamento;
import com.smartech.sistemacooperativa.dominio.DetalleFamiliarSocio;
import com.smartech.sistemacooperativa.dominio.Distrito;
import com.smartech.sistemacooperativa.dominio.EstadoCivil;
import com.smartech.sistemacooperativa.dominio.Familiar;
import com.smartech.sistemacooperativa.dominio.GradoInstruccion;
import com.smartech.sistemacooperativa.dominio.Huella;
import com.smartech.sistemacooperativa.dominio.Parentesco;
import com.smartech.sistemacooperativa.dominio.Provincia;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.TipoDocumentoIdentidad;
import com.smartech.sistemacooperativa.dominio.TipoTrabajador;
import com.smartech.sistemacooperativa.gui.util.AWTUtil;
import com.smartech.sistemacooperativa.gui.util.FingerprintUtil;
import com.smartech.sistemacooperativa.gui.util.MyFingerprintRegister;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.gui.util.TableCellListener;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import com.smartech.sistemacooperativa.util.interfaces.IObservable;
import com.smartech.sistemacooperativa.util.interfaces.IObserver;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Smartech
 */
public class IFrmRegistroSocio extends javax.swing.JInternalFrame implements IObservable {

    private Socio objSocio;
    private List<Departamento> lstDepartamentos = new ArrayList<>();
    private List<TipoTrabajador> lstTipoTrabajador = new ArrayList<>();
    private final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    private List<Huella> lstHuellas = new ArrayList<>();
    private List<EstadoCivil> lstEstadosCiviles = new ArrayList<>();
    private List<GradoInstruccion> lstGradosInstruccion = new ArrayList<>();
    private List<IObserver> lstIObservers = new ArrayList<>();
    private List<TipoDocumentoIdentidad> lstTiposDocumentoIdentidad = new ArrayList<>();
    private List<Parentesco> lstParentescos = new ArrayList<>();
    private List<TipoDocumentoIdentidad> lstTiposDocumentoIdentidadBeneficiario = new ArrayList<>();
    private List<DetalleFamiliarSocio> lstDetalleFamiliarSocio = new ArrayList<>();
    private BufferedImage foto = null;
    private File fileFoto = null;

    public IFrmRegistroSocio(Socio objSocio) {
        initComponents();
        AWTUtil.setUpperCase();
        this.lstTiposDocumentoIdentidad = QueryFacade.getInstance().getAllTiposDocumentoIdentidad();
        this.updateCmbTiposDocumentoIdentidadSocio();
        this.cmbTipoDocumentoIdentidad.setSelectedItem(objSocio.getObjTipoDocumentoIdentidad().getNombre());
        
        this.lstTiposDocumentoIdentidadBeneficiario = QueryFacade.getInstance().getAllTiposDocumentoIdentidad();
        this.updateCmbTiposDocumentoIdentidadBeneficiario();
        
        this.setTextFieldsConfiguration();

        this.objSocio = objSocio;
        
        this.foto = objSocio.getFoto();
        this.updateFoto();

        this.lstDepartamentos = QueryFacade.getInstance().getAllDepartamentos();
        this.lstTipoTrabajador = QueryFacade.getInstance().getAllTiposTrabajador();
        this.lstEstadosCiviles = QueryFacade.getInstance().getAllEstadosCiviles();
        this.lstGradosInstruccion = QueryFacade.getInstance().getAllGradosInstruccion();

        this.txtCodigo.setText(objSocio.getCodigo());
        this.txtNombre.setText(objSocio.getNombre());
        this.txtApellidoPaterno.setText(objSocio.getApellidoPaterno());
        this.txtApellidoMaterno.setText(objSocio.getApellidoMaterno());
        this.txtDocumentoIdentidad.setText(objSocio.getDocumentoIdentidad());
        this.dtpFechaNacimiento.setDate(new Date(objSocio.getFechaNacimiento().getTime()));
        this.txtTelefono.setText(objSocio.getTelefono());
        this.txtCelular.setText(objSocio.getCelular());
        this.bgpSexo.add(this.rdbFemenino);
        this.bgpSexo.add(this.rdbMasculino);
        if (objSocio.isSexo()) {
            this.rdbMasculino.setSelected(true);
        } else {
            this.rdbFemenino.setSelected(true);
        }
        this.txtDireccion.setText(objSocio.getDireccion());

        this.updateCmbEstadosCiviles();
        this.cmbEstadoCivil.setSelectedItem(objSocio.getObjEstadoCivil().getNombre());
        this.updateCmbGradosIntruccion();
        this.cmbGradoInstruccion.setSelectedItem(objSocio.getObjGradoInstruccion().getNombre());

        this.updateCmbDepartamentos();
        this.cmbDepartamento.setSelectedItem(objSocio.getObjDistrito().getObjProvincia().getObjDepartamento().getNombre());
        this.updateCmbProvincias();
        this.cmbProvincia.setSelectedItem(objSocio.getObjDistrito().getObjProvincia().getNombre());
        this.updateCmbDistritos();
        this.cmbDistrito.setSelectedItem(objSocio.getObjDistrito().getNombre());

        this.txtOrigen.setText(objSocio.getOrigen());
        this.txtOcupacion.setText(objSocio.getOcupacion());

        this.updateTipoTrabajador();
        this.cmbTipoTrabajador.setSelectedItem(objSocio.getObjTipoTrabajador().getNombre());
        this.txtDireccionLaboral.setEditable(!objSocio.getDireccionLaboral().isEmpty());
        this.txtDireccionLaboral.setText(objSocio.getDireccionLaboral());
        this.txtIngresoMensual.setText(objSocio.getIngresoMensual().toPlainString());
        
        this.lstHuellas = objSocio.getObjUsuario().getLstHuellas();
        this.updateTblHuellas();
        
        this.bgpSexoBeneficiario.add(this.rdbFemeninoBeneficiario);
        this.bgpSexoBeneficiario.add(this.rdbMasculinoBeneficiario);
        this.rdbMasculinoBeneficiario.setSelected(true);
        this.lstParentescos = QueryFacade.getInstance().getAllParentescos();
        this.updateParentesco();
        this.lstDetalleFamiliarSocio = objSocio.getLstFamiliaresSocio();
        this.updateTblBeneficiarios();
        this.dtpFechaNacimientoBeneficiario.setDate(new Date());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgpSexo = new javax.swing.ButtonGroup();
        bgpSexoBeneficiario = new javax.swing.ButtonGroup();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel3 = new javax.swing.JPanel();
        pnlDatosPersonales = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        txtApellidoPaterno = new javax.swing.JTextField();
        txtApellidoMaterno = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txtDocumentoIdentidad = new javax.swing.JTextField();
        txtOrigen = new javax.swing.JTextField();
        txtTelefono = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        txtCelular = new javax.swing.JTextField();
        dtpFechaNacimiento = new org.jdesktop.swingx.JXDatePicker();
        jLabel16 = new javax.swing.JLabel();
        txtDireccion = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        cmbDepartamento = new javax.swing.JComboBox<>();
        cmbProvincia = new javax.swing.JComboBox<>();
        cmbDistrito = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        txtOcupacion = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        rdbMasculino = new javax.swing.JRadioButton();
        rdbFemenino = new javax.swing.JRadioButton();
        jLabel35 = new javax.swing.JLabel();
        txtCodigo = new javax.swing.JTextField();
        btnRegistrarHuellas = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblHuellas = new javax.swing.JTable();
        cmbEstadoCivil = new javax.swing.JComboBox<>();
        cmbGradoInstruccion = new javax.swing.JComboBox<>();
        jLabel10 = new javax.swing.JLabel();
        cmbTipoDocumentoIdentidad = new javax.swing.JComboBox<>();
        jPanel1 = new javax.swing.JPanel();
        jLabel24 = new javax.swing.JLabel();
        cmbTipoTrabajador = new javax.swing.JComboBox<>();
        jLabel25 = new javax.swing.JLabel();
        txtDireccionLaboral = new javax.swing.JTextField();
        txtIngresoMensual = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        txtNombreBeneficiario = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        txtApellidoPaternoBeneficiario = new javax.swing.JTextField();
        jLabel59 = new javax.swing.JLabel();
        txtApellidoMaternoBeneficiario = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        cmbTipoDocumentoIdentidadBeneficiario = new javax.swing.JComboBox<>();
        jLabel60 = new javax.swing.JLabel();
        txtDocumentoIdentidadBeneficiario = new javax.swing.JTextField();
        jLabel61 = new javax.swing.JLabel();
        dtpFechaNacimientoBeneficiario = new org.jdesktop.swingx.JXDatePicker();
        jLabel34 = new javax.swing.JLabel();
        cmbParentesco = new javax.swing.JComboBox<>();
        lblDescripcionParentesco = new javax.swing.JLabel();
        txtDescripcionParentesco = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        txtDireccionBeneficiario = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        txtCelularBeneficiario = new javax.swing.JTextField();
        jLabel62 = new javax.swing.JLabel();
        txtTelefonoBeneficiario = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblBeneficiarios = new javax.swing.JTable();
        btnBuscarSocio = new javax.swing.JButton();
        jLabel31 = new javax.swing.JLabel();
        rdbMasculinoBeneficiario = new javax.swing.JRadioButton();
        rdbFemeninoBeneficiario = new javax.swing.JRadioButton();
        btnAgregarBeneficiario = new javax.swing.JButton();
        btnQuitarBeneficiario = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        btnCargarFoto = new javax.swing.JButton();
        scrlPnlFoto = new javax.swing.JScrollPane();
        lblFoto = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Registro de Socio");

        pnlDatosPersonales.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos Personales"));

        jLabel1.setText("Nombres:");

        jLabel2.setText("Apellido Paterno:");

        jLabel3.setText("Apellido Materno:");

        jLabel4.setText("Fecha de nacimiento:");

        jLabel5.setText("Documento de Identidad:");

        jLabel7.setText("Natural de:");

        jLabel8.setText("Grado de instrucción:");

        jLabel9.setText("Estado civil:");

        jLabel13.setText("Departamento:");

        txtTelefono.setMinimumSize(new java.awt.Dimension(100, 20));

        jLabel14.setText("Teléfono:");

        jLabel15.setText("Celular:");

        txtCelular.setMinimumSize(new java.awt.Dimension(100, 20));

        jLabel16.setText("Dirección:");

        jLabel17.setText("Provincia:");

        jLabel18.setText("Distrito:");

        cmbDepartamento.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbDepartamento.setMinimumSize(new java.awt.Dimension(100, 20));
        cmbDepartamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbDepartamentoActionPerformed(evt);
            }
        });

        cmbProvincia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbProvincia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbProvinciaActionPerformed(evt);
            }
        });

        cmbDistrito.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel6.setText("Ocupacion:");

        jLabel26.setText("Sexo:");

        rdbMasculino.setText("M");

        rdbFemenino.setText("F");

        jLabel35.setText("Código:");

        btnRegistrarHuellas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/huella_41x41.png"))); // NOI18N
        btnRegistrarHuellas.setText("Registrar Huellas");
        btnRegistrarHuellas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarHuellasActionPerformed(evt);
            }
        });

        tblHuellas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(tblHuellas);

        cmbGradoInstruccion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel10.setText("Tipo de Documento:");

        cmbTipoDocumentoIdentidad.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbTipoDocumentoIdentidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTipoDocumentoIdentidadActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlDatosPersonalesLayout = new javax.swing.GroupLayout(pnlDatosPersonales);
        pnlDatosPersonales.setLayout(pnlDatosPersonalesLayout);
        pnlDatosPersonalesLayout.setHorizontalGroup(
            pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDatosPersonalesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                        .addComponent(jScrollPane3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRegistrarHuellas))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlDatosPersonalesLayout.createSequentialGroup()
                        .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtDireccion)
                                    .addComponent(txtOrigen)))
                            .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                                        .addComponent(dtpFechaNacimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel14)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtTelefono, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addComponent(txtApellidoPaterno)
                                    .addComponent(txtNombre)
                                    .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                                        .addComponent(cmbTipoDocumentoIdentidad, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel5)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtDocumentoIdentidad, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGap(18, 18, 18)
                        .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel35, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel26, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel15, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtApellidoMaterno)
                            .addComponent(txtCelular, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbEstadoCivil, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                                        .addComponent(rdbMasculino)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(rdbFemenino))
                                    .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlDatosPersonalesLayout.createSequentialGroup()
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbDepartamento, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel17)
                        .addGap(8, 8, 8)
                        .addComponent(cmbProvincia, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbDistrito, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlDatosPersonalesLayout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtOcupacion, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbGradoInstruccion, 0, 327, Short.MAX_VALUE)
                        .addGap(192, 192, 192)))
                .addContainerGap())
        );
        pnlDatosPersonalesLayout.setVerticalGroup(
            pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDatosPersonalesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtNombre)
                        .addComponent(jLabel35, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtCodigo))
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtApellidoPaterno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(txtApellidoMaterno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(cmbEstadoCivil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(cmbTipoDocumentoIdentidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(txtDocumentoIdentidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(dtpFechaNacimiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14)
                    .addComponent(txtTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15)
                    .addComponent(txtCelular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(txtDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel26)
                    .addComponent(rdbMasculino)
                    .addComponent(rdbFemenino))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtOrigen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(cmbDepartamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17)
                    .addComponent(cmbProvincia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18)
                    .addComponent(cmbDistrito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtOcupacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(cmbGradoInstruccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRegistrarHuellas))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Actividad Laboral"));

        jLabel24.setText("Tipo de trabajador:");

        cmbTipoTrabajador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTipoTrabajadorActionPerformed(evt);
            }
        });

        jLabel25.setText("Centro de Trabajo:");

        jLabel27.setText("Ingreso mensual:");

        jLabel28.setText("soles.");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtDireccionLaboral, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(cmbTipoTrabajador, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel27)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtIngresoMensual, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel28)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(12, 12, 12))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtIngresoMensual, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel24)
                        .addComponent(cmbTipoTrabajador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel27)
                        .addComponent(jLabel28)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel25)
                    .addComponent(txtDireccionLaboral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Beneficiarios"));

        jLabel20.setText("Nombre:");

        txtNombreBeneficiario.setMinimumSize(new java.awt.Dimension(250, 20));

        jLabel21.setText("Apellido Paterno:");

        txtApellidoPaternoBeneficiario.setMinimumSize(new java.awt.Dimension(250, 20));

        jLabel59.setText("Apellido Materno:");

        jLabel36.setText("Tipo de Documento:");

        cmbTipoDocumentoIdentidadBeneficiario.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbTipoDocumentoIdentidadBeneficiario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTipoDocumentoIdentidadBeneficiarioActionPerformed(evt);
            }
        });

        jLabel60.setText("Documento de Identidad:");

        jLabel61.setText("Fecha de nacimiento:");

        jLabel34.setText("Parentesco:");

        cmbParentesco.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbParentesco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbParentescoActionPerformed(evt);
            }
        });

        lblDescripcionParentesco.setText("Descripcion:");

        jLabel32.setText("Dirección:");

        jLabel29.setText("Celular:");

        jLabel62.setText("Teléfono:");

        tblBeneficiarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblBeneficiarios);

        btnBuscarSocio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_search.png"))); // NOI18N
        btnBuscarSocio.setText("Buscar Socio");
        btnBuscarSocio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarSocioActionPerformed(evt);
            }
        });

        jLabel31.setText("Sexo:");

        rdbMasculinoBeneficiario.setText("M");

        rdbFemeninoBeneficiario.setText("F");

        btnAgregarBeneficiario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_add.png"))); // NOI18N
        btnAgregarBeneficiario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarBeneficiarioActionPerformed(evt);
            }
        });

        btnQuitarBeneficiario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/basura.png"))); // NOI18N
        btnQuitarBeneficiario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQuitarBeneficiarioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnAgregarBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnQuitarBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel32, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel20, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(txtDireccionBeneficiario)
                                        .addGap(18, 18, 18))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(txtNombreBeneficiario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel21)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtApellidoPaternoBeneficiario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGap(14, 14, 14)
                                        .addComponent(jLabel59)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtApellidoMaternoBeneficiario))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabel29)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtCelularBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel62)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtTelefonoBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel36)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cmbTipoDocumentoIdentidadBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel60)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtDocumentoIdentidadBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel61)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(dtpFechaNacimientoBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel34)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cmbParentesco, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(lblDescripcionParentesco)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtDescripcionParentesco)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdbMasculinoBeneficiario)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdbFemeninoBeneficiario))
                            .addComponent(btnBuscarSocio, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(txtNombreBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21)
                    .addComponent(txtApellidoPaternoBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel59)
                    .addComponent(txtApellidoMaternoBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscarSocio))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel60)
                    .addComponent(txtDocumentoIdentidadBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel61)
                    .addComponent(dtpFechaNacimientoBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel34)
                    .addComponent(cmbParentesco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDescripcionParentesco)
                    .addComponent(txtDescripcionParentesco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel31)
                    .addComponent(rdbMasculinoBeneficiario)
                    .addComponent(rdbFemeninoBeneficiario)
                    .addComponent(jLabel36)
                    .addComponent(cmbTipoDocumentoIdentidadBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel32)
                    .addComponent(txtDireccionBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel29)
                    .addComponent(txtCelularBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel62)
                    .addComponent(txtTelefonoBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnAgregarBeneficiario)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 67, Short.MAX_VALUE)
                        .addComponent(btnQuitarBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(49, 49, 49))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
        );

        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_save.png"))); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Foto"));

        btnCargarFoto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_upload.png"))); // NOI18N
        btnCargarFoto.setText("Cargar");
        btnCargarFoto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCargarFotoActionPerformed(evt);
            }
        });

        scrlPnlFoto.setAutoscrolls(true);
        scrlPnlFoto.setMaximumSize(new java.awt.Dimension(150, 150));

        lblFoto.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        lblFoto.setMaximumSize(new java.awt.Dimension(250, 250));
        scrlPnlFoto.setViewportView(lblFoto);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(scrlPnlFoto, javax.swing.GroupLayout.DEFAULT_SIZE, 555, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCargarFoto))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(btnCargarFoto)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(scrlPnlFoto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnGuardar, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addComponent(pnlDatosPersonales, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(0, 0, 0))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlDatosPersonales, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnGuardar))
        );

        jScrollPane2.setViewportView(jPanel3);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmbDepartamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbDepartamentoActionPerformed
        this.updateCmbProvincias();
        this.updateCmbDistritos();
    }//GEN-LAST:event_cmbDepartamentoActionPerformed

    private void cmbProvinciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbProvinciaActionPerformed
        this.updateCmbDistritos();
    }//GEN-LAST:event_cmbProvinciaActionPerformed

    private void btnRegistrarHuellasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarHuellasActionPerformed
        MyFingerprintRegister myFingerprintRegister = new MyFingerprintRegister(PrincipalJRibbon.getInstance().getFrame(), true);
        SwingUtil.centerOnScreen(myFingerprintRegister);
        myFingerprintRegister.setVisible(true);

        this.lstHuellas = FingerprintUtil.getHuellas(myFingerprintRegister.getFingerPrints());

        this.updateTblHuellas();
    }//GEN-LAST:event_btnRegistrarHuellasActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        if (SwingUtil.hasBlankInputs(this.txtNombre, this.txtApellidoPaterno, this.txtApellidoMaterno, this.txtDocumentoIdentidad, this.txtCelular, this.txtTelefono, this.txtIngresoMensual, this.txtCodigo)) {
            JOptionPane.showMessageDialog(this, "Error: revise los datos.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        if (!TipoDocumentoIdentidad.existeLongitud(this.txtDocumentoIdentidad.getText().trim().length(), this.lstTiposDocumentoIdentidad)) {
            JOptionPane.showMessageDialog(this, "Documento de identidad de socio no es válido", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        if(this.txtCodigo.getText().trim().length() != 4){
            JOptionPane.showMessageDialog(this, "Longitud del codigo incorrecto.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (this.lstHuellas.isEmpty()) {
            int respuesta = JOptionPane.showConfirmDialog(this, "No ha registrado huellas. ¿Desea continuar?", "Información", JOptionPane.WARNING_MESSAGE);

            if (respuesta == JOptionPane.CANCEL_OPTION) {
                return;
            }
        }

        try {
            if (this.objSocio != null) {
                this.objSocio.setNombre(this.txtNombre.getText().trim());
                this.objSocio.setApellidoPaterno(this.txtApellidoPaterno.getText().trim());
                this.objSocio.setApellidoMaterno(this.txtApellidoMaterno.getText().trim());
                this.objSocio.setFechaNacimiento(DateUtil.getTimestamp(this.dtpFechaNacimiento.getDate()));
                this.objSocio.setDireccion(this.txtDireccion.getText().trim());
                this.objSocio.setDocumentoIdentidad(this.txtDocumentoIdentidad.getText().trim());
                this.objSocio.setTelefono(this.txtTelefono.getText().trim());
                this.objSocio.setCelular(this.txtCelular.getText().trim());
                this.objSocio.setSexo(this.rdbMasculino.isSelected());
                this.objSocio.setOrigen(this.txtOrigen.getText().trim());
                this.objSocio.setObjEstadoCivil(this.lstEstadosCiviles.get(this.cmbEstadoCivil.getSelectedIndex()));
                this.objSocio.setFechaModificacion(DateUtil.currentTimestamp());
                this.objSocio.setCodigo(this.txtCodigo.getText().trim());
                this.objSocio.setOcupacion(this.txtOcupacion.getText().trim());
                this.objSocio.setObjGradoInstruccion(this.lstGradosInstruccion.get(this.cmbGradoInstruccion.getSelectedIndex()));
                this.objSocio.setDireccionLaboral(this.txtDireccionLaboral.getText().trim());
                this.objSocio.setIngresoMensual(new BigDecimal(this.txtIngresoMensual.getText().trim()));
                this.objSocio.setObjDistrito(this.lstDepartamentos.get(this.cmbDepartamento.getSelectedIndex()).getLstProvincias().get(this.cmbProvincia.getSelectedIndex()).getLstDistritos().get(this.cmbDistrito.getSelectedIndex()));
                this.objSocio.setObjTipoTrabajador(this.lstTipoTrabajador.get(this.cmbTipoTrabajador.getSelectedIndex()));
                this.objSocio.getObjUsuario().setLstHuellas(this.lstHuellas);
                this.objSocio.setLstDetalleFamiliarSocio(this.lstDetalleFamiliarSocio);

                boolean result = AdmisionSociosProcessLogic.getInstance().actualizarSocioOnly(this.objSocio, this.fileFoto);

                if (result) {
                    JOptionPane.showMessageDialog(this, "Socio actualizado correctamente.", "Socio", JOptionPane.INFORMATION_MESSAGE);
                    this.notifyObservers();
                    this.dispose();
                } else {
                    JOptionPane.showMessageDialog(this, "Error en la actualizacion", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void cmbTipoDocumentoIdentidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTipoDocumentoIdentidadActionPerformed
        SwingUtil.setDocumentoIdentidadInput(this.lstTiposDocumentoIdentidad.get(this.cmbTipoDocumentoIdentidad.getSelectedIndex()), this.txtDocumentoIdentidad);
    }//GEN-LAST:event_cmbTipoDocumentoIdentidadActionPerformed

    private void cmbTipoDocumentoIdentidadBeneficiarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTipoDocumentoIdentidadBeneficiarioActionPerformed
        SwingUtil.setDocumentoIdentidadInput(this.lstTiposDocumentoIdentidadBeneficiario.get(this.cmbTipoDocumentoIdentidadBeneficiario.getSelectedIndex()), this.txtDocumentoIdentidadBeneficiario);
    }//GEN-LAST:event_cmbTipoDocumentoIdentidadBeneficiarioActionPerformed

    private void cmbParentescoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbParentescoActionPerformed
        if (this.lstParentescos.get(this.cmbParentesco.getSelectedIndex()).getNombre().equalsIgnoreCase("Otros")) {
            this.lblDescripcionParentesco.setVisible(true);
            this.txtDescripcionParentesco.setVisible(true);
        } else {
            this.lblDescripcionParentesco.setVisible(false);
            this.txtDescripcionParentesco.setVisible(false);
            this.txtDescripcionParentesco.setText("");
        }
    }//GEN-LAST:event_cmbParentescoActionPerformed

    private void btnBuscarSocioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarSocioActionPerformed
        DlgBuscarSocio dlgBuscarSocio = new DlgBuscarSocio(PrincipalJRibbon.getInstance(), true, true);
        SwingUtil.centerOnScreen(dlgBuscarSocio);
        dlgBuscarSocio.setVisible(true);

        Socio objSocioBuscar = dlgBuscarSocio.getObjSocioSeleccionado();

        if (objSocioBuscar != null) {
            this.txtNombreBeneficiario.setText(objSocioBuscar.getNombre());
            this.txtApellidoPaternoBeneficiario.setText(objSocioBuscar.getApellidoPaterno());
            this.txtApellidoMaternoBeneficiario.setText(objSocioBuscar.getApellidoMaterno());
            this.txtDocumentoIdentidadBeneficiario.setText(objSocioBuscar.getDocumentoIdentidad());
            this.dtpFechaNacimientoBeneficiario.setDate(DateUtil.getDate(objSocioBuscar.getFechaNacimiento()));
            this.txtDireccionBeneficiario.setText(objSocioBuscar.getDireccion());
            this.txtTelefonoBeneficiario.setText(objSocioBuscar.getTelefono());
            this.txtCelularBeneficiario.setText(objSocioBuscar.getCelular());
            this.cmbTipoDocumentoIdentidadBeneficiario.setSelectedItem(objSocioBuscar.getObjTipoDocumentoIdentidad().getNombre());
            this.cmbParentesco.requestFocusInWindow();
        }
    }//GEN-LAST:event_btnBuscarSocioActionPerformed

    private void btnAgregarBeneficiarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarBeneficiarioActionPerformed
        if (SwingUtil.hasBlankInputs(this.txtNombreBeneficiario, this.txtApellidoPaternoBeneficiario, this.txtApellidoMaternoBeneficiario, this.txtDocumentoIdentidadBeneficiario, this.txtDireccionBeneficiario, this.txtTelefonoBeneficiario, this.txtCelularBeneficiario)) {
            JOptionPane.showMessageDialog(this, "Datos Incorrectos. Revise los datos ingresados", "Beneficiario", JOptionPane.ERROR_MESSAGE);

        } else {
            if (!TipoDocumentoIdentidad.existeLongitud(this.txtDocumentoIdentidadBeneficiario.getText().trim().length(), this.lstTiposDocumentoIdentidadBeneficiario)) {
                JOptionPane.showMessageDialog(this, "Documento de identidad de beneficiario no es válido", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            Familiar objFamiliar = new Familiar(
                0,
                txtNombreBeneficiario.getText(),
                txtApellidoPaternoBeneficiario.getText(),
                txtApellidoMaternoBeneficiario.getText(),
                dtpFechaNacimientoBeneficiario.getDate() == null ? null : new Timestamp(dtpFechaNacimientoBeneficiario.getDate().getTime()),
                txtDireccionBeneficiario.getText(),
                txtDocumentoIdentidadBeneficiario.getText(),
                "",
                txtTelefonoBeneficiario.getText(),
                txtCelularBeneficiario.getText(),
                rdbMasculinoBeneficiario.isSelected(),
                "",
                true,
                new Timestamp(new Date().getTime()),
                new Timestamp(new Date().getTime()),
                null,
                this.lstTiposDocumentoIdentidadBeneficiario.get(this.cmbTipoDocumentoIdentidadBeneficiario.getSelectedIndex()),
                new ArrayList<>());

            Parentesco objParentesco = this.lstParentescos.get(this.cmbParentesco.getSelectedIndex());

            DetalleFamiliarSocio objDetalleFamiliarSocio = new DetalleFamiliarSocio(0, this.txtDescripcionParentesco.getText().trim(), BigDecimal.ZERO, null, objFamiliar, objParentesco);

            this.lstDetalleFamiliarSocio.add(objDetalleFamiliarSocio);
            AdmisionSociosProcessLogic.getInstance().ajustarPorcentajeBeneficio(lstDetalleFamiliarSocio);
            this.updateTblBeneficiarios();
            this.btnQuitarBeneficiario.setEnabled(true);
        }
    }//GEN-LAST:event_btnAgregarBeneficiarioActionPerformed

    private void btnQuitarBeneficiarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQuitarBeneficiarioActionPerformed
        if (tblBeneficiarios.getSelectedRow() != -1) {
            int respuesta = JOptionPane.showConfirmDialog(this, "Está seguro de querer eliminar el Ingreso?", "Beneficiario", JOptionPane.WARNING_MESSAGE);

            if (respuesta == JOptionPane.YES_OPTION) {
                this.lstDetalleFamiliarSocio.remove(tblBeneficiarios.getSelectedRow());
                AdmisionSociosProcessLogic.getInstance().ajustarPorcentajeBeneficio(lstDetalleFamiliarSocio);
                this.updateTblBeneficiarios();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un Beneficiario", "Beneficiarios", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnQuitarBeneficiarioActionPerformed

    private void cmbTipoTrabajadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTipoTrabajadorActionPerformed
        if (this.cmbTipoTrabajador.getSelectedItem().equals("DEPENDIENTE")) {
            this.txtDireccionLaboral.setEditable(true);
        }else{
            this.txtDireccionLaboral.setEditable(false);
            SwingUtil.clear(this.txtDireccionLaboral);
        }
    }//GEN-LAST:event_cmbTipoTrabajadorActionPerformed

    private void btnCargarFotoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCargarFotoActionPerformed
        try {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Images", "jpg", "png"));
            fileChooser.setAcceptAllFileFilterUsed(true);
            int result = fileChooser.showOpenDialog(this);
            if (result == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fileChooser.getSelectedFile();
                System.out.println("Selected file: " + selectedFile.getAbsolutePath());
                this.fileFoto = selectedFile;
                this.foto = ImageIO.read(selectedFile);
                this.updateFoto();
                JOptionPane.showMessageDialog(this, "Foto cargada con exito.", "Documento", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnCargarFotoActionPerformed

    private void setTextFieldsConfiguration() {
        SwingUtil.setDocumentoIdentidadInput(this.lstTiposDocumentoIdentidad.get(this.cmbTipoDocumentoIdentidad.getSelectedIndex()), this.txtDocumentoIdentidad);
        SwingUtil.setDocumentoIdentidadInput(this.lstTiposDocumentoIdentidadBeneficiario.get(this.cmbTipoDocumentoIdentidadBeneficiario.getSelectedIndex()), this.txtDocumentoIdentidadBeneficiario);
        SwingUtil.setPositiveNumericInput(this.txtIngresoMensual);
        SwingUtil.setPhoneInput(this.txtCelular, this.txtTelefono, this.txtCelularBeneficiario, this.txtTelefonoBeneficiario);
        SwingUtil.setCodigoInput(4, this.txtCodigo);
    }

    //<editor-fold defaultstate="collapsed" desc="Variables">
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bgpSexo;
    private javax.swing.ButtonGroup bgpSexoBeneficiario;
    private javax.swing.JButton btnAgregarBeneficiario;
    private javax.swing.JButton btnBuscarSocio;
    private javax.swing.JButton btnCargarFoto;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnQuitarBeneficiario;
    private javax.swing.JButton btnRegistrarHuellas;
    private javax.swing.JComboBox<String> cmbDepartamento;
    private javax.swing.JComboBox<String> cmbDistrito;
    private javax.swing.JComboBox<String> cmbEstadoCivil;
    private javax.swing.JComboBox<String> cmbGradoInstruccion;
    private javax.swing.JComboBox<String> cmbParentesco;
    private javax.swing.JComboBox<String> cmbProvincia;
    private javax.swing.JComboBox<String> cmbTipoDocumentoIdentidad;
    private javax.swing.JComboBox<String> cmbTipoDocumentoIdentidadBeneficiario;
    private javax.swing.JComboBox<String> cmbTipoTrabajador;
    private org.jdesktop.swingx.JXDatePicker dtpFechaNacimiento;
    private org.jdesktop.swingx.JXDatePicker dtpFechaNacimientoBeneficiario;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblDescripcionParentesco;
    private javax.swing.JLabel lblFoto;
    private javax.swing.JPanel pnlDatosPersonales;
    private javax.swing.JRadioButton rdbFemenino;
    private javax.swing.JRadioButton rdbFemeninoBeneficiario;
    private javax.swing.JRadioButton rdbMasculino;
    private javax.swing.JRadioButton rdbMasculinoBeneficiario;
    private javax.swing.JScrollPane scrlPnlFoto;
    private javax.swing.JTable tblBeneficiarios;
    private javax.swing.JTable tblHuellas;
    private javax.swing.JTextField txtApellidoMaterno;
    private javax.swing.JTextField txtApellidoMaternoBeneficiario;
    private javax.swing.JTextField txtApellidoPaterno;
    private javax.swing.JTextField txtApellidoPaternoBeneficiario;
    private javax.swing.JTextField txtCelular;
    private javax.swing.JTextField txtCelularBeneficiario;
    private javax.swing.JTextField txtCodigo;
    private javax.swing.JTextField txtDescripcionParentesco;
    private javax.swing.JTextField txtDireccion;
    private javax.swing.JTextField txtDireccionBeneficiario;
    private javax.swing.JTextField txtDireccionLaboral;
    private javax.swing.JTextField txtDocumentoIdentidad;
    private javax.swing.JTextField txtDocumentoIdentidadBeneficiario;
    private javax.swing.JTextField txtIngresoMensual;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtNombreBeneficiario;
    private javax.swing.JTextField txtOcupacion;
    private javax.swing.JTextField txtOrigen;
    private javax.swing.JTextField txtTelefono;
    private javax.swing.JTextField txtTelefonoBeneficiario;
    // End of variables declaration//GEN-END:variables
    //</editor-fold>
    
    private void setTblBeneficiariosCellListener() {
        Action action = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TableCellListener tableCellListener = (TableCellListener) e.getSource();
                if (tableCellListener.getColumn() == 10) {
                    BigDecimal oldValue = new BigDecimal(tableCellListener.getOldValue().toString());
                    BigDecimal newValue = new BigDecimal(tableCellListener.getNewValue().toString());
                    DetalleFamiliarSocio objDetalleFamiliarSocio = lstDetalleFamiliarSocio.get(tableCellListener.getRow());
                    if (newValue.compareTo(BigDecimal.ZERO) > 0 && newValue.compareTo(BigDecimal.valueOf(100)) <= 0) {
                        objDetalleFamiliarSocio.setPorcentajeBeneficio(newValue);
                        AdmisionSociosProcessLogic.getInstance().ajustarPorcentajeBeneficio(lstDetalleFamiliarSocio, objDetalleFamiliarSocio);
                    } else {
                        JOptionPane.showMessageDialog(IFrmRegistroSocio.this, "Valor incorrecto", "Porcentaje", JOptionPane.ERROR_MESSAGE);
                        objDetalleFamiliarSocio.setPorcentajeBeneficio(oldValue);
                    }
                    updateTblBeneficiarios();
                }
            }
        };

        TableCellListener tableCellListener = new TableCellListener(this.tblBeneficiarios, action);
    }
    
    private void updateCmbDepartamentos() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (Departamento objDepartamento : this.lstDepartamentos) {
            comboBoxModel.addElement(objDepartamento.getNombre());
        }

        this.cmbDepartamento.setModel(comboBoxModel);
    }

    private void updateCmbProvincias() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        Departamento objDepartamento = this.lstDepartamentos.get(this.cmbDepartamento.getSelectedIndex());
        for (Provincia objProvincia : objDepartamento.getLstProvincias()) {
            comboBoxModel.addElement(objProvincia.getNombre());
        }

        this.cmbProvincia.setModel(comboBoxModel);
    }

    private void updateCmbDistritos() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        Provincia objProvincia = this.lstDepartamentos.get(this.cmbDepartamento.getSelectedIndex()).getLstProvincias().get(this.cmbProvincia.getSelectedIndex());
        for (Distrito objDistrito : objProvincia.getLstDistritos()) {
            comboBoxModel.addElement(objDistrito.getNombre());
        }
        this.cmbDistrito.setModel(comboBoxModel);
    }

    private void updateTipoTrabajador() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (TipoTrabajador objTipoTrabajador : this.lstTipoTrabajador) {
            comboBoxModel.addElement(objTipoTrabajador.getNombre());
        }

        this.cmbTipoTrabajador.setModel(comboBoxModel);
    }

    private void updateCmbEstadosCiviles() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (EstadoCivil objEstadoCivil : this.lstEstadosCiviles) {
            comboBoxModel.addElement(objEstadoCivil.getNombre());
        }

        this.cmbEstadoCivil.setModel(comboBoxModel);
    }

    private void updateCmbGradosIntruccion() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (GradoInstruccion objGradoInstruccion : this.lstGradosInstruccion) {
            comboBoxModel.addElement(objGradoInstruccion.getNombre());
        }

        this.cmbGradoInstruccion.setModel(comboBoxModel);
    }
    
    private void updateParentesco() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (Parentesco objParentesco : this.lstParentescos) {
            comboBoxModel.addElement(objParentesco.getNombre());
        }

        this.cmbParentesco.setModel(comboBoxModel);
    }
    
    private void updateCmbTiposDocumentoIdentidadSocio(){
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();
        
        for(TipoDocumentoIdentidad objTipoDocumentoIdentidad : this.lstTiposDocumentoIdentidad){
            comboBoxModel.addElement(objTipoDocumentoIdentidad.getNombre());
        }
        
        this.cmbTipoDocumentoIdentidad.setModel(comboBoxModel);
    }
    
    private void updateCmbTiposDocumentoIdentidadBeneficiario(){
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();
        
        for(TipoDocumentoIdentidad objTipoDocumentoIdentidad : this.lstTiposDocumentoIdentidad){
            comboBoxModel.addElement(objTipoDocumentoIdentidad.getNombre());
        }
        
        this.cmbTipoDocumentoIdentidadBeneficiario.setModel(comboBoxModel);
    }

    private void updateTblHuellas() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tableModel.addColumn("Mano");
        tableModel.addColumn("Dedo");

        for (Huella objHuella : this.lstHuellas) {
            tableModel.addRow(new Object[]{
                objHuella.isMano() ? "Derecha" : "Izquierda",
                objHuella.getDedo()
            });
        }

        this.tblHuellas.setModel(tableModel);
    }
    
    private void updateTblBeneficiarios() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return column == 10;
            }
        };
        tableModel.addColumn("Nombre");
        tableModel.addColumn("Apellido Paterno");
        tableModel.addColumn("Apellido Materno");
        tableModel.addColumn("DNI");
        tableModel.addColumn("Fecha Nacimiento");
        tableModel.addColumn("Domicilio");
        tableModel.addColumn("Telefono");
        tableModel.addColumn("Celular");
        tableModel.addColumn("Sexo");
        tableModel.addColumn("Parentesco");
        tableModel.addColumn("Porcentaje Beneficio");

        for (DetalleFamiliarSocio objDetalleFamiliarSocio : this.lstDetalleFamiliarSocio) {
            tableModel.addRow(new Object[]{
                objDetalleFamiliarSocio.getObjFamiliar().getNombre(),
                objDetalleFamiliarSocio.getObjFamiliar().getApellidoPaterno(),
                objDetalleFamiliarSocio.getObjFamiliar().getApellidoMaterno(),
                objDetalleFamiliarSocio.getObjFamiliar().getDocumentoIdentidad(),
                this.sdf.format(objDetalleFamiliarSocio.getObjFamiliar().getFechaNacimiento()),
                objDetalleFamiliarSocio.getObjFamiliar().getDireccion(),
                objDetalleFamiliarSocio.getObjFamiliar().getTelefono(),
                objDetalleFamiliarSocio.getObjFamiliar().getCelular(),
                objDetalleFamiliarSocio.getObjFamiliar().isSexo() ? "M" : "F",
                objDetalleFamiliarSocio.getObjParentesco().getNombre(),
                objDetalleFamiliarSocio.getPorcentajeBeneficio().toPlainString()
            });
        }

        this.tblBeneficiarios.setModel(tableModel);
        this.setTblBeneficiariosCellListener();

        SwingUtil.clear(this.txtNombreBeneficiario, this.txtApellidoMaternoBeneficiario, this.txtApellidoPaternoBeneficiario, this.txtCelularBeneficiario, this.txtDireccionBeneficiario, this.txtDocumentoIdentidadBeneficiario, this.txtTelefonoBeneficiario);
        this.dtpFechaNacimientoBeneficiario.setDate(new Date());
        this.rdbMasculinoBeneficiario.setSelected(true);
        this.rdbFemeninoBeneficiario.setSelected(false);
        this.cmbParentesco.setSelectedIndex(0);
    }
    
    private void updateFoto() {
        this.lblFoto.setIcon(this.foto == null ? new ImageIcon() : new ImageIcon(this.foto));
        this.scrlPnlFoto.setPreferredSize(new Dimension(150, 150));
    }

    @Override
    public void addObserver(IObserver objIObserver) {
        this.lstIObservers.add(objIObserver);
    }

    @Override
    public List<IObserver> getObserverList() {
        return this.lstIObservers;
    }

    @Override
    public void notifyObservers() {
        for (IObserver objIObserver : this.lstIObservers) {
            objIObserver.update(this);
        }
    }
}
