package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.process.EmpleadosProcessLogic;
import com.smartech.sistemacooperativa.dominio.Departamento;
import com.smartech.sistemacooperativa.dominio.Distrito;
import com.smartech.sistemacooperativa.dominio.Empleado;
import com.smartech.sistemacooperativa.dominio.EstadoCivil;
import com.smartech.sistemacooperativa.dominio.GradoInstruccion;
import com.smartech.sistemacooperativa.dominio.Huella;
import com.smartech.sistemacooperativa.dominio.Provincia;
import com.smartech.sistemacooperativa.dominio.TipoDocumentoIdentidad;
import com.smartech.sistemacooperativa.dominio.TipoUsuario;
import com.smartech.sistemacooperativa.dominio.Usuario;
import com.smartech.sistemacooperativa.gui.util.AWTUtil;
import com.smartech.sistemacooperativa.gui.util.FingerprintUtil;
import com.smartech.sistemacooperativa.gui.util.MyFingerprintRegister;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import com.smartech.sistemacooperativa.util.generics.StringUtil;
import com.smartech.sistemacooperativa.util.interfaces.IObservable;
import com.smartech.sistemacooperativa.util.interfaces.IObserver;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Smartech
 */
public class IFrmRegistroEmpleado extends javax.swing.JInternalFrame implements IObservable {

    private Empleado objEmpleado;
    private List<IObserver> lstIObservers = new ArrayList<>();
    private List<Huella> lstHuellas = new ArrayList<>();
    private List<Departamento> lstDepartamentos = new ArrayList<>();
    private List<EstadoCivil> lstEstadosCiviles = new ArrayList<>();
    private List<GradoInstruccion> lstGradosInstruccion = new ArrayList<>();
    private List<TipoUsuario> lstTiposUsuario = new ArrayList<>();
    private List<TipoDocumentoIdentidad> lstTiposDocumentoIdentidad = new ArrayList<>();

    public IFrmRegistroEmpleado() {
        initComponents();
        AWTUtil.setUpperCase();
        this.lstTiposDocumentoIdentidad = QueryFacade.getInstance().getAllTiposDocumentoIdentidad();
        this.updateCmbTiposDocumentoIdentidad();
        this.setTextFieldsConfiguration();

        this.lstDepartamentos = QueryFacade.getInstance().getAllDepartamentos();
        this.updateCmbDepartamentos();
        this.updateCmbProvincias();
        this.updateCmbDistritos();

        this.lstEstadosCiviles = QueryFacade.getInstance().getAllEstadosCiviles();
        this.updateCmbEstadosCiviles();
        this.lstGradosInstruccion = QueryFacade.getInstance().getAllGradosInstruccion();
        this.updateCmbGradosIntruccion();
        this.lstTiposUsuario = QueryFacade.getInstance().getAllTiposUsuarioEmpleadosDebajoNivel(PrincipalJRibbon.getInstance().getObjUsuarioLogeado());
        this.updateCmbTiposUsuario();        

        this.rgbSexo.add(this.rdbFemenino);
        this.rgbSexo.add(this.rdbMasculino);

        this.rdbMasculino.setSelected(true);
        this.dtpFechaNacimiento.setDate(new Date());
    }

    public IFrmRegistroEmpleado(Empleado objEmpleado) {
        initComponents();
        AWTUtil.setUpperCase();
        this.objEmpleado = objEmpleado;
        this.lstTiposDocumentoIdentidad = QueryFacade.getInstance().getAllTiposDocumentoIdentidad();
        this.updateCmbTiposDocumentoIdentidad();
        this.cmbTipoDocumentoIdentidad.setSelectedItem(objEmpleado.getObjTipoDocumentoIdentidad().getNombre());
        this.setTextFieldsConfiguration();

        this.lstHuellas = objEmpleado.getObjUsuario().getLstHuellas();
        this.updateTblHuellas();
        this.txtUsername.setText(objEmpleado.getObjUsuario().getUsername());
        this.lstDepartamentos = QueryFacade.getInstance().getAllDepartamentos();
        this.lstEstadosCiviles = QueryFacade.getInstance().getAllEstadosCiviles();
        this.lstGradosInstruccion = QueryFacade.getInstance().getAllGradosInstruccion();
        this.lstTiposUsuario = QueryFacade.getInstance().getAllTiposUsuarioEmpleadosDebajoNivel(PrincipalJRibbon.getInstance().getObjUsuarioLogeado());

        this.txtNombre.setText(objEmpleado.getNombre());
        this.txtApellidoPaterno.setText(objEmpleado.getApellidoPaterno());
        this.txtDocumentoIdentidad.setText(objEmpleado.getDocumentoIdentidad());
        this.txtApellidoMaterno.setText(objEmpleado.getApellidoMaterno());
        this.dtpFechaNacimiento.setDate(new Date(objEmpleado.getFechaNacimiento().getTime()));
        this.txtTelefono.setText(objEmpleado.getTelefono());
        this.txtCelular.setText(objEmpleado.getCelular());
        this.rdbMasculino.setSelected(objEmpleado.isSexo());
        this.txtDireccion.setText(objEmpleado.getDireccion());
        this.chkActivo.setSelected(objEmpleado.isActivo());

        this.updateCmbEstadosCiviles();
        this.cmbEstadoCivil.setSelectedItem(objEmpleado.getObjEstadoCivil().getNombre());
        this.updateCmbGradosIntruccion();
        this.cmbGradoInstruccion.setSelectedItem(objEmpleado.getObjGradoInstruccion().getNombre());
        this.updateCmbTiposUsuario();
        this.cmbTiposUsuario.setSelectedItem(objEmpleado.getObjUsuario().getObjTipoUsuario().getNombre());

        this.updateCmbDepartamentos();
        this.cmbDepartamento.setSelectedItem(objEmpleado.getObjDistrito().getObjProvincia().getObjDepartamento().getNombre());
        this.updateCmbProvincias();
        this.cmbProvincia.setSelectedItem(objEmpleado.getObjDistrito().getObjProvincia().getNombre());
        this.updateCmbDistritos();
        this.cmbDistrito.setSelectedItem(objEmpleado.getObjDistrito().getNombre());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        rgbSexo = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel3 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        txtUsername = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtPassword = new javax.swing.JPasswordField();
        btnGuardar = new javax.swing.JButton();
        chkEditarUsuario = new javax.swing.JCheckBox();
        pnlDatosPersonales = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        btnRegistrarHuellas = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblHuellas = new javax.swing.JTable();
        txtNombre = new javax.swing.JTextField();
        txtApellidoPaterno = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtApellidoMaterno = new javax.swing.JTextField();
        txtDocumentoIdentidad = new javax.swing.JTextField();
        dtpFechaNacimiento = new org.jdesktop.swingx.JXDatePicker();
        txtDireccion = new javax.swing.JTextField();
        cmbDepartamento = new javax.swing.JComboBox<>();
        jLabel17 = new javax.swing.JLabel();
        cmbProvincia = new javax.swing.JComboBox<>();
        jLabel18 = new javax.swing.JLabel();
        cmbDistrito = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        cmbEstadoCivil = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        cmbGradoInstruccion = new javax.swing.JComboBox<>();
        jLabel15 = new javax.swing.JLabel();
        txtCelular = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txtTelefono = new javax.swing.JTextField();
        rdbMasculino = new javax.swing.JRadioButton();
        rdbFemenino = new javax.swing.JRadioButton();
        jLabel26 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        cmbTiposUsuario = new javax.swing.JComboBox<>();
        chkActivo = new javax.swing.JCheckBox();
        cmbTipoDocumentoIdentidad = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setTitle("Registro de Empleado");

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Usuario"));

        jLabel6.setText("Username:");

        jLabel10.setText("Password:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtUsername)
                .addGap(18, 18, 18)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPassword)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtUsername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(txtPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_save.png"))); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        chkEditarUsuario.setText("Editar Usuario");
        chkEditarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkEditarUsuarioActionPerformed(evt);
            }
        });

        pnlDatosPersonales.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos Personales"));

        jLabel1.setText("Nombres:");

        jLabel2.setText("Apellido Paterno:");

        jLabel3.setText("Tipo de Documento:");

        jLabel4.setText("Fecha de nacimiento:");

        jLabel13.setText("Departamento:");

        jLabel16.setText("Dirección:");

        btnRegistrarHuellas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/huella_41x41.png"))); // NOI18N
        btnRegistrarHuellas.setText("Registrar Huellas");
        btnRegistrarHuellas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarHuellasActionPerformed(evt);
            }
        });

        tblHuellas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(tblHuellas);

        jLabel5.setText("Apellido Materno:");

        cmbDepartamento.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbDepartamento.setMinimumSize(new java.awt.Dimension(100, 20));
        cmbDepartamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbDepartamentoActionPerformed(evt);
            }
        });

        jLabel17.setText("Provincia:");

        cmbProvincia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbProvincia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbProvinciaActionPerformed(evt);
            }
        });

        jLabel18.setText("Distrito:");

        cmbDistrito.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel9.setText("Estado civil:");

        jLabel8.setText("Grado de instrucción:");

        cmbGradoInstruccion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel15.setText("Celular:");

        txtCelular.setMinimumSize(new java.awt.Dimension(100, 20));

        jLabel14.setText("Teléfono:");

        txtTelefono.setMinimumSize(new java.awt.Dimension(100, 20));

        rdbMasculino.setText("M");

        rdbFemenino.setText("F");

        jLabel26.setText("Sexo:");

        jLabel11.setText("Cargo:");

        cmbTiposUsuario.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        chkActivo.setText("Activo");

        cmbTipoDocumentoIdentidad.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbTipoDocumentoIdentidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTipoDocumentoIdentidadActionPerformed(evt);
            }
        });

        jLabel7.setText("Documento de Identidad:");

        javax.swing.GroupLayout pnlDatosPersonalesLayout = new javax.swing.GroupLayout(pnlDatosPersonales);
        pnlDatosPersonales.setLayout(pnlDatosPersonalesLayout);
        pnlDatosPersonalesLayout.setHorizontalGroup(
            pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDatosPersonalesLayout.createSequentialGroup()
                        .addComponent(jScrollPane3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRegistrarHuellas))
                    .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                        .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel16, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                                        .addComponent(cmbDepartamento, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel17)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(cmbProvincia, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel18)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(cmbDistrito, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                                        .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(cmbTipoDocumentoIdentidad, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(dtpFechaNacimiento, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE))
                                        .addGap(18, 18, 18)
                                        .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                                                .addComponent(jLabel7)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txtDocumentoIdentidad, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(jLabel9)
                                                .addGap(25, 25, 25)
                                                .addComponent(cmbEstadoCivil, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(jLabel8)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(cmbGradoInstruccion, javax.swing.GroupLayout.PREFERRED_SIZE, 261, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                                                .addComponent(jLabel15)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txtCelular, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(jLabel14)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txtTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                .addGap(28, 28, 28)
                                .addComponent(chkActivo))
                            .addComponent(txtDireccion)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDatosPersonalesLayout.createSequentialGroup()
                                .addComponent(txtNombre)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel26)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdbMasculino)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdbFemenino))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDatosPersonalesLayout.createSequentialGroup()
                                .addComponent(txtApellidoPaterno)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtApellidoMaterno, javax.swing.GroupLayout.PREFERRED_SIZE, 369, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                                .addComponent(cmbTiposUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 455, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())))
        );
        pnlDatosPersonalesLayout.setVerticalGroup(
            pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDatosPersonalesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(rdbMasculino)
                        .addComponent(rdbFemenino)
                        .addComponent(jLabel26))
                    .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtApellidoPaterno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(txtApellidoMaterno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtDocumentoIdentidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cmbEstadoCivil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(cmbGradoInstruccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chkActivo)
                    .addComponent(cmbTipoDocumentoIdentidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(dtpFechaNacimiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15)
                    .addComponent(txtCelular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14)
                    .addComponent(txtTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(txtDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(cmbDepartamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17)
                    .addComponent(cmbProvincia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18)
                    .addComponent(cmbDistrito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(cmbTiposUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                        .addComponent(btnRegistrarHuellas)
                        .addContainerGap(54, Short.MAX_VALUE))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(chkEditarUsuario)
            .addComponent(pnlDatosPersonales, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnGuardar)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addComponent(pnlDatosPersonales, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                .addComponent(chkEditarUsuario)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnGuardar)
                .addContainerGap())
        );

        jScrollPane1.setViewportView(jPanel3);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmbDepartamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbDepartamentoActionPerformed
        this.updateCmbProvincias();
        this.updateCmbDistritos();
    }//GEN-LAST:event_cmbDepartamentoActionPerformed

    private void cmbProvinciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbProvinciaActionPerformed
        this.updateCmbDistritos();
    }//GEN-LAST:event_cmbProvinciaActionPerformed

    private void btnRegistrarHuellasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarHuellasActionPerformed
        MyFingerprintRegister myFingerprintRegister = new MyFingerprintRegister(PrincipalJRibbon.getInstance().getFrame(), true);
        SwingUtil.centerOnScreen(myFingerprintRegister);
        myFingerprintRegister.setVisible(true);

        this.lstHuellas = FingerprintUtil.getHuellas(myFingerprintRegister.getFingerPrints());

        this.updateTblHuellas();
    }//GEN-LAST:event_btnRegistrarHuellasActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        String message = "";

        if (this.objEmpleado == null) {
            if (SwingUtil.hasBlankInputs(this.txtNombre, this.txtApellidoPaterno, this.txtDocumentoIdentidad, this.txtApellidoMaterno, this.txtCelular, this.txtTelefono, this.txtUsername, this.txtPassword)) {
                return;
            }
        } else if (SwingUtil.hasBlankInputs(this.txtNombre, this.txtApellidoPaterno, this.txtDocumentoIdentidad, this.txtApellidoMaterno, this.txtCelular, this.txtTelefono, this.txtUsername)) {
            return;
        }
        if (this.lstHuellas.isEmpty()) {
            int respuesta = JOptionPane.showConfirmDialog(this, "No ha registrado huellas. ¿Desea continuar?", "Información", JOptionPane.WARNING_MESSAGE);

            if (respuesta == JOptionPane.CANCEL_OPTION) {
                return;
            }
        }

        try {
            if (this.objEmpleado == null) {
                Usuario objUsuario = new Usuario(
                        0,
                        this.txtUsername.getText().trim(),
                        StringUtil.encodeString(new String(this.txtPassword.getPassword()).trim()),
                        true,
                        true,
                        this.lstTiposUsuario.get(this.cmbTiposUsuario.getSelectedIndex()),
                        this.lstHuellas);
                this.objEmpleado = new Empleado(
                        0,
                        this.txtNombre.getText().trim(),
                        this.txtApellidoPaterno.getText().trim(),
                        this.txtApellidoMaterno.getText().trim(),
                        DateUtil.getTimestamp(this.dtpFechaNacimiento.getDate()),
                        this.txtDireccion.getText().trim(),
                        this.txtDocumentoIdentidad.getText().trim(),
                        "",
                        this.txtTelefono.getText().trim(),
                        this.txtCelular.getText().trim(),
                        this.rdbMasculino.isSelected(),
                        "",
                        true,
                        DateUtil.currentTimestamp(),
                        DateUtil.currentTimestamp(),
                        this.chkActivo.isSelected(),
                        this.lstEstadosCiviles.get(this.cmbEstadoCivil.getSelectedIndex()),
                        this.lstTiposDocumentoIdentidad.get(this.cmbTipoDocumentoIdentidad.getSelectedIndex()),
                        this.lstDepartamentos.get(this.cmbDepartamento.getSelectedIndex()).getLstProvincias().get(this.cmbProvincia.getSelectedIndex()).getLstDistritos().get(this.cmbDistrito.getSelectedIndex()),
                        this.lstGradosInstruccion.get(this.cmbGradoInstruccion.getSelectedIndex()),
                        PrincipalJRibbon.getInstance().getObjEstablecimiento(),
                        objUsuario);

                message = EmpleadosProcessLogic.getInstance().registrarEmpleado(this.objEmpleado);
            } else {
                this.objEmpleado.setNombre(this.txtNombre.getText().trim());
                this.objEmpleado.setApellidoPaterno(this.txtApellidoPaterno.getText().trim());
                this.objEmpleado.setApellidoMaterno(this.txtApellidoMaterno.getText().trim());
                this.objEmpleado.setFechaNacimiento(DateUtil.getTimestamp(this.dtpFechaNacimiento.getDate()));
                this.objEmpleado.setDireccion(this.txtDireccion.getText().trim());
                this.objEmpleado.setDocumentoIdentidad(this.txtDocumentoIdentidad.getText().trim());
                this.objEmpleado.setTelefono(this.txtTelefono.getText().trim());
                this.objEmpleado.setCelular(this.txtCelular.getText().trim());
                this.objEmpleado.setSexo(this.rdbMasculino.isSelected());
                this.objEmpleado.setActivo(this.chkActivo.isSelected());
                this.objEmpleado.setObjEstadoCivil(this.lstEstadosCiviles.get(this.cmbEstadoCivil.getSelectedIndex()));
                this.objEmpleado.setObjTipoDocumentoIdentidad(this.lstTiposDocumentoIdentidad.get(this.cmbTipoDocumentoIdentidad.getSelectedIndex()));
                this.objEmpleado.setFechaModificacion(DateUtil.currentTimestamp());
                this.objEmpleado.setObjGradoInstruccion(this.lstGradosInstruccion.get(this.cmbGradoInstruccion.getSelectedIndex()));
                this.objEmpleado.setObjDistrito(this.lstDepartamentos.get(this.cmbDepartamento.getSelectedIndex()).getLstProvincias().get(this.cmbProvincia.getSelectedIndex()).getLstDistritos().get(this.cmbDistrito.getSelectedIndex()));
                this.objEmpleado.getObjUsuario().setLstHuellas(this.lstHuellas);
                this.objEmpleado.getObjUsuario().setUsername(this.txtUsername.getText().trim());
                this.objEmpleado.getObjUsuario().setPassword(StringUtil.encodeString(new String(this.txtPassword.getPassword())));
                this.objEmpleado.getObjUsuario().setActivo(this.chkActivo.isSelected());

                message = EmpleadosProcessLogic.getInstance().actualizarEmpleado(this.objEmpleado);
            }

            if (!message.contains("Error")) {
                JOptionPane.showMessageDialog(this, message, "Empleado", JOptionPane.INFORMATION_MESSAGE);
                this.notifyObservers();
                this.dispose();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void chkEditarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkEditarUsuarioActionPerformed
        this.updateChkActivo();
    }//GEN-LAST:event_chkEditarUsuarioActionPerformed

    private void cmbTipoDocumentoIdentidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTipoDocumentoIdentidadActionPerformed
        SwingUtil.setDocumentoIdentidadInput(this.lstTiposDocumentoIdentidad.get(this.cmbTipoDocumentoIdentidad.getSelectedIndex()), this.txtDocumentoIdentidad);
    }//GEN-LAST:event_cmbTipoDocumentoIdentidadActionPerformed

    //<editor-fold defaultstate="collapsed" desc="Variables">
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnRegistrarHuellas;
    private javax.swing.JCheckBox chkActivo;
    private javax.swing.JCheckBox chkEditarUsuario;
    private javax.swing.JComboBox<String> cmbDepartamento;
    private javax.swing.JComboBox<String> cmbDistrito;
    private javax.swing.JComboBox<String> cmbEstadoCivil;
    private javax.swing.JComboBox<String> cmbGradoInstruccion;
    private javax.swing.JComboBox<String> cmbProvincia;
    private javax.swing.JComboBox<String> cmbTipoDocumentoIdentidad;
    private javax.swing.JComboBox<String> cmbTiposUsuario;
    private org.jdesktop.swingx.JXDatePicker dtpFechaNacimiento;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JPanel pnlDatosPersonales;
    private javax.swing.JRadioButton rdbFemenino;
    private javax.swing.JRadioButton rdbMasculino;
    private javax.swing.ButtonGroup rgbSexo;
    private javax.swing.JTable tblHuellas;
    private javax.swing.JTextField txtApellidoMaterno;
    private javax.swing.JTextField txtApellidoPaterno;
    private javax.swing.JTextField txtCelular;
    private javax.swing.JTextField txtDireccion;
    private javax.swing.JTextField txtDocumentoIdentidad;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JPasswordField txtPassword;
    private javax.swing.JTextField txtTelefono;
    private javax.swing.JTextField txtUsername;
    // End of variables declaration//GEN-END:variables
    //</editor-fold>
    
    private void setTextFieldsConfiguration() {
        SwingUtil.setDocumentoIdentidadInput(this.lstTiposDocumentoIdentidad.get(this.cmbTipoDocumentoIdentidad.getSelectedIndex()), this.txtDocumentoIdentidad);
        SwingUtil.setPhoneInput(this.txtCelular, this.txtTelefono);
        SwingUtil.setUsernameInput(this.txtUsername);
        if (this.objEmpleado == null) {
            this.chkActivo.setVisible(false);
            this.chkActivo.setSelected(false);
        }else{
            this.chkActivo.setSelected(true);
        }
        this.updateChkActivo();
    }

    private void updateChkActivo() {
        boolean activo = this.chkEditarUsuario.isSelected();
        this.txtUsername.setEditable(activo);
        this.txtPassword.setEditable(activo);
    }

    private void updateCmbDepartamentos() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (Departamento objDepartamento : this.lstDepartamentos) {
            comboBoxModel.addElement(objDepartamento.getNombre());
        }

        this.cmbDepartamento.setModel(comboBoxModel);
    }

    private void updateCmbProvincias() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        Departamento objDepartamento = this.lstDepartamentos.get(this.cmbDepartamento.getSelectedIndex());
        for (Provincia objProvincia : objDepartamento.getLstProvincias()) {
            comboBoxModel.addElement(objProvincia.getNombre());
        }

        this.cmbProvincia.setModel(comboBoxModel);
    }

    private void updateCmbDistritos() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        Provincia objProvincia = this.lstDepartamentos.get(this.cmbDepartamento.getSelectedIndex()).getLstProvincias().get(this.cmbProvincia.getSelectedIndex());
        for (Distrito objDistrito : objProvincia.getLstDistritos()) {
            comboBoxModel.addElement(objDistrito.getNombre());
        }
        this.cmbDistrito.setModel(comboBoxModel);
    }

    private void updateCmbEstadosCiviles() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (EstadoCivil objEstadoCivil : this.lstEstadosCiviles) {
            comboBoxModel.addElement(objEstadoCivil.getNombre());
        }

        this.cmbEstadoCivil.setModel(comboBoxModel);
    }

    private void updateCmbGradosIntruccion() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (GradoInstruccion objGradoInstruccion : this.lstGradosInstruccion) {
            comboBoxModel.addElement(objGradoInstruccion.getNombre());
        }

        this.cmbGradoInstruccion.setModel(comboBoxModel);
    }

    private void updateCmbTiposUsuario() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (TipoUsuario objTipoUsuario : this.lstTiposUsuario) {
            comboBoxModel.addElement(objTipoUsuario.getNombre());
        }

        this.cmbTiposUsuario.setModel(comboBoxModel);
    }
    
    private void updateCmbTiposDocumentoIdentidad(){
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();
        
        for(TipoDocumentoIdentidad objTipoDocumentoIdentidad : this.lstTiposDocumentoIdentidad){
            comboBoxModel.addElement(objTipoDocumentoIdentidad.getNombre());
        }
        
        this.cmbTipoDocumentoIdentidad.setModel(comboBoxModel);
    }

    private void updateTblHuellas() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tableModel.addColumn("Mano");
        tableModel.addColumn("Dedo");

        for (Huella objHuella : this.lstHuellas) {
            tableModel.addRow(new Object[]{
                objHuella.isMano() ? "Derecha" : "Izquierda",
                objHuella.getDedo()
            });
        }

        this.tblHuellas.setModel(tableModel);
    }

    @Override
    public void addObserver(IObserver objIObserver) {
        this.lstIObservers.add(objIObserver);
    }

    @Override
    public List<IObserver> getObserverList() {
        return this.lstIObservers;
    }

    @Override
    public void notifyObservers() {
        for (IObserver objIObserver : this.lstIObservers) {
            objIObserver.update(this);
        }
    }
}
