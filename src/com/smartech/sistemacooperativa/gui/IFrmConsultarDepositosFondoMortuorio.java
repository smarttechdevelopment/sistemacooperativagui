/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.process.CuentasProcessLogic;
import com.smartech.sistemacooperativa.dominio.Concepto;
import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Deposito;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.TipoDocumentoIdentidad;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import com.smartech.sistemacooperativa.util.interfaces.IObservable;
import com.smartech.sistemacooperativa.util.interfaces.IObserver;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Smartech
 */
public class IFrmConsultarDepositosFondoMortuorio extends javax.swing.JInternalFrame implements IObserver, IObservable {

    private List<IObserver> lstObservers = new ArrayList<>();

    public static int COMBO_INDEX_TARJETA = 0; // default
    public static int COMBO_INDEX_DOCUMENTO_IDENTIDAD_INDISTINTO = 1;

    private List<Deposito> lstDepositos = new ArrayList<>();
    private final TableRowSorter trsDepositos = new TableRowSorter();

    private Socio objSocio;
    private Cuenta objCuentaFondoMortuorio;

    private List<TipoDocumentoIdentidad> lstTiposDocumentoIdentidad = new ArrayList<>();

    public IFrmConsultarDepositosFondoMortuorio() {
        initComponents();

        this.dtpFechaDesde.setDate(DateUtil.getStartFinishYearDates(new Date()).get(0));
        this.dtpFechaHasta.setDate(DateUtil.getStartFinishYearDates(new Date()).get(1));

        this.lstTiposDocumentoIdentidad = QueryFacade.getInstance().getAllTiposDocumentoIdentidad();
        updateCmbModoBuscar();

        updateInterfaz();
//        this.updateLstDepositosSocio();
//        this.updatetblDepositosRealizadosFecha();
        pack();
    }

    private void updateInterfaz() {
        this.txtNombreSocio.setEditable(false);
        this.txtDocumentoSocio.setEditable(false);
        this.txtFondoMortuorio.setEditable(false);

        this.txtNombreSocio.setText(this.objSocio != null ? this.objSocio.getNombreCompleto() : "");
        this.txtDocumentoSocio.setText(this.objSocio != null ? this.objSocio.getDocumentoIdentidad() : "");

        if (this.objCuentaFondoMortuorio != null) {
            this.cmbModoBuscar.setEnabled(false);
            this.txtBuscarSocio.setText("");
        }

        this.updateBtnBuscar();

        if (this.objCuentaFondoMortuorio != null) {
            this.updatetblDepositosRealizados();
            this.txtFondoMortuorio.setText(this.objCuentaFondoMortuorio.getSaldo().toPlainString());
        }

        this.btnNuevoAporte.setEnabled(this.objCuentaFondoMortuorio != null);
        this.btnTransferenciaCuentaAporte.setEnabled(this.objCuentaFondoMortuorio != null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        dtpFechaDesde = new org.jdesktop.swingx.JXDatePicker();
        dtpFechaHasta = new org.jdesktop.swingx.JXDatePicker();
        jLabel2 = new javax.swing.JLabel();
        txtBuscarSocio = new javax.swing.JTextField();
        btnBuscar = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        txtFondoMortuorio = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblDepositosFondoMortuorio = new javax.swing.JTable();
        btnNuevoAporte = new javax.swing.JButton();
        btnTransferenciaCuentaAporte = new javax.swing.JButton();
        cmbModoBuscar = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        txtNombreSocio = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtDocumentoSocio = new javax.swing.JTextField();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Consulta de fondo mortuorio");
        setToolTipText("");
        setMinimumSize(new java.awt.Dimension(215, 60));
        setPreferredSize(new java.awt.Dimension(610, 422));

        jLabel1.setText("Fecha:");

        jLabel2.setText("Buscar:");

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_search.png"))); // NOI18N
        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        jLabel3.setText("Fondo acumulado:");

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Depósitos realizados"));

        tblDepositosFondoMortuorio.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(tblDepositosFondoMortuorio);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 677, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 445, Short.MAX_VALUE)
        );

        btnNuevoAporte.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/nuevo.png"))); // NOI18N
        btnNuevoAporte.setText("Nuevo aporte");
        btnNuevoAporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoAporteActionPerformed(evt);
            }
        });

        btnTransferenciaCuentaAporte.setText("Aporte desde cuenta");
        btnTransferenciaCuentaAporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTransferenciaCuentaAporteActionPerformed(evt);
            }
        });

        cmbModoBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbModoBuscarActionPerformed(evt);
            }
        });

        jLabel4.setText("Socio:");

        jLabel5.setText("Documento:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnTransferenciaCuentaAporte)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnNuevoAporte))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbModoBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBuscarSocio)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnBuscar))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(9, 9, 9)
                        .addComponent(txtNombreSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 284, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDocumentoSocio))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dtpFechaDesde, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(dtpFechaHasta, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtFondoMortuorio, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(dtpFechaDesde, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dtpFechaHasta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtBuscarSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscar)
                    .addComponent(cmbModoBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtNombreSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(txtDocumentoSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtFondoMortuorio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNuevoAporte)
                    .addComponent(btnTransferenciaCuentaAporte))
                .addContainerGap())
        );

        jScrollPane1.setViewportView(jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1251, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 630, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        int index = this.cmbModoBuscar.getSelectedIndex();
        if (index < 0) {
            index = 0;
        }
        
        if (this.objSocio == null) {
            if (index != COMBO_INDEX_TARJETA && SwingUtil.hasBlankInputs(this.txtBuscarSocio)) {
                JOptionPane.showInternalMessageDialog(this, "Datos incompletos", "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                if (index == COMBO_INDEX_TARJETA) {
                    DlgIngresarTarjeta objDlgIngresarTarjeta = new DlgIngresarTarjeta(PrincipalJRibbon.getInstance().getFrame(), true, false, false);
                    objDlgIngresarTarjeta.setModal(true);
                    SwingUtil.centerOnScreen(objDlgIngresarTarjeta);
                    objDlgIngresarTarjeta.setVisible(true);

                    if (objDlgIngresarTarjeta.getObjTarjeta() != null) {
                        boolean estado = true;
                        this.objSocio = objDlgIngresarTarjeta.getObjTarjeta().getObjSocio(estado);
                        if (this.objSocio == null) {
                            JOptionPane.showMessageDialog(this, "El socio ha sido dado de baja", "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                } else if (index == COMBO_INDEX_DOCUMENTO_IDENTIDAD_INDISTINTO) {
                    this.objSocio = QueryFacade.getInstance().getSocioByDocumentoIdentidad(this.txtBuscarSocio.getText(), this.lstTiposDocumentoIdentidad.get(index), false);
                } else {
                    this.objSocio = QueryFacade.getInstance().getSocioByDocumentoIdentidad(this.txtBuscarSocio.getText(), false);
                }

                if (this.objSocio != null) {
                    this.objCuentaFondoMortuorio = CuentasProcessLogic.getInstance().getCuentaFondoMortuorio(this.objSocio);

                    if (this.objCuentaFondoMortuorio != null) {
                        this.lstDepositos = QueryFacade.getInstance().getAllDepositos(this.objCuentaFondoMortuorio);
                    } else {
                        JOptionPane.showInternalMessageDialog(this, "No tiene cuenta de fondo mortuorio", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                    this.updatetblDepositosRealizados();
                } else {
                    JOptionPane.showInternalMessageDialog(this, "Socio no existe", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        }else{
            this.objSocio = null;
            this.objCuentaFondoMortuorio = null;
        }

        this.updateInterfaz();
    }//GEN-LAST:event_btnBuscarActionPerformed

    public void updateBtnBuscar() {
        int index = this.cmbModoBuscar.getSelectedIndex();
        if (index < 0) {
            index = 0;
        }
        if (this.objSocio == null) {
            if (index == COMBO_INDEX_TARJETA) {
                this.btnBuscar.setText("Ingresar Tarjeta");
            } else {
                this.btnBuscar.setText("Buscar");
            }
        } else {
            this.btnBuscar.setText("Nueva búsqueda");
        }
    }
    private void cmbModoBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbModoBuscarActionPerformed
        this.updateBtnBuscar();
    }//GEN-LAST:event_cmbModoBuscarActionPerformed

    private void btnTransferenciaCuentaAporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTransferenciaCuentaAporteActionPerformed
        DlgRealizarTransferenciaCuenta dlgRealizarTransferenciaCuenta = new DlgRealizarTransferenciaCuenta(PrincipalJRibbon.getInstance().getFrame(), true, null, this.objCuentaFondoMortuorio);
        dlgRealizarTransferenciaCuenta.addObserver(this);
        SwingUtil.centerOnScreen(dlgRealizarTransferenciaCuenta);
        dlgRealizarTransferenciaCuenta.setVisible(true);
        this.lstDepositos = QueryFacade.getInstance().getAllDepositos(this.objCuentaFondoMortuorio);
        this.updatetblDepositosRealizados();
    }//GEN-LAST:event_btnTransferenciaCuentaAporteActionPerformed

    private void btnNuevoAporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoAporteActionPerformed
        DlgSolicitarMovimiento objDlgSolicitarMovimiento = new DlgSolicitarMovimiento(PrincipalJRibbon.getInstance().getFrame(), true, QueryFacade.getInstance().getConcepto(Concepto.CONCEPTO_DEPOSITOS), this.objCuentaFondoMortuorio);
        PrincipalJRibbon.setObservers(objDlgSolicitarMovimiento);
        SwingUtil.centerOnScreen(objDlgSolicitarMovimiento);
        objDlgSolicitarMovimiento.setVisible(true);
        this.lstDepositos = QueryFacade.getInstance().getAllDepositos(this.objCuentaFondoMortuorio);
        this.updatetblDepositosRealizados();
    }//GEN-LAST:event_btnNuevoAporteActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnNuevoAporte;
    private javax.swing.JButton btnTransferenciaCuentaAporte;
    private javax.swing.JComboBox<String> cmbModoBuscar;
    private org.jdesktop.swingx.JXDatePicker dtpFechaDesde;
    private org.jdesktop.swingx.JXDatePicker dtpFechaHasta;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblDepositosFondoMortuorio;
    private javax.swing.JTextField txtBuscarSocio;
    private javax.swing.JTextField txtDocumentoSocio;
    private javax.swing.JTextField txtFondoMortuorio;
    private javax.swing.JTextField txtNombreSocio;
    // End of variables declaration//GEN-END:variables

    private void updateCmbModoBuscar() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        comboBoxModel.addElement("Ingresar tarjeta");
        for (TipoDocumentoIdentidad objTipoDocumentoIdentidad : this.lstTiposDocumentoIdentidad) {
            comboBoxModel.addElement(objTipoDocumentoIdentidad.getNombre());
        }
        comboBoxModel.addElement("Indistinto");
        COMBO_INDEX_DOCUMENTO_IDENTIDAD_INDISTINTO = this.lstTiposDocumentoIdentidad.size() + 1;

        comboBoxModel.setSelectedItem(comboBoxModel.getElementAt(COMBO_INDEX_TARJETA));
        this.cmbModoBuscar.setModel(comboBoxModel);
    }

//    public void updateLstDepositosSocio() {
//
//        this.lstDepositos = QueryFacade.getInstance().getAllDepositosFondoMortuorio(this.dtpFechaDesde.getDate(), this.dtpFechaHasta.getDate());
//
//    }
    public void updatetblDepositosRealizados() {

        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tableModel.addColumn("Monto");
        tableModel.addColumn("Moneda");
        tableModel.addColumn("Fecha de registro");
        tableModel.addColumn("Fecha de modificación");
        tableModel.addColumn("Habilitado");
        tableModel.addColumn("Pagado");
        tableModel.addColumn("Depositante");

        String activo, pagado, moneda;

        for (Deposito objDeposito : this.lstDepositos) {

            activo = objDeposito.isHabilitado() ? "HABILITADO" : "INHABILITADO";
            pagado = objDeposito.isPagado() ? "PAGADO" : "PENDIENTE";

            moneda = objDeposito.getObjCuenta().getObjTipoCuenta().getObjMoneda().getAbreviatura();

            tableModel.addRow(new Object[]{
                objDeposito.getMonto(),
                moneda,
                objDeposito.getFechaRegistro(),
                objDeposito.getFechaModificacion(),
                activo,
                pagado,
                objDeposito.getDniDepositante()
            });
        }

        this.tblDepositosFondoMortuorio.setModel(tableModel);
        this.trsDepositos.setModel(tableModel);
        this.tblDepositosFondoMortuorio.setRowSorter(this.trsDepositos);
    }

    public void update() {
//        this.updateLstDepositosSocio();
        this.updatetblDepositosRealizados();
    }

    @Override
    public void update(IObservable objIObservable) {
        this.update();
    }

    @Override
    public void addObserver(IObserver objIObserver) {
        this.lstObservers.add(objIObserver);
    }

    @Override
    public List<IObserver> getObserverList() {
        return this.lstObservers;
    }

    @Override
    public void notifyObservers() {
        for (IObserver observer : getObserverList()) {
            observer.update(this);
        }
    }

}
