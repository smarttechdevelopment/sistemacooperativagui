package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.process.TiposInteresProcessLogic;
import com.smartech.sistemacooperativa.dominio.TipoInteres;
import com.smartech.sistemacooperativa.util.interfaces.IObservable;
import com.smartech.sistemacooperativa.util.interfaces.IObserver;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Smartech
 */
public class IFrmConsultarTiposInteres extends javax.swing.JInternalFrame implements IObserver {

    private List<TipoInteres> lstTiposInteres = new ArrayList<>();

    public IFrmConsultarTiposInteres() {
        initComponents();
        this.update();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblTiposInteres = new javax.swing.JTable();
        btnNuevo = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnActualizar = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Tipos de Interes");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Tipos de Interes"));

        tblTiposInteres.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblTiposInteres);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 738, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 283, Short.MAX_VALUE)
        );

        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/nuevo.png"))); // NOI18N
        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/basura.png"))); // NOI18N
        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnActualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/actualizacion.png"))); // NOI18N
        btnActualizar.setText("Actualizar");
        btnActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnEliminar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnActualizar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnNuevo)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnNuevo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnActualizar)
                    .addComponent(btnEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        IFrmRegistrarTipoInteres frmRegistrarTipoInteres = new IFrmRegistrarTipoInteres();
        frmRegistrarTipoInteres.addObserver(this);
        PrincipalJRibbon.getInstance().getDesktopPane().add(frmRegistrarTipoInteres);
        frmRegistrarTipoInteres.setVisible(true);
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarActionPerformed
        int index = this.tblTiposInteres.getSelectedRow();

        if (index != -1) {
            IFrmRegistrarTipoInteres frmRegistrarTipoInteres = new IFrmRegistrarTipoInteres(this.lstTiposInteres.get(index));
            frmRegistrarTipoInteres.addObserver(this);
            PrincipalJRibbon.getInstance().getDesktopPane().add(frmRegistrarTipoInteres);
            frmRegistrarTipoInteres.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un Tipo de Interes", "Tipo de Interes", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnActualizarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        int index = this.tblTiposInteres.getSelectedRow();

        if (index != -1) {
            String message = TiposInteresProcessLogic.getInstance().eliminarTipoInteres(this.lstTiposInteres.get(index));

            if (!message.contains("Error")) {
                JOptionPane.showMessageDialog(this, message, "Tipo de Interes", JOptionPane.INFORMATION_MESSAGE);
                this.update();
            }else{
                JOptionPane.showMessageDialog(this, message, "Tipo de Interes", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un Tipo de Interes", "Tipo de Interes", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActualizar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblTiposInteres;
    // End of variables declaration//GEN-END:variables

    private void update() {
        this.updateLstTiposInteres();
        this.updateTblTiposInteres();
    }

    private void updateLstTiposInteres() {
        this.lstTiposInteres = QueryFacade.getInstance().getAllTiposInteres();
    }

    private void updateTblTiposInteres() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tableModel.addColumn("Nombre");
        tableModel.addColumn("Plazos");

        for (TipoInteres objTipoInteres : this.lstTiposInteres) {
            tableModel.addRow(new Object[]{
                objTipoInteres.getNombre(),
                objTipoInteres.getDiasPlazos()
            });
        }

        this.tblTiposInteres.setModel(tableModel);
    }

    @Override
    public void update(IObservable objIObservable) {
        this.update();
    }
}
