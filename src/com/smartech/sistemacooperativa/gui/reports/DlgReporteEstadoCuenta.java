package com.smartech.sistemacooperativa.gui.reports;

import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.facade.ReportFacade;
import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.TipoDocumentoIdentidad;
import com.smartech.sistemacooperativa.gui.DlgIngresarTarjeta;
import com.smartech.sistemacooperativa.gui.PrincipalJRibbon;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import com.smartech.sistemacooperativa.util.reporting.ReportUtil;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.pentaho.reporting.engine.classic.core.parameters.ParameterDefinitionEntry;
import org.pentaho.reporting.engine.classic.core.parameters.PlainParameter;

/**
 *
 * @author Smartech
 */
public class DlgReporteEstadoCuenta extends javax.swing.JDialog {

    private Socio objSocio = null;

    public static int COMBO_INDEX_DOCUMENTO_IDENTIDAD_INDISTINTO = 1;
    public static final int COMBO_INDEX_CODIGO = 1;

    private List<Cuenta> lstCuentas = new ArrayList<>();
    private boolean permitirBuscarEnTxtBusarSocio = true;

    private List<TipoDocumentoIdentidad> lstTiposDocumentoIdentidad = new ArrayList<>();

    public DlgReporteEstadoCuenta(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();

        List<Date> lstDates = DateUtil.getStartFinishMonthDates(new Date());
        this.dtpSinceDate.setDate(lstDates.get(0));
        this.dtpToDate.setDate(lstDates.get(1));

        this.lstTiposDocumentoIdentidad = QueryFacade.getInstance().getAllTiposDocumentoIdentidad();

        this.updateCmbModo();

        this.btnGenerarReporte.setEnabled(false);
        this.setCmbModo();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        dtpSinceDate = new org.jdesktop.swingx.JXDatePicker();
        jLabel3 = new javax.swing.JLabel();
        dtpToDate = new org.jdesktop.swingx.JXDatePicker();
        btnGenerarReporte = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        cmbModoBuscaSocio = new javax.swing.JComboBox<>();
        txtBuscarSocio = new javax.swing.JTextField();
        btnIngresarTarjeta = new javax.swing.JButton();
        lblCuentasOSocios = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblCuentas = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Estado de Cuenta");

        jLabel2.setText("Desde:");

        dtpSinceDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dtpSinceDateActionPerformed(evt);
            }
        });

        jLabel3.setText("Hasta:");

        btnGenerarReporte.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/reporte.png"))); // NOI18N
        btnGenerarReporte.setText("Generar Reporte");
        btnGenerarReporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerarReporteActionPerformed(evt);
            }
        });

        jLabel1.setText("Buscar:");

        cmbModoBuscaSocio.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione" }));
        cmbModoBuscaSocio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbModoBuscaSocioActionPerformed(evt);
            }
        });

        txtBuscarSocio.setMinimumSize(new java.awt.Dimension(80, 20));
        txtBuscarSocio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarSocioKeyReleased(evt);
            }
        });

        btnIngresarTarjeta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/credit-card.png"))); // NOI18N
        btnIngresarTarjeta.setText("Ingresar Tarjeta");
        btnIngresarTarjeta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIngresarTarjetaActionPerformed(evt);
            }
        });

        lblCuentasOSocios.setText("Cuentas habilitadas de socio");

        tblCuentas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblCuentas);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbModoBuscaSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBuscarSocio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnIngresarTarjeta))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 591, Short.MAX_VALUE)
                    .addComponent(lblCuentasOSocios)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dtpSinceDate, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dtpToDate, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnGenerarReporte)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cmbModoBuscaSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBuscarSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnIngresarTarjeta))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblCuentasOSocios)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(dtpSinceDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(dtpToDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(btnGenerarReporte)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void dtpSinceDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dtpSinceDateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_dtpSinceDateActionPerformed

    private void btnGenerarReporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerarReporteActionPerformed
        try {
            int index = this.tblCuentas.getSelectedRow();

            if (index != -1) {
                final PlainParameter paramTitle = new PlainParameter("title", String.class);
                paramTitle.setDefaultValue("Reporte de Estado de Cuenta");

                Cuenta objCuenta = this.lstCuentas.get(index);

                Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> reportData = ReportFacade.getInstance().getEstadoCuentaData(objCuenta, this.dtpSinceDate.getDate(), this.dtpToDate.getDate());

                ReportUtil.showReport(PrincipalJRibbon.getInstance(), new URL("file:rpt_cooperativa_estadocuenta.prpt"), ReportUtil.getDataOnly(reportData), ReportUtil.getParametersOnly(reportData));
            } else {
                JOptionPane.showMessageDialog(this, "Seleccione una Cuenta", "Cuenta", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnGenerarReporteActionPerformed

    private void cmbModoBuscaSocioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbModoBuscaSocioActionPerformed
        this.setCmbModo();
    }//GEN-LAST:event_cmbModoBuscaSocioActionPerformed

    private void txtBuscarSocioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarSocioKeyReleased
        String message;

        // Buscar cuentas de socio
        message = listarCuentas(); // Busca según los componentes radioButton y Tarjeta ingresada. Retorna un mensaje cuando realiza una búsqueda y no existe socio.
        // No realiza la búsqueda cuando ha sido encontrado un socio recientemente y los datos como DNI o código son los mismos
        // No realiza la búsqueda mientras no complete la cantidad de caracteres según la opción seleccionada
        //Actualiza tabla con las cuentas
        
        if(this.objSocio != null){
            this.lstCuentas = QueryFacade.getInstance().getAllCuentas(this.objSocio);
        }
        
        this.updateTblCuentasHabilitadas();

        // Muestra mensaje
        if (this.permitirBuscarEnTxtBusarSocio && message.length() > 0) {
            this.lstCuentas.clear();
            updateTblCuentasHabilitadas();
            JOptionPane.showMessageDialog(this, message, "Información", JOptionPane.INFORMATION_MESSAGE);
            this.permitirBuscarEnTxtBusarSocio = false;
        } else {
            this.permitirBuscarEnTxtBusarSocio = true;
        }

        if (this.tblCuentas.getRowCount() == 0) {
            this.btnGenerarReporte.setEnabled(false);
        } else {
            this.btnGenerarReporte.setEnabled(true);
        }
    }//GEN-LAST:event_txtBuscarSocioKeyReleased

    private void btnIngresarTarjetaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIngresarTarjetaActionPerformed
        this.txtBuscarSocio.setText("Con tarjeta");
        this.cmbModoBuscaSocio.setEnabled(false);

        DlgIngresarTarjeta objDlgIngresarTarjeta = new DlgIngresarTarjeta(PrincipalJRibbon.getInstance().getFrame(), true, false, false);
        objDlgIngresarTarjeta.setModal(true);
        SwingUtil.centerOnScreen(objDlgIngresarTarjeta);
        objDlgIngresarTarjeta.setVisible(true);

        if (objDlgIngresarTarjeta.getObjTarjeta() != null) {
            this.objSocio = objDlgIngresarTarjeta.getObjTarjeta().getObjSocio();
            this.lstCuentas = QueryFacade.getInstance().getAllCuentas(this.objSocio);
            updateTblCuentasHabilitadas();
            this.btnGenerarReporte.setEnabled(true);
        } else {
            this.txtBuscarSocio.setText("");
            this.cmbModoBuscaSocio.setEnabled(true);
        }
    }//GEN-LAST:event_btnIngresarTarjetaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGenerarReporte;
    private javax.swing.JButton btnIngresarTarjeta;
    private javax.swing.JComboBox<String> cmbModoBuscaSocio;
    private org.jdesktop.swingx.JXDatePicker dtpSinceDate;
    private org.jdesktop.swingx.JXDatePicker dtpToDate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblCuentasOSocios;
    private javax.swing.JTable tblCuentas;
    private javax.swing.JTextField txtBuscarSocio;
    // End of variables declaration//GEN-END:variables

    private void setCmbModo() {
        this.txtBuscarSocio.setText("");
        this.txtBuscarSocio.setEnabled(true);
        if (this.cmbModoBuscaSocio.getSelectedIndex() == COMBO_INDEX_DOCUMENTO_IDENTIDAD_INDISTINTO) {
            SwingUtil.setDocumentoIdentidadInput(new TipoDocumentoIdentidad(), this.txtBuscarSocio);
        } else {
            SwingUtil.setDocumentoIdentidadInput(this.lstTiposDocumentoIdentidad.get(this.cmbModoBuscaSocio.getSelectedIndex()), this.txtBuscarSocio);
        }
    }

    private String listarCuentas() {
        String message = "";
        boolean realizaBusqueda = false;

        if (this.txtBuscarSocio.isEnabled()) {
            if (this.cmbModoBuscaSocio.getSelectedIndex() == COMBO_INDEX_DOCUMENTO_IDENTIDAD_INDISTINTO) {
                if (this.objSocio != null) {
                    if (!this.objSocio.getDocumentoIdentidad().equals(this.txtBuscarSocio.getText()) && TipoDocumentoIdentidad.existeLongitud(this.txtBuscarSocio.getText().trim().length(), this.lstTiposDocumentoIdentidad)) {
                        realizaBusqueda = true;
                        this.objSocio = QueryFacade.getInstance().getSocioByDocumentoIdentidad(this.txtBuscarSocio.getText(), false);
                    }
                } else if (TipoDocumentoIdentidad.existeLongitud(this.txtBuscarSocio.getText().trim().length(), this.lstTiposDocumentoIdentidad)) {
                    realizaBusqueda = true;
                    this.objSocio = QueryFacade.getInstance().getSocioByDocumentoIdentidad(this.txtBuscarSocio.getText(), false);
                }
            } else if (this.txtBuscarSocio.getText().trim().length() == this.lstTiposDocumentoIdentidad.get(this.cmbModoBuscaSocio.getSelectedIndex()).getLongitud()) {
                realizaBusqueda = true;
                this.objSocio = QueryFacade.getInstance().getSocioByDocumentoIdentidad(this.txtBuscarSocio.getText().trim(), this.lstTiposDocumentoIdentidad.get(this.cmbModoBuscaSocio.getSelectedIndex()), false);
            }

            if (realizaBusqueda) {
                if (this.objSocio == null) {
                    message = "No existe socio";
                }
            }

        } else if (this.objSocio != null) {
            if (this.objSocio.isActivo() && this.objSocio.isEstado()) {
                if (this.objSocio.getLstCuentas().isEmpty()) {
                    message = "No tiene cuentas activas";
                }
            } else {
                message = "No está activo";
            }
        } else {
            message = "No existe socio";
        }

        return message;
    }

    private void updateTblCuentasHabilitadas() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        if (this.lstCuentas != null) {
            tableModel.addColumn("Codigo");
            tableModel.addColumn("Saldo");
            tableModel.addColumn("Tipo de cuenta");
            tableModel.addColumn("Tasa de interés");
            tableModel.addColumn("Activo");

            String activo;
            for (Cuenta objCuenta : this.lstCuentas) {

                activo = objCuenta.isActivo() ? "ACTIVA" : "INACTIVA";

                tableModel.addRow(new Object[]{
                    objCuenta.getCodigo(),
                    objCuenta.getSaldo(),
                    objCuenta.getObjTipoCuenta().getNombre(),
                    objCuenta.getObjTasaInteres().getNombre(),
                    activo
                });
            }
        }

        this.tblCuentas.setModel(tableModel);
    }

    private void updateCmbModo() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (TipoDocumentoIdentidad objTipoDocumentoIdentidad : this.lstTiposDocumentoIdentidad) {
            comboBoxModel.addElement(objTipoDocumentoIdentidad.getNombre());
        }
        comboBoxModel.addElement("Indistinto");
        COMBO_INDEX_DOCUMENTO_IDENTIDAD_INDISTINTO = this.lstTiposDocumentoIdentidad.size();

        this.cmbModoBuscaSocio.setModel(comboBoxModel);
    }
}
