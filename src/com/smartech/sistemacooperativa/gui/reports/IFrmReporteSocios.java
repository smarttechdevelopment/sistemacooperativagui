package com.smartech.sistemacooperativa.gui.reports;

import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.facade.ReportFacade;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.gui.PrincipalJRibbon;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.util.reporting.ReportUtil;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import org.pentaho.reporting.engine.classic.core.parameters.ParameterDefinitionEntry;
import org.pentaho.reporting.engine.classic.core.parameters.PlainParameter;

/**
 *
 * @author Smartech
 */
public class IFrmReporteSocios extends javax.swing.JInternalFrame {

    private PrincipalJRibbon principalJRibbon;
    private List<Socio> lstSocios;
    private TableRowSorter<TableModel> trsSocios = new TableRowSorter<>();
    private final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy - hh:mm a");
    private boolean tipoReporte;

    public IFrmReporteSocios(PrincipalJRibbon principalJRibbon, boolean tipoReporte) {
        initComponents();
        this.principalJRibbon = principalJRibbon;
        this.update();
        this.tipoReporte = this.setTipoReporte(tipoReporte);
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtBuscarSocio = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblSocios = new javax.swing.JTable();
        chkDetallado = new javax.swing.JCheckBox();
        btnGenerar = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setTitle("Reporte de Socios");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos de socios"));

        jLabel1.setText("Buscar:");

        txtBuscarSocio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarSocioKeyReleased(evt);
            }
        });

        tblSocios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblSocios);

        chkDetallado.setText("Detallado");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtBuscarSocio)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(chkDetallado))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 676, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtBuscarSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chkDetallado))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 422, Short.MAX_VALUE)
                .addContainerGap())
        );

        btnGenerar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/reporte.png"))); // NOI18N
        btnGenerar.setText("Generar Reporte");
        btnGenerar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(555, Short.MAX_VALUE)
                .addComponent(btnGenerar)
                .addContainerGap())
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnGenerar)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtBuscarSocioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarSocioKeyReleased
        SwingUtil.textFilter(evt, this.tblSocios, this, this.txtBuscarSocio, this.trsSocios);
    }//GEN-LAST:event_txtBuscarSocioKeyReleased

    private void btnGenerarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerarActionPerformed
        try {
            final PlainParameter paramTitle = new PlainParameter("title", String.class);

            // -------REPORTE GENERAL----------
            if(this.tipoReporte){
                // ~ DETALLADO ~
                if (this.chkDetallado.isSelected()) {
                paramTitle.setDefaultValue("Reporte Detallado de los Socios");
                
                Map<HashMap<String,TableModel>, List<ParameterDefinitionEntry>> reportData = ReportFacade.getInstance().getAllSociosDetallado();

                if (!reportData.isEmpty()) {
                    ReportUtil.showReport(PrincipalJRibbon.getInstance(), new URL("file:rpt_cooperativa_general_detallado_socios.prpt"), ReportUtil.getDataOnly(reportData), ReportUtil.getParametersOnly(reportData));
                } else {
                    JOptionPane.showMessageDialog(this, "No hay socios.", "Información", JOptionPane.INFORMATION_MESSAGE);
                }

                // ~ NO DETALLADO ~
                } else {

                    paramTitle.setDefaultValue("Reporte No Detallado de los Socios");
                
                    Map<HashMap<String,TableModel>, List<ParameterDefinitionEntry>> reportData = ReportFacade.getInstance().getAllSociosNoDetallado();

                    if (!reportData.isEmpty()) {
                        ReportUtil.showReport(PrincipalJRibbon.getInstance(), new URL("file:rpt_cooperativa_general_no_detallado_socios.prpt"), ReportUtil.getDataOnly(reportData), ReportUtil.getParametersOnly(reportData));
                    } else {
                        JOptionPane.showMessageDialog(this, "No hay socios.", "Información", JOptionPane.INFORMATION_MESSAGE);
                    }
                    

                }
            
            // ----------REPORTE INDIVIDUAL----------
            } else { 
                paramTitle.setDefaultValue("Reporte del Socio");

                if(tblSocios.getSelectedRow() == -1){
                    JOptionPane.showMessageDialog(this, "Por favor, seleccione un socio.", "Información", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    Map<HashMap<String,TableModel>, List<ParameterDefinitionEntry>> reportData = ReportFacade.getInstance().getOneSocioDetallado(lstSocios.get(tblSocios.getSelectedRow()));

                    if (!reportData.isEmpty()) {
                        ReportUtil.showReport(principalJRibbon, new URL("file:rpt_cooperativa_individual_socio_detallado.prpt"), ReportUtil.getDataOnly(reportData), ReportUtil.getParametersOnly(reportData));
                    } else {
                        JOptionPane.showMessageDialog(this, "No hay información del socio.", "Información", JOptionPane.INFORMATION_MESSAGE);
                    }
                }                
            }
            

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnGenerarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGenerar;
    private javax.swing.JCheckBox chkDetallado;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblSocios;
    private javax.swing.JTextField txtBuscarSocio;
    // End of variables declaration//GEN-END:variables

    private boolean setTipoReporte(boolean general){
        if(general == false){ //reporte individual
            this.chkDetallado.setVisible(false);
        }
        return general;
    }
    
    private void updateLstSocios() {
        this.lstSocios = QueryFacade.getInstance().getAllSociosActivos(false);
    }

    private void updateTblSocios() {
        DefaultTableModel defaultTableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        defaultTableModel.addColumn("Nombre");
        defaultTableModel.addColumn("DNI");
        defaultTableModel.addColumn("Fecha Ingreso");

        for (Socio objSocio : this.lstSocios) {
            defaultTableModel.addRow(new Object[]{
                objSocio.getNombreCompleto(),
                objSocio.getDocumentoIdentidad(),
                this.sdf.format(new Date(objSocio.getFechaRegistro().getTime()))
            });
        }

        this.tblSocios.setModel(defaultTableModel);
        this.trsSocios.setModel(defaultTableModel);
        this.tblSocios.setRowSorter(this.trsSocios);

    }

    private void update() {
        this.updateLstSocios();
        this.updateTblSocios();
    }

}
