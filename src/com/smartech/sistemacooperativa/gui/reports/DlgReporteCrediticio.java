package com.smartech.sistemacooperativa.gui.reports;

import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.facade.ReportFacade;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.TipoDocumentoIdentidad;
import com.smartech.sistemacooperativa.gui.DlgIngresarTarjeta;
import com.smartech.sistemacooperativa.gui.PrincipalJRibbon;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.util.reporting.ReportUtil;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.TableModel;
import org.pentaho.reporting.engine.classic.core.parameters.ParameterDefinitionEntry;
import org.pentaho.reporting.engine.classic.core.parameters.PlainParameter;

/**
 *
 * @author Smartech
 */
public class DlgReporteCrediticio extends javax.swing.JDialog {

    public static int COMBO_INDEX_DOCUMENTO_IDENTIDAD_INDISTINTO = 0;
    //private boolean permitirBuscarEnTxtBusarSocio = true;
    private Socio objSocio;
    private List<TipoDocumentoIdentidad> lstTiposDocumentoIdentidad = new ArrayList<>();

    public DlgReporteCrediticio(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();

        this.lstTiposDocumentoIdentidad = QueryFacade.getInstance().getAllTiposDocumentoIdentidad();
        this.updateCmbModo();

        this.btnGenerarReporte.setEnabled(false);
        this.setCmbModo();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnGenerarReporte = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        cmbModoBuscaSocio = new javax.swing.JComboBox<>();
        txtBuscarSocio = new javax.swing.JTextField();
        btnIngresarTarjeta = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtNombreSocio = new javax.swing.JTextField();
        txtDocumentoIdentidad = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtTipoDocumento = new javax.swing.JTextField();
        btnLimpiarSocio = new javax.swing.JButton();
        chkDetallado = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Reporte Crediticio");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Reporte Crediticio"));

        btnGenerarReporte.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/reporte.png"))); // NOI18N
        btnGenerarReporte.setText("Generar Reporte");
        btnGenerarReporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerarReporteActionPerformed(evt);
            }
        });

        jLabel1.setText("Buscar:");

        cmbModoBuscaSocio.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione" }));
        cmbModoBuscaSocio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbModoBuscaSocioActionPerformed(evt);
            }
        });

        txtBuscarSocio.setMinimumSize(new java.awt.Dimension(80, 20));
        txtBuscarSocio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarSocioKeyReleased(evt);
            }
        });

        btnIngresarTarjeta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/credit-card.png"))); // NOI18N
        btnIngresarTarjeta.setText("Ingresar Tarjeta");
        btnIngresarTarjeta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIngresarTarjetaActionPerformed(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Socio"));

        jLabel2.setText("Nombre:");

        jLabel3.setText("Documento de Identidad:");

        txtNombreSocio.setEditable(false);

        txtDocumentoIdentidad.setEditable(false);

        jLabel4.setText("Tipo de Documento:");

        txtTipoDocumento.setEditable(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDocumentoIdentidad, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTipoDocumento))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNombreSocio)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtNombreSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtDocumentoIdentidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(txtTipoDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnLimpiarSocio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/basura.png"))); // NOI18N
        btnLimpiarSocio.setText("Limpiar Socio");
        btnLimpiarSocio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarSocioActionPerformed(evt);
            }
        });

        chkDetallado.setText("Detallado");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnLimpiarSocio)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnGenerarReporte))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbModoBuscaSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBuscarSocio, javax.swing.GroupLayout.DEFAULT_SIZE, 365, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnIngresarTarjeta)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(chkDetallado)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cmbModoBuscaSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBuscarSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnIngresarTarjeta)
                    .addComponent(chkDetallado))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGenerarReporte)
                    .addComponent(btnLimpiarSocio))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGenerarReporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerarReporteActionPerformed
        try {
            if (this.objSocio != null) {
                if (this.chkDetallado.isSelected()) {
                    final PlainParameter paramTitle = new PlainParameter("title", String.class);
                    paramTitle.setDefaultValue("Reporte Crediticio");

                    Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> reportData = ReportFacade.getInstance().getReporteCrediticioDetallado(this.objSocio);

                    ReportUtil.showReport(PrincipalJRibbon.getInstance(), new URL("file:rpt_cooperativa_reportecrediticio_detallado.prpt"), ReportUtil.getDataOnly(reportData), ReportUtil.getParametersOnly(reportData));
                } else {
                    final PlainParameter paramTitle = new PlainParameter("title", String.class);
                    paramTitle.setDefaultValue("Reporte Crediticio");

                    Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> reportData = ReportFacade.getInstance().getReporteCrediticio(this.objSocio);

                    ReportUtil.showReport(PrincipalJRibbon.getInstance(), new URL("file:rpt_cooperativa_reportecrediticio.prpt"), ReportUtil.getDataOnly(reportData), ReportUtil.getParametersOnly(reportData));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnGenerarReporteActionPerformed

    private void cmbModoBuscaSocioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbModoBuscaSocioActionPerformed
        this.setCmbModo();
    }//GEN-LAST:event_cmbModoBuscaSocioActionPerformed

    private void txtBuscarSocioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarSocioKeyReleased
        String message;

        // Buscar socio
        message = getSocio();
        this.updateCamposSocio();
        this.btnGenerarReporte.setEnabled(this.objSocio != null);

        // Muestra mensaje
        if (!message.isEmpty()) {
            JOptionPane.showMessageDialog(this, message, "Información", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_txtBuscarSocioKeyReleased

    private void btnIngresarTarjetaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIngresarTarjetaActionPerformed
        this.txtBuscarSocio.setText("Con tarjeta");
        this.cmbModoBuscaSocio.setEnabled(false);

        DlgIngresarTarjeta objDlgIngresarTarjeta = new DlgIngresarTarjeta(PrincipalJRibbon.getInstance().getFrame(), true, false, false);
        objDlgIngresarTarjeta.setModal(true);
        SwingUtil.centerOnScreen(objDlgIngresarTarjeta);
        objDlgIngresarTarjeta.setVisible(true);

        if (objDlgIngresarTarjeta.getObjTarjeta() != null) {
            this.objSocio = objDlgIngresarTarjeta.getObjTarjeta().getObjSocio();
            this.btnGenerarReporte.setEnabled(true);
        } else {
            this.txtBuscarSocio.setText("");
            this.cmbModoBuscaSocio.setEnabled(true);
        }

        this.updateCamposSocio();
    }//GEN-LAST:event_btnIngresarTarjetaActionPerformed

    private void btnLimpiarSocioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarSocioActionPerformed
        this.objSocio = null;
        this.cmbModoBuscaSocio.setEnabled(true);
        this.updateCamposSocio();
    }//GEN-LAST:event_btnLimpiarSocioActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGenerarReporte;
    private javax.swing.JButton btnIngresarTarjeta;
    private javax.swing.JButton btnLimpiarSocio;
    private javax.swing.JCheckBox chkDetallado;
    private javax.swing.JComboBox<String> cmbModoBuscaSocio;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField txtBuscarSocio;
    private javax.swing.JTextField txtDocumentoIdentidad;
    private javax.swing.JTextField txtNombreSocio;
    private javax.swing.JTextField txtTipoDocumento;
    // End of variables declaration//GEN-END:variables

    private void updateCamposSocio() {
        if (this.objSocio != null) {
            this.txtNombreSocio.setText(this.objSocio.getNombreCompleto());
            this.txtDocumentoIdentidad.setText(this.objSocio.getDocumentoIdentidad());
            this.txtTipoDocumento.setText(this.objSocio.getObjTipoDocumentoIdentidad().getNombre());
        } else {
            SwingUtil.clear(this.txtNombreSocio, this.txtDocumentoIdentidad, this.txtTipoDocumento);
        }
    }

    private void setCmbModo() {
        if (this.cmbModoBuscaSocio.getSelectedIndex() >= this.lstTiposDocumentoIdentidad.size()) {
            this.txtBuscarSocio.setText("");
            this.txtBuscarSocio.setEnabled(true);
            SwingUtil.setDocumentoIdentidadInput(new TipoDocumentoIdentidad(), this.txtBuscarSocio);
        } else {
            this.txtBuscarSocio.setText("");
            this.txtBuscarSocio.setEnabled(true);
            SwingUtil.setDocumentoIdentidadInput(this.lstTiposDocumentoIdentidad.get(this.cmbModoBuscaSocio.getSelectedIndex()), this.txtBuscarSocio);
        }
    }

    private String getSocio() {
        String message = "";
        boolean realizaBusqueda = false;

        if (this.txtBuscarSocio.isEnabled()) {
            if (this.objSocio != null) {
                if (this.cmbModoBuscaSocio.getSelectedIndex() == COMBO_INDEX_DOCUMENTO_IDENTIDAD_INDISTINTO) {
                    if (!this.objSocio.getDocumentoIdentidad().equals(this.txtBuscarSocio.getText()) && TipoDocumentoIdentidad.existeLongitud(this.txtBuscarSocio.getText().trim().length(), this.lstTiposDocumentoIdentidad)) {
                        realizaBusqueda = true;
                        this.objSocio = QueryFacade.getInstance().getSocioByDocumentoIdentidad(this.txtBuscarSocio.getText(), false);
                    }
                } else if (this.lstTiposDocumentoIdentidad.get(this.cmbModoBuscaSocio.getSelectedIndex()).getLongitud() == this.txtBuscarSocio.getText().trim().length()) {
                    realizaBusqueda = true;
                    this.objSocio = QueryFacade.getInstance().getSocioByDocumentoIdentidad(this.txtBuscarSocio.getText(), this.lstTiposDocumentoIdentidad.get(this.cmbModoBuscaSocio.getSelectedIndex()), false);
                }
            } else if (TipoDocumentoIdentidad.existeLongitud(this.txtBuscarSocio.getText().trim().length(), this.lstTiposDocumentoIdentidad) && this.cmbModoBuscaSocio.getSelectedIndex() == COMBO_INDEX_DOCUMENTO_IDENTIDAD_INDISTINTO) {
                realizaBusqueda = true;
                this.objSocio = QueryFacade.getInstance().getSocioByDocumentoIdentidad(this.txtBuscarSocio.getText(), false);
            } else if (this.cmbModoBuscaSocio.getSelectedIndex() != COMBO_INDEX_DOCUMENTO_IDENTIDAD_INDISTINTO) {
                if (this.lstTiposDocumentoIdentidad.get(this.cmbModoBuscaSocio.getSelectedIndex()).getLongitud() == this.txtBuscarSocio.getText().trim().length()) {
                    realizaBusqueda = true;
                    this.objSocio = QueryFacade.getInstance().getSocioByDocumentoIdentidad(this.txtBuscarSocio.getText(), this.lstTiposDocumentoIdentidad.get(this.cmbModoBuscaSocio.getSelectedIndex()), false);
                }
            }

            if (realizaBusqueda) {
                if (this.objSocio == null) {
                    message = "No existe socio";
                }
            }

        } else if (this.objSocio != null) {
            if (!(this.objSocio.isActivo() && this.objSocio.isEstado())) {
                message = "No está activo";
            }
        } else {
            message = "No existe socio";
        }

        return message;
    }

    private void updateCmbModo() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (TipoDocumentoIdentidad objTipoDocumentoIdentidad : this.lstTiposDocumentoIdentidad) {
            comboBoxModel.addElement(objTipoDocumentoIdentidad.getNombre());
        }
        comboBoxModel.addElement("Indistinto");
        COMBO_INDEX_DOCUMENTO_IDENTIDAD_INDISTINTO = this.lstTiposDocumentoIdentidad.size();

        this.cmbModoBuscaSocio.setModel(comboBoxModel);
    }
}
