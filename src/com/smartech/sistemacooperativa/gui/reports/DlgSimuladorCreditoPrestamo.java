package com.smartech.sistemacooperativa.gui.reports;

import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.facade.ReportFacade;
import com.smartech.sistemacooperativa.bll.process.PrestamosProcessLogic;
import com.smartech.sistemacooperativa.dominio.Prestamo;
import com.smartech.sistemacooperativa.dominio.TasaInteres;
import com.smartech.sistemacooperativa.dominio.TipoInteres;
import com.smartech.sistemacooperativa.gui.PrincipalJRibbon;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.util.reporting.ReportUtil;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.TableModel;
import org.pentaho.reporting.engine.classic.core.parameters.ParameterDefinitionEntry;

/**
 *
 * @author Smartech
 */
public class DlgSimuladorCreditoPrestamo extends javax.swing.JDialog {

    private List<TipoInteres> lstTiposInteres = new ArrayList<>();
    private List<TasaInteres> lstTasasInteres = new ArrayList<>();

    public DlgSimuladorCreditoPrestamo(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();

        this.setConfiguration();
        this.lstTiposInteres = QueryFacade.getInstance().getAllTiposInteres();
        this.updateCmbTiposInteres();
        this.updateCmbTasasInteres();
        this.dtpFechaDesembolso.setDate(new Date());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        cmbTiposInteres = new javax.swing.JComboBox<>();
        dtpFechaDesembolso = new org.jdesktop.swingx.JXDatePicker();
        txtCapital = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtCuotas = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        cmbTasasInteres = new javax.swing.JComboBox<>();
        btnGenerarReporte = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Simulador Financiero");

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Simulador de Credito"));

        jLabel2.setText("Tipo de Interes:");

        jLabel5.setText("Capital:");

        jLabel6.setText("Fecha Desembolso:");

        cmbTiposInteres.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbTiposInteres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTiposInteresActionPerformed(evt);
            }
        });

        jLabel3.setText("Tasa de Interes:");

        jLabel4.setText("Cuotas:");

        cmbTasasInteres.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbTiposInteres, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(dtpFechaDesembolso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCuotas)
                            .addComponent(cmbTasasInteres, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txtCapital, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
                        .addGap(237, 237, 237)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cmbTiposInteres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(cmbTasasInteres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(dtpFechaDesembolso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(txtCuotas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtCapital, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        btnGenerarReporte.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/reporte.png"))); // NOI18N
        btnGenerarReporte.setText("Generar Reporte");
        btnGenerarReporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerarReporteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnGenerarReporte)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnGenerarReporte)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmbTiposInteresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTiposInteresActionPerformed
        this.updateLstTasasInteres();
        this.updateCmbTasasInteres();
    }//GEN-LAST:event_cmbTiposInteresActionPerformed

    private void btnGenerarReporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerarReporteActionPerformed
        try {
            if (!SwingUtil.hasBlankInputs(this.txtCapital, this.txtCuotas)) {
                Prestamo objPrestamo = PrestamosProcessLogic.getInstance().generarPrestamo(this.lstTasasInteres.get(this.cmbTasasInteres.getSelectedIndex()),
                        new BigDecimal(this.txtCapital.getText().trim()),
                        Integer.parseInt(this.txtCuotas.getText().trim()),
                        this.dtpFechaDesembolso.getDate());
                if (objPrestamo != null) {
                    Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> reportData = ReportFacade.getInstance().getSimulacionCrediticia(objPrestamo, this.lstTasasInteres.get(this.cmbTasasInteres.getSelectedIndex()), this.dtpFechaDesembolso.getDate());
                    ReportUtil.showReport(PrincipalJRibbon.getInstance(), new URL("file:rpt_cooperativa_simuladorcrediticio.prpt"), ReportUtil.getDataOnly(reportData), ReportUtil.getParametersOnly(reportData));
                }
            } else {
                JOptionPane.showMessageDialog(this, "Error en el registro. Revise los campos.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnGenerarReporteActionPerformed

    private void setConfiguration() {
        SwingUtil.setPhoneInput(this.txtCuotas);
        SwingUtil.setPositiveNumericInput(this.txtCapital);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGenerarReporte;
    private javax.swing.JComboBox<String> cmbTasasInteres;
    private javax.swing.JComboBox<String> cmbTiposInteres;
    private org.jdesktop.swingx.JXDatePicker dtpFechaDesembolso;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField txtCapital;
    private javax.swing.JTextField txtCuotas;
    // End of variables declaration//GEN-END:variables

    private void updateCmbTiposInteres() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (TipoInteres objTipoInteres : this.lstTiposInteres) {
            comboBoxModel.addElement(objTipoInteres.getNombre());
        }

        this.cmbTiposInteres.setModel(comboBoxModel);
        
        this.updateLstTasasInteres();
    }

    private void updateLstTasasInteres() {
        TipoInteres objTipoInteres = this.lstTiposInteres.get(this.cmbTiposInteres.getSelectedIndex());
        this.lstTasasInteres.clear();
        for (TasaInteres objTasaInteres : objTipoInteres.getLstTasasInteres()) {
            if (objTasaInteres.isEstado()) {
                this.lstTasasInteres.add(objTasaInteres);
            }
        }
    }

    private void updateCmbTasasInteres() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (TasaInteres objTasaInteres : this.lstTasasInteres) {
            comboBoxModel.addElement(objTasaInteres.getNombre());
        }

        this.cmbTasasInteres.setModel(comboBoxModel);
    }
}
