package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.util.generics.StringUtil;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import javax.swing.JOptionPane;

/**
 *
 * @author Smartech
 */
public class DlgRecibidor extends javax.swing.JDialog {

    private String montoRecibidoString = "";
    private BigDecimal montoTotal = BigDecimal.ZERO;
    private BigDecimal montoRecibido = BigDecimal.ZERO;
    private boolean efectuado = false;

    public DlgRecibidor(java.awt.Frame parent, boolean modal, BigDecimal montoTotal) {
        super(parent, modal);
        initComponents();

        setTitle("Recepcion de Efectivo");

        this.montoTotal = montoTotal;
        this.txtMontoTotal.setText(this.montoTotal.toPlainString());
        this.txtMontoVuelto.setText(BigDecimal.ZERO.toPlainString());

        SwingUtil.setPositiveNumericInput(this.txtMontoRecibido);

        this.txtMontoRecibido.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                int position = txtMontoRecibido.getCaretPosition();
                montoRecibidoString = txtMontoRecibido.getText();
                if (e.getKeyChar() == KeyEvent.VK_CLEAR || e.getKeyChar() == KeyEvent.VK_BACK_SPACE || e.getKeyChar() == KeyEvent.VK_DELETE) {
                    calcularMontoVuelto(position);
                } else if (e.getKeyChar() != KeyEvent.VK_ENTER) {
                    
                    if (e.getKeyChar() == '.') {
                        if (!montoRecibidoString.contains(String.valueOf(e.getKeyChar()))) {
                            montoRecibidoString = new StringBuilder(montoRecibidoString).insert(position, e.getKeyChar()).toString();
                        }
                    } else {
                        montoRecibidoString = new StringBuilder(montoRecibidoString).insert(position, e.getKeyChar()).toString();
                    }
                    if (montoRecibidoString.equals(".")) {
                        montoRecibidoString = StringUtil.deleteIndex(montoRecibidoString, position);
                    }
                    calcularMontoVuelto(position);
                }
            }
        });
    }

    public boolean isEfectuado() {
        return efectuado;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtMontoTotal = new javax.swing.JTextField();
        btnEfectuar = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txtMontoRecibido = new javax.swing.JTextField();
        txtMontoVuelto = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("Monto Total:");

        txtMontoTotal.setEditable(false);

        btnEfectuar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_checked.png"))); // NOI18N
        btnEfectuar.setText("Efectuar");
        btnEfectuar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEfectuarActionPerformed(evt);
            }
        });

        jLabel2.setText("Monto Recibido:");

        txtMontoVuelto.setEditable(false);

        jLabel3.setText("Monto Vuelto:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnEfectuar))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtMontoRecibido)
                                .addComponent(txtMontoVuelto, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtMontoTotal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtMontoTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtMontoRecibido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtMontoVuelto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnEfectuar)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnEfectuarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEfectuarActionPerformed
        if (this.montoRecibido.compareTo(this.montoTotal) >= 0) {
            this.efectuado = true;
            this.dispose();
        } else {
            JOptionPane.showMessageDialog(this, "Monto Insuficiente", "Monto", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnEfectuarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEfectuar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JTextField txtMontoRecibido;
    private javax.swing.JTextField txtMontoTotal;
    private javax.swing.JTextField txtMontoVuelto;
    // End of variables declaration//GEN-END:variables

    private void calcularMontoVuelto(int position) {
        try {
            if (this.montoRecibidoString.length() >= 1) {
                if (this.montoRecibidoString.charAt(this.montoRecibidoString.length() - 1) != '.') {
                    this.montoRecibido = new BigDecimal(this.montoRecibidoString);
                } else if (this.montoRecibidoString.length() == 1) {
                    this.montoRecibido = BigDecimal.ZERO;
                } else {
                    String montoRecibidoTempString = this.montoRecibidoString.substring(0, this.montoRecibidoString.length() - 1);
                    this.montoRecibido = new BigDecimal(montoRecibidoTempString);
                }

                BigDecimal montoVuelto = this.montoRecibido.subtract(this.montoTotal);

                if (montoVuelto.compareTo(BigDecimal.ZERO) >= 0) {
                    this.txtMontoVuelto.setText(montoVuelto.toPlainString());
                } else {
                    this.txtMontoVuelto.setText(BigDecimal.ZERO.toPlainString());
                }
            } else {
                this.txtMontoVuelto.setText(BigDecimal.ZERO.toPlainString());
            }
        } catch (Exception e) {
            montoRecibidoString = StringUtil.deleteIndex(montoRecibidoString, position);
        }
    }
}
