/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.process.CuentasProcessLogic;
import com.smartech.sistemacooperativa.bll.process.TransferenciasProcessLogic;
import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Empleado;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.util.interfaces.IObservable;
import com.smartech.sistemacooperativa.util.interfaces.IObserver;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Smartech
 */
public class DlgRealizarTransferenciaCuenta extends javax.swing.JDialog implements IObservable {

    List<IObserver> lstObservers = new ArrayList<>();

//    private boolean confirmaCuentaOrigen = false;
//    private boolean confirmaCuentaDestino = false;
    private Cuenta objCuentaOrigen = null;
    private Cuenta objCuentaDestino = null;
//    private StringBuilder mensaje;

    private Map<Socio, Boolean> mapSocios = new HashMap<>();

    public DlgRealizarTransferenciaCuenta(java.awt.Frame parent, boolean modal, Cuenta objCuentaOrigen, Cuenta objCuentaDestino) {
        super(parent, modal);
        initComponents();

//        mensaje = new StringBuilder();
        setTitle("Transferencia intercuentas");

        SwingUtil.setAccountNumberInput(this.txtCuentaOrigen, this.txtCuentaDestino);
        SwingUtil.setPositiveNumericInput(this.txtMontoTransferencia, this.txtMontoTransferido);

        if (objCuentaOrigen == null && objCuentaDestino == null) {
            this.txtCuentaDestino.setEditable(false);
            this.btnConfirmarDestino.setEnabled(false);

            this.txtMontoTransferencia.setEditable(false);
            this.txtMontoTransferido.setEditable(false);

            this.lblMonedaOrigen.setText("No especificado");
            this.lblMonedaDestino.setText("No especificado");

            this.btnConfirmarTransferencia.setEnabled(false);
            this.btnDesaprobar.setEnabled(false);
            this.btnVerificarSocio.setEnabled(false);
            this.txtaDescipcionDestino.setEditable(false);
        } else {
            this.objCuentaOrigen = objCuentaOrigen;
            this.objCuentaDestino = objCuentaDestino;
            this.updateTblSocios();
            this.updateInterfaz();
        }
        pack();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtCuentaOrigen = new javax.swing.JTextField();
        btnConfirmarOrigen = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txtCuentaDestino = new javax.swing.JTextField();
        btnConfirmarDestino = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        txtMontoTransferencia = new javax.swing.JTextField();
        btnConfirmarTransferencia = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        lblMonedaOrigen = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtMontoTransferido = new javax.swing.JTextField();
        lblMonedaDestino = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblSocios = new javax.swing.JTable();
        btnDesaprobar = new javax.swing.JButton();
        btnVerificarSocio = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtaDescipcionDestino = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("Código de cuenta origen:");

        btnConfirmarOrigen.setText("Confirmar origen");
        btnConfirmarOrigen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmarOrigenActionPerformed(evt);
            }
        });

        jLabel2.setText("Código de cuenta destino:");

        btnConfirmarDestino.setText("Confirmar destino");
        btnConfirmarDestino.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmarDestinoActionPerformed(evt);
            }
        });

        jLabel3.setText("Monto de transferencia:");

        txtMontoTransferencia.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtMontoTransferenciaKeyReleased(evt);
            }
        });

        btnConfirmarTransferencia.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_checked.png"))); // NOI18N
        btnConfirmarTransferencia.setText("Confirmar transferencia");
        btnConfirmarTransferencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmarTransferenciaActionPerformed(evt);
            }
        });

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_cancel.png"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        lblMonedaOrigen.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblMonedaOrigen.setText("PEN");

        jLabel5.setText("Monto transferido:");

        lblMonedaDestino.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblMonedaDestino.setText("PEN");

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Descripción de cuenta de origen"));

        tblSocios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(tblSocios);

        btnDesaprobar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_cancel.png"))); // NOI18N
        btnDesaprobar.setText("Desaprobar");
        btnDesaprobar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDesaprobarActionPerformed(evt);
            }
        });

        btnVerificarSocio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_checked.png"))); // NOI18N
        btnVerificarSocio.setText("Verificar");
        btnVerificarSocio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerificarSocioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(btnDesaprobar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnVerificarSocio))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 119, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnDesaprobar)
                    .addComponent(btnVerificarSocio)))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Descripción de cuenta de destino"));

        txtaDescipcionDestino.setColumns(20);
        txtaDescipcionDestino.setRows(5);
        jScrollPane2.setViewportView(txtaDescipcionDestino);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(btnCancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnConfirmarTransferencia))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(5, 5, 5)
                                .addComponent(txtCuentaOrigen, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtCuentaDestino)
                                    .addComponent(txtMontoTransferencia)
                                    .addComponent(txtMontoTransferido))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(btnConfirmarDestino, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnConfirmarOrigen, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(lblMonedaDestino, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 43, Short.MAX_VALUE)
                                .addComponent(lblMonedaOrigen, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtCuentaOrigen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnConfirmarOrigen))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtCuentaDestino, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnConfirmarDestino))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtMontoTransferencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblMonedaOrigen))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtMontoTransferido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblMonedaDestino)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnConfirmarTransferencia)
                    .addComponent(btnCancelar))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private boolean verificarCuentas(Cuenta objCuenta) {
        if (this.objCuentaOrigen == null || this.objCuentaDestino == null) {
            return true;
        } else {
            if (this.objCuentaOrigen.getId() == this.objCuentaDestino.getId()) {
                if (objCuenta.equals(this.objCuentaOrigen)) {
                    this.objCuentaOrigen = null;
                } else {
                    this.objCuentaDestino = null;
                }
                return false;
            }
            return true;
        }
    }
    private void btnConfirmarOrigenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmarOrigenActionPerformed
        if (this.objCuentaOrigen == null) {
            this.confirmarCuentaOrigen();
        } else {
            this.cancelarCuentaOrigen();
        }
    }//GEN-LAST:event_btnConfirmarOrigenActionPerformed

    private void confirmarCuentaOrigen() {
        if (SwingUtil.hasBlankInputs(this.txtCuentaOrigen)) {
            return;
        } else if (this.txtCuentaOrigen.getText().trim().equals("") || this.txtCuentaOrigen.getText().length() < Cuenta.DIGITOS_CODIGO) {
            JOptionPane.showMessageDialog(this, "Ingrese una cuenta válida", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        this.objCuentaOrigen = QueryFacade.getInstance().getCuentaHabilitada(this.txtCuentaOrigen.getText());

        if (this.objCuentaOrigen != null) {
            TransferenciasProcessLogic.getInstance().generarSocios(this.objCuentaOrigen);
            this.mapSocios = TransferenciasProcessLogic.getInstance().getMapSocios();
            this.updateTblSocios();
        } else {
            JOptionPane.showMessageDialog(this, "No existe cuenta", "Error", JOptionPane.ERROR_MESSAGE);
        }

        this.setMontoEditable(this.objCuentaOrigen);
        this.updateInterfaz();
    }

    private void cancelarCuentaOrigen() {
        this.objCuentaOrigen = null;

        this.txtCuentaDestino.setEditable(false);
        this.btnConfirmarDestino.setEnabled(this.objCuentaDestino != null);

//        this.confirmaCuentaOrigen = false;
        this.txtMontoTransferencia.setEditable(false);
        this.btnConfirmarTransferencia.setEnabled(false);

        this.mapSocios.clear();
        this.updateTblSocios();
        this.updateInterfaz();
    }

    private void btnConfirmarDestinoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmarDestinoActionPerformed
        if (this.objCuentaDestino == null) {
            this.confirmarCuentaDestino();
        } else {
            this.cancelarCuentaDestino();
        }
    }//GEN-LAST:event_btnConfirmarDestinoActionPerformed

    private void confirmarCuentaDestino() {
        if (SwingUtil.hasBlankInputs(this.txtCuentaDestino)) {
            return;
        } else if (this.txtCuentaDestino.getText().trim().equals("") || this.txtCuentaDestino.getText().length() < Cuenta.DIGITOS_CODIGO) {
            JOptionPane.showMessageDialog(this, "Ingrese una cuenta válida", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        this.objCuentaDestino = QueryFacade.getInstance().getCuentaHabilitada(this.txtCuentaDestino.getText());

        if (this.objCuentaDestino != null) {
            if (this.objCuentaOrigen != null) {

            }
        } else {
            JOptionPane.showMessageDialog(this, "No existe cuenta", "Error", JOptionPane.ERROR_MESSAGE);
        }

        this.setMontoEditable(this.objCuentaDestino);
        this.updateInterfaz();
    }

    private void cancelarCuentaDestino() {
        this.objCuentaDestino = null;

//        this.confirmaCuentaDestino = false;
        TransferenciasProcessLogic.getInstance().cancelarAprobacionSocios();
        this.mapSocios = TransferenciasProcessLogic.getInstance().getMapSocios();
        this.updateTblSocios();
        this.updateInterfaz();
    }

    private void setMontoEditable(Cuenta objCuenta) {
        if (this.objCuentaOrigen != null && this.objCuentaDestino != null) {
            if (this.verificarCuentas(objCuenta)) {
                // Monto editable
                this.txtMontoTransferencia.setEditable(true);
                this.lblMonedaDestino.setText(this.objCuentaOrigen.getObjTipoCuenta().getObjMoneda().getAbreviatura());
                if (this.objCuentaDestino.getObjTipoCuenta().getObjMoneda().equalsTo(this.objCuentaOrigen.getObjTipoCuenta().getObjMoneda())) {
                    this.setTextMontoTransferido();
                    this.btnConfirmarTransferencia.setEnabled(true);
                } else {
                    this.btnConfirmarTransferencia.setEnabled(false);
                }
            } else {
                JOptionPane.showMessageDialog(this, "Error en las cuentas", "Error", JOptionPane.ERROR_MESSAGE);
                this.txtMontoTransferencia.setEditable(false);
                this.btnConfirmarTransferencia.setEnabled(false);
            }
        } else {
            this.txtMontoTransferencia.setEditable(false);
            SwingUtil.clear(this.txtMontoTransferencia);
        }
    }

    private void updateInterfaz() {
        if (this.objCuentaDestino == null) {
            SwingUtil.clear(this.txtCuentaDestino);
        }
        if (this.objCuentaOrigen == null) {
            SwingUtil.clear(this.txtCuentaOrigen);
        }
        this.txtCuentaDestino.setEditable(this.objCuentaDestino == null && this.objCuentaOrigen != null);
        this.txtCuentaOrigen.setEditable(this.objCuentaOrigen == null);
        this.btnConfirmarDestino.setEnabled(!(this.objCuentaDestino == null && this.objCuentaOrigen == null));
        this.btnConfirmarDestino.setText(this.objCuentaDestino == null ? "Confirmar destino" : "Cancelar destino");
        this.btnConfirmarOrigen.setText(this.objCuentaOrigen == null ? "Confirmar origen" : "Cancelar origen");
        this.btnConfirmarTransferencia.setEnabled(this.objCuentaDestino != null && this.objCuentaOrigen != null && TransferenciasProcessLogic.getInstance().verificarAprobacionSocios(this.objCuentaOrigen));
        this.lblMonedaOrigen.setText(this.objCuentaOrigen != null ? this.objCuentaOrigen.getObjTipoCuenta().getObjMoneda().getAbreviatura() : "No especificado");
        this.lblMonedaDestino.setText(this.objCuentaDestino != null ? this.objCuentaDestino.getObjTipoCuenta().getObjMoneda().getAbreviatura() : "No especificado");

        if (this.objCuentaOrigen == null && this.objCuentaDestino == null) {
            SwingUtil.clear(this.txtMontoTransferencia);
        }
        if (this.objCuentaDestino == null || this.objCuentaOrigen == null) {
            SwingUtil.clear(this.txtMontoTransferido);
        }

        this.btnDesaprobar.setEnabled(this.objCuentaOrigen != null && this.objCuentaDestino != null);
        this.btnVerificarSocio.setEnabled(this.objCuentaOrigen != null && this.objCuentaDestino != null);

        this.txtaDescipcionDestino.setText("");
        if (this.objCuentaDestino != null) {
            this.txtaDescipcionDestino.append(" Cuenta: " + this.objCuentaDestino.getCodigo());
            this.txtaDescipcionDestino.append("\t");
            this.txtaDescipcionDestino.append(" Saldo: " + this.objCuentaDestino.getSaldo());
            this.txtaDescipcionDestino.append("\n");
            this.txtaDescipcionDestino.append(" Socios: ");
            int size = this.objCuentaDestino.getLstSocios().size();
            for (int i = 0; i < size; i++) {
                this.txtaDescipcionDestino.append("\n");
                this.txtaDescipcionDestino.append((i + 1) + ".- ");
                this.txtaDescipcionDestino.append(" Nombre: ");
                this.txtaDescipcionDestino.append(this.objCuentaDestino.getLstSocios().get(i).getNombreCompleto());
                this.txtaDescipcionDestino.append(" DNI: ");
                this.txtaDescipcionDestino.append(this.objCuentaDestino.getLstSocios().get(i).getDocumentoIdentidad());
            }
        }
    }

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnConfirmarTransferenciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmarTransferenciaActionPerformed
        BigDecimal monto;
        if (SwingUtil.hasBlankInputs(this.txtMontoTransferencia)) {
            return;
        } else if (!this.txtMontoTransferencia.getText().trim().equals("")) {
            monto = new BigDecimal(this.txtMontoTransferencia.getText());
        } else {
            JOptionPane.showMessageDialog(this, "Ingrese un monto adecuado", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (this.objCuentaOrigen != null && this.objCuentaDestino != null) {
            int respuesta = JOptionPane.showConfirmDialog(this, "Confirmar Transferencia", "Transferencia", JOptionPane.WARNING_MESSAGE);

            if (respuesta == JOptionPane.YES_OPTION) {
                String resultado;

                resultado = TransferenciasProcessLogic.getInstance().registrarTransferenciaCuentas(monto, this.objCuentaOrigen, this.objCuentaDestino, PrincipalJRibbon.getInstance().getObjUsuarioLogeado(), PrincipalJRibbon.getInstance().getObjCaja(), (Empleado) PrincipalJRibbon.getInstance().getObjPersona());

                if (resultado.contains("Error")) {
                    JOptionPane.showMessageDialog(this, resultado, "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this, resultado, "Información", JOptionPane.INFORMATION_MESSAGE);
                    PrincipalJRibbon.setObservers(this);
                    this.notifyObservers();
                    this.dispose();
                }

            }
        }
    }//GEN-LAST:event_btnConfirmarTransferenciaActionPerformed

    private void btnDesaprobarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDesaprobarActionPerformed
        int rowSelected = this.tblSocios.getSelectedRow();
        if (rowSelected < 0) {
            JOptionPane.showMessageDialog(this, "Seleccciona un socio", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            TransferenciasProcessLogic.getInstance().actualizarAprobacionSocio(this.objCuentaOrigen.getLstSocios().get(rowSelected), false);
            this.mapSocios = TransferenciasProcessLogic.getInstance().getMapSocios();
            this.updateTblSocios();
            this.updateInterfaz();
        }

    }//GEN-LAST:event_btnDesaprobarActionPerformed

    private void btnVerificarSocioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerificarSocioActionPerformed
        int selectedRow = this.tblSocios.getSelectedRow();
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, "Seleccciona un socio", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            DlgIngresarTarjeta dlgIngresarTarjeta = new DlgIngresarTarjeta(PrincipalJRibbon.getInstance().getFrame(), true, true, true);
            SwingUtil.centerOnScreen(dlgIngresarTarjeta);
            dlgIngresarTarjeta.setVisible(true);

            if (dlgIngresarTarjeta.getObjTarjeta() != null) {
                if (CuentasProcessLogic.getInstance().comprobarEstadoActividadSocio(dlgIngresarTarjeta.getObjTarjeta().getObjSocio())) {
                    TransferenciasProcessLogic.getInstance().actualizarAprobacionSocio(dlgIngresarTarjeta.getObjTarjeta().getObjSocio(), true);
                } else {
                    JOptionPane.showMessageDialog(this, "Socio inactivo", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                TransferenciasProcessLogic.getInstance().actualizarAprobacionSocio(this.objCuentaOrigen.getLstSocios().get(selectedRow), false);
            }
            this.mapSocios = TransferenciasProcessLogic.getInstance().getMapSocios();
            this.updateTblSocios();
            this.updateInterfaz();
        }
    }//GEN-LAST:event_btnVerificarSocioActionPerformed

    private void txtMontoTransferenciaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMontoTransferenciaKeyReleased
        this.setTextMontoTransferido();
    }//GEN-LAST:event_txtMontoTransferenciaKeyReleased

    private void setTextMontoTransferido() {
        if (this.objCuentaOrigen != null && this.objCuentaDestino != null) {
            if (this.objCuentaOrigen.getObjTipoCuenta().getObjMoneda() != null && this.objCuentaOrigen.getObjTipoCuenta().getObjMoneda() != null) {

                String monto = this.txtMontoTransferencia.getText().trim().isEmpty() ? "0" : this.txtMontoTransferencia.getText().trim();
                this.txtMontoTransferido.setText(this.objCuentaOrigen.getObjTipoCuenta().getObjMoneda().getValorPEN(new BigDecimal(monto)).toPlainString());

            }
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnConfirmarDestino;
    private javax.swing.JButton btnConfirmarOrigen;
    private javax.swing.JButton btnConfirmarTransferencia;
    private javax.swing.JButton btnDesaprobar;
    private javax.swing.JButton btnVerificarSocio;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblMonedaDestino;
    private javax.swing.JLabel lblMonedaOrigen;
    private javax.swing.JTable tblSocios;
    private javax.swing.JTextField txtCuentaDestino;
    private javax.swing.JTextField txtCuentaOrigen;
    private javax.swing.JTextField txtMontoTransferencia;
    private javax.swing.JTextField txtMontoTransferido;
    private javax.swing.JTextArea txtaDescipcionDestino;
    // End of variables declaration//GEN-END:variables

    private void updateTblSocios() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tableModel.addColumn("Nombre de socio");
        tableModel.addColumn("DNI");
        tableModel.addColumn("Aprobado");

        for (Map.Entry<Socio, Boolean> entry : this.mapSocios.entrySet()) {
            Socio key = entry.getKey();
            Boolean value = entry.getValue();

            tableModel.addRow(new Object[]{
                key.getNombreCompleto(),
                key.getDocumentoIdentidad(),
                value ? "APROBADO" : "NO APROBADO"
            });

        }

        this.tblSocios.setModel(tableModel);

    }

    @Override
    public void addObserver(IObserver objIObserver) {
        this.lstObservers.add(objIObserver);
    }

    @Override
    public List<IObserver> getObserverList() {
        return this.lstObservers;
    }

    @Override
    public void notifyObservers() {
        for (IObserver observer : this.lstObservers) {
            observer.update(this);
        }
    }

}
