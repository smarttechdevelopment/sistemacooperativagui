package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.process.ConfiguracionGeneralProcessLogic;
import com.smartech.sistemacooperativa.dominio.Moneda;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

/**
 *
 * @author Smartech
 */
public class DlgTipoCambio extends javax.swing.JDialog {

    private List<Moneda> lstMonedas = new ArrayList<>();
    private BigDecimal ultimaTasaCambio = BigDecimal.ZERO, nuevaTasaCambio = BigDecimal.ZERO;

    public DlgTipoCambio(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();

        this.lstMonedas = QueryFacade.getInstance().getAllMonedasCambio();
        this.updateCmbMonedas();

        if (!this.lstMonedas.isEmpty()) {
            this.ultimaTasaCambio = this.lstMonedas.get(0).getValorActual();
        }

        this.txtUltimaTasaCambio.setText(this.ultimaTasaCambio.toPlainString());
        SwingUtil.setPositiveNumericInput(this.txtNuevaTasaCambio);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cmbMonedas = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        txtUltimaTasaCambio = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtNuevaTasaCambio = new javax.swing.JTextField();
        btnGuardar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Tipo de Cambio");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Tipo de Cambio"));

        jLabel1.setText("Moneda:");

        cmbMonedas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbMonedasActionPerformed(evt);
            }
        });

        jLabel2.setText("Ultima Tasa de Cambio:");

        txtUltimaTasaCambio.setEditable(false);
        txtUltimaTasaCambio.setEnabled(false);

        jLabel3.setText("Nueva Tasa de Cambio:");

        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_save.png"))); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbMonedas, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtUltimaTasaCambio)
                            .addComponent(txtNuevaTasaCambio, javax.swing.GroupLayout.DEFAULT_SIZE, 133, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnGuardar)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cmbMonedas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtUltimaTasaCambio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtNuevaTasaCambio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnGuardar)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmbMonedasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbMonedasActionPerformed
        this.ultimaTasaCambio = this.lstMonedas.get(this.cmbMonedas.getSelectedIndex()).getValorActual();
        this.txtUltimaTasaCambio.setText(this.ultimaTasaCambio.toPlainString());
    }//GEN-LAST:event_cmbMonedasActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        if (!SwingUtil.hasBlankInputs(this.txtNuevaTasaCambio)) {
            Moneda objMoneda = this.lstMonedas.get(this.cmbMonedas.getSelectedIndex());
            
            objMoneda.setValorActual(new BigDecimal(this.txtNuevaTasaCambio.getText().trim()));

            boolean result = ConfiguracionGeneralProcessLogic.getInstance().actualizarMoneda(objMoneda);

            if (result) {
                JOptionPane.showMessageDialog(this, "Tipo de Cambio actualizado correctamente.", "Tipo de Cambio", JOptionPane.INFORMATION_MESSAGE);
                this.dispose();
            } else {
                JOptionPane.showMessageDialog(this, "Error al actualizar el Tipo de Cambio.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGuardar;
    private javax.swing.JComboBox<String> cmbMonedas;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtNuevaTasaCambio;
    private javax.swing.JTextField txtUltimaTasaCambio;
    // End of variables declaration//GEN-END:variables

    private void updateCmbMonedas() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (Moneda objMoneda : this.lstMonedas) {
            comboBoxModel.addElement(objMoneda.getNombre());
        }

        this.cmbMonedas.setModel(comboBoxModel);
    }
}
