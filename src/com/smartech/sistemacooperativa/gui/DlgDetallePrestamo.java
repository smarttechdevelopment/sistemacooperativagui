package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.process.PrestamosProcessLogic;
import com.smartech.sistemacooperativa.dominio.Cuota;
import com.smartech.sistemacooperativa.dominio.Prestamo;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author Smartech
 */
public class DlgDetallePrestamo extends javax.swing.JDialog {

    private Prestamo objPrestamo = null;

    public DlgDetallePrestamo(java.awt.Frame parent, boolean modal, Prestamo objPrestamo) {
        super(parent, modal);
        initComponents();

        this.objPrestamo = objPrestamo;
        this.updateCamposPrestatario();
        this.updateTblCuotas();
        this.updateTblFiadores();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtNombrePrestatario = new javax.swing.JTextField();
        txtFechaPrestamo = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtDNIPrestatario = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtRUCPrestatario = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtMontoPrestamo = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtCuotas = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblCuotas = new javax.swing.JTable();
        btnPagar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblFiadores = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Detalle de Prestamo");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Detalle de Prestamo"));

        jLabel1.setText("Prestatario:");

        jLabel2.setText("Fecha de Prestamo:");

        txtNombrePrestatario.setEditable(false);

        txtFechaPrestamo.setEditable(false);

        jLabel3.setText("DNI:");

        txtDNIPrestatario.setEditable(false);

        jLabel4.setText("RUC:");

        txtRUCPrestatario.setEditable(false);

        jLabel5.setText("Monto:");

        txtMontoPrestamo.setEditable(false);

        jLabel6.setText("Cuotas:");

        txtCuotas.setEditable(false);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Cuotas"));

        tblCuotas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblCuotas);

        btnPagar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/money-bag.png"))); // NOI18N
        btnPagar.setText("Administrar Pago");
        btnPagar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPagarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnPagar))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnPagar))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Garantes"));

        tblFiadores.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(tblFiadores);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtFechaPrestamo, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtMontoPrestamo, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCuotas, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtNombrePrestatario, javax.swing.GroupLayout.PREFERRED_SIZE, 325, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtDNIPrestatario, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtRUCPrestatario, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 56, Short.MAX_VALUE))
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtNombrePrestatario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(txtDNIPrestatario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(txtRUCPrestatario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtFechaPrestamo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(txtMontoPrestamo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(txtCuotas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnPagarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPagarActionPerformed
        if (this.objPrestamo != null) {
            List<Cuota> lstCuotas = this.getCuotasSeleccionadas();
            if (!lstCuotas.isEmpty()) {
                DlgPagarCuota dlgPagarCuota = new DlgPagarCuota(PrincipalJRibbon.getInstance().getFrame(), true, lstCuotas, this.objPrestamo);
                SwingUtil.centerOnScreen(dlgPagarCuota);
                dlgPagarCuota.setVisible(true);
            }
            /*int selectedRow = this.tblCuotas.getSelectedRow();
            if (selectedRow > -1) {
                
                IFrmConsultarPagos frmConsultarPagos = new IFrmConsultarPagos();
                PrincipalJRibbon.setObservers(frmConsultarPagos);
                PrincipalJRibbon.setObserverToObservables(frmConsultarPagos);
                PrincipalJRibbon.getInstance().getDesktopPane().add(frmConsultarPagos);
                frmConsultarPagos.setVisible(true);
                try {
                    frmConsultarPagos.setMaximum(true);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else {
                JOptionPane.showMessageDialog(this, "Seleccione una cuota", "Prestamo", JOptionPane.ERROR_MESSAGE);
            }*/
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un Prestamo", "Prestamo", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnPagarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnPagar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblCuotas;
    private javax.swing.JTable tblFiadores;
    private javax.swing.JTextField txtCuotas;
    private javax.swing.JTextField txtDNIPrestatario;
    private javax.swing.JTextField txtFechaPrestamo;
    private javax.swing.JTextField txtMontoPrestamo;
    private javax.swing.JTextField txtNombrePrestatario;
    private javax.swing.JTextField txtRUCPrestatario;
    // End of variables declaration//GEN-END:variables

    public void updateCamposPrestatario() {
        this.txtNombrePrestatario.setText(this.objPrestamo.getObjSolicitudPrestamo().getObjSocioPrestatario().getNombreCompleto());
        this.txtDNIPrestatario.setText(this.objPrestamo.getObjSolicitudPrestamo().getObjSocioPrestatario().getDocumentoIdentidad());
        this.txtRUCPrestatario.setText(this.objPrestamo.getObjSolicitudPrestamo().getObjSocioPrestatario().getRUC() == null ? "" : this.objPrestamo.getObjSolicitudPrestamo().getObjSocioPrestatario().getRUC());
        this.txtFechaPrestamo.setText(DateUtil.getRegularDate(DateUtil.getDate(this.objPrestamo.getFechaRegistro())));
        this.txtMontoPrestamo.setText(this.objPrestamo.getObjSolicitudPrestamo().getMontoAprobado().toPlainString());
        this.txtCuotas.setText(String.valueOf(this.objPrestamo.getLstCuotas().size()));
    }

    public void updateTblFiadores() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tableModel.addColumn("Nombres y Apellidos");
        tableModel.addColumn("DNI");
        tableModel.addColumn("Sexo");
        tableModel.addColumn("Direccion");
        tableModel.addColumn("Ingreso Mensual");
        tableModel.addColumn("Saldo en Cuenta de Aportes");

        for (Socio objSocioFiador : this.objPrestamo.getObjSolicitudPrestamo().getLstSociosGarantes()) {
            tableModel.addRow(new Object[]{
                objSocioFiador.getNombreCompleto(),
                objSocioFiador.getDocumentoIdentidad(),
                objSocioFiador.isSexo() ? "Masculino" : "Femenino",
                objSocioFiador.getDireccion(),
                objSocioFiador.getIngresoMensual().toPlainString(),
                PrestamosProcessLogic.getInstance().calcularSaldoAportes(objSocioFiador)});
        }

        this.tblFiadores.setModel(tableModel);
    }

    public List<Cuota> getCuotasSeleccionadas() {
        List<Cuota> lstCuotas = new ArrayList<>();
        List<Integer> lstIndices = this.getSelectedIndexes(this.tblCuotas.getModel());

        if (verificarIndices(lstIndices)) {
            for (Integer indice : lstIndices) {
                Cuota objCuotaExistente = this.objPrestamo.getLstCuotas().get(indice);
                lstCuotas.add(new Cuota(objCuotaExistente.getId(), 
                        objCuotaExistente.getMonto(), 
                        objCuotaExistente.getNumero(), 
                        objCuotaExistente.getInteres(), 
                        objCuotaExistente.getAmortizacion(), 
                        objCuotaExistente.getMora(), 
                        objCuotaExistente.getDeudaPendiente(), 
                        objCuotaExistente.getFechaVencimiento(), 
                        objCuotaExistente.getFechaPago(), 
                        objCuotaExistente.isPagado(), 
                        objCuotaExistente.getFechaRegistro(), 
                        objCuotaExistente.isEstado(), 
                        objCuotaExistente.getObjUsuario(), 
                        objCuotaExistente.getObjPrestamo()));
            }
            boolean pagado = false;
            for (Cuota objCuota : lstCuotas) {
                if (objCuota.isPagado()) {
                    pagado = true;
                    break;
                }
            }
            if (pagado) {
                lstCuotas.clear();
                JOptionPane.showMessageDialog(this, "Cuota(s) pagada(s) seleccionada(s).", "Cuotas", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Cuotas no consecutivas.", "Cuotas", JOptionPane.ERROR_MESSAGE);
        }

        return lstCuotas;
    }

    private boolean verificarIndices(List<Integer> lstIndices) {
        if (!lstIndices.isEmpty() && lstIndices.size() > 1) {
            int firstValue = lstIndices.get(0);
            for (int i = 1; i < lstIndices.size(); i++) {
                if (lstIndices.get(i) != firstValue + 1) {
                    return false;
                }
                firstValue++;
            }

            return true;
        } else {
            return lstIndices.size() == 1;
        }
    }

    private List<Integer> getSelectedIndexes(TableModel tableModel) {
        List<Integer> listSelectedIndexes = new ArrayList<>();

        for (int i = 0; i < tableModel.getRowCount(); i++) {
            if ((Boolean) tableModel.getValueAt(i, 0)) {
                listSelectedIndexes.add(i);
            }
        }

        return listSelectedIndexes;
    }

    public void updateTblCuotas() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return column == 0;
            }

            @Override
            public Class<?> getColumnClass(int i) {
                switch (i) {
                    case 0:
                        return Boolean.class;
                    default:
                        return super.getColumnClass(i);
                }
            }
        };
        tableModel.addColumn(" ");
        tableModel.addColumn("Monto");
        tableModel.addColumn("Interes");
        tableModel.addColumn("Amortizacion");
        tableModel.addColumn("Mora");
        tableModel.addColumn("Fecha Vencimiento");
        tableModel.addColumn("Estado");
        tableModel.addColumn("Fecha de Pago");

        for (Cuota objCuota : this.objPrestamo.getLstCuotas()) {
            tableModel.addRow(new Object[]{
                false,
                objCuota.getMonto().toPlainString(),
                objCuota.getInteres().toPlainString(),
                objCuota.getAmortizacion().toPlainString(),
                objCuota.isPagado() ? objCuota.getMora().toPlainString() : objCuota.calcularInteresMoratorio(DateUtil.currentTimestamp()).toPlainString(),
                DateUtil.getRegularDate(DateUtil.getDate(objCuota.getFechaVencimiento())),
                objCuota.isPagado() ? "Pagado" : objCuota.getFechaVencimiento().before(DateUtil.currentTimestamp()) ? "Vencido" : "Por Pagar",
                objCuota.isPagado() ? DateUtil.getRegularDate(DateUtil.getDate(objCuota.getFechaPago())) : "-"
            });
        }

        this.tblCuotas.setModel(tableModel);
        this.tblCuotas.getColumnModel().getColumn(0).setMaxWidth(30);
    }
}
