/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.process.AdmisionSociosProcessLogic;
import com.smartech.sistemacooperativa.bll.process.PrestamosProcessLogic;
import com.smartech.sistemacooperativa.bll.process.RenunciaSocioProcessLogic;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.TipoDocumentoIdentidad;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.util.interfaces.IObservable;
import com.smartech.sistemacooperativa.util.interfaces.IObserver;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Smartech
 */
public class IFrmReingresoSocio extends javax.swing.JInternalFrame implements IObservable{

    List<IObserver> lstObservers  = new ArrayList<>();
    
    private Socio objSocio;
    private boolean permiteActivarSocio = false;
    private boolean comprobado = false;
    //private SolicitudRetiroSocio objSolicitudRetiroSocio;
    //private StringBuilder descripcionSocio;
    
    public IFrmReingresoSocio() {
        initComponents();
        SwingUtil.setDocumentoIdentidadInput(new TipoDocumentoIdentidad(), this.txtBuscarSocio);
        this.txtaDescripcion.setEditable(false);
        this.btnActivarSocio.setEnabled(false);
        pack();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtBuscarSocio = new javax.swing.JTextField();
        btnComprobar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtaDescripcion = new javax.swing.JTextArea();
        btnActivarSocio = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setTitle("Reingreso de socio");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Socio"));

        jLabel1.setText("Buscar:");

        btnComprobar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_checked.png"))); // NOI18N
        btnComprobar.setText("Comprobar");
        btnComprobar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnComprobarActionPerformed(evt);
            }
        });

        txtaDescripcion.setColumns(20);
        txtaDescripcion.setRows(5);
        jScrollPane1.setViewportView(txtaDescripcion);

        btnActivarSocio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_checked.png"))); // NOI18N
        btnActivarSocio.setText("Activar Socio");
        btnActivarSocio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActivarSocioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBuscarSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnComprobar)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnActivarSocio)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtBuscarSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnComprobar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnActivarSocio)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnComprobarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnComprobarActionPerformed
        if(SwingUtil.hasBlankInputs(this.txtBuscarSocio)){
            return;
        }

        if(!this.comprobado){
            this.btnComprobar.setText("Seleccionar");
            this.objSocio = QueryFacade.getInstance().getSocioByDocumentoIdentidad(this.txtBuscarSocio.getText(), false);
            if(this.objSocio != null){

                this.txtaDescripcion.append("Socio: ");
                this.txtaDescripcion.append(this.objSocio.getNombreCompleto());
                this.txtaDescripcion.append("\n");
                this.txtaDescripcion.append(this.objSocio.getObjTipoDocumentoIdentidad().getNombre() + ": ");
                this.txtaDescripcion.append(this.objSocio.getDocumentoIdentidad());
                String estadoActividad;
                estadoActividad = RenunciaSocioProcessLogic.getInstance().getEstadoActividadSocio(this.objSocio);
                this.txtaDescripcion.append("\n");
                this.txtaDescripcion.append("Estado de actividad: ");
                this.txtaDescripcion.append(estadoActividad);
                if(!this.objSocio.isActivo()){
                    if(estadoActividad.equals(Socio.ESTADO_ACTIVIDAD_ACTIVO)){
                        permiteActivarSocio = true;
                    }else if(estadoActividad.equals(Socio.ESTADO_ACTIVIDAD_PASIVO)){
                        permiteActivarSocio = false;
                        JOptionPane.showMessageDialog(this, "Necesita cancelar aportes", "Información", JOptionPane.INFORMATION_MESSAGE);
                        /* Verificar en aportes */
                        //
                        //  APORTES (Process Logic Aportes)
                        //
                    }
                }else{
                    //estadoActividad = RenunciaSocioProcessLogic.getInstance().getEstadoActividadSocio(this.objSocio);
                    JOptionPane.showMessageDialog(this, "El socio ya está activo", "Información", JOptionPane.INFORMATION_MESSAGE);
                }
                this.txtaDescripcion.append("\n");
                this.txtaDescripcion.append("Estado de Socio: ");
                this.txtaDescripcion.append(this.objSocio.isEstado() ? "ACTIVO" : "INACTIVO");
            }else{
                JOptionPane.showMessageDialog(this, "No existe socio", "Error", JOptionPane.ERROR_MESSAGE);
            }
            
        }else{
            this.btnComprobar.setText("Comprobar");
            this.objSocio = null;
            this.permiteActivarSocio = false;
            this.txtaDescripcion.setText("");
        }
        

        if(this.permiteActivarSocio){
            this.comprobado = true;
            this.btnActivarSocio.setEnabled(true);
        }else{
            this.comprobado = false;
            this.btnActivarSocio.setEnabled(false);
        }
    }//GEN-LAST:event_btnComprobarActionPerformed

    private void btnActivarSocioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActivarSocioActionPerformed
        if(permiteActivarSocio){
            int respuesta = JOptionPane.showConfirmDialog(this, "Confirmar Reingreso de socio: " + this.objSocio.getNombreCompleto() + "(" + this.objSocio.getDocumentoIdentidad() + ")", "Socio", JOptionPane.INFORMATION_MESSAGE);
            if (respuesta == JOptionPane.YES_OPTION) {
                
                if(this.objSocio != null){
                    if(!this.objSocio.isRenunciado()){
                        if(!this.objSocio.isActivo()){
                            int confirmar = JOptionPane.showConfirmDialog(this, "El socio está inactivo. ¿Desea habilitar pago de cuotas?", "Reingreso", JOptionPane.YES_NO_OPTION);
                            if (confirmar == JOptionPane.YES_OPTION) {
                                DlgRegistrarAporte dlgRegistrarAporte = new DlgRegistrarAporte(PrincipalJRibbon.getInstance().getFrame(), true, AdmisionSociosProcessLogic.getInstance().generarPagoReingreso(this.objSocio.getObjTarjeta(), PrincipalJRibbon.getInstance().getObjUsuarioLogeado()));
                                SwingUtil.centerOnScreen(dlgRegistrarAporte);
                                PrincipalJRibbon.setObservers(dlgRegistrarAporte);
                                dlgRegistrarAporte.setVisible(true);
                            }
                        }else{
                            if(PrestamosProcessLogic.getInstance().debeAportes(this.objSocio)){
                                int confirmar = JOptionPane.showConfirmDialog(this, "El socio debe aportes. ¿Desea habilitar pago de cuotas?", "Información", JOptionPane.YES_NO_OPTION);
                                if (confirmar == JOptionPane.YES_OPTION) {
                                    DlgRegistrarAporte dlgRegistrarAporte = new DlgRegistrarAporte(PrincipalJRibbon.getInstance().getFrame(), true, AdmisionSociosProcessLogic.getInstance().generarPagoReingreso(this.objSocio.getObjTarjeta(), PrincipalJRibbon.getInstance().getObjUsuarioLogeado()));
                                    SwingUtil.centerOnScreen(dlgRegistrarAporte);
                                    PrincipalJRibbon.setObservers(dlgRegistrarAporte);
                                    dlgRegistrarAporte.setVisible(true);
                                }
                            }
                        }
                    }else{
                        int confirmar = JOptionPane.showConfirmDialog(this, "El socio ha renunciado. Debe crear una solicitud de ingreso. ¿Desea continuar?", "Información", JOptionPane.YES_NO_OPTION);
                        if (confirmar == JOptionPane.YES_OPTION) {
                            IFrmRegistroSolicitudIngreso frmRegistroSolicitudIngreso = new IFrmRegistroSolicitudIngreso(PrincipalJRibbon.getInstance());

                            PrincipalJRibbon.getInstance().getDesktopPane().add(frmRegistroSolicitudIngreso);
                            frmRegistroSolicitudIngreso.setVisible(true);
                            try {
                                frmRegistroSolicitudIngreso.setMaximum(true);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                }
            }
        }else{
            JOptionPane.showMessageDialog(this, "Compruebe correctamente al socio", "Información", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_btnActivarSocioActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActivarSocio;
    private javax.swing.JButton btnComprobar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField txtBuscarSocio;
    private javax.swing.JTextArea txtaDescripcion;
    // End of variables declaration//GEN-END:variables

    @Override
    public void addObserver(IObserver objIObserver) {
        this.lstObservers.add(objIObserver);
    }

    @Override
    public List<IObserver> getObserverList() {
        return this.lstObservers;
    }

    @Override
    public void notifyObservers() {
        for(IObserver observer : this.lstObservers){
            observer.update(this);
        }
    }

    
}
