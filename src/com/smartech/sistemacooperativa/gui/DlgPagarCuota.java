package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.process.PrestamosProcessLogic;
import com.smartech.sistemacooperativa.dominio.Cuota;
import com.smartech.sistemacooperativa.dominio.HabilitacionCuotas;
import com.smartech.sistemacooperativa.dominio.Prestamo;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.gui.util.TableCellListener;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import com.smartech.sistemacooperativa.util.interfaces.IObservable;
import com.smartech.sistemacooperativa.util.interfaces.IObserver;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Smartech
 */
public class DlgPagarCuota extends javax.swing.JDialog implements IObservable{

    private List<Cuota> lstCuotas = new ArrayList<>();
    private List<IObserver> lstIObservers = new ArrayList<>();
    
    public DlgPagarCuota(java.awt.Frame parent, boolean modal, List<Cuota> lstCuotas, Prestamo objPrestamo) {
        super(parent, modal);
        initComponents();
        
        this.lstCuotas = lstCuotas;
        PrestamosProcessLogic.getInstance().updateMontoCuota(this.lstCuotas, new Date());
        SwingUtil.setPositiveNumericInput(this.txtInteres);
        this.txtMontoTotal.setText(PrestamosProcessLogic.getInstance().getMontoTotalPago(lstCuotas).toPlainString());
        this.txtInteres.setText(objPrestamo.getTem().multiply(BigDecimal.valueOf(100)).toPlainString());
        this.updateTblCuotas();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblCuotas = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        txtMontoTotal = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtInteres = new javax.swing.JTextField();
        btnActualizarInteres = new javax.swing.JButton();
        btnGenerarPago = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cuotas");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Pago de Cuotas"));

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Cuotas"));

        tblCuotas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblCuotas);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
        );

        jLabel1.setText("Monto Total:");

        txtMontoTotal.setEditable(false);

        jLabel2.setText("Interes:");

        btnActualizarInteres.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/actualizacion.png"))); // NOI18N
        btnActualizarInteres.setText("Actualizar");
        btnActualizarInteres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarInteresActionPerformed(evt);
            }
        });

        btnGenerarPago.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/change.png"))); // NOI18N
        btnGenerarPago.setText("Generar Pago");
        btnGenerarPago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerarPagoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtMontoTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtInteres, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnActualizarInteres)
                        .addGap(0, 14, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnGenerarPago)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtMontoTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(txtInteres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnActualizarInteres))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnGenerarPago)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnActualizarInteresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarInteresActionPerformed
        BigDecimal interes = new BigDecimal(this.txtInteres.getText());
        interes = interes.divide(BigDecimal.valueOf(100), 4, RoundingMode.HALF_UP);
        PrestamosProcessLogic.getInstance().actualizarInteresCuotas(Prestamo.METODO_ALEMAN, this.lstCuotas, interes);
        this.updateTblCuotas();
        this.txtMontoTotal.setText(PrestamosProcessLogic.getInstance().getMontoTotalPago(this.lstCuotas).toPlainString());
    }//GEN-LAST:event_btnActualizarInteresActionPerformed

    private void btnGenerarPagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerarPagoActionPerformed
        HabilitacionCuotas objHabilitacionCuotas = new HabilitacionCuotas(
                0,
                new BigDecimal(this.txtMontoTotal.getText()),
                DateUtil.currentTimestamp(),
                null,
                true,
                PrincipalJRibbon.getInstance().getObjUsuarioLogeado(),
                true,
                false,
                this.lstCuotas,
                new ArrayList<>());
        
        boolean result = PrestamosProcessLogic.getInstance().registrarHabilitacionCuotas(objHabilitacionCuotas);
        
        if(result){
            JOptionPane.showMessageDialog(this, "Pago generado correctamente.", "Pago", JOptionPane.INFORMATION_MESSAGE);
            this.notifyObservers();
            this.dispose();
        }else{
            JOptionPane.showMessageDialog(this, "Error al generar el pago.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnGenerarPagoActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActualizarInteres;
    private javax.swing.JButton btnGenerarPago;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblCuotas;
    private javax.swing.JTextField txtInteres;
    private javax.swing.JTextField txtMontoTotal;
    // End of variables declaration//GEN-END:variables

    private void setTblCuotasCellListener() {
        Action action = new AbstractAction(){
            @Override
            public void actionPerformed(ActionEvent e) {
                TableCellListener tableCellListener = (TableCellListener)e.getSource();
                if (tableCellListener.getColumn() == 3) {
                    BigDecimal oldValue = new BigDecimal(tableCellListener.getOldValue().toString());
                    BigDecimal newValue = new BigDecimal(tableCellListener.getNewValue().toString());
                    (lstCuotas.get(tableCellListener.getRow())).setMora(newValue);
                    (lstCuotas.get(tableCellListener.getRow())).updateMontoTotal();
                    updateTblCuotas();
                    txtMontoTotal.setText(PrestamosProcessLogic.getInstance().getMontoTotalPago(lstCuotas).toPlainString());
                }
            }
        };
        TableCellListener tableCellListener = new TableCellListener(this.tblCuotas, action);
    }
    
    public void updateTblCuotas() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return column == 0 || column == 3;
            }
        };
        tableModel.addColumn("Monto");
        tableModel.addColumn("Interes");
        tableModel.addColumn("Amortizacion");
        tableModel.addColumn("Mora");
        tableModel.addColumn("Fecha Vencimiento");
        tableModel.addColumn("Estado");

        for (Cuota objCuota : this.lstCuotas) {
            tableModel.addRow(new Object[]{
                objCuota.getMonto().toPlainString(),
                objCuota.getInteres().toPlainString(),
                objCuota.getAmortizacion().toPlainString(),
                objCuota.getMora().toPlainString(),
                DateUtil.getRegularDate(DateUtil.getDate(objCuota.getFechaVencimiento())),
                objCuota.getFechaVencimiento().before(DateUtil.currentTimestamp()) ? "Vencido" : "Por Pagar",
            });
        }

        this.tblCuotas.setModel(tableModel);
        this.setTblCuotasCellListener();
    }

    @Override
    public void addObserver(IObserver objIObserver) {
        this.lstIObservers.add(objIObserver);
    }

    @Override
    public List<IObserver> getObserverList() {
        return this.lstIObservers;
    }

    @Override
    public void notifyObservers() {
        for(IObserver objIObserver : this.lstIObservers){
            objIObserver.update(this);
        }
    }
}
