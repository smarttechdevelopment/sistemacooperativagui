package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.facade.SecurityFacade;
import com.smartech.sistemacooperativa.bll.process.CajaProcessLogic;
import com.smartech.sistemacooperativa.dominio.Caja;
import com.smartech.sistemacooperativa.dominio.Empleado;
import com.smartech.sistemacooperativa.dominio.Establecimiento;
import com.smartech.sistemacooperativa.dominio.HistoricoCaja;
import com.smartech.sistemacooperativa.dominio.Persona;
import com.smartech.sistemacooperativa.dominio.Socio;
import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

/**
 *
 * @author Smartech
 */
public class DlgSeleccionarEstablecimiento extends javax.swing.JDialog {

    private static final int COMBO_OPCION_DEFAULT = 0;

    private List<Establecimiento> lstEstablecimientos = new ArrayList<>();
    private List<Caja> lstCajas = new ArrayList<>();
    private List<Empleado> lstEmpleados = new ArrayList<>();
    private Empleado objEmpleado = null;

    public DlgSeleccionarEstablecimiento(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                parent.dispose();
            }
        });

        this.setTitle("Seleccione el Establecimiento");

        this.lstEmpleados = new ArrayList<>((Collection) PrincipalJRibbon.getInstance().getLstPersonas());
        this.updateLstEstablecimientos();
        this.updateComboBoxEstablecimientos();

        this.cmbEstablecimientos.setSelectedIndex(0);

        if (PrincipalJRibbon.getInstance().getLstUsuarios().get(0).getObjTipoUsuario().isAccesocaja()) {
            this.lblCaja.setVisible(true);
            this.cmbEstablecimientos.setSelectedIndex(COMBO_OPCION_DEFAULT);
            this.cmbCajas.setVisible(true);
            this.updateLstCajas(this.lstEstablecimientos.get(this.cmbEstablecimientos.getSelectedIndex()));
            this.updateComboBoxCajas();
            this.updateObjEmpleado();
            this.updateCaja();
        } else {
            this.lblCaja.setVisible(false);
            this.cmbCajas.setVisible(false);
            this.lblMensaje.setVisible(false);
            this.jPanel2.setVisible(false);
        }

        pack();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cmbEstablecimientos = new javax.swing.JComboBox<>();
        btnSeleccionar = new javax.swing.JButton();
        lblCaja = new javax.swing.JLabel();
        cmbCajas = new javax.swing.JComboBox<>();
        jPanel2 = new javax.swing.JPanel();
        lblMensaje = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("Establecimiento:");

        cmbEstablecimientos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbEstablecimientos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbEstablecimientosActionPerformed(evt);
            }
        });

        btnSeleccionar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_checked.png"))); // NOI18N
        btnSeleccionar.setText("Seleccionar");
        btnSeleccionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeleccionarActionPerformed(evt);
            }
        });

        lblCaja.setText("Caja:");

        cmbCajas.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lblMensaje.setText("jLabel2");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblMensaje)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(lblMensaje)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnSeleccionar))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1)
                            .addComponent(lblCaja))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbEstablecimientos, 0, 262, Short.MAX_VALUE)
                            .addComponent(cmbCajas, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cmbEstablecimientos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbCajas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCaja))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSeleccionar)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSeleccionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeleccionarActionPerformed
        Establecimiento objEstablecimiento = this.lstEstablecimientos.get(this.cmbEstablecimientos.getSelectedIndex());

        Persona objPersona = SecurityFacade.getInstance().verifyEstablecimiento(PrincipalJRibbon.getInstance().getLstPersonas(), objEstablecimiento);
        if (objPersona != null) {
            PrincipalJRibbon principalJRibbon = PrincipalJRibbon.getInstance();
            principalJRibbon.setObjPersona(objPersona);
            boolean result = false;

            if (objPersona instanceof Empleado) {
                Empleado objEmpleado = (Empleado) objPersona;
                if (objEmpleado.getObjUsuario().isActivo()) {
                    principalJRibbon.setObjUsuarioLogeado(objEmpleado.getObjUsuario());
                    if (objEmpleado.getObjUsuario().getObjTipoUsuario().isAccesocaja()) {
                        Caja objCaja = this.lstCajas.get(this.cmbCajas.getSelectedIndex());
                        result = CajaProcessLogic.getInstance().abrirCaja(objCaja, (Empleado) objPersona);
                        if (result) {
                            PrincipalJRibbon.getInstance().setObjCaja(objCaja);
                            System.out.println("estado de caja " + PrincipalJRibbon.getInstance().getObjCaja().getCodigo() + ": " + PrincipalJRibbon.getInstance().getObjCaja().isAbierto());
                        }else{
                            JOptionPane.showMessageDialog(PrincipalJRibbon.getInstance().getFrame(), "Caja ya aperturada", "Caja", JOptionPane.ERROR_MESSAGE);
                            this.updateLstCajas(objEstablecimiento);
                            this.updateComboBoxCajas();
                        }
                    }else{
                        result = true;
                    }
                } else {
                    JOptionPane.showMessageDialog(PrincipalJRibbon.getInstance().getFrame(), "El Usuario se encuentra Inactivo", "Usuario", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                Socio objSocio = (Socio) objPersona;
                if (objSocio.getObjUsuario().isActivo()) {
                    result = true;
                    principalJRibbon.setObjUsuarioLogeado(objSocio.getObjUsuario());
                } else {
                    JOptionPane.showMessageDialog(PrincipalJRibbon.getInstance().getFrame(), "El Usuario se encuentra Inactivo", "Usuario", JOptionPane.ERROR_MESSAGE);
                }
            }

            if (result) {
                PrincipalJRibbon.getInstance().setObjEstablecimiento(objEstablecimiento);
                PrincipalJRibbon.getInstance().setTxtEstablecimiento(new JLabel());
                PrincipalJRibbon.getInstance().setTxtEstablecimientoText(objEstablecimiento.getNombre());
                PrincipalJRibbon.main();
                this.dispose();
            }
        } else {
            JOptionPane.showMessageDialog(PrincipalJRibbon.getInstance().getFrame(), "Usuario no permitido en el Establecimiento", "Establecimiento", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnSeleccionarActionPerformed

    private void cmbEstablecimientosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbEstablecimientosActionPerformed
        if (this.cmbCajas.isVisible()) {
            this.updateLstCajas(this.lstEstablecimientos.get(this.cmbEstablecimientos.getSelectedIndex()));
            this.updateComboBoxCajas();
        }
        this.updateObjEmpleado();
        this.updateCaja();
    }//GEN-LAST:event_cmbEstablecimientosActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSeleccionar;
    private javax.swing.JComboBox<String> cmbCajas;
    private javax.swing.JComboBox<String> cmbEstablecimientos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblCaja;
    private javax.swing.JLabel lblMensaje;
    // End of variables declaration//GEN-END:variables

    private void updateCaja() {
        HistoricoCaja objHistoricoCaja = QueryFacade.getInstance().getLastHistoricoCaja(this.objEmpleado);
        this.lblMensaje.setHorizontalAlignment(SwingConstants.CENTER);
        if (objHistoricoCaja != null) {
            String advertencia = "";
            boolean advertir = true;
            for(Caja objCaja : this.lstCajas){
                if(objCaja.getId() == objHistoricoCaja.getObjCaja().getId()){
                    advertir = false;
                }
            }
            if(advertir){
                //JOptionPane.showMessageDialog(PrincipalJRibbon.getInstance().getFrame(), "La caja "+objHistoricoCaja.getObjCaja().getCodigo()+" no está disponible", "Advertencia", JOptionPane.WARNING_MESSAGE);
                advertencia = "No disponible.";
                this.lblMensaje.setForeground(Color.decode("#E77E7E"));
            }
            this.lblMensaje.setText("Última Caja utilizada por el Usuario: " + objHistoricoCaja.getObjCaja().getCodigo() + ". " + advertencia);
            this.cmbCajas.setSelectedItem(objHistoricoCaja.getObjCaja().getCodigo());
        } else {
            this.lblMensaje.setText("Primer ingreso a Caja en el Establecimiento");
        }
        
    }

    private void updateObjEmpleado() {
        this.objEmpleado = this.lstEmpleados.get(this.cmbEstablecimientos.getSelectedIndex());
    }

    private void updateLstEstablecimientos() {
        this.lstEstablecimientos.clear();
        for (Empleado objEmpleado : this.lstEmpleados) {
            this.lstEstablecimientos.add(objEmpleado.getObjEstablecimiento());
        }
    }

    private void updateLstCajas(Establecimiento objEstablecimiento) {
        this.lstCajas = QueryFacade.getInstance().getAllCajasCerradas(objEstablecimiento);
    }

    private void updateComboBoxEstablecimientos() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (Establecimiento objEstablecimiento : this.lstEstablecimientos) {
            comboBoxModel.addElement(objEstablecimiento.getNombre());
        }

        this.cmbEstablecimientos.setModel(comboBoxModel);
    }

    private void updateComboBoxCajas() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (Caja objCaja : this.lstCajas) {
            comboBoxModel.addElement(objCaja.getCodigo());
        }

        this.cmbCajas.setModel(comboBoxModel);
    }

}
