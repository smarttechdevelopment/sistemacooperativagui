package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.facade.ReportFacade;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.SolicitudIngreso;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.util.generics.FileUtil;
import com.smartech.sistemacooperativa.util.interfaces.IObservable;
import com.smartech.sistemacooperativa.util.interfaces.IObserver;
import com.smartech.sistemacooperativa.util.reporting.ReportUtil;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import org.pentaho.reporting.engine.classic.core.parameters.ParameterDefinitionEntry;
import org.pentaho.reporting.engine.classic.core.parameters.PlainParameter;

/**
 *
 * @author Smartech
 */
public class IFrmConsultarSocios extends javax.swing.JInternalFrame implements IObserver {

    private List<Socio> lstSocios = new ArrayList<>();
    private TableRowSorter<TableModel> trsSocios = new TableRowSorter<>();

    public IFrmConsultarSocios() {
        initComponents();

        this.update();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtBuscarSocio = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblSocios = new javax.swing.JTable();
        chkAllowInactivos = new javax.swing.JCheckBox();
        btnActualizar = new javax.swing.JButton();
        btnFichaInscripcion = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Socios");

        jLabel1.setText("Buscar:");

        txtBuscarSocio.setMinimumSize(new java.awt.Dimension(80, 20));
        txtBuscarSocio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarSocioKeyReleased(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Socios"));

        tblSocios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblSocios);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 669, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 336, Short.MAX_VALUE)
        );

        chkAllowInactivos.setText("Considerar Inactivos");
        chkAllowInactivos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkAllowInactivosActionPerformed(evt);
            }
        });

        btnActualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/actualizacion.png"))); // NOI18N
        btnActualizar.setText("Actualizar");
        btnActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarActionPerformed(evt);
            }
        });

        btnFichaInscripcion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_eye.png"))); // NOI18N
        btnFichaInscripcion.setText("Mostrar Ficha de Inscripcion");
        btnFichaInscripcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFichaInscripcionActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBuscarSocio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(chkAllowInactivos))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnFichaInscripcion)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnActualizar)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBuscarSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(chkAllowInactivos))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnActualizar)
                    .addComponent(btnFichaInscripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void chkAllowInactivosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkAllowInactivosActionPerformed
        this.update();
    }//GEN-LAST:event_chkAllowInactivosActionPerformed

    private void btnActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarActionPerformed
        int index = this.tblSocios.getSelectedRow();

        if (index != -1) {
            index = this.tblSocios.convertRowIndexToModel(index);
            Socio objSocio = this.lstSocios.get(index);
            IFrmRegistroSocio frmRegistroSocio = new IFrmRegistroSocio(objSocio);
            frmRegistroSocio.addObserver(this);
            PrincipalJRibbon.getInstance().getDesktopPane().add(frmRegistroSocio);
            frmRegistroSocio.setVisible(true);

            try {
                frmRegistroSocio.setMaximum(true);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un Socio", "Socio", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnActualizarActionPerformed

    private void txtBuscarSocioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarSocioKeyReleased
        SwingUtil.textFilter(evt, this.tblSocios, this, this.txtBuscarSocio, this.trsSocios);
    }//GEN-LAST:event_txtBuscarSocioKeyReleased

    private void btnFichaInscripcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFichaInscripcionActionPerformed
        try {
            int index = this.tblSocios.getSelectedRow();

            if (index != -1) {
                index = this.tblSocios.convertRowIndexToModel(index);
                Socio objSocio = this.lstSocios.get(index);
                SolicitudIngreso objSolicitudIngreso = QueryFacade.getInstance().getSolicitudIngresoVigente(objSocio);
                if (objSolicitudIngreso != null) {
                    final PlainParameter paramTitle = new PlainParameter("title", String.class);
                    int answer = JOptionPane.showConfirmDialog(this, "¿Desea mostrar la Ficha de inscripcion del Socio seleccionado?", "Solicitud", JOptionPane.INFORMATION_MESSAGE);
                    if (answer == JOptionPane.YES_OPTION) {

                        paramTitle.setDefaultValue("Ficha de Inscripción");

                        Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> reportData = ReportFacade.getInstance().getFichaInscripcion(objSolicitudIngreso, PrincipalJRibbon.getInstance().getObjEstablecimiento());

                        if (!reportData.isEmpty()) {
                            String extension = FileUtil.getExtension(objSolicitudIngreso.getObjSocio().getRutaFoto());
                            File outputfile = new File("image." + extension);
                            if (objSolicitudIngreso.getObjSocio().getFoto() != null) {
                                ImageIO.write(objSolicitudIngreso.getObjSocio().getFoto(), extension, outputfile);
                            }
                            ReportUtil.showReport(PrincipalJRibbon.getInstance(), new URL("file:rpt_cooperativa_inscripcion_socio.prpt"), ReportUtil.getDataOnly(reportData), ReportUtil.getParametersOnly(reportData));
                            outputfile.delete();
                        } else {
                            JOptionPane.showMessageDialog(this, "Ocurrio un error..", "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }else{
                    JOptionPane.showMessageDialog(this, "No se encuentra la Solicitud de Ingreso del Socio", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(this, "Seleccione un socio", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnFichaInscripcionActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActualizar;
    private javax.swing.JButton btnFichaInscripcion;
    private javax.swing.JCheckBox chkAllowInactivos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblSocios;
    private javax.swing.JTextField txtBuscarSocio;
    // End of variables declaration//GEN-END:variables

    private void update() {
        this.updateLstSocios();
        this.updateTblSocios();
    }

    private void updateLstSocios() {
        this.lstSocios = this.chkAllowInactivos.isSelected() ? QueryFacade.getInstance().getAllSocios(true) : QueryFacade.getInstance().getAllSociosActivos(true);
    }

    private void updateTblSocios() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tableModel.addColumn("Codigo");
        tableModel.addColumn("Nombre");
        tableModel.addColumn("DNI");
        tableModel.addColumn("RUC");

        for (Socio objSocio : this.lstSocios) {
            tableModel.addRow(new Object[]{
                objSocio.getCodigo(),
                objSocio.getNombreCompleto(),
                objSocio.getDocumentoIdentidad(),
                objSocio.getRUC()
            });
        }

        this.trsSocios.setModel(tableModel);
        this.tblSocios.setModel(tableModel);
        this.tblSocios.setRowSorter(this.trsSocios);
    }

    @Override
    public void update(IObservable objIObservable) {
        this.update();
    }
}
