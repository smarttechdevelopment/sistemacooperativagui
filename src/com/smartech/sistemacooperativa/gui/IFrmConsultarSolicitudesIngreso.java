package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.process.AdmisionSociosProcessLogic;
import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.facade.ReportFacade;
import com.smartech.sistemacooperativa.dominio.Aporte;
import com.smartech.sistemacooperativa.dominio.DetalleDocumentoSolicitud;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.SolicitudIngreso;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.util.generics.FileUtil;
import com.smartech.sistemacooperativa.util.interfaces.*;
import com.smartech.sistemacooperativa.util.reporting.ReportUtil;
import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import org.pentaho.reporting.engine.classic.core.parameters.ParameterDefinitionEntry;
import org.pentaho.reporting.engine.classic.core.parameters.PlainParameter;

/**
 *
 * @author Smartech
 */
public class IFrmConsultarSolicitudesIngreso extends javax.swing.JInternalFrame implements IObserver, IObservable {

    List<IObserver> lstObservers = new ArrayList<>();

    private PrincipalJRibbon principalJRibbon;
    private List<SolicitudIngreso> lstSolicitudesPendientesPago;
    private List<SolicitudIngreso> lstSolicitudesPendientesDocumento;
    private Socio objSocio = null;
    private SolicitudIngreso objSolicitudIngreso = null;
    private final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

    private TableRowSorter<TableModel> trsSolicitudesPendientesPago = new TableRowSorter<>();
    private TableRowSorter<TableModel> trsSolicitudesPendientesDocumento = new TableRowSorter<>();

    public IFrmConsultarSolicitudesIngreso(PrincipalJRibbon principalJRibbon) {
        initComponents();
        this.principalJRibbon = principalJRibbon;

        this.updateLstSolicitudesPendientesDocumento();
        this.updateLstSolicitudesPendientesPago();
        this.updateTblSolicitudesPendientesDocumento();
        this.updateTblSolicitudesPendientesPago();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblSolicitudesPendientesPago = new javax.swing.JTable();
        btnActualizarPendientesPago = new javax.swing.JButton();
        btnEliminarPendientesPago = new javax.swing.JButton();
        btnFichaInscripcion = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblSolicitudesPendientesDocumento = new javax.swing.JTable();
        btnActualizarPendientesDocumento = new javax.swing.JButton();
        btnEliminarPendientesDocumento = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtSocio = new javax.swing.JTextField();
        btnNuevaSolicitudIngreso = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Solicitudes de Ingreso");

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Solicitudes Pendientes"));

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Pendientes de Pago"));

        tblSolicitudesPendientesPago.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblSolicitudesPendientesPago);

        btnActualizarPendientesPago.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_checked.png"))); // NOI18N
        btnActualizarPendientesPago.setText("Habilitar Pago");
        btnActualizarPendientesPago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarPendientesPagoActionPerformed(evt);
            }
        });

        btnEliminarPendientesPago.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/basura.png"))); // NOI18N
        btnEliminarPendientesPago.setText("Eliminar");
        btnEliminarPendientesPago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarPendientesPagoActionPerformed(evt);
            }
        });

        btnFichaInscripcion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_eye.png"))); // NOI18N
        btnFichaInscripcion.setText("Mostrar Ficha de Inscripcion");
        btnFichaInscripcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFichaInscripcionActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 614, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(btnEliminarPendientesPago)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnFichaInscripcion)
                .addGap(94, 94, 94)
                .addComponent(btnActualizarPendientesPago))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnActualizarPendientesPago)
                    .addComponent(btnEliminarPendientesPago, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFichaInscripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Pendientes de Documentos"));

        tblSolicitudesPendientesDocumento.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(tblSolicitudesPendientesDocumento);

        btnActualizarPendientesDocumento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/actualizacion.png"))); // NOI18N
        btnActualizarPendientesDocumento.setText("Actualizar");
        btnActualizarPendientesDocumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarPendientesDocumentoActionPerformed(evt);
            }
        });

        btnEliminarPendientesDocumento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/basura.png"))); // NOI18N
        btnEliminarPendientesDocumento.setText("Eliminar");
        btnEliminarPendientesDocumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarPendientesDocumentoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(btnEliminarPendientesDocumento)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnActualizarPendientesDocumento))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnActualizarPendientesDocumento)
                    .addComponent(btnEliminarPendientesDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jLabel1.setText("Buscar:");

        txtSocio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSocioKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtSocio)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnNuevaSolicitudIngreso.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/nuevo.png"))); // NOI18N
        btnNuevaSolicitudIngreso.setText("Nuevo");
        btnNuevaSolicitudIngreso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevaSolicitudIngresoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnNuevaSolicitudIngreso)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(10, 10, 10)
                .addComponent(btnNuevaSolicitudIngreso)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnEliminarPendientesDocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarPendientesDocumentoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnEliminarPendientesDocumentoActionPerformed

    private void btnNuevaSolicitudIngresoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevaSolicitudIngresoActionPerformed
        IFrmRegistroSolicitudIngreso frmRegistroSolicitudIngreso = new IFrmRegistroSolicitudIngreso(this.principalJRibbon);
        frmRegistroSolicitudIngreso.addObserver(this);

        this.principalJRibbon.getDesktopPane().add(frmRegistroSolicitudIngreso);
        frmRegistroSolicitudIngreso.setVisible(true);
        try {
            frmRegistroSolicitudIngreso.setMaximum(true);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }//GEN-LAST:event_btnNuevaSolicitudIngresoActionPerformed

    private void btnActualizarPendientesDocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarPendientesDocumentoActionPerformed
        int index = this.tblSolicitudesPendientesDocumento.getSelectedRow();
        if (index != -1) {
            index = this.tblSolicitudesPendientesDocumento.convertRowIndexToModel(index);
            this.objSolicitudIngreso = this.lstSolicitudesPendientesDocumento.get(index);

            IFrmRegistroSolicitudIngreso frmRegistroSolicitudIngreso = new IFrmRegistroSolicitudIngreso(principalJRibbon, this.objSolicitudIngreso);
            frmRegistroSolicitudIngreso.addObserver(this);

            this.principalJRibbon.getDesktopPane().add(frmRegistroSolicitudIngreso);
            frmRegistroSolicitudIngreso.setVisible(true);
            try {
                frmRegistroSolicitudIngreso.setMaximum(true);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione una Solicitud ", "Solicitud", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnActualizarPendientesDocumentoActionPerformed

    private void txtSocioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSocioKeyReleased
        SwingUtil.textFilter(evt, this.tblSolicitudesPendientesPago, this, this.txtSocio, this.trsSolicitudesPendientesPago);
        SwingUtil.textFilter(evt, this.tblSolicitudesPendientesDocumento, this, this.txtSocio, this.trsSolicitudesPendientesDocumento);
    }//GEN-LAST:event_txtSocioKeyReleased

    private void btnActualizarPendientesPagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarPendientesPagoActionPerformed
        int index = this.tblSolicitudesPendientesPago.getSelectedRow();

        if (index != -1) {
            index = this.tblSolicitudesPendientesPago.convertRowIndexToModel(index);
            SolicitudIngreso objSolicitudIngresoSeleccionado = this.lstSolicitudesPendientesPago.get(index);
            if (!AdmisionSociosProcessLogic.getInstance().existePagoAporteIngreso(objSolicitudIngresoSeleccionado.getObjSocio())) {
                int answer = JOptionPane.showConfirmDialog(this, "Se creara el Pago de Inscripcion de " + Aporte.MONEDA_BASE.getSimbolo() + Aporte.PAGO_APORTE_SOLES + "\nDesea continuar?", "Pago", JOptionPane.INFORMATION_MESSAGE);

                if (answer == JOptionPane.YES_OPTION) {

                    boolean result = AdmisionSociosProcessLogic.getInstance().generarPagoInscripcion(objSolicitudIngresoSeleccionado, this.principalJRibbon.getObjUsuarioLogeado());

                    if (result) {
                        JOptionPane.showMessageDialog(this, "Pago generado con exito!", "Pago", JOptionPane.INFORMATION_MESSAGE);
                        this.notifyObservers();
                    } else {
                        JOptionPane.showMessageDialog(this, "Error al generar el pago", "Error", JOptionPane.ERROR_MESSAGE);
                    }

                }
            } else {
                JOptionPane.showMessageDialog(this, "Pago ya generado", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione una solicitud", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnActualizarPendientesPagoActionPerformed

    private void btnEliminarPendientesPagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarPendientesPagoActionPerformed
        int index = this.tblSolicitudesPendientesPago.getSelectedRow();

        if (index != -1) {
            index = this.tblSolicitudesPendientesPago.convertRowIndexToModel(index);
            SolicitudIngreso objSolicitudIngresoSeleccionado = this.lstSolicitudesPendientesPago.get(index);
            int answer = JOptionPane.showConfirmDialog(this, "Desea eliminar la Solicitud?\nSe desaprobaran los documentos presentados\nSe eliminara el pago habilitado si existiese", "Solicitud", JOptionPane.INFORMATION_MESSAGE);
            if (answer == JOptionPane.YES_OPTION) {
                boolean result = AdmisionSociosProcessLogic.getInstance().desaprobarDocumentosSolicitudIngresoPorPagar(objSolicitudIngresoSeleccionado);

                if (result) {
                    JOptionPane.showMessageDialog(this, "Solicitud eliminada con exito!", "Solicitud", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this, "Error al eliminar la solicitud", "Error", JOptionPane.ERROR_MESSAGE);
                }
                this.update();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione una solicitud", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnEliminarPendientesPagoActionPerformed

    private void btnFichaInscripcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFichaInscripcionActionPerformed
        try {
            int index = this.tblSolicitudesPendientesPago.getSelectedRow();

            if (index != -1) {
                index = this.tblSolicitudesPendientesPago.convertRowIndexToModel(index);
                SolicitudIngreso objSolicitudIngresoSeleccionado = this.lstSolicitudesPendientesPago.get(index);
                final PlainParameter paramTitle = new PlainParameter("title", String.class);
                int answer = JOptionPane.showConfirmDialog(this, "¿Desea mostrar la Ficha de inscripcion del socio de la solicitud seleccionada?", "Solicitud", JOptionPane.INFORMATION_MESSAGE);
                if (answer == JOptionPane.YES_OPTION) {

                    paramTitle.setDefaultValue("Ficha de Inscripción");

                    Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> reportData = ReportFacade.getInstance().getFichaInscripcion(objSolicitudIngresoSeleccionado, principalJRibbon.getObjEstablecimiento());

                    if (!reportData.isEmpty()) {
                        String extension = FileUtil.getExtension(objSolicitudIngresoSeleccionado.getObjSocio().getRutaFoto());
                        File outputfile = new File("image." + extension);
                        if (objSolicitudIngresoSeleccionado.getObjSocio().getFoto() != null) {
                            ImageIO.write(objSolicitudIngresoSeleccionado.getObjSocio().getFoto(), extension, outputfile);
                        }
                        ReportUtil.showReport(PrincipalJRibbon.getInstance(), new URL("file:rpt_cooperativa_inscripcion_socio.prpt"), ReportUtil.getDataOnly(reportData), ReportUtil.getParametersOnly(reportData));
                        outputfile.delete();
                    } else {
                        JOptionPane.showMessageDialog(this, "Ocurrio un error en la generacion del reporte.", "Error", JOptionPane.ERROR_MESSAGE);
                    }

                }
            } else {
                JOptionPane.showMessageDialog(this, "Seleccione una solicitud", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnFichaInscripcionActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActualizarPendientesDocumento;
    private javax.swing.JButton btnActualizarPendientesPago;
    private javax.swing.JButton btnEliminarPendientesDocumento;
    private javax.swing.JButton btnEliminarPendientesPago;
    private javax.swing.JButton btnFichaInscripcion;
    private javax.swing.JButton btnNuevaSolicitudIngreso;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable tblSolicitudesPendientesDocumento;
    private javax.swing.JTable tblSolicitudesPendientesPago;
    private javax.swing.JTextField txtSocio;
    // End of variables declaration//GEN-END:variables

    private void updateLstSolicitudesPendientesPago() {
        this.lstSolicitudesPendientesPago = QueryFacade.getInstance().getAllSolicitudesIngresoPorPagar();
    }

    private void updateLstSolicitudesPendientesDocumento() {
        this.lstSolicitudesPendientesDocumento = QueryFacade.getInstance().getAllSolicitudesIngresoPorAprobarDocumentos();
    }

    private void updateTblSolicitudesPendientesPago() {
        DefaultTableModel defaultTableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        defaultTableModel.addColumn("Persona");
        defaultTableModel.addColumn("DNI");
        defaultTableModel.addColumn("Fecha de Solicitud");
        defaultTableModel.addColumn("Vigencia");

        for (SolicitudIngreso objSolicitud : this.lstSolicitudesPendientesPago) {
            defaultTableModel.addRow(new Object[]{
                objSolicitud.getObjSocio().getNombreCompleto(),
                objSolicitud.getObjSocio().getDocumentoIdentidad(),
                this.sdf.format(new Date(objSolicitud.getFechaRegistro().getTime())),
                this.sdf.format(new Date(objSolicitud.getFechaVencimiento().getTime()))
            });
        }

        this.tblSolicitudesPendientesPago.setModel(defaultTableModel);
        this.trsSolicitudesPendientesPago.setModel(defaultTableModel);
        this.tblSolicitudesPendientesPago.setRowSorter(this.trsSolicitudesPendientesPago);
    }

    private void updateTblSolicitudesPendientesDocumento() {
        DefaultTableModel defaultTableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        defaultTableModel.addColumn("Persona");
        defaultTableModel.addColumn("DNI");
        defaultTableModel.addColumn("Fecha de Solicitud");
        defaultTableModel.addColumn("Vigencia");
        defaultTableModel.addColumn("Numero de documentos por aprobar");

        for (SolicitudIngreso objSolicitud : this.lstSolicitudesPendientesDocumento) {
            int porAprobar = 0;
            for (DetalleDocumentoSolicitud objDetalleDocumento : objSolicitud.getLstDetalleDocumentoSolicitud()) {
                if (!objDetalleDocumento.isAprobado()) {
                    porAprobar++;

                }
            }
            defaultTableModel.addRow(new Object[]{
                objSolicitud.getObjSocio().getNombreCompleto(),
                objSolicitud.getObjSocio().getDocumentoIdentidad(),
                this.sdf.format(new Date(objSolicitud.getFechaRegistro().getTime())),
                this.sdf.format(new Date(objSolicitud.getFechaVencimiento().getTime())),
                porAprobar
            });
        }

        this.tblSolicitudesPendientesDocumento.setModel(defaultTableModel);
        this.trsSolicitudesPendientesDocumento.setModel(defaultTableModel);
        this.tblSolicitudesPendientesDocumento.setRowSorter(this.trsSolicitudesPendientesDocumento);
    }

    public void update() {
        this.updateLstSolicitudesPendientesDocumento();
        this.updateLstSolicitudesPendientesPago();
        this.updateTblSolicitudesPendientesDocumento();
        this.updateTblSolicitudesPendientesPago();
    }

    @Override
    public void update(IObservable objIObservable) {
        this.update();
    }

    @Override
    public void addObserver(IObserver objIObserver) {
        this.lstObservers.add(objIObserver);
    }

    @Override
    public List<IObserver> getObserverList() {
        return this.lstObservers;
    }

    @Override
    public void notifyObservers() {
        for (IObserver observer : this.lstObservers) {
            observer.update(this);
        }
    }

}
