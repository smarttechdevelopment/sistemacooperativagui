package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.process.PrestamosProcessLogic;
import com.smartech.sistemacooperativa.dominio.Cuota;
import com.smartech.sistemacooperativa.dominio.Prestamo;
import com.smartech.sistemacooperativa.util.generics.BigDecimalUtil;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Smartech
 */
public class DlgAjustarCuotas extends javax.swing.JDialog {

    private Prestamo objPrestamo = null;
    private Prestamo objNuevoPrestamo = null;

    public Prestamo getObjNuevoPrestamo() {
        return objNuevoPrestamo;
    }

    public DlgAjustarCuotas(java.awt.Frame parent, boolean modal, Prestamo objPrestamo) {
        super(parent, modal);
        initComponents();

        this.objPrestamo = objPrestamo;
        this.objNuevoPrestamo = new Prestamo(
                this.objPrestamo.getId(),
                this.objPrestamo.getCuotas(),
                this.objPrestamo.getMonto(),
                this.objPrestamo.getTem(),
                this.objPrestamo.getTed(),
                this.objPrestamo.getFechaRegistro(),
                this.objPrestamo.getFechaModificacion(),
                this.objPrestamo.isEstado(),
                this.objPrestamo.isPagado(),
                this.objPrestamo.getObjSolicitudPrestamo(),
                this.objPrestamo.isAprobado());
        List<Cuota> lstNuevasCuotas = new ArrayList<>();
        for (Cuota objCuota : this.objPrestamo.getLstCuotas()) {
            lstNuevasCuotas.add(new Cuota(
                    objCuota.getId(),
                    objCuota.getMonto(),
                    objCuota.getNumero(),
                    objCuota.getInteres(),
                    objCuota.getAmortizacion(),
                    objCuota.getMora(),
                    objCuota.getDeudaPendiente(),
                    objCuota.getFechaVencimiento(),
                    objCuota.getFechaPago(),
                    objCuota.isPagado(),
                    objCuota.getFechaRegistro(),
                    objCuota.isEstado(),
                    objCuota.getObjUsuario(),
                    this.objNuevoPrestamo));
        }
        this.objNuevoPrestamo.setLstCuotas(lstNuevasCuotas);
        this.txtMontoTotal.setText(objPrestamo.getMonto().toPlainString());

        this.updateSpnFechaPago();
        this.updateTblPlanActual();
        this.updateTblNuevoPlan();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtMontoTotal = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblPlanActual = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblNuevoPlan = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        spnFechaPago = new javax.swing.JSpinner();
        btnGuardar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Ajustar Cuotas");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Plan de Pagos"));

        jLabel1.setText("Monto Total:");

        txtMontoTotal.setEditable(false);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Plan Actual"));

        tblPlanActual.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblPlanActual);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Nuevo Plan"));

        tblNuevoPlan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(tblNuevoPlan);

        jLabel2.setText("Fecha de Pago:");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 472, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(spnFechaPago, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(spnFechaPago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE))
        );

        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_save.png"))); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_cancel.png"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtMontoTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(btnCancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnGuardar)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtMontoTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGuardar)
                    .addComponent(btnCancelar))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        int confirmar = JOptionPane.showConfirmDialog(this, "¿Confirma ajuste de cuotas?", "Cuotas", JOptionPane.YES_NO_OPTION);
        if (confirmar == JOptionPane.YES_OPTION) {
            boolean result = PrestamosProcessLogic.getInstance().ajustarCuotas(this.objNuevoPrestamo);
            if (result) {
                JOptionPane.showMessageDialog(this, "Cuotas actualizadas correctamente", "Cuotas", JOptionPane.INFORMATION_MESSAGE);
                this.dispose();
            } else {
                JOptionPane.showMessageDialog(this, "Error actualizando cuotas", "Cuotas", JOptionPane.INFORMATION_MESSAGE);
            }
        }

    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSpinner spnFechaPago;
    private javax.swing.JTable tblNuevoPlan;
    private javax.swing.JTable tblPlanActual;
    private javax.swing.JTextField txtMontoTotal;
    // End of variables declaration//GEN-END:variables

    private void updateSpnFechaPago() {
        Calendar calendar = DateUtil.getCalendar(this.objPrestamo.getLstCuotas().get(0).getFechaVencimiento());
        SpinnerModel spinnerModel = new SpinnerNumberModel(calendar.get(Calendar.DAY_OF_MONTH),
                DateUtil.getCalendar(DateUtil.zeroMonthTime(calendar)).get(Calendar.DAY_OF_MONTH),
                DateUtil.getCalendar(DateUtil.lastMonthTime(calendar)).get(Calendar.DAY_OF_MONTH),
                1);
        this.spnFechaPago.setModel(spinnerModel);

        this.spnFechaPago.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                String dia = spnFechaPago.getValue().toString();
                int lastDay = DateUtil.getCalendar(DateUtil.lastMonthTime(objNuevoPrestamo.getLstCuotas().get(0).getFechaVencimiento() )).get(Calendar.DAY_OF_MONTH);
                System.out.println("Value: " + dia);
                if (dia.matches("[\\d]+")) {
                    if(Integer.parseInt(dia) <= 0){
                        spnFechaPago.setValue(1);
                    }else if(Integer.parseInt(dia) > lastDay){
                        spnFechaPago.setValue(lastDay);
                    }else{
                        objNuevoPrestamo.ajustarCuotasMensual(Integer.parseInt(spnFechaPago.getValue().toString()));
                        updateTblNuevoPlan();
                    }
                }
//                System.out.println(e.getSource());

            }
        });
    }

    private void updateTblPlanActual() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tableModel.addColumn("Monto");
        tableModel.addColumn("Fecha de Vencimiento");

        for (Cuota objCuota : this.objPrestamo.getLstCuotas()) {
            tableModel.addRow(new Object[]{
                BigDecimalUtil.round(objCuota.getMonto()).toPlainString(),
                DateUtil.getRegularDate(DateUtil.getDate(objCuota.getFechaVencimiento()))
            });
        }

        this.tblPlanActual.setModel(tableModel);
    }

    private void updateTblNuevoPlan() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tableModel.addColumn("Monto");
        tableModel.addColumn("Fecha de Vencimiento");

        for (Cuota objCuota : this.objNuevoPrestamo.getLstCuotas()) {
            tableModel.addRow(new Object[]{
                BigDecimalUtil.round(objCuota.getMonto()).toPlainString(),
                DateUtil.getRegularDate(DateUtil.getDate(objCuota.getFechaVencimiento()))
            });
        }

        this.tblNuevoPlan.setModel(tableModel);
    }
}
