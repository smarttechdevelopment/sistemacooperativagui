package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.process.AdmisionSociosProcessLogic;
import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.dominio.Aporte;
import com.smartech.sistemacooperativa.dominio.Aprobacion;
import com.smartech.sistemacooperativa.dominio.Departamento;
import com.smartech.sistemacooperativa.dominio.DetalleDocumentoSolicitud;
import com.smartech.sistemacooperativa.dominio.DetalleFamiliarSocio;
import com.smartech.sistemacooperativa.dominio.Distrito;
import com.smartech.sistemacooperativa.dominio.Documento;
import com.smartech.sistemacooperativa.dominio.EstadoCivil;
import com.smartech.sistemacooperativa.dominio.Familiar;
import com.smartech.sistemacooperativa.dominio.GradoInstruccion;
import com.smartech.sistemacooperativa.dominio.Huella;
import com.smartech.sistemacooperativa.dominio.Parentesco;
import com.smartech.sistemacooperativa.dominio.Provincia;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.SolicitudIngreso;
import com.smartech.sistemacooperativa.dominio.TipoDocumentoIdentidad;
import com.smartech.sistemacooperativa.dominio.TipoSolicitud;
import com.smartech.sistemacooperativa.dominio.TipoTrabajador;
import com.smartech.sistemacooperativa.gui.util.AWTUtil;
import com.smartech.sistemacooperativa.gui.util.FingerprintUtil;
import com.smartech.sistemacooperativa.gui.util.MyFingerprintRegister;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.gui.util.TableCellListener;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import com.smartech.sistemacooperativa.util.interfaces.IObservable;
import com.smartech.sistemacooperativa.util.interfaces.IObserver;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Smartech
 */
public class IFrmRegistroSolicitudIngreso extends javax.swing.JInternalFrame implements IObservable {

    private List<IObserver> lstIObservers = new ArrayList<>();
    private PrincipalJRibbon principalJRibbon;
    private SolicitudIngreso objSolicitud = null;
    private TipoSolicitud objTipoSolicitud = null;
    private List<DetalleDocumentoSolicitud> lstDetalleDocumentoSolicitud = new ArrayList<>();
    private List<DetalleFamiliarSocio> lstDetalleFamiliarSocio = new ArrayList<>();
    private List<Departamento> lstDepartamentos = new ArrayList<>();
    private List<TipoTrabajador> lstTipoTrabajador = new ArrayList<>();
    private List<Parentesco> lstParentescos = new ArrayList<>();
    private final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    private List<Huella> lstHuellas = new ArrayList<>();
    private List<EstadoCivil> lstEstadosCiviles = new ArrayList<>();
    private List<GradoInstruccion> lstGradosInstruccion = new ArrayList<>();
    private List<Aprobacion> lstAprobaciones = new ArrayList<>();
    private List<TipoDocumentoIdentidad> lstTiposDocumentoIdentidadSocio = new ArrayList<>();
    private List<TipoDocumentoIdentidad> lstTiposDocumentoIdentidadBeneficiario = new ArrayList<>();
    private Map<Documento, File> mapArchivos = new HashMap<>();
    private BufferedImage foto = null;
    private File fileFoto = null;

    public IFrmRegistroSolicitudIngreso(PrincipalJRibbon principalJRibbon) {
        initComponents();
        AWTUtil.setUpperCase();
        this.lstTiposDocumentoIdentidadSocio = QueryFacade.getInstance().getAllTiposDocumentoIdentidad();
        this.updateCmbTiposDocumentoIdentidadSocio();
        this.lstTiposDocumentoIdentidadBeneficiario = QueryFacade.getInstance().getAllTiposDocumentoIdentidad();
        this.updateCmbTiposDocumentoIdentidadBeneficiario();
        this.setTextFieldsConfiguration();
        this.principalJRibbon = principalJRibbon;

        this.txtDireccionLaboral.setEditable(false);
        this.txtCodigo.setText(AdmisionSociosProcessLogic.getInstance().generarCodigoSocio());
        this.txtCodigoSolicitud.setText(AdmisionSociosProcessLogic.getInstance().generarCodigoSolicitudIngreso());
        this.dtpFechaVencimiento.setDate(DateUtil.getDate(DateUtil.add(DateUtil.currentTimestamp(), Calendar.MONTH, 1)));

        this.btnQuitarBeneficiario.setEnabled(false);

        this.objTipoSolicitud = QueryFacade.getInstance().getTipoSolicitud(1, PrincipalJRibbon.getInstance().getObjEstablecimiento()); //1 -> Ingreso Socio
        this.lstDetalleDocumentoSolicitud = this.objTipoSolicitud.generarDetalleDocumentoSolicitud(this.principalJRibbon.getObjUsuarioLogeado());
        this.lstAprobaciones = this.objTipoSolicitud.generarAprobaciones();

        this.lstDepartamentos = QueryFacade.getInstance().getAllDepartamentos();
        this.updateCmbDepartamentos();
        this.updateCmbProvincias();
        this.updateCmbDistritos();

        this.lstEstadosCiviles = QueryFacade.getInstance().getAllEstadosCiviles();
        this.updateCmbEstadosCiviles();
        this.lstGradosInstruccion = QueryFacade.getInstance().getAllGradosInstruccion();
        this.updateCmbGradosIntruccion();

        this.lstTipoTrabajador = QueryFacade.getInstance().getAllTiposTrabajador();
        this.updateTipoTrabajador();

        this.rbgSexo.add(this.rdbFemenino);
        this.rbgSexo.add(this.rdbMasculino);

        this.rdbMasculino.setSelected(true);

        this.rbgSexoBeneficiario.add(this.rdbFemeninoBeneficiario);
        this.rbgSexoBeneficiario.add(this.rdbMasculinoBeneficiario);

        this.rdbMasculinoBeneficiario.setSelected(true);

        this.dtpFechaIngreso.setDate(new Date());
        this.dtpFechaNacimiento.setDate(new Date());
        this.dtpFechaNacimientoBeneficiario.setDate(new Date());

        this.lstParentescos = QueryFacade.getInstance().getAllParentescos();
        this.updateParentesco();

        this.updateTblDocumentos();
        this.updateTblAprobaciones();
    }

    public IFrmRegistroSolicitudIngreso(PrincipalJRibbon principalJRibbon, SolicitudIngreso objSolicitud) {
        initComponents();
        AWTUtil.setUpperCase();
        this.foto = objSolicitud.getObjSocio().getFoto();
        this.updateFoto();
        this.lstTiposDocumentoIdentidadSocio = QueryFacade.getInstance().getAllTiposDocumentoIdentidad();
        this.updateCmbTiposDocumentoIdentidadSocio();
        this.lstTiposDocumentoIdentidadBeneficiario = QueryFacade.getInstance().getAllTiposDocumentoIdentidad();
        this.updateCmbTiposDocumentoIdentidadBeneficiario();
        this.cmbTipoDocumentoIdentidadSocio.setSelectedItem(objSolicitud.getObjSocio().getObjTipoDocumentoIdentidad().getNombre());
        this.setTextFieldsConfiguration();
        this.principalJRibbon = principalJRibbon;
        this.objSolicitud = objSolicitud;
        this.objTipoSolicitud = QueryFacade.getInstance().getTipoSolicitud(1, PrincipalJRibbon.getInstance().getObjEstablecimiento()); //1 -> Ingreso Socio
        this.lstDepartamentos = QueryFacade.getInstance().getAllDepartamentos();
        this.lstTipoTrabajador = QueryFacade.getInstance().getAllTiposTrabajador();

        this.lstEstadosCiviles = QueryFacade.getInstance().getAllEstadosCiviles();
        this.lstGradosInstruccion = QueryFacade.getInstance().getAllGradosInstruccion();

        this.txtCodigo.setText(objSolicitud.getObjSocio().getCodigo());
        this.txtNombre.setText(objSolicitud.getObjSocio().getNombre());
        this.txtApellidoPaterno.setText(objSolicitud.getObjSocio().getApellidoPaterno());
        this.txtApellidoMaterno.setText(objSolicitud.getObjSocio().getApellidoMaterno());
        this.txtDocumentoIdentidadSocio.setText(objSolicitud.getObjSocio().getDocumentoIdentidad());
        this.dtpFechaNacimiento.setDate(DateUtil.getDate(objSolicitud.getObjSocio().getFechaNacimiento()));
        this.txtTelefono.setText(objSolicitud.getObjSocio().getTelefono());
        this.txtCelular.setText(objSolicitud.getObjSocio().getCelular());
        if (objSolicitud.getObjSocio().isSexo()) {
            this.rdbMasculino.setSelected(true);
        } else {
            this.rdbFemenino.setSelected(true);
        }
        this.txtDireccion.setText(objSolicitud.getObjSocio().getDireccion());

        this.updateCmbEstadosCiviles();
        this.cmbEstadoCivil.setSelectedItem(objSolicitud.getObjSocio().getObjEstadoCivil().getNombre());
        this.updateCmbGradosIntruccion();
        this.cmbGradoInstruccion.setSelectedItem(objSolicitud.getObjSocio().getObjGradoInstruccion().getNombre());

        this.updateCmbDepartamentos();
        this.cmbDepartamento.setSelectedItem(objSolicitud.getObjSocio().getObjDistrito().getObjProvincia().getObjDepartamento().getNombre());
        this.updateCmbProvincias();
        this.cmbProvincia.setSelectedItem(objSolicitud.getObjSocio().getObjDistrito().getObjProvincia().getNombre());
        this.updateCmbDistritos();
        this.cmbDistrito.setSelectedItem(objSolicitud.getObjSocio().getObjDistrito().getNombre());

        this.txtOrigen.setText(objSolicitud.getObjSocio().getOrigen());
        this.txtOcupacion.setText(objSolicitud.getObjSocio().getOcupacion());

        this.txtCodigoSolicitud.setText(objSolicitud.getCodigo());
        this.dtpFechaVencimiento.setDate(DateUtil.getDate(objSolicitud.getFechaVencimiento()));
        this.dtpFechaIngreso.setDate(DateUtil.getDate(objSolicitud.getObjSocio().getFechaIngreso()));

        this.updateTipoTrabajador();
        this.cmbTipoTrabajador.setSelectedItem(objSolicitud.getObjSocio().getObjTipoTrabajador().getNombre());
        this.txtDireccionLaboral.setEditable(!objSolicitud.getObjSocio().getDireccionLaboral().isEmpty());
        this.txtDireccionLaboral.setText(objSolicitud.getObjSocio().getDireccionLaboral());
        this.txtIngresoMensual.setText(objSolicitud.getObjSocio().getIngresoMensual().toPlainString());

        this.lstParentescos = QueryFacade.getInstance().getAllParentescos();
        this.updateParentesco();
        this.lstDetalleFamiliarSocio = objSolicitud.getObjSocio().getLstFamiliaresSocio();
        this.updateTblBeneficiarios();
        this.dtpFechaNacimientoBeneficiario.setDate(new Date());

        this.lstDetalleDocumentoSolicitud = this.objSolicitud.getLstDetalleDocumentoSolicitud();
        this.updateTblDocumentos();

        this.lstHuellas = this.objSolicitud.getObjSocio().getObjUsuario().getLstHuellas();
        this.updateTblHuellas();

        this.lstAprobaciones = this.objSolicitud.getLstAprobaciones();
        this.updateTblAprobaciones();
    }

    private void setTextFieldsConfiguration() {
        SwingUtil.setDocumentoIdentidadInput(this.lstTiposDocumentoIdentidadSocio.get(this.cmbTipoDocumentoIdentidadSocio.getSelectedIndex()), this.txtDocumentoIdentidadSocio);
        SwingUtil.setDocumentoIdentidadInput(this.lstTiposDocumentoIdentidadBeneficiario.get(this.cmbTipoDocumentoIdentidadBeneficiario.getSelectedIndex()), this.txtDocumentoIdentidadBeneficiario);
        SwingUtil.setPositiveNumericInput(this.txtIngresoMensual);
        SwingUtil.setPhoneInput(this.txtCelular, this.txtTelefono, this.txtTelefonoBeneficiario, this.txtCelularBeneficiario);
        SwingUtil.setCodigoInput(4, this.txtCodigo);
        this.lblDescripcionParentesco.setVisible(false);
        this.txtDescripcionParentesco.setVisible(false);
        this.lblMontoAporte.setText(Aporte.MONEDA_BASE.getSimbolo() + " " + Aporte.PAGO_APORTE_SOLES.toPlainString());
        this.txtCodigoSolicitud.setEditable(PrincipalJRibbon.getInstance().getObjUsuarioLogeado().getObjTipoUsuario().getNivel() > 1);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        rbgSexo = new javax.swing.ButtonGroup();
        rbgSexoBeneficiario = new javax.swing.ButtonGroup();
        jScrollPane5 = new javax.swing.JScrollPane();
        jPanel4 = new javax.swing.JPanel();
        pnlDatosPersonales = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        txtApellidoPaterno = new javax.swing.JTextField();
        txtDocumentoIdentidadSocio = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txtApellidoMaterno = new javax.swing.JTextField();
        txtOrigen = new javax.swing.JTextField();
        txtTelefono = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        txtCelular = new javax.swing.JTextField();
        dtpFechaNacimiento = new org.jdesktop.swingx.JXDatePicker();
        jLabel16 = new javax.swing.JLabel();
        txtDireccion = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        cmbDepartamento = new javax.swing.JComboBox<>();
        cmbProvincia = new javax.swing.JComboBox<>();
        cmbDistrito = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        txtOcupacion = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        rdbMasculino = new javax.swing.JRadioButton();
        rdbFemenino = new javax.swing.JRadioButton();
        jLabel35 = new javax.swing.JLabel();
        txtCodigo = new javax.swing.JTextField();
        btnRegistrarHuellas = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblHuellas = new javax.swing.JTable();
        cmbEstadoCivil = new javax.swing.JComboBox<>();
        cmbGradoInstruccion = new javax.swing.JComboBox<>();
        jLabel23 = new javax.swing.JLabel();
        cmbTipoDocumentoIdentidadSocio = new javax.swing.JComboBox<>();
        pnlBeneficiarios = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        txtApellidoPaternoBeneficiario = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        txtDocumentoIdentidadBeneficiario = new javax.swing.JTextField();
        txtApellidoMaternoBeneficiario = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        dtpFechaNacimientoBeneficiario = new org.jdesktop.swingx.JXDatePicker();
        jLabel28 = new javax.swing.JLabel();
        txtCelularBeneficiario = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        txtTelefonoBeneficiario = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        rdbMasculinoBeneficiario = new javax.swing.JRadioButton();
        rdbFemeninoBeneficiario = new javax.swing.JRadioButton();
        jLabel32 = new javax.swing.JLabel();
        txtDireccionBeneficiario = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblBeneficiarios = new javax.swing.JTable();
        txtNombreBeneficiario = new javax.swing.JTextField();
        btnAgregarBeneficiario = new javax.swing.JButton();
        btnQuitarBeneficiario = new javax.swing.JButton();
        jLabel34 = new javax.swing.JLabel();
        cmbParentesco = new javax.swing.JComboBox<>();
        lblDescripcionParentesco = new javax.swing.JLabel();
        txtDescripcionParentesco = new javax.swing.JTextField();
        btnBuscarSocio = new javax.swing.JButton();
        jLabel36 = new javax.swing.JLabel();
        cmbTipoDocumentoIdentidadBeneficiario = new javax.swing.JComboBox<>();
        pnlSolicitud = new javax.swing.JPanel();
        jLabel30 = new javax.swing.JLabel();
        txtCodigoSolicitud = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        dtpFechaVencimiento = new org.jdesktop.swingx.JXDatePicker();
        pnlActividadLaboral = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        cmbTipoTrabajador = new javax.swing.JComboBox<>();
        txtDireccionLaboral = new javax.swing.JTextField();
        txtIngresoMensual = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        pnlDocumentos = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblDocumentos = new javax.swing.JTable();
        btnAprobarDocumento = new javax.swing.JButton();
        btnDeshabilitarDocumento = new javax.swing.JButton();
        btnCargarDocumento = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblAprobaciones = new javax.swing.JTable();
        btnAprobarSolicitud = new javax.swing.JButton();
        btnDesaprobarSolicitud = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        btnCargarFoto = new javax.swing.JButton();
        scrlPnlFoto = new javax.swing.JScrollPane();
        lblFoto = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        lblMontoAporte = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        dtpFechaIngreso = new org.jdesktop.swingx.JXDatePicker();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Solicitud de Ingreso de Socio");
        setMinimumSize(new java.awt.Dimension(1075, 735));
        setPreferredSize(new java.awt.Dimension(1100, 756));

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        pnlDatosPersonales.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos Personales"));

        jLabel1.setText("Nombres:");

        jLabel2.setText("Apellido Paterno:");

        jLabel3.setText("Documento de Identidad:");

        jLabel4.setText("Fecha de nacimiento:");

        jLabel5.setText("Apellido Materno:");

        jLabel7.setText("Natural de:");

        jLabel8.setText("Grado de instrucción:");

        jLabel9.setText("Estado civil:");

        jLabel13.setText("Departamento:");

        txtTelefono.setMinimumSize(new java.awt.Dimension(100, 20));

        jLabel14.setText("Teléfono:");

        jLabel15.setText("Celular:");

        txtCelular.setMinimumSize(new java.awt.Dimension(100, 20));

        jLabel16.setText("Dirección:");

        jLabel17.setText("Provincia:");

        jLabel18.setText("Distrito:");

        cmbDepartamento.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbDepartamento.setMinimumSize(new java.awt.Dimension(100, 20));
        cmbDepartamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbDepartamentoActionPerformed(evt);
            }
        });

        cmbProvincia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbProvincia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbProvinciaActionPerformed(evt);
            }
        });

        cmbDistrito.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel6.setText("Ocupacion:");

        jLabel26.setText("Sexo:");

        rdbMasculino.setText("M");

        rdbFemenino.setText("F");

        jLabel35.setText("Código:");

        btnRegistrarHuellas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/huella_41x41.png"))); // NOI18N
        btnRegistrarHuellas.setText("Registrar Huellas");
        btnRegistrarHuellas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarHuellasActionPerformed(evt);
            }
        });

        tblHuellas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(tblHuellas);

        cmbGradoInstruccion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel23.setText("Tipo de Documento:");

        cmbTipoDocumentoIdentidadSocio.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbTipoDocumentoIdentidadSocio.setMinimumSize(new java.awt.Dimension(280, 20));
        cmbTipoDocumentoIdentidadSocio.setPreferredSize(new java.awt.Dimension(280, 20));
        cmbTipoDocumentoIdentidadSocio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTipoDocumentoIdentidadSocioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlDatosPersonalesLayout = new javax.swing.GroupLayout(pnlDatosPersonales);
        pnlDatosPersonales.setLayout(pnlDatosPersonalesLayout);
        pnlDatosPersonalesLayout.setHorizontalGroup(
            pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDatosPersonalesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlDatosPersonalesLayout.createSequentialGroup()
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbDepartamento, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel17)
                        .addGap(44, 44, 44)
                        .addComponent(cmbProvincia, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbDistrito, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(38, 38, 38))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlDatosPersonalesLayout.createSequentialGroup()
                        .addComponent(jScrollPane3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRegistrarHuellas))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlDatosPersonalesLayout.createSequentialGroup()
                        .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtDireccion)
                                    .addComponent(txtOrigen)))
                            .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                                        .addComponent(dtpFechaNacimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel14)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtTelefono, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                                        .addComponent(cmbTipoDocumentoIdentidadSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel3)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtDocumentoIdentidadSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addComponent(txtApellidoPaterno)
                                    .addComponent(txtNombre))))
                        .addGap(18, 18, 18)
                        .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel15))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(cmbEstadoCivil, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtApellidoMaterno, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlDatosPersonalesLayout.createSequentialGroup()
                                        .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(txtCodigo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlDatosPersonalesLayout.createSequentialGroup()
                                                .addComponent(txtCelular, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(rdbMasculino)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(rdbFemenino)))
                                        .addGap(0, 0, Short.MAX_VALUE))))
                            .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cmbGradoInstruccion, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtOcupacion))))
        );
        pnlDatosPersonalesLayout.setVerticalGroup(
            pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDatosPersonalesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                        .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtCodigo)
                            .addComponent(jLabel35, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(txtApellidoMaterno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(cmbEstadoCivil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel15)
                            .addComponent(txtCelular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel26, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(rdbMasculino)
                            .addComponent(rdbFemenino))
                        .addGap(58, 58, 58))
                    .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                        .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtNombre)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtApellidoPaterno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtDocumentoIdentidadSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel23)
                            .addComponent(cmbTipoDocumentoIdentidadSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(dtpFechaNacimiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel14)
                            .addComponent(txtTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel16)
                            .addComponent(txtDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8)
                            .addComponent(cmbGradoInstruccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(txtOrigen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(cmbDepartamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17)
                    .addComponent(cmbProvincia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18)
                    .addComponent(cmbDistrito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtOcupacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDatosPersonalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDatosPersonalesLayout.createSequentialGroup()
                        .addComponent(btnRegistrarHuellas)
                        .addContainerGap())
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
        );

        pnlBeneficiarios.setBorder(javax.swing.BorderFactory.createTitledBorder("Beneficiarios"));

        jLabel20.setText("Nombre:");

        jLabel21.setText("Apellido Paterno:");

        txtApellidoPaternoBeneficiario.setMinimumSize(new java.awt.Dimension(250, 20));

        jLabel24.setText("Apellido Materno:");

        jLabel25.setText("Documento de Identidad:");

        jLabel27.setText("Fecha de nacimiento:");

        jLabel28.setText("Teléfono:");

        jLabel29.setText("Celular:");

        jLabel31.setText("Sexo:");

        rdbMasculinoBeneficiario.setText("M");

        rdbFemeninoBeneficiario.setText("F");

        jLabel32.setText("Dirección:");

        tblBeneficiarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblBeneficiarios);

        txtNombreBeneficiario.setMinimumSize(new java.awt.Dimension(250, 20));

        btnAgregarBeneficiario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_add.png"))); // NOI18N
        btnAgregarBeneficiario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarBeneficiarioActionPerformed(evt);
            }
        });

        btnQuitarBeneficiario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/basura.png"))); // NOI18N
        btnQuitarBeneficiario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQuitarBeneficiarioActionPerformed(evt);
            }
        });

        jLabel34.setText("Parentesco:");

        cmbParentesco.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbParentesco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbParentescoActionPerformed(evt);
            }
        });

        lblDescripcionParentesco.setText("Descripcion:");

        txtDescripcionParentesco.setMinimumSize(new java.awt.Dimension(130, 20));

        btnBuscarSocio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_search.png"))); // NOI18N
        btnBuscarSocio.setText("Socio");
        btnBuscarSocio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarSocioActionPerformed(evt);
            }
        });

        jLabel36.setText("Tipo de Documento:");

        cmbTipoDocumentoIdentidadBeneficiario.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbTipoDocumentoIdentidadBeneficiario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTipoDocumentoIdentidadBeneficiarioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlBeneficiariosLayout = new javax.swing.GroupLayout(pnlBeneficiarios);
        pnlBeneficiarios.setLayout(pnlBeneficiariosLayout);
        pnlBeneficiariosLayout.setHorizontalGroup(
            pnlBeneficiariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBeneficiariosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlBeneficiariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlBeneficiariosLayout.createSequentialGroup()
                        .addGroup(pnlBeneficiariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel32, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel20, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(4, 4, 4)
                        .addGroup(pnlBeneficiariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlBeneficiariosLayout.createSequentialGroup()
                                .addComponent(txtDireccionBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, 707, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel29)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCelularBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel28)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtTelefonoBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlBeneficiariosLayout.createSequentialGroup()
                                .addComponent(txtNombreBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel21)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtApellidoPaternoBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel24)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtApellidoMaternoBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel31)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdbMasculinoBeneficiario)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdbFemeninoBeneficiario)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnBuscarSocio)))
                        .addGap(0, 562, Short.MAX_VALUE))
                    .addGroup(pnlBeneficiariosLayout.createSequentialGroup()
                        .addComponent(jLabel36)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbTipoDocumentoIdentidadBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel25)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDocumentoIdentidadBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel27)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dtpFechaNacimientoBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel34)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbParentesco, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lblDescripcionParentesco)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDescripcionParentesco, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlBeneficiariosLayout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlBeneficiariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnAgregarBeneficiario, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnQuitarBeneficiario, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        pnlBeneficiariosLayout.setVerticalGroup(
            pnlBeneficiariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBeneficiariosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlBeneficiariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(txtNombreBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21)
                    .addComponent(txtApellidoPaternoBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel24)
                    .addComponent(txtApellidoMaternoBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel31)
                    .addComponent(rdbMasculinoBeneficiario)
                    .addComponent(rdbFemeninoBeneficiario)
                    .addComponent(btnBuscarSocio))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlBeneficiariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel25)
                    .addComponent(txtDocumentoIdentidadBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel27)
                    .addComponent(dtpFechaNacimientoBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel34)
                    .addComponent(cmbParentesco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDescripcionParentesco)
                    .addComponent(jLabel36)
                    .addComponent(cmbTipoDocumentoIdentidadBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDescripcionParentesco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlBeneficiariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel32)
                    .addComponent(txtDireccionBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel29)
                    .addComponent(txtCelularBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel28)
                    .addComponent(txtTelefonoBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlBeneficiariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlBeneficiariosLayout.createSequentialGroup()
                        .addComponent(btnAgregarBeneficiario)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnQuitarBeneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(37, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
        );

        pnlSolicitud.setBorder(javax.swing.BorderFactory.createTitledBorder("Solicitud"));

        jLabel30.setText("Código:");

        txtCodigoSolicitud.setEditable(false);

        jLabel33.setText("Vencimiento:");

        javax.swing.GroupLayout pnlSolicitudLayout = new javax.swing.GroupLayout(pnlSolicitud);
        pnlSolicitud.setLayout(pnlSolicitudLayout);
        pnlSolicitudLayout.setHorizontalGroup(
            pnlSolicitudLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSolicitudLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlSolicitudLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlSolicitudLayout.createSequentialGroup()
                        .addComponent(jLabel30)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtCodigoSolicitud))
                    .addGroup(pnlSolicitudLayout.createSequentialGroup()
                        .addComponent(jLabel33)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dtpFechaVencimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 110, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnlSolicitudLayout.setVerticalGroup(
            pnlSolicitudLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSolicitudLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(pnlSolicitudLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel30)
                    .addComponent(txtCodigoSolicitud))
                .addGap(14, 14, 14)
                .addGroup(pnlSolicitudLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel33)
                    .addComponent(dtpFechaVencimiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pnlActividadLaboral.setBorder(javax.swing.BorderFactory.createTitledBorder("Actividad Laboral"));

        jLabel10.setText("Tipo de trabajador:");

        jLabel11.setText("Centro de Trabajo:");

        jLabel12.setText("Ingreso mensual:");

        cmbTipoTrabajador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTipoTrabajadorActionPerformed(evt);
            }
        });

        jLabel19.setText("soles.");

        javax.swing.GroupLayout pnlActividadLaboralLayout = new javax.swing.GroupLayout(pnlActividadLaboral);
        pnlActividadLaboral.setLayout(pnlActividadLaboralLayout);
        pnlActividadLaboralLayout.setHorizontalGroup(
            pnlActividadLaboralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlActividadLaboralLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlActividadLaboralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlActividadLaboralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlActividadLaboralLayout.createSequentialGroup()
                        .addComponent(txtIngresoMensual)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel19)
                        .addGap(34, 34, 34))
                    .addComponent(cmbTipoTrabajador, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtDireccionLaboral)))
        );
        pnlActividadLaboralLayout.setVerticalGroup(
            pnlActividadLaboralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlActividadLaboralLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlActividadLaboralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(cmbTipoTrabajador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlActividadLaboralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(txtDireccionLaboral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlActividadLaboralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txtIngresoMensual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19))
                .addGap(3, 3, 3))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Aprobaciones"));

        pnlDocumentos.setBorder(javax.swing.BorderFactory.createTitledBorder("Documentos"));

        tblDocumentos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(tblDocumentos);

        btnAprobarDocumento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_checked.png"))); // NOI18N
        btnAprobarDocumento.setText("Aprobar Documento");
        btnAprobarDocumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAprobarDocumentoActionPerformed(evt);
            }
        });

        btnDeshabilitarDocumento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_cancel.png"))); // NOI18N
        btnDeshabilitarDocumento.setText("Desaprobar Documento");
        btnDeshabilitarDocumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeshabilitarDocumentoActionPerformed(evt);
            }
        });

        btnCargarDocumento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_upload.png"))); // NOI18N
        btnCargarDocumento.setText("Cargar Documento");
        btnCargarDocumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCargarDocumentoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlDocumentosLayout = new javax.swing.GroupLayout(pnlDocumentos);
        pnlDocumentos.setLayout(pnlDocumentosLayout);
        pnlDocumentosLayout.setHorizontalGroup(
            pnlDocumentosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDocumentosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 446, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDocumentosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnDeshabilitarDocumento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAprobarDocumento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnCargarDocumento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        pnlDocumentosLayout.setVerticalGroup(
            pnlDocumentosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDocumentosLayout.createSequentialGroup()
                .addComponent(btnAprobarDocumento)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDeshabilitarDocumento)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCargarDocumento)
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Cargos"));

        tblAprobaciones.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane4.setViewportView(tblAprobaciones);

        btnAprobarSolicitud.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_checked.png"))); // NOI18N
        btnAprobarSolicitud.setText("Aprobar Solicitud");
        btnAprobarSolicitud.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAprobarSolicitudActionPerformed(evt);
            }
        });

        btnDesaprobarSolicitud.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_cancel.png"))); // NOI18N
        btnDesaprobarSolicitud.setText("Desaprobar Solicitud");
        btnDesaprobarSolicitud.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDesaprobarSolicitudActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 868, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnDesaprobarSolicitud, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAprobarSolicitud, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(btnAprobarSolicitud)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDesaprobarSolicitud)
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlDocumentos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pnlDocumentos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_save.png"))); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Foto"));

        btnCargarFoto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_upload.png"))); // NOI18N
        btnCargarFoto.setText("Cargar");
        btnCargarFoto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCargarFotoActionPerformed(evt);
            }
        });

        scrlPnlFoto.setAutoscrolls(true);
        scrlPnlFoto.setMaximumSize(new java.awt.Dimension(150, 150));

        lblFoto.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        scrlPnlFoto.setViewportView(lblFoto);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(scrlPnlFoto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCargarFoto))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(btnCargarFoto)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(scrlPnlFoto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jLabel22.setText("Aporte del mes de inscripción:");

        lblMontoAporte.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblMontoAporte.setText("s/. 20.00");

        jLabel37.setText("Fecha de Ingreso");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel22)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblMontoAporte))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel37)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dtpFechaIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(lblMontoAporte))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel37)
                    .addComponent(dtpFechaIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(43, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlBeneficiarios, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(pnlDatosPersonales, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(pnlActividadLaboral, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnlSolicitud, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(9, 9, 9)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnGuardar)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlDatosPersonales, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(pnlActividadLaboral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(pnlSolicitud, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlBeneficiarios, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnGuardar)
                .addContainerGap())
        );

        jScrollPane5.setViewportView(jPanel4);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 1800, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 774, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        boolean resultado = false;
        boolean juridico = false;
        String message = "";

        if (SwingUtil.hasBlankInputs(this.txtNombre, this.txtApellidoPaterno, this.txtDocumentoIdentidadSocio, this.txtApellidoMaterno, this.txtCelular, this.txtTelefono, this.txtIngresoMensual, this.txtCodigo)) {
            JOptionPane.showMessageDialog(this, "Error: Revise los campos.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (!TipoDocumentoIdentidad.existeLongitud(this.txtDocumentoIdentidadSocio.getText().trim().length(), this.lstTiposDocumentoIdentidadSocio)) {
            JOptionPane.showMessageDialog(this, "Documento de identidad de socio no es válido", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        if(this.txtCodigo.getText().trim().length() != 4){
            JOptionPane.showMessageDialog(this, "Longitud del codigo incorrecto.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (this.lstHuellas.isEmpty()) {
            int respuesta = JOptionPane.showConfirmDialog(this, "No ha registrado huellas. ¿Desea continuar?", "Información", JOptionPane.WARNING_MESSAGE);

            if (respuesta == JOptionPane.CANCEL_OPTION) {
                return;
            }
        }

        try {
            if (this.objSolicitud == null || this.objSolicitud.getId() == 0) {
                Socio objSocio = new Socio(
                        0,
                        this.foto,
                        "",
                        txtNombre.getText().trim(),
                        txtApellidoPaterno.getText().trim(),
                        txtApellidoMaterno.getText().trim(),
                        DateUtil.getTimestamp(this.dtpFechaNacimiento.getDate()),
                        txtDireccion.getText().trim(),
                        txtDocumentoIdentidadSocio.getText().trim(),
                        "",
                        txtTelefono.getText().trim(),
                        txtCelular.getText().trim(),
                        rdbMasculino.isSelected(),
                        txtOrigen.getText().trim(),
                        true,
                        DateUtil.getTimestamp(this.dtpFechaIngreso.getDate()),
                        DateUtil.currentTimestamp(),
                        DateUtil.currentTimestamp(),
                        txtCodigo.getText().trim(),
                        txtOcupacion.getText().trim(),
                        txtDireccionLaboral.getText().trim(),
                        new BigDecimal(txtIngresoMensual.getText().trim()),
                        false,
                        juridico,
                        false,
                        this.lstDepartamentos.get(this.cmbDepartamento.getSelectedIndex()).getLstProvincias().get(this.cmbProvincia.getSelectedIndex()).getLstDistritos().get(this.cmbDistrito.getSelectedIndex()),
                        null,
                        this.lstTipoTrabajador.get(this.cmbTipoTrabajador.getSelectedIndex()),
                        null,
                        this.lstGradosInstruccion.get(this.cmbGradoInstruccion.getSelectedIndex()),
                        this.lstEstadosCiviles.get(this.cmbEstadoCivil.getSelectedIndex()),
                        this.lstTiposDocumentoIdentidadSocio.get(this.cmbTipoDocumentoIdentidadSocio.getSelectedIndex()),
                        new ArrayList<>(),
                        this.lstDetalleFamiliarSocio
                );

                for (DetalleFamiliarSocio objDetalleFamiliarSocio : this.lstDetalleFamiliarSocio) {
                    objDetalleFamiliarSocio.setObjSocio(objSocio);
                }

                this.objSolicitud = new SolicitudIngreso(0,
                        this.txtCodigoSolicitud.getText().trim(),
                        this.objTipoSolicitud,
                        objSocio,
                        false, //aprobado
                        false, //pagado
                        true, //estado
                        DateUtil.getTimestamp(this.dtpFechaVencimiento.getDate()),
                        DateUtil.currentTimestamp(),
                        DateUtil.currentTimestamp(),
                        principalJRibbon.getObjUsuarioLogeado(),
                        this.lstDetalleDocumentoSolicitud,
                        this.lstAprobaciones
                );

                this.objSolicitud.updateAllDocumentosAprobados();

                for (DetalleDocumentoSolicitud objDetalleDocumentoSolicitud : this.lstDetalleDocumentoSolicitud) {
                    objDetalleDocumentoSolicitud.setObjSolicitud(this.objSolicitud);
                }

                message = AdmisionSociosProcessLogic.getInstance().registrarSolicitudIngreso(this.objSolicitud, this.lstHuellas, PrincipalJRibbon.getInstance().getObjEstablecimiento(), this.mapArchivos, this.fileFoto);

                resultado = !message.contains("Error");
            } else {
                this.objSolicitud.getObjSocio().setNombre(this.txtNombre.getText().trim());
                this.objSolicitud.getObjSocio().setApellidoPaterno(this.txtApellidoPaterno.getText().trim());
                this.objSolicitud.getObjSocio().setApellidoMaterno(this.txtApellidoMaterno.getText().trim());
                this.objSolicitud.getObjSocio().setFechaNacimiento(DateUtil.getTimestamp(this.dtpFechaNacimiento.getDate()));
                this.objSolicitud.getObjSocio().setDireccion(this.txtDireccion.getText().trim());
                this.objSolicitud.getObjSocio().setDocumentoIdentidad(this.txtDocumentoIdentidadSocio.getText().trim());
                this.objSolicitud.getObjSocio().setTelefono(this.txtTelefono.getText().trim());
                this.objSolicitud.getObjSocio().setCelular(this.txtCelular.getText().trim());
                this.objSolicitud.getObjSocio().setSexo(this.rdbMasculino.isSelected());
                this.objSolicitud.getObjSocio().setOrigen(this.txtOrigen.getText().trim());
                this.objSolicitud.getObjSocio().setObjEstadoCivil(this.lstEstadosCiviles.get(this.cmbEstadoCivil.getSelectedIndex()));
                this.objSolicitud.getObjSocio().setFechaModificacion(DateUtil.currentTimestamp());
                this.objSolicitud.getObjSocio().setCodigo(this.txtCodigo.getText().trim());
                this.objSolicitud.getObjSocio().setOcupacion(this.txtOcupacion.getText().trim());
                this.objSolicitud.getObjSocio().setObjGradoInstruccion(this.lstGradosInstruccion.get(this.cmbGradoInstruccion.getSelectedIndex()));
                this.objSolicitud.getObjSocio().setDireccionLaboral(this.txtDireccionLaboral.getText().trim());
                this.objSolicitud.getObjSocio().setIngresoMensual(new BigDecimal(this.txtIngresoMensual.getText().trim()));
                this.objSolicitud.getObjSocio().setObjDistrito(this.lstDepartamentos.get(this.cmbDepartamento.getSelectedIndex()).getLstProvincias().get(this.cmbProvincia.getSelectedIndex()).getLstDistritos().get(this.cmbDistrito.getSelectedIndex()));
                this.objSolicitud.getObjSocio().setObjTipoTrabajador(this.lstTipoTrabajador.get(this.cmbTipoTrabajador.getSelectedIndex()));
                this.objSolicitud.getObjSocio().getObjUsuario().setLstHuellas(this.lstHuellas);
                this.objSolicitud.getObjSocio().setLstDetalleFamiliarSocio(this.lstDetalleFamiliarSocio);

                this.objSolicitud.setCodigo(this.txtCodigoSolicitud.getText().trim());
                this.objSolicitud.setFechaVencimiento(DateUtil.getTimestamp(this.dtpFechaVencimiento.getDate()));
                this.objSolicitud.updateAllDocumentosAprobados();

                message = AdmisionSociosProcessLogic.getInstance().actualizarSolicitudIngreso(this.objSolicitud, PrincipalJRibbon.getInstance().getObjEstablecimiento(), this.mapArchivos, this.fileFoto);
                
                resultado = !message.contains("Error");
            }

            if (resultado) {
                JOptionPane.showMessageDialog(this, message, "Socio", JOptionPane.INFORMATION_MESSAGE);
                this.notifyObservers();
                this.dispose();
            } else {
                JOptionPane.showMessageDialog(this, message, "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void cmbDepartamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbDepartamentoActionPerformed
        this.updateCmbProvincias();
        this.updateCmbDistritos();
    }//GEN-LAST:event_cmbDepartamentoActionPerformed

    private void cmbProvinciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbProvinciaActionPerformed
        this.updateCmbDistritos();
    }//GEN-LAST:event_cmbProvinciaActionPerformed

    private void btnAgregarBeneficiarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarBeneficiarioActionPerformed
        if (SwingUtil.hasBlankInputs(this.txtNombreBeneficiario, this.txtApellidoPaternoBeneficiario, this.txtApellidoMaternoBeneficiario, this.txtDocumentoIdentidadBeneficiario, this.txtDireccionBeneficiario, this.txtTelefonoBeneficiario, this.txtCelularBeneficiario)) {
            JOptionPane.showMessageDialog(this, "Datos Incorrectos. Revise los datos ingresados", "Beneficiario", JOptionPane.ERROR_MESSAGE);
        } else {

            if (!TipoDocumentoIdentidad.existeLongitud(this.txtDocumentoIdentidadBeneficiario.getText().trim().length(), this.lstTiposDocumentoIdentidadBeneficiario)) {
                JOptionPane.showMessageDialog(this, "Documento de identidad de beneficiario no es válido", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }

            Familiar objFamiliar = new Familiar(
                    0,
                    txtNombreBeneficiario.getText(),
                    txtApellidoPaternoBeneficiario.getText(),
                    txtApellidoMaternoBeneficiario.getText(),
                    dtpFechaNacimientoBeneficiario.getDate() == null ? null : new Timestamp(dtpFechaNacimientoBeneficiario.getDate().getTime()),
                    txtDireccionBeneficiario.getText(),
                    txtDocumentoIdentidadBeneficiario.getText(),
                    "",
                    txtTelefonoBeneficiario.getText(),
                    txtCelularBeneficiario.getText(),
                    rdbMasculinoBeneficiario.isSelected(),
                    "",
                    true,
                    new Timestamp(new Date().getTime()),
                    new Timestamp(new Date().getTime()),
                    null,
                    this.lstTiposDocumentoIdentidadBeneficiario.get(this.cmbTipoDocumentoIdentidadBeneficiario.getSelectedIndex()),
                    new ArrayList<>());

            Parentesco objParentesco = this.lstParentescos.get(this.cmbParentesco.getSelectedIndex());

            DetalleFamiliarSocio objDetalleFamiliarSocio = new DetalleFamiliarSocio(0, this.txtDescripcionParentesco.getText().trim(), BigDecimal.ZERO, null, objFamiliar, objParentesco);

            lstDetalleFamiliarSocio.add(objDetalleFamiliarSocio);
            AdmisionSociosProcessLogic.getInstance().ajustarPorcentajeBeneficio(lstDetalleFamiliarSocio);
            this.updateTblBeneficiarios();
            this.btnQuitarBeneficiario.setEnabled(true);
        }
    }//GEN-LAST:event_btnAgregarBeneficiarioActionPerformed

    private void btnQuitarBeneficiarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQuitarBeneficiarioActionPerformed
        if (tblBeneficiarios.getSelectedRow() != -1) {
            int respuesta = JOptionPane.showConfirmDialog(this, "Está seguro de querer eliminar el Ingreso?", "Beneficiario", JOptionPane.WARNING_MESSAGE);

            if (respuesta == JOptionPane.YES_OPTION) {
                lstDetalleFamiliarSocio.remove(tblBeneficiarios.getSelectedRow());
                AdmisionSociosProcessLogic.getInstance().ajustarPorcentajeBeneficio(lstDetalleFamiliarSocio);
                this.updateTblBeneficiarios();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un Beneficiario", "Beneficiarios", JOptionPane.WARNING_MESSAGE);
        }

    }//GEN-LAST:event_btnQuitarBeneficiarioActionPerformed

    private void btnAprobarDocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAprobarDocumentoActionPerformed
        int index = this.tblDocumentos.getSelectedRow();
        if (index != -1) {
            this.lstDetalleDocumentoSolicitud.get(index).setAprobado(true);
            this.lstDetalleDocumentoSolicitud.get(index).setFechaEntrega(DateUtil.currentTimestamp());
            this.updateTblDocumentos();
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un Documento", "Documentos", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnAprobarDocumentoActionPerformed

    private void btnDeshabilitarDocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeshabilitarDocumentoActionPerformed
        int index = this.tblDocumentos.getSelectedRow();
        if (index != -1) {
            this.lstDetalleDocumentoSolicitud.get(index).setAprobado(false);
            this.lstDetalleDocumentoSolicitud.get(index).setFechaEntrega(null);
            this.updateTblDocumentos();
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un Documento", "Documentos", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnDeshabilitarDocumentoActionPerformed

    private void btnRegistrarHuellasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarHuellasActionPerformed
        MyFingerprintRegister myFingerprintRegister = new MyFingerprintRegister(PrincipalJRibbon.getInstance().getFrame(), true);
        SwingUtil.centerOnScreen(myFingerprintRegister);
        myFingerprintRegister.setVisible(true);

        this.lstHuellas = FingerprintUtil.getHuellas(myFingerprintRegister.getFingerPrints());

        this.updateTblHuellas();
    }//GEN-LAST:event_btnRegistrarHuellasActionPerformed

    private void btnDesaprobarSolicitudActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDesaprobarSolicitudActionPerformed
        TipoSolicitud.updateAprobacion(PrincipalJRibbon.getInstance().getObjUsuarioLogeado().getObjTipoUsuario(), this.lstAprobaciones, false);
        this.updateTblAprobaciones();
    }//GEN-LAST:event_btnDesaprobarSolicitudActionPerformed

    private void btnAprobarSolicitudActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAprobarSolicitudActionPerformed
        TipoSolicitud.updateAprobacion(PrincipalJRibbon.getInstance().getObjUsuarioLogeado().getObjTipoUsuario(), this.lstAprobaciones, true);
        this.updateTblAprobaciones();
    }//GEN-LAST:event_btnAprobarSolicitudActionPerformed

    private void cmbParentescoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbParentescoActionPerformed
        if (this.lstParentescos.get(this.cmbParentesco.getSelectedIndex()).getNombre().equalsIgnoreCase("Otros")) {
            this.lblDescripcionParentesco.setVisible(true);
            this.txtDescripcionParentesco.setVisible(true);
        } else {
            this.lblDescripcionParentesco.setVisible(false);
            this.txtDescripcionParentesco.setVisible(false);
            this.txtDescripcionParentesco.setText("");
        }
    }//GEN-LAST:event_cmbParentescoActionPerformed

    private void btnBuscarSocioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarSocioActionPerformed
        DlgBuscarSocio dlgBuscarSocio = new DlgBuscarSocio(this.principalJRibbon, true, true);
        SwingUtil.centerOnScreen(dlgBuscarSocio);
        dlgBuscarSocio.setVisible(true);

        Socio objSocio = dlgBuscarSocio.getObjSocioSeleccionado();

        if (objSocio != null) {
            this.txtNombreBeneficiario.setText(objSocio.getNombre());
            this.txtApellidoPaternoBeneficiario.setText(objSocio.getApellidoPaterno());
            this.txtApellidoMaternoBeneficiario.setText(objSocio.getApellidoMaterno());
            this.cmbTipoDocumentoIdentidadBeneficiario.setSelectedItem(objSocio.getObjTipoDocumentoIdentidad().getNombre());
            SwingUtil.setDocumentoIdentidadInput(objSocio.getObjTipoDocumentoIdentidad(), this.txtDocumentoIdentidadBeneficiario);
            this.txtDocumentoIdentidadBeneficiario.setText(objSocio.getDocumentoIdentidad());
            this.dtpFechaNacimientoBeneficiario.setDate(DateUtil.getDate(objSocio.getFechaNacimiento()));
            this.txtDireccionBeneficiario.setText(objSocio.getDireccion());
            this.txtTelefonoBeneficiario.setText(objSocio.getTelefono());
            this.txtCelularBeneficiario.setText(objSocio.getCelular());
            this.cmbParentesco.requestFocusInWindow();
        }
    }//GEN-LAST:event_btnBuscarSocioActionPerformed

    private void cmbTipoDocumentoIdentidadSocioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTipoDocumentoIdentidadSocioActionPerformed
        SwingUtil.setDocumentoIdentidadInput(this.lstTiposDocumentoIdentidadSocio.get(this.cmbTipoDocumentoIdentidadSocio.getSelectedIndex()), this.txtDocumentoIdentidadSocio);
    }//GEN-LAST:event_cmbTipoDocumentoIdentidadSocioActionPerformed

    private void cmbTipoDocumentoIdentidadBeneficiarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTipoDocumentoIdentidadBeneficiarioActionPerformed
        SwingUtil.setDocumentoIdentidadInput(this.lstTiposDocumentoIdentidadBeneficiario.get(this.cmbTipoDocumentoIdentidadBeneficiario.getSelectedIndex()), this.txtDocumentoIdentidadBeneficiario);
    }//GEN-LAST:event_cmbTipoDocumentoIdentidadBeneficiarioActionPerformed

    private void cmbTipoTrabajadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTipoTrabajadorActionPerformed
        if (this.cmbTipoTrabajador.getSelectedItem().equals("DEPENDIENTE")) {
            this.txtDireccionLaboral.setEditable(true);
        } else {
            this.txtDireccionLaboral.setEditable(false);
            SwingUtil.clear(this.txtDireccionLaboral);
        }
    }//GEN-LAST:event_cmbTipoTrabajadorActionPerformed

    private void btnCargarDocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCargarDocumentoActionPerformed
        int index = this.tblDocumentos.getSelectedRow();

        if (index != -1) {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("PDF Documents", "pdf"));
            fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Word Documents", "docx", "doc"));
            fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Images", "jpg", "png"));
            fileChooser.setAcceptAllFileFilterUsed(true);
            int result = fileChooser.showOpenDialog(this);
            if (result == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fileChooser.getSelectedFile();
                System.out.println("Selected file: " + selectedFile.getAbsolutePath());
                this.mapArchivos.put(this.lstDetalleDocumentoSolicitud.get(index).getObjDocumento(), selectedFile);
                JOptionPane.showMessageDialog(this, "Documento cargado con exito.", "Documento", JOptionPane.INFORMATION_MESSAGE);
                this.updateTblDocumentos();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un Documento", "Documentos", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnCargarDocumentoActionPerformed

    private void btnCargarFotoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCargarFotoActionPerformed
        try {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Images", "jpg", "png"));
            fileChooser.setAcceptAllFileFilterUsed(true);
            int result = fileChooser.showOpenDialog(this);
            if (result == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fileChooser.getSelectedFile();
                System.out.println("Selected file: " + selectedFile.getAbsolutePath());
                this.fileFoto = selectedFile;
                this.foto = ImageIO.read(selectedFile);
                this.updateFoto();
                JOptionPane.showMessageDialog(this, "Foto cargada con exito.", "Documento", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnCargarFotoActionPerformed

    // <editor-fold defaultstate="collapsed" desc="Variables Declaration"> 
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregarBeneficiario;
    private javax.swing.JButton btnAprobarDocumento;
    private javax.swing.JButton btnAprobarSolicitud;
    private javax.swing.JButton btnBuscarSocio;
    private javax.swing.JButton btnCargarDocumento;
    private javax.swing.JButton btnCargarFoto;
    private javax.swing.JButton btnDesaprobarSolicitud;
    private javax.swing.JButton btnDeshabilitarDocumento;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnQuitarBeneficiario;
    private javax.swing.JButton btnRegistrarHuellas;
    private javax.swing.JComboBox<String> cmbDepartamento;
    private javax.swing.JComboBox<String> cmbDistrito;
    private javax.swing.JComboBox<String> cmbEstadoCivil;
    private javax.swing.JComboBox<String> cmbGradoInstruccion;
    private javax.swing.JComboBox<String> cmbParentesco;
    private javax.swing.JComboBox<String> cmbProvincia;
    private javax.swing.JComboBox<String> cmbTipoDocumentoIdentidadBeneficiario;
    private javax.swing.JComboBox<String> cmbTipoDocumentoIdentidadSocio;
    private javax.swing.JComboBox<String> cmbTipoTrabajador;
    private org.jdesktop.swingx.JXDatePicker dtpFechaIngreso;
    private org.jdesktop.swingx.JXDatePicker dtpFechaNacimiento;
    private org.jdesktop.swingx.JXDatePicker dtpFechaNacimientoBeneficiario;
    private org.jdesktop.swingx.JXDatePicker dtpFechaVencimiento;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblDescripcionParentesco;
    private javax.swing.JLabel lblFoto;
    private javax.swing.JLabel lblMontoAporte;
    private javax.swing.JPanel pnlActividadLaboral;
    private javax.swing.JPanel pnlBeneficiarios;
    private javax.swing.JPanel pnlDatosPersonales;
    private javax.swing.JPanel pnlDocumentos;
    private javax.swing.JPanel pnlSolicitud;
    private javax.swing.ButtonGroup rbgSexo;
    private javax.swing.ButtonGroup rbgSexoBeneficiario;
    private javax.swing.JRadioButton rdbFemenino;
    private javax.swing.JRadioButton rdbFemeninoBeneficiario;
    private javax.swing.JRadioButton rdbMasculino;
    private javax.swing.JRadioButton rdbMasculinoBeneficiario;
    private javax.swing.JScrollPane scrlPnlFoto;
    private javax.swing.JTable tblAprobaciones;
    private javax.swing.JTable tblBeneficiarios;
    private javax.swing.JTable tblDocumentos;
    private javax.swing.JTable tblHuellas;
    private javax.swing.JTextField txtApellidoMaterno;
    private javax.swing.JTextField txtApellidoMaternoBeneficiario;
    private javax.swing.JTextField txtApellidoPaterno;
    private javax.swing.JTextField txtApellidoPaternoBeneficiario;
    private javax.swing.JTextField txtCelular;
    private javax.swing.JTextField txtCelularBeneficiario;
    private javax.swing.JTextField txtCodigo;
    private javax.swing.JTextField txtCodigoSolicitud;
    private javax.swing.JTextField txtDescripcionParentesco;
    private javax.swing.JTextField txtDireccion;
    private javax.swing.JTextField txtDireccionBeneficiario;
    private javax.swing.JTextField txtDireccionLaboral;
    private javax.swing.JTextField txtDocumentoIdentidadBeneficiario;
    private javax.swing.JTextField txtDocumentoIdentidadSocio;
    private javax.swing.JTextField txtIngresoMensual;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtNombreBeneficiario;
    private javax.swing.JTextField txtOcupacion;
    private javax.swing.JTextField txtOrigen;
    private javax.swing.JTextField txtTelefono;
    private javax.swing.JTextField txtTelefonoBeneficiario;
    // End of variables declaration//GEN-END:variables

    // </editor-fold>
    private void updateFoto() {
        this.lblFoto.setIcon(this.foto == null ? new ImageIcon() : new ImageIcon(this.foto));
        this.scrlPnlFoto.setPreferredSize(new Dimension(150, 150));
    }

    private void setTblBeneficiariosCellListener() {
        Action action = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TableCellListener tableCellListener = (TableCellListener) e.getSource();
                if (tableCellListener.getColumn() == 10) {
                    BigDecimal oldValue = new BigDecimal(tableCellListener.getOldValue().toString());
                    BigDecimal newValue = new BigDecimal(tableCellListener.getNewValue().toString());
                    DetalleFamiliarSocio objDetalleFamiliarSocio = lstDetalleFamiliarSocio.get(tableCellListener.getRow());
                    if (newValue.compareTo(BigDecimal.ZERO) > 0 && newValue.compareTo(BigDecimal.valueOf(100)) <= 0) {
                        objDetalleFamiliarSocio.setPorcentajeBeneficio(newValue);
                        AdmisionSociosProcessLogic.getInstance().ajustarPorcentajeBeneficio(lstDetalleFamiliarSocio, objDetalleFamiliarSocio);
                    } else {
                        JOptionPane.showMessageDialog(IFrmRegistroSolicitudIngreso.this, "Valor incorrecto", "Porcentaje", JOptionPane.ERROR_MESSAGE);
                        objDetalleFamiliarSocio.setPorcentajeBeneficio(oldValue);
                    }
                    updateTblBeneficiarios();
                }
            }
        };

        TableCellListener tableCellListener = new TableCellListener(this.tblBeneficiarios, action);
    }

    private void updateCmbDepartamentos() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (Departamento objDepartamento : this.lstDepartamentos) {
            comboBoxModel.addElement(objDepartamento.getNombre());
        }

        this.cmbDepartamento.setModel(comboBoxModel);
    }

    private void updateCmbProvincias() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        Departamento objDepartamento = this.lstDepartamentos.get(this.cmbDepartamento.getSelectedIndex());
        for (Provincia objProvincia : objDepartamento.getLstProvincias()) {
            comboBoxModel.addElement(objProvincia.getNombre());
        }

        this.cmbProvincia.setModel(comboBoxModel);
    }

    private void updateCmbDistritos() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        Provincia objProvincia = this.lstDepartamentos.get(this.cmbDepartamento.getSelectedIndex()).getLstProvincias().get(this.cmbProvincia.getSelectedIndex());
        for (Distrito objDistrito : objProvincia.getLstDistritos()) {
            comboBoxModel.addElement(objDistrito.getNombre());
        }
        this.cmbDistrito.setModel(comboBoxModel);
    }

    private void updateTipoTrabajador() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (TipoTrabajador objTipoTrabajador : this.lstTipoTrabajador) {
            comboBoxModel.addElement(objTipoTrabajador.getNombre());
        }

        this.cmbTipoTrabajador.setModel(comboBoxModel);

    }

    private void updateParentesco() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (Parentesco objParentesco : this.lstParentescos) {
            comboBoxModel.addElement(objParentesco.getNombre());
        }

        this.cmbParentesco.setModel(comboBoxModel);
    }

    private void updateCmbEstadosCiviles() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (EstadoCivil objEstadoCivil : this.lstEstadosCiviles) {
            comboBoxModel.addElement(objEstadoCivil.getNombre());
        }

        this.cmbEstadoCivil.setModel(comboBoxModel);
    }

    private void updateCmbGradosIntruccion() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (GradoInstruccion objGradoInstruccion : this.lstGradosInstruccion) {
            comboBoxModel.addElement(objGradoInstruccion.getNombre());
        }

        this.cmbGradoInstruccion.setModel(comboBoxModel);
    }

    private void updateCmbTiposDocumentoIdentidadSocio() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (TipoDocumentoIdentidad objTipoDocumentoIdentidad : this.lstTiposDocumentoIdentidadSocio) {
            comboBoxModel.addElement(objTipoDocumentoIdentidad.getNombre());
        }

        this.cmbTipoDocumentoIdentidadSocio.setModel(comboBoxModel);
    }

    private void updateCmbTiposDocumentoIdentidadBeneficiario() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (TipoDocumentoIdentidad objTipoDocumentoIdentidad : this.lstTiposDocumentoIdentidadSocio) {
            comboBoxModel.addElement(objTipoDocumentoIdentidad.getNombre());
        }

        this.cmbTipoDocumentoIdentidadBeneficiario.setModel(comboBoxModel);
    }

    private void updateTblBeneficiarios() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return column == 10;
            }
        };
        tableModel.addColumn("Nombre");
        tableModel.addColumn("Apellido Paterno");
        tableModel.addColumn("Apellido Materno");
        tableModel.addColumn("DNI");
        tableModel.addColumn("Fecha Nacimiento");
        tableModel.addColumn("Domicilio");
        tableModel.addColumn("Telefono");
        tableModel.addColumn("Celular");
        tableModel.addColumn("Sexo");
        tableModel.addColumn("Parentesco");
        tableModel.addColumn("Porcentaje Beneficio");

        for (DetalleFamiliarSocio objDetalleFamiliarSocio : this.lstDetalleFamiliarSocio) {
            tableModel.addRow(new Object[]{
                objDetalleFamiliarSocio.getObjFamiliar().getNombre(),
                objDetalleFamiliarSocio.getObjFamiliar().getApellidoPaterno(),
                objDetalleFamiliarSocio.getObjFamiliar().getApellidoMaterno(),
                objDetalleFamiliarSocio.getObjFamiliar().getDocumentoIdentidad(),
                this.sdf.format(objDetalleFamiliarSocio.getObjFamiliar().getFechaNacimiento()),
                objDetalleFamiliarSocio.getObjFamiliar().getDireccion(),
                objDetalleFamiliarSocio.getObjFamiliar().getTelefono(),
                objDetalleFamiliarSocio.getObjFamiliar().getCelular(),
                objDetalleFamiliarSocio.getObjFamiliar().isSexo() ? "M" : "F",
                objDetalleFamiliarSocio.getObjParentesco().getNombre(),
                objDetalleFamiliarSocio.getPorcentajeBeneficio().toPlainString()
            });
        }

        this.tblBeneficiarios.setModel(tableModel);
        this.setTblBeneficiariosCellListener();

        SwingUtil.clear(this.txtNombreBeneficiario, this.txtApellidoMaternoBeneficiario, this.txtApellidoPaternoBeneficiario, this.txtCelularBeneficiario, this.txtDireccionBeneficiario, this.txtDocumentoIdentidadBeneficiario, this.txtTelefonoBeneficiario);
        this.dtpFechaNacimientoBeneficiario.setDate(new Date());
        this.rdbMasculinoBeneficiario.setSelected(true);
        this.rdbFemeninoBeneficiario.setSelected(false);
        this.cmbParentesco.setSelectedIndex(0);
    }

    private void updateTblDocumentos() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tableModel.addColumn("Nombre");
        tableModel.addColumn("Aprobado");
        tableModel.addColumn("Archivo");

        for (DetalleDocumentoSolicitud objDetalleDocumentoSolicitud : this.lstDetalleDocumentoSolicitud) {
            tableModel.addRow(new Object[]{
                objDetalleDocumentoSolicitud.getObjDocumento().getNombre(),
                objDetalleDocumentoSolicitud.isAprobado() ? "Aprobado" : "No Aprobado",
                objDetalleDocumentoSolicitud.getRepresentacion() != null ? "Guardado" : this.mapArchivos.containsKey(objDetalleDocumentoSolicitud.getObjDocumento()) ? "Cargado" : "Sin Archivo"
            });
        }

        this.tblDocumentos.setModel(tableModel);
    }

    private void updateTblHuellas() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tableModel.addColumn("Mano");
        tableModel.addColumn("Dedo");

        for (Huella objHuella : this.lstHuellas) {
            tableModel.addRow(new Object[]{
                objHuella.isMano() ? "Derecha" : "Izquierda",
                objHuella.getDedo()
            });
        }

        this.tblHuellas.setModel(tableModel);
    }

    private void updateTblAprobaciones() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tableModel.addColumn("Usuario");
        tableModel.addColumn("Estado");

        for (Aprobacion objAprobacion : this.lstAprobaciones) {
            tableModel.addRow(new Object[]{
                objAprobacion.getObjTipoUsuario().getNombre(),
                objAprobacion.isAprobado() ? "Aprobado" : "No Aprobado"
            });
        }

        this.tblAprobaciones.setModel(tableModel);
    }

    @Override
    public void addObserver(IObserver objIObserver) {
        this.lstIObservers.add(objIObserver);
    }

    @Override
    public List<IObserver> getObserverList() {
        return this.lstIObservers;
    }

    @Override
    public void notifyObservers() {
        for (IObserver objIObserver : this.lstIObservers) {
            objIObserver.update(this);
        }
    }
}
