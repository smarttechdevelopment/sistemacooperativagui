package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.process.CajaProcessLogic;
import com.smartech.sistemacooperativa.dominio.MovimientoAdministrativo;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import com.smartech.sistemacooperativa.util.interfaces.IObservable;
import com.smartech.sistemacooperativa.util.interfaces.IObserver;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Smartech
 */
public class IFrmConsultarMovimientosAdministrativos extends javax.swing.JInternalFrame implements IObserver {

    private List<MovimientoAdministrativo> lstMovimientosAdministrativos = new ArrayList<>();
    private TableRowSorter<TableModel> trsMovimientos = new TableRowSorter<>();

    public IFrmConsultarMovimientosAdministrativos() {
        initComponents();

        this.bgpPagos.add(this.rdbEfectuados);
        this.bgpPagos.add(this.rdbHabilitados);
        this.rdbHabilitados.setSelected(true);
        
        this.bgpTipo.add(this.rdbIngreso);
        this.bgpTipo.add(this.rdbEgreso);
        this.rdbIngreso.setSelected(true);

        this.dtpSinceDate.setDate(new Date());
        this.dtpToDate.setDate(new Date());
        
        this.update();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgpPagos = new javax.swing.ButtonGroup();
        bgpTipo = new javax.swing.ButtonGroup();
        jLabel3 = new javax.swing.JLabel();
        dtpSinceDate = new org.jdesktop.swingx.JXDatePicker();
        jLabel4 = new javax.swing.JLabel();
        dtpToDate = new org.jdesktop.swingx.JXDatePicker();
        rdbEfectuados = new javax.swing.JRadioButton();
        rdbHabilitados = new javax.swing.JRadioButton();
        btnActualizar = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txtEmpleado = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblMovimientos = new javax.swing.JTable();
        btnEliminar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        rdbIngreso = new javax.swing.JRadioButton();
        rdbEgreso = new javax.swing.JRadioButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Movimientos Administrativos");

        jLabel3.setText("Desde:");

        dtpSinceDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dtpSinceDateActionPerformed(evt);
            }
        });

        jLabel4.setText("Hasta:");

        rdbEfectuados.setText("Efectuados");
        rdbEfectuados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbEfectuadosActionPerformed(evt);
            }
        });

        rdbHabilitados.setText("Habilitados");
        rdbHabilitados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbHabilitadosActionPerformed(evt);
            }
        });

        btnActualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/actualizacion.png"))); // NOI18N
        btnActualizar.setText("Actualizar");
        btnActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarActionPerformed(evt);
            }
        });

        jLabel2.setText("Empleado:");

        txtEmpleado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtEmpleadoKeyReleased(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Movimientos"));

        tblMovimientos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblMovimientos);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 237, Short.MAX_VALUE)
        );

        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/basura.png"))); // NOI18N
        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/nuevo.png"))); // NOI18N
        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        rdbIngreso.setText("Ingreso");
        rdbIngreso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbIngresoActionPerformed(evt);
            }
        });

        rdbEgreso.setText("Egreso");
        rdbEgreso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbEgresoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(dtpSinceDate, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(dtpToDate, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(rdbEfectuados)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdbHabilitados)
                                .addGap(18, 18, 18)
                                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(rdbIngreso)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdbEgreso)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 97, Short.MAX_VALUE)
                                .addComponent(btnActualizar))
                            .addComponent(txtEmpleado)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnEliminar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnNuevo)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(dtpSinceDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4)
                        .addComponent(dtpToDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(rdbEfectuados)
                        .addComponent(rdbHabilitados)
                        .addComponent(btnActualizar)
                        .addComponent(rdbIngreso)
                        .addComponent(rdbEgreso)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNuevo)
                    .addComponent(btnEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void dtpSinceDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dtpSinceDateActionPerformed
        this.update();
    }//GEN-LAST:event_dtpSinceDateActionPerformed

    private void rdbEfectuadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdbEfectuadosActionPerformed
        this.update();
        this.updateBtnEliminar();
    }//GEN-LAST:event_rdbEfectuadosActionPerformed

    private void rdbHabilitadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdbHabilitadosActionPerformed
        this.update();
        this.updateBtnEliminar();
    }//GEN-LAST:event_rdbHabilitadosActionPerformed

    private void btnActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarActionPerformed
        this.update();
    }//GEN-LAST:event_btnActualizarActionPerformed

    private void txtEmpleadoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEmpleadoKeyReleased
        SwingUtil.textFilter(evt, this.tblMovimientos, this, this.txtEmpleado, this.trsMovimientos);
    }//GEN-LAST:event_txtEmpleadoKeyReleased

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        if (this.rdbEfectuados.isSelected()) {
            JOptionPane.showMessageDialog(this, "Movimiento ya pagado", "Movimientos Administrativos", JOptionPane.ERROR_MESSAGE);
        } else {
            int index = this.tblMovimientos.convertRowIndexToModel(this.tblMovimientos.getSelectedRow());

            if (index != -1) {
                boolean result = CajaProcessLogic.getInstance().eliminarMovimientoAdministrativo(this.lstMovimientosAdministrativos.get(index));
                if (result) {
                    JOptionPane.showMessageDialog(this, "Seleccione un Movimiento", "Movimientos Administrativos", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this, "Error en el registro", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(this, "Seleccione un Movimiento", "Movimientos Administrativos", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        if (PrincipalJRibbon.getInstance().getObjCaja().isAbierto()) {
            DlgMovimientoCaja dlgMovimientoCaja = new DlgMovimientoCaja(PrincipalJRibbon.getInstance().getFrame(), true);
            SwingUtil.centerOnScreen(dlgMovimientoCaja);
            dlgMovimientoCaja.addObserver(this);
            dlgMovimientoCaja.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this, "No ha abierto caja", "Información", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void rdbIngresoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdbIngresoActionPerformed
        this.update();
    }//GEN-LAST:event_rdbIngresoActionPerformed

    private void rdbEgresoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdbEgresoActionPerformed
        this.update();
    }//GEN-LAST:event_rdbEgresoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bgpPagos;
    private javax.swing.ButtonGroup bgpTipo;
    private javax.swing.JButton btnActualizar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnNuevo;
    private org.jdesktop.swingx.JXDatePicker dtpSinceDate;
    private org.jdesktop.swingx.JXDatePicker dtpToDate;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JRadioButton rdbEfectuados;
    private javax.swing.JRadioButton rdbEgreso;
    private javax.swing.JRadioButton rdbHabilitados;
    private javax.swing.JRadioButton rdbIngreso;
    private javax.swing.JTable tblMovimientos;
    private javax.swing.JTextField txtEmpleado;
    // End of variables declaration//GEN-END:variables

    private void updateBtnEliminar() {
        this.btnEliminar.setVisible(this.rdbHabilitados.isSelected());
    }
    
    private void update() {
        this.lstMovimientosAdministrativos = this.rdbHabilitados.isSelected() ? QueryFacade.getInstance().getAllMovimientosAdministrativosHabilitados(this.dtpSinceDate.getDate(), this.dtpToDate.getDate(), this.rdbIngreso.isSelected()) : QueryFacade.getInstance().getAllMovimientosAdministrativosPagados(this.dtpSinceDate.getDate(), this.dtpToDate.getDate(), this.rdbIngreso.isSelected());
        this.updateTblMovimientos();
    }

    private void updateTblMovimientos() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tableModel.addColumn("Empleado");
        tableModel.addColumn("Monto");
        tableModel.addColumn("Fecha");
        tableModel.addColumn("Estado");

        for (MovimientoAdministrativo objMovimientoAdministrativo : this.lstMovimientosAdministrativos) {
            tableModel.addRow(new Object[]{
                objMovimientoAdministrativo.getObjEmpleado().getNombreCompleto(),
                objMovimientoAdministrativo.getMonto(),
                DateUtil.getRegularDate(DateUtil.getDate(objMovimientoAdministrativo.getFechaRegistro())),
                objMovimientoAdministrativo.isPagado() ? "PAGADO" : "PENDIENTE"
            });
        }

        this.tblMovimientos.setModel(tableModel);
        this.trsMovimientos.setModel(tableModel);
        this.tblMovimientos.setRowSorter(this.trsMovimientos);
    }

    @Override
    public void update(IObservable objIObservable) {
        this.update();
    }
}
