package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.facade.SecurityFacade;
import com.smartech.sistemacooperativa.dominio.Tarjeta;
import com.smartech.sistemacooperativa.dominio.TipoUsuario;
import com.smartech.sistemacooperativa.gui.util.FingerprintUtil;
import com.smartech.sistemacooperativa.gui.util.MyCardDialog;
import com.smartech.sistemacooperativa.gui.util.MyFingerprintVerification;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import com.smartech.sistemacooperativa.util.generics.StringUtil;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JOptionPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 *
 * @author Smartech
 */
public class DlgIngresarTarjeta extends javax.swing.JDialog {

    private String lastNumeroTarjeta = "";
    private String numeroTarjeta = "";
    private String clave = "";
    private Tarjeta objTarjeta = null;
    private Tarjeta objTarjetaReturn = null;
    private boolean autenticacion;
    private char ultimoCaracter;
    private long tiempoUltimoCaracter;
    private boolean forzarBiometrico;

    public DlgIngresarTarjeta(java.awt.Frame parent, boolean modal, boolean autenticacion, boolean forzarBiometrico) {
        super(parent, modal);
        initComponents();

        this.autenticacion = autenticacion;
        this.forzarBiometrico = forzarBiometrico;

        this.btnVerificarHuella.setVisible(!this.forzarBiometrico);

        if (!this.autenticacion) {
            this.txtClaveTarjeta.setVisible(false);
            this.btnVerificarHuella.setVisible(false);
            this.lblClave.setVisible(false);
            this.btnVerificacionAdministrativa.setVisible(false);
        }

        //Cambiar por algo mas bonito...
        TipoUsuario objTipoUsuario1 = QueryFacade.getInstance().getTipoUsuario(1);
        TipoUsuario objTipoUsuario2 = QueryFacade.getInstance().getTipoUsuario(1);

        if (!(PrincipalJRibbon.getInstance().getObjUsuarioLogeado().getObjTipoUsuario().getId() == objTipoUsuario1.getId() || PrincipalJRibbon.getInstance().getObjUsuarioLogeado().getObjTipoUsuario().getId() == objTipoUsuario2.getId())) {
            this.btnVerificacionAdministrativa.setVisible(false);
        }

        this.txtNumeroTarjeta.setDocument(new PlainDocument() {
            @Override
            public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
                if (getLength() + str.length() <= Tarjeta.DIGITOS_TARJETA && str.matches("[\\*\\d*]+")) {
                    if (str.length() == Tarjeta.DIGITOS_TARJETA) {
                        super.insertString(offs, str, a);
                        getTarjeta();
                    } else if (getLength() == 0) {
                        if (numeroTarjeta.length() == 1) {
                            super.insertString(offs, "*", a);
                        } else {
                            super.insertString(offs, str, a);
                        }
                    } else if (getLength() + str.length() <= Tarjeta.DIGITOS_TARJETA - 4) {
                        super.insertString(offs, "*", a);
                    } else if (getLength() + str.length() == Tarjeta.DIGITOS_TARJETA) {
                        super.insertString(offs, str, a);
                        getTarjeta();
                    } else {
                        super.insertString(offs, str, a);
                    }
                }
            }
        });

        this.txtClaveTarjeta.setDocument(new PlainDocument() {
            @Override
            public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
                if (getLength() + str.length() <= 4 && str.matches("\\*+") && objTarjeta != null) {
                    super.insertString(offs, str, a);
                }
            }
        });

        this.txtNumeroTarjeta.addKeyListener(this.getNumeroTarjetaKeyListener());
        this.txtClaveTarjeta.addKeyListener(this.getClaveKeyListener());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtNumeroTarjeta = new javax.swing.JTextField();
        chkEditar = new javax.swing.JCheckBox();
        lblClave = new javax.swing.JLabel();
        txtClaveTarjeta = new javax.swing.JTextField();
        btnSeleccionar = new javax.swing.JButton();
        btnVerificarHuella = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JLabel();
        txtDNI = new javax.swing.JLabel();
        txtRUC = new javax.swing.JLabel();
        btnVerificacionAdministrativa = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Tarjeta");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Ingresar Tarjeta de Socio"));

        jLabel1.setText("Numero:");

        txtNumeroTarjeta.setEditable(false);

        chkEditar.setText("Editar");
        chkEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkEditarActionPerformed(evt);
            }
        });

        lblClave.setText("Clave:");

        txtClaveTarjeta.setEditable(false);

        btnSeleccionar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_checked.png"))); // NOI18N
        btnSeleccionar.setText("Seleccionar");
        btnSeleccionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeleccionarActionPerformed(evt);
            }
        });

        btnVerificarHuella.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/huella_24x24.png"))); // NOI18N
        btnVerificarHuella.setText("Biometrico");
        btnVerificarHuella.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerificarHuellaActionPerformed(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Socio"));

        jLabel2.setText("Nombre:");

        jLabel3.setText("DNI:");

        jLabel4.setText("RUC:");

        txtNombre.setText("-");

        txtDNI.setText("-");

        txtRUC.setText("-");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtNombre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtDNI, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtRUC, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtNombre))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtDNI))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtRUC))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        btnVerificacionAdministrativa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/huella_24x24.png"))); // NOI18N
        btnVerificacionAdministrativa.setText("Verificacion Administrativa");
        btnVerificacionAdministrativa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerificacionAdministrativaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(lblClave, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtClaveTarjeta, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnVerificarHuella)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnVerificacionAdministrativa))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(btnSeleccionar))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(txtNumeroTarjeta)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(chkEditar)))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(chkEditar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtNumeroTarjeta))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtClaveTarjeta, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblClave, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnVerificarHuella)
                    .addComponent(btnVerificacionAdministrativa))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSeleccionar)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSeleccionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeleccionarActionPerformed
        if (this.objTarjeta != null) {
            validarTarjeta();
        } else {
            JOptionPane.showMessageDialog(this, "Ingrese una Tarjeta", "Tarjeta", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnSeleccionarActionPerformed

    private void chkEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkEditarActionPerformed
        if (!this.chkEditar.isSelected()) {
            this.txtNumeroTarjeta.setText("");
            this.numeroTarjeta = "";
            this.txtNumeroTarjeta.setEditable(false);
        } else {
            this.numeroTarjeta = this.lastNumeroTarjeta;
            this.txtNumeroTarjeta.setEditable(true);
            this.objTarjeta = null;
            this.txtClaveTarjeta.setEditable(false);
        }
    }//GEN-LAST:event_chkEditarActionPerformed

    private void btnVerificarHuellaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerificarHuellaActionPerformed
        this.analizarBiometrico();
    }//GEN-LAST:event_btnVerificarHuellaActionPerformed

    private void btnVerificacionAdministrativaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerificacionAdministrativaActionPerformed
        if (this.objTarjeta != null) {
            DlgVerificacionBiometricaAdministrador dlgVerificacionBiometricaAdministrador = new DlgVerificacionBiometricaAdministrador(PrincipalJRibbon.getInstance().getFrame(), true);
            SwingUtil.centerOnScreen(dlgVerificacionBiometricaAdministrador);
            dlgVerificacionBiometricaAdministrador.setVisible(true);

            boolean result = dlgVerificacionBiometricaAdministrador.isResult();

            if (result) {
                this.objTarjetaReturn = this.crearTarjeta();
                this.dispose();
            } else {
                this.objTarjetaReturn = null;
            }
        } else {
            JOptionPane.showMessageDialog(this, "Ingrese una Tarjeta", "Tarjeta", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnVerificacionAdministrativaActionPerformed

    //<editor-fold defaultstate="collapsed" desc="Variables">
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSeleccionar;
    private javax.swing.JButton btnVerificacionAdministrativa;
    private javax.swing.JButton btnVerificarHuella;
    private javax.swing.JCheckBox chkEditar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblClave;
    private javax.swing.JTextField txtClaveTarjeta;
    private javax.swing.JLabel txtDNI;
    private javax.swing.JLabel txtNombre;
    private javax.swing.JTextField txtNumeroTarjeta;
    private javax.swing.JLabel txtRUC;
    // End of variables declaration//GEN-END:variables
    //</editor-fold>

    private void analizarBiometrico() {
        if (this.objTarjeta != null) {
            MyFingerprintVerification myFingerprintVerification = new MyFingerprintVerification(PrincipalJRibbon.getInstance().getFrame(), true, this.objTarjeta.getObjSocio().getObjUsuario());
            SwingUtil.centerOnScreen(myFingerprintVerification);
            myFingerprintVerification.setVisible(true);

            boolean result = myFingerprintVerification.isMatched();

            if (result) {
                this.validarTarjetaBiometrico();
            } else {
                if (this.forzarBiometrico) {
                    JOptionPane.showMessageDialog(this, "No se verifico la huella", "Huella", JOptionPane.ERROR_MESSAGE);
                    this.dispose();
                }
                this.objTarjetaReturn = null;
            }
        } else {
            JOptionPane.showMessageDialog(this, "Ingrese una Tarjeta", "Tarjeta", JOptionPane.ERROR_MESSAGE);
        }
    }

    public Tarjeta getObjTarjeta() {
        return this.objTarjetaReturn;
    }

    private void clearCamposSocio() {
        this.txtNombre.setText("-");
        this.txtDNI.setText("-");
        this.txtRUC.setText("-");
    }

    private void actualizarCamposSocio() {
        if (this.objTarjeta == null || this.objTarjeta.getId() == 0 || !this.objTarjeta.isActivo()) {
            this.clearCamposSocio();
        } else {
            this.txtNombre.setText(this.objTarjeta.getObjSocio().getNombreCompleto());
            this.txtDNI.setText(this.objTarjeta.getObjSocio().getDocumentoIdentidad());
            this.txtRUC.setText(this.objTarjeta.getObjSocio().getRUC());
        }
    }

    private void getTarjeta() {
        this.objTarjeta = QueryFacade.getInstance().getTarjeta(this.numeroTarjeta);
        this.actualizarCamposSocio();
        if (this.objTarjeta != null && this.objTarjeta.getId() != 0 && this.objTarjeta.isActivo()) {
            this.txtClaveTarjeta.setEditable(true);
            this.txtClaveTarjeta.requestFocusInWindow();
        } else {
            MyCardDialog myCardDialog = new MyCardDialog(PrincipalJRibbon.getInstance().getFrame(), true);
            if (this.objTarjeta == null) {
                myCardDialog.setMessage("Tarjeta Inexistente");
            } else {
                myCardDialog.setMessage(this.objTarjeta.isActivo() ? "Tarjeta Bloqueada" : "Tarjeta Inactiva");
            }
            SwingUtil.centerOnScreen(myCardDialog);
            myCardDialog.setVisible(true);
        }
    }

    private Tarjeta crearTarjeta() {
        return new Tarjeta(
                this.objTarjeta.getId(),
                this.objTarjeta.getCodigo(),
                this.objTarjeta.getPin(),
                this.objTarjeta.getFechaEntrega(),
                this.objTarjeta.isActivo(),
                this.objTarjeta.getIntentos(),
                this.objTarjeta.getFechaRegistro(),
                this.objTarjeta.getFechaModificacion(),
                this.objTarjeta.getFechaBloqueoIntentos(),
                this.objTarjeta.isEstado(),
                this.objTarjeta.getObjTipoTarjeta(),
                this.objTarjeta.getObjSocio());
    }

    private void validarTarjetaBiometrico() {
        this.objTarjetaReturn = this.crearTarjeta();
        this.dispose();
    }

    private void validarTarjeta() {
        if (this.autenticacion) {
            boolean result = SecurityFacade.getInstance().verifyCard(this.objTarjeta, StringUtil.encodeString(this.clave));

            if (result) {
                if (this.forzarBiometrico) {
                    this.analizarBiometrico();
                } else {
                    this.objTarjetaReturn = this.crearTarjeta();
                    this.dispose();
                }
            } else {
                JOptionPane.showMessageDialog(this, "Clave incorrecta, quedan " + (SecurityFacade.MAX_INTENTOS_TARJETA - this.objTarjeta.getIntentos()) + " intentos", "Clave Invalida", JOptionPane.ERROR_MESSAGE);
                if (this.objTarjeta.getIntentos() == 3 && this.objTarjeta.getFechaBloqueoIntentos() != null) {
                    this.dispose();
                }
            }
        } else if (this.objTarjeta.isActivo()) {
            this.objTarjetaReturn = this.crearTarjeta();
            this.dispose();
        } else {
            MyCardDialog myCardDialog = new MyCardDialog(PrincipalJRibbon.getInstance().getFrame(), true);
            myCardDialog.setMessage("Tarjeta Inactiva");
            SwingUtil.centerOnScreen(myCardDialog);
            myCardDialog.setVisible(true);
        }
    }

    private KeyListener getNumeroTarjetaKeyListener() {
        return new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (e.getKeyChar() == KeyEvent.VK_CLEAR || e.getKeyChar() == KeyEvent.VK_BACK_SPACE) {
                    if (!numeroTarjeta.isEmpty()) {
                        numeroTarjeta = StringUtil.deleteLastIndex(numeroTarjeta);
                        txtNumeroTarjeta.setText(Tarjeta.getNumeroTarjetaEncriptado(numeroTarjeta));
                        txtClaveTarjeta.setText("");
                        txtClaveTarjeta.setEditable(false);
                        clearCamposSocio();
                    }
                } else if (e.getKeyChar() != KeyEvent.VK_ENTER) {
                    numeroTarjeta += e.getKeyChar();
                    if (chkEditar.isSelected()) {
                        if (numeroTarjeta.length() > Tarjeta.DIGITOS_TARJETA) {
                            numeroTarjeta = numeroTarjeta.substring(0, Tarjeta.DIGITOS_TARJETA);
                        }
                    } else if (numeroTarjeta.charAt(numeroTarjeta.length() - 1) == '_') {
                        try {
                            numeroTarjeta = Tarjeta.getNumeroTarjeta(numeroTarjeta);
                            txtNumeroTarjeta.setText(Tarjeta.getNumeroTarjetaEncriptado(numeroTarjeta));
                            lastNumeroTarjeta = numeroTarjeta;
                            numeroTarjeta = "";
                        } catch (Exception ex) {
                            numeroTarjeta = lastNumeroTarjeta;
                        }
                    }
                    ultimoCaracter = e.getKeyChar();
                    tiempoUltimoCaracter = DateUtil.currentTimestamp().getTime();
                }
            }
        };
    }

    private KeyListener getClaveKeyListener() {
        return new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                long timeDiff = DateUtil.currentTimestamp().getTime() - tiempoUltimoCaracter;
                System.out.println("Tiempo: " + timeDiff);
                if (txtClaveTarjeta.isEditable()) {
                    if (e.getKeyChar() == KeyEvent.VK_CLEAR || e.getKeyChar() == KeyEvent.VK_BACK_SPACE) {
                        if (!clave.isEmpty()) {
                            clave = StringUtil.deleteLastIndex(clave);
                        }
                    } else if (timeDiff > 20) {
                        if (e.getKeyChar() != KeyEvent.VK_ENTER) {
                            String key = String.valueOf(e.getKeyChar());
                            if (key.matches("\\d+")) {
                                clave += e.getKeyChar();
                                if (clave.length() > 4) {
                                    clave = clave.substring(0, 4);
                                }
                            }
                        } else if (clave.length() == 4) {
                            validarTarjeta();
                        }
                        txtClaveTarjeta.setText(StringUtil.getAsterisks(clave.length()));
                    }
                }
                tiempoUltimoCaracter = DateUtil.currentTimestamp().getTime();
            }
        };
    }
}
