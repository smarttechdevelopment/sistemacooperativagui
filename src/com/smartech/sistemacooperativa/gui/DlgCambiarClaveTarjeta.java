package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.process.AdmisionSociosProcessLogic;
import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.facade.SecurityFacade;
import com.smartech.sistemacooperativa.dominio.Tarjeta;
import com.smartech.sistemacooperativa.gui.util.MyCardDialog;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import com.smartech.sistemacooperativa.util.generics.StringUtil;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JOptionPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 *
 * @author Smartech
 */
public class DlgCambiarClaveTarjeta extends javax.swing.JDialog {

    public static int CAMBIAR_CLAVE = 1;
    public static int ACTIVAR_TARJETA = 2;

    private String lastNumeroTarjeta = "";
    private String numeroTarjeta = "";
    private String clave = "";
    private String nuevaClave = "";
    private String nuevaClaveRepetida = "";
    private Tarjeta objTarjeta = null;
    private boolean verificado = false;
    private int modo = 1;

    private char ultimoCaracter;
    private long tiempoUltimoCaracter;

    public DlgCambiarClaveTarjeta(java.awt.Frame parent, boolean modal, int modo) {
        super(parent, modal);
        initComponents();
        this.chkEditar.setVisible(false);
        this.modo = modo;

        if (this.modo == ACTIVAR_TARJETA) {
            setTitle("Activar Tarjeta");
            this.lblClave.setVisible(false);
            this.txtClaveTarjeta.setVisible(false);
            this.txtNuevaClave.setMinimumSize(new Dimension(80, 20));
        } else {
            setTitle("Cambiar Clave de Tarjeta");
        }

        this.txtNumeroTarjeta.setDocument(new PlainDocument() {
            @Override
            public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
                if (getLength() + str.length() <= Tarjeta.DIGITOS_TARJETA && str.matches("[\\*\\d*]+")) {
                    if (str.length() == Tarjeta.DIGITOS_TARJETA) {
                        super.insertString(offs, str, a);
                        getTarjeta();
                    } else if (getLength() == 0) {
                        if (numeroTarjeta.length() == 1) {
                            super.insertString(offs, "*", a);
                        } else {
                            super.insertString(offs, str, a);
                        }
                    } else if (getLength() + str.length() <= Tarjeta.DIGITOS_TARJETA - 4) {
                        super.insertString(offs, "*", a);
                    } else if (getLength() + str.length() == Tarjeta.DIGITOS_TARJETA) {
                        super.insertString(offs, str, a);
                        getTarjeta();
                    } else {
                        super.insertString(offs, str, a);
                    }
                }
            }
        });

        this.txtClaveTarjeta.setDocument(new PlainDocument() {
            @Override
            public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
                if (getLength() + str.length() <= 4 && str.matches("\\*+") && objTarjeta != null) {
                    super.insertString(offs, str, a);
                }
            }
        });

        this.txtNuevaClave.setDocument(new PlainDocument() {
            @Override
            public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
                if (getLength() + str.length() <= 4 && str.matches("\\*+") && objTarjeta != null) {
                    super.insertString(offs, str, a);
                }
            }
        });

        this.txtRepetirNuevaClave.setDocument(new PlainDocument() {
            @Override
            public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
                if (getLength() + str.length() <= 4 && str.matches("\\*+") && objTarjeta != null) {
                    super.insertString(offs, str, a);
                }
            }
        });

        this.txtNumeroTarjeta.addKeyListener(this.getNumeroTarjetaKeyListener());
        if (this.modo == CAMBIAR_CLAVE) {
            this.txtClaveTarjeta.addKeyListener(this.getClaveKeyListener());
        }
        this.txtNuevaClave.addKeyListener(this.getNuevaClaveKeyListener());
        this.txtRepetirNuevaClave.addKeyListener(this.getNuevaClaveRepetidaKeyListener());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtNumeroTarjeta = new javax.swing.JTextField();
        chkEditar = new javax.swing.JCheckBox();
        lblClave = new javax.swing.JLabel();
        txtClaveTarjeta = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtNuevaClave = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtRepetirNuevaClave = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JLabel();
        txtDNI = new javax.swing.JLabel();
        txtRUC = new javax.swing.JLabel();
        btnGuardar = new javax.swing.JButton();

        jLabel2.setText("jLabel2");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Cambiar Clave"));

        jLabel1.setText("Numero:");

        txtNumeroTarjeta.setEditable(false);

        chkEditar.setText("Editar");
        chkEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkEditarActionPerformed(evt);
            }
        });

        lblClave.setText("Clave Actual:");

        txtClaveTarjeta.setEditable(false);

        jLabel3.setText("Clave Nueva:");

        txtNuevaClave.setEditable(false);

        jLabel4.setText("Repetir Clave Nueva");

        txtRepetirNuevaClave.setEditable(false);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Socio"));

        jLabel5.setText("Nombre:");

        jLabel6.setText("DNI:");

        jLabel7.setText("RUC:");

        txtNombre.setText("-");

        txtDNI.setText("-");

        txtRUC.setText("-");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtNombre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtDNI, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtRUC, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtNombre))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtDNI))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtRUC))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_save.png"))); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNumeroTarjeta)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(chkEditar)
                        .addGap(10, 10, 10))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblClave)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtClaveTarjeta))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtNuevaClave, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addComponent(jLabel4)
                        .addGap(4, 4, 4)
                        .addComponent(txtRepetirNuevaClave, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnGuardar)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chkEditar, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtNumeroTarjeta, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtClaveTarjeta, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblClave, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNuevaClave, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtRepetirNuevaClave, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnGuardar)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void chkEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkEditarActionPerformed
        if (!this.chkEditar.isSelected()) {
            this.txtNumeroTarjeta.setText("");
            this.numeroTarjeta = "";
            this.txtNumeroTarjeta.setEditable(false);
        } else {
            this.numeroTarjeta = this.lastNumeroTarjeta;
            this.txtNumeroTarjeta.setEditable(true);
            this.objTarjeta = null;
            this.txtClaveTarjeta.setEditable(false);
        }
    }//GEN-LAST:event_chkEditarActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        if (this.objTarjeta != null) {
            if ((this.objTarjeta.isActivo() && this.modo == CAMBIAR_CLAVE) || (this.modo == ACTIVAR_TARJETA)) {
                if (this.nuevaClave.equals(this.nuevaClaveRepetida)) {
                    this.objTarjeta.setPin(StringUtil.encodeString(this.nuevaClave));
                    this.objTarjeta.setActivo(true);

                    boolean result = AdmisionSociosProcessLogic.getInstance().actualizarTarjeta(this.objTarjeta);

                    if (result) {
                        JOptionPane.showMessageDialog(this, "Tarjeta " + (this.modo == ACTIVAR_TARJETA ? "activada" : "actualizada") + " con exito", "Tarjeta", JOptionPane.INFORMATION_MESSAGE);
                        this.dispose();
                    } else {
                        JOptionPane.showMessageDialog(this, "Error en la activacion", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Las claves ingresadas no coinciden", "Clave", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(this, "La tarjeta está inactiva. Active la tarjeta primero", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Ingrese una Tarjeta", "Tarjeta", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGuardar;
    private javax.swing.JCheckBox chkEditar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblClave;
    private javax.swing.JTextField txtClaveTarjeta;
    private javax.swing.JLabel txtDNI;
    private javax.swing.JLabel txtNombre;
    private javax.swing.JTextField txtNuevaClave;
    private javax.swing.JTextField txtNumeroTarjeta;
    private javax.swing.JLabel txtRUC;
    private javax.swing.JTextField txtRepetirNuevaClave;
    // End of variables declaration//GEN-END:variables

    private void clearCamposSocio() {
        this.txtNombre.setText("-");
        this.txtDNI.setText("-");
        this.txtRUC.setText("-");
    }

    private void actualizarCamposSocio() {
        if (this.objTarjeta == null || this.objTarjeta.getId() == 0) {
            this.clearCamposSocio();
        } else {
            this.txtNombre.setText(this.objTarjeta.getObjSocio().getNombreCompleto());
            this.txtDNI.setText(this.objTarjeta.getObjSocio().getDocumentoIdentidad());
            this.txtRUC.setText(this.objTarjeta.getObjSocio().getRUC());
        }
    }

    private void getTarjeta() {
        this.objTarjeta = QueryFacade.getInstance().getTarjeta(this.numeroTarjeta);
        this.actualizarCamposSocio();
        if (this.objTarjeta != null && this.objTarjeta.getId() != 0) {
            if (this.objTarjeta.isActivo()) {
                if (this.modo == CAMBIAR_CLAVE) {
                    this.verificado = true;
                    this.activarCambioClave();
                    this.txtClaveTarjeta.setEditable(true);
                    this.txtClaveTarjeta.requestFocusInWindow();
                } else {
                    this.clearCamposSocio();
                    this.desactivarCambioClave();
                    MyCardDialog myCardDialog = new MyCardDialog(PrincipalJRibbon.getInstance().getFrame(), true);
                    myCardDialog.setMessage("Tarjeta ya Activada");
                    SwingUtil.centerOnScreen(myCardDialog);
                    myCardDialog.setVisible(true);
                }
            } else if (this.modo == ACTIVAR_TARJETA) {
                this.txtClaveTarjeta.setText("0000");
                this.verificado = true;
                this.activarCambioClave();
            } else {

            }
        } else {
            this.clearCamposSocio();
            this.desactivarCambioClave();
            MyCardDialog myCardDialog = new MyCardDialog(PrincipalJRibbon.getInstance().getFrame(), true);
            if (this.objTarjeta == null) {
                myCardDialog.setMessage("Tarjeta Inexistente");
            } else {
                myCardDialog.setMessage(this.objTarjeta.isActivo() ? "Tarjeta Bloqueada" : "Tarjeta Inactiva");
            }
            SwingUtil.centerOnScreen(myCardDialog);
            myCardDialog.setVisible(true);
        }
    }

    private void activarCambioClave() {
        if (this.verificado) {
            this.txtNuevaClave.setEditable(true);
            this.txtNuevaClave.requestFocusInWindow();
            this.txtRepetirNuevaClave.setEditable(true);
        }
    }

    private void desactivarCambioClave() {
        this.txtNuevaClave.setEditable(false);
        this.txtRepetirNuevaClave.setEditable(false);
        this.nuevaClave = "";
        this.nuevaClaveRepetida = "";
    }

    private void validarTarjeta() {
        this.verificado = SecurityFacade.getInstance().verifyCard(this.objTarjeta, StringUtil.encodeString(this.clave));

        if (this.verificado) {
            this.txtClaveTarjeta.setEnabled(false);
            this.txtClaveTarjeta.setEditable(false);
            this.activarCambioClave();
        } else {
            JOptionPane.showMessageDialog(this, "Clave incorrecta, quedan " + (SecurityFacade.MAX_INTENTOS_TARJETA - this.objTarjeta.getIntentos()) + " intentos", "Clave Invalida", JOptionPane.ERROR_MESSAGE);
            if (this.objTarjeta.getIntentos() == 3 && this.objTarjeta.getFechaBloqueoIntentos() != null) {
                this.dispose();
            }
        }
    }

    private KeyListener getNumeroTarjetaKeyListener() {
        return new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (e.getKeyChar() == KeyEvent.VK_CLEAR || e.getKeyChar() == KeyEvent.VK_BACK_SPACE) {
                    if (!numeroTarjeta.isEmpty()) {
                        numeroTarjeta = StringUtil.deleteLastIndex(numeroTarjeta);
                        txtNumeroTarjeta.setText(Tarjeta.getNumeroTarjetaEncriptado(numeroTarjeta));
                        txtClaveTarjeta.setText("");
                        txtClaveTarjeta.setEditable(false);
                        clearCamposSocio();
                    }
                } else if (e.getKeyChar() != KeyEvent.VK_ENTER) {
                    numeroTarjeta += e.getKeyChar();
                    if (chkEditar.isSelected()) {
                        if (numeroTarjeta.length() > Tarjeta.DIGITOS_TARJETA) {
                            numeroTarjeta = numeroTarjeta.substring(0, Tarjeta.DIGITOS_TARJETA);
                        }
                    } else if (numeroTarjeta.charAt(numeroTarjeta.length() - 1) == '_') {
                        try {
                            numeroTarjeta = Tarjeta.getNumeroTarjeta(numeroTarjeta);
                            txtNumeroTarjeta.setText(Tarjeta.getNumeroTarjetaEncriptado(numeroTarjeta));
                            lastNumeroTarjeta = numeroTarjeta;
                            numeroTarjeta = "";
                        } catch (Exception ex) {
                            //ex.printStackTrace();
                            numeroTarjeta = lastNumeroTarjeta;
                        }
                    }
                    ultimoCaracter = e.getKeyChar();
                    tiempoUltimoCaracter = DateUtil.currentTimestamp().getTime();
                }
                System.out.println(numeroTarjeta);
            }
        };
    }

    private KeyListener getClaveKeyListener() {
        return new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                long timeDiff = DateUtil.currentTimestamp().getTime() - tiempoUltimoCaracter;
                System.out.println("Tiempo: " + timeDiff);
                if (txtClaveTarjeta.isEditable()) {
                    if (e.getKeyChar() == KeyEvent.VK_CLEAR || e.getKeyChar() == KeyEvent.VK_BACK_SPACE) {
                        if (!clave.isEmpty()) {
                            clave = StringUtil.deleteLastIndex(clave);
                        }
                    } else if (timeDiff > 20) {
                        if (e.getKeyChar() != KeyEvent.VK_ENTER) {
                            String key = String.valueOf(e.getKeyChar());
                            if (key.matches("\\d+")) {
                                clave += e.getKeyChar();
                                if (clave.length() > 4) {
                                    clave = clave.substring(0, 4);
                                }
                            }
                        } else if (clave.length() == 4) {
                            validarTarjeta();
                        }
                        txtClaveTarjeta.setText(StringUtil.getAsterisks(clave.length()));
                    }
                }
                tiempoUltimoCaracter = DateUtil.currentTimestamp().getTime();
            }
        };
    }

    private KeyListener getNuevaClaveKeyListener() {
        return new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                long timeDiff = DateUtil.currentTimestamp().getTime() - tiempoUltimoCaracter;
                System.out.println("Tiempo: " + timeDiff);
                if (timeDiff > 20) {
                    if (e.getKeyChar() == KeyEvent.VK_CLEAR || e.getKeyChar() == KeyEvent.VK_BACK_SPACE) {
                        if (!nuevaClave.isEmpty()) {
                            nuevaClave = StringUtil.deleteLastIndex(nuevaClave);
                        }
                    } else {
                        String key = String.valueOf(e.getKeyChar());
                        if (key.matches("\\d+")) {
                            nuevaClave += e.getKeyChar();
                            if (nuevaClave.length() > 4) {
                                nuevaClave = nuevaClave.substring(0, 4);
                            }
                        }
                    }
                }
                txtNuevaClave.setText(StringUtil.getAsterisks(nuevaClave.length()));
            }
        };
    }

    private KeyListener getNuevaClaveRepetidaKeyListener() {
        return new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (e.getKeyChar() == KeyEvent.VK_CLEAR || e.getKeyChar() == KeyEvent.VK_BACK_SPACE) {
                    if (!nuevaClaveRepetida.isEmpty()) {
                        nuevaClaveRepetida = StringUtil.deleteLastIndex(nuevaClaveRepetida);
                    }
                } else {
                    String key = String.valueOf(e.getKeyChar());
                    if (key.matches("\\d+")) {
                        nuevaClaveRepetida += e.getKeyChar();
                        if (nuevaClaveRepetida.length() > 4) {
                            nuevaClaveRepetida = nuevaClaveRepetida.substring(0, 4);
                        }
                    }
                }
                txtRepetirNuevaClave.setText(StringUtil.getAsterisks(nuevaClaveRepetida.length()));
            }
        };
    }
}
