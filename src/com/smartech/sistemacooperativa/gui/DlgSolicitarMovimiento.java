package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.process.DepositosProcessLogic;
import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.process.RetirosProcessLogic;
import com.smartech.sistemacooperativa.dominio.Concepto;
import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Deposito;
import com.smartech.sistemacooperativa.dominio.Empleado;
import com.smartech.sistemacooperativa.dominio.Moneda;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.dominio.Retiro;
import com.smartech.sistemacooperativa.dominio.TipoDocumentoIdentidad;
import com.smartech.sistemacooperativa.dominio.TipoPago;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.util.interfaces.IObservable;
import com.smartech.sistemacooperativa.util.interfaces.IObserver;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

/**
 *
 * @author Smartech
 */
public class DlgSolicitarMovimiento extends javax.swing.JDialog implements IObservable {

    private final List<IObserver> lstObservers = new ArrayList<>();

    private boolean permiteRegistrar = false;
    private Concepto objConcepto = null;

    private Cuenta objCuentaSolicitada = null;

    private List<TipoPago> lstTipoPagos = new ArrayList<>();
    private List<Moneda> lstMonedas = new ArrayList();
    private List<TipoDocumentoIdentidad> lstTiposDocumentoIdentidad = new ArrayList<>();

    public Cuenta getObjCuentaSolicitada() {
        return objCuentaSolicitada;
    }

    public void setObjCuentaSolicitada(Cuenta objCuentaSolicitada) {
        this.objCuentaSolicitada = objCuentaSolicitada;
    }

    public List<TipoPago> getLstTipoPagos() {
        return lstTipoPagos;
    }

    public void setLstTipoPagos(List<TipoPago> lstTipoPagos) {
        this.lstTipoPagos = lstTipoPagos;
    }

    public Concepto getObjConcepto() {
        return objConcepto;
    }

    public DlgSolicitarMovimiento(java.awt.Frame parent, boolean modal, Concepto objConcepto, Cuenta objCuentaSeleccionada) {
        super(parent, modal);
        initComponents();

        this.objCuentaSolicitada = objCuentaSeleccionada;
        this.objConcepto = objConcepto;
        this.lstTiposDocumentoIdentidad();
        this.updateCmbDocumentosIdentidad();

        if (this.getObjConcepto().getId() == Concepto.CONCEPTO_DEPOSITOS) {
            Deposito.establecerMontoAdvertencia(PrincipalJRibbon.getInstance().getObjUsuarioLogeado());
        } else if (this.getObjConcepto().getId() == Concepto.CONCEPTO_RETIROS_CUENTA) {
            this.txtDocumentoIdentidadDepositante.setVisible(false);
            this.lblDNIDepositante.setVisible(false);
            this.cmbDocumentoIdentidad.setVisible(false);
        }

        SwingUtil.setPositiveNumericInput(this.txtMontoPago);
        SwingUtil.setDocumentoIdentidadInput(this.lstTiposDocumentoIdentidad.get(0), this.txtDocumentoIdentidadDepositante);

        setTitle("Cuenta: " + this.objCuentaSolicitada.getCodigo());

        this.txtaIndicaciones.setText("");
        this.txtaIndicaciones.setEditable(false);

        this.btnGuardarPendiente.setText("Guardar " + this.getObjConcepto().getNombre());
        this.btnGuardarPendiente.setEnabled(false);
        pack();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtMontoPago = new javax.swing.JTextField();
        lblDNIDepositante = new javax.swing.JLabel();
        txtDocumentoIdentidadDepositante = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtaIndicaciones = new javax.swing.JTextArea();
        btnGuardarPendiente = new javax.swing.JButton();
        cmbDocumentoIdentidad = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        jLabel2.setText("Monto:");

        txtMontoPago.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtMontoPagoKeyReleased(evt);
            }
        });

        lblDNIDepositante.setText("Depositante:");

        txtaIndicaciones.setColumns(20);
        txtaIndicaciones.setRows(5);
        jScrollPane1.setViewportView(txtaIndicaciones);

        btnGuardarPendiente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_save.png"))); // NOI18N
        btnGuardarPendiente.setText("Guardar Movimiento");
        btnGuardarPendiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarPendienteActionPerformed(evt);
            }
        });

        cmbDocumentoIdentidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbDocumentoIdentidadActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblDNIDepositante, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtDocumentoIdentidadDepositante)
                            .addComponent(txtMontoPago, javax.swing.GroupLayout.DEFAULT_SIZE, 183, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbDocumentoIdentidad, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnGuardarPendiente)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDNIDepositante)
                    .addComponent(txtDocumentoIdentidadDepositante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbDocumentoIdentidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtMontoPago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnGuardarPendiente)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGuardarPendienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarPendienteActionPerformed
        Date fecha = new Date();

        if (this.getObjConcepto().getId() == Concepto.CONCEPTO_DEPOSITOS) {

            if (SwingUtil.hasBlankInputs(this.txtDocumentoIdentidadDepositante, this.txtMontoPago)) {
                JOptionPane.showMessageDialog(this, "Datos(s) requeridos", "Información", JOptionPane.INFORMATION_MESSAGE);
                return;
            }

        } else if (this.getObjConcepto().getId() == Concepto.CONCEPTO_RETIROS_CUENTA) {
            if (SwingUtil.hasBlankInputs(this.txtMontoPago)) {
                JOptionPane.showMessageDialog(this, "Datos(s) requeridos", "Información", JOptionPane.INFORMATION_MESSAGE);
                return;
            }
        }

        if (this.txtDocumentoIdentidadDepositante.isVisible()) {
            if (!verificaDepositante()) {
                JOptionPane.showMessageDialog(this, "Depositante no válido", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
        }

        String txtAcreedor = "";
        String txtDepositante = this.txtDocumentoIdentidadDepositante.getText().trim();

        BigDecimal monto = new BigDecimal(this.txtMontoPago.getText());

        if (monto.compareTo(BigDecimal.ZERO) == 0) {
            this.txtMontoPago.setText("");
            SwingUtil.hasBlankInputs(this.txtMontoPago);
            return;
        }

        if (this.getObjConcepto().getId() == Concepto.CONCEPTO_DEPOSITOS) {
            // Ver los pagos y el depósito habilitado antes
            Deposito objDeposito = new Deposito(
                    0,
                    monto,
                    new Timestamp(fecha.getTime()),
                    new Timestamp(fecha.getTime()),
                    true,
                    this.objCuentaSolicitada.getObjUsuario(),
                    txtDepositante,
                    true,
                    false,
                    this.objCuentaSolicitada,
                    txtAcreedor,
                    new ArrayList<>());

            int respuesta = JOptionPane.showConfirmDialog(this, "Confirmar Depósito de " + objDeposito.getObjCuenta().getObjTipoCuenta().getObjMoneda().getSimbolo()+ " " + objDeposito.getMonto().toPlainString(), "Cuenta", JOptionPane.WARNING_MESSAGE);

            if (respuesta == JOptionPane.YES_OPTION) {
                String mensaje = DepositosProcessLogic.getInstance().registrarDeposito(objDeposito, (Empleado) PrincipalJRibbon.getInstance().getObjPersona(), PrincipalJRibbon.getInstance().getObjEstablecimiento(), PrincipalJRibbon.getInstance().getObjCaja());

                if (mensaje.toLowerCase().contains("error")) {
                    JOptionPane.showMessageDialog(this, mensaje, "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this, mensaje, "Información", JOptionPane.INFORMATION_MESSAGE);
                }
                this.notifyObservers();
                this.dispose();
            }
        } else if (this.getObjConcepto().getId() == Concepto.CONCEPTO_RETIROS_CUENTA) {

            // Faltan validaciones de usuario
            Retiro objRetiro = new Retiro(
                    0,
                    monto,
                    new Timestamp(new Date().getTime()),
                    new Timestamp(new Date().getTime()),
                    true,
                    this.objCuentaSolicitada.getObjUsuario(),
                    this.objCuentaSolicitada,
                    txtAcreedor,
                    new ArrayList<>(),
                    true,
                    false);

            int respuesta = JOptionPane.showConfirmDialog(this, "Confirmar Retiro", "Cuenta", JOptionPane.WARNING_MESSAGE);

            if (respuesta == JOptionPane.YES_OPTION) {

                String mensaje = RetirosProcessLogic.getInstance().registrarRetiro(objRetiro, (Empleado) PrincipalJRibbon.getInstance().getObjPersona());

                if (mensaje.toLowerCase().contains("error")) {
                    JOptionPane.showMessageDialog(this, mensaje, "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this, mensaje, "Información", JOptionPane.INFORMATION_MESSAGE);
                }
                this.notifyObservers();
                this.dispose();
            }
        }
    }//GEN-LAST:event_btnGuardarPendienteActionPerformed

    private void txtMontoPagoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMontoPagoKeyReleased

        if (this.txtMontoPago.getText().length() > 0) {

            if (this.getObjConcepto().getId() == Concepto.CONCEPTO_DEPOSITOS) {

                if (Deposito.advertirMontoDeposito(new BigDecimal(this.txtMontoPago.getText()), this.objCuentaSolicitada.getObjTipoCuenta().getObjMoneda())) {
                    this.txtaIndicaciones.setText("Deberá realizar el procedimiento de 'Lavado de Activos'");
                    this.permiteRegistrar = true;
                } else {
                    this.txtaIndicaciones.setText("Sin notificaciones");
                    this.permiteRegistrar = true;
                }
            } else if (this.getObjConcepto().getId() == Concepto.CONCEPTO_RETIROS_CUENTA) {

                if (Retiro.advertirMontoRetiro(new BigDecimal(this.txtMontoPago.getText()), this.objCuentaSolicitada.getObjTipoCuenta().getObjMoneda(), PrincipalJRibbon.getInstance().getObjUsuarioLogeado().getObjTipoUsuario())) {
                    this.txtaIndicaciones.setText("Usted no cuenta con el permiso para realizar el retiro.\nComuníquese con la persona encargada.");
                    this.permiteRegistrar = false;

                } else {
                    this.txtaIndicaciones.setText("Sin notificaciones");
                    this.permiteRegistrar = true;
                }
            }

        } else if (this.txtMontoPago.getText().length() == 1) {
            if (this.txtMontoPago.getText().equals("0")) {
                this.txtaIndicaciones.setText("Ingrese un monto");
                this.permiteRegistrar = false;
            } else {
                this.txtaIndicaciones.setText("Sin notificaciones");
                this.permiteRegistrar = true;
            }
        } else {
            this.txtaIndicaciones.setText("Sin notificaciones");
            this.permiteRegistrar = true;
        }

        this.btnGuardarPendiente.setEnabled(this.permiteRegistrar);
    }//GEN-LAST:event_txtMontoPagoKeyReleased


    private void cmbDocumentoIdentidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbDocumentoIdentidadActionPerformed
        int selected = this.cmbDocumentoIdentidad.getSelectedIndex();
        SwingUtil.setDocumentoIdentidadInput(selected > -1 ? this.lstTiposDocumentoIdentidad.get(selected) : this.lstTiposDocumentoIdentidad.get(0), this.txtDocumentoIdentidadDepositante);
    }//GEN-LAST:event_cmbDocumentoIdentidadActionPerformed

    private boolean verificaDepositante() {
        boolean resultado = false;

        if (!this.txtDocumentoIdentidadDepositante.getText().trim().isEmpty()) {
            if (this.cmbDocumentoIdentidad.getSelectedIndex() > -1) {
                if (this.txtDocumentoIdentidadDepositante.getText().trim().length() == this.lstTiposDocumentoIdentidad.get(this.cmbDocumentoIdentidad.getSelectedIndex()).getLongitud()) {
                    resultado = true;
                }
            }
        }

        return resultado;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGuardarPendiente;
    private javax.swing.JComboBox<String> cmbDocumentoIdentidad;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblDNIDepositante;
    private javax.swing.JTextField txtDocumentoIdentidadDepositante;
    private javax.swing.JTextField txtMontoPago;
    private javax.swing.JTextArea txtaIndicaciones;
    // End of variables declaration//GEN-END:variables

    private void lstTiposDocumentoIdentidad() {
        this.lstTiposDocumentoIdentidad = QueryFacade.getInstance().getAllTiposDocumentoIdentidad();
    }

    private void updateCmbDocumentosIdentidad() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (TipoDocumentoIdentidad objTipoDocumentoIdentidad : this.lstTiposDocumentoIdentidad) {
            comboBoxModel.addElement(objTipoDocumentoIdentidad.getNombre());
        }
        this.cmbDocumentoIdentidad.setModel(comboBoxModel);
    }

    @Override
    public void addObserver(IObserver objIObserver) {
        this.lstObservers.add(objIObserver);
    }

    @Override
    public List<IObserver> getObserverList() {
        return this.lstObservers;
    }

    @Override
    public void notifyObservers() {
        for (IObserver objObserver : this.lstObservers) {
            objObserver.update(this);
        }
    }

}
