package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.process.PagosProcessLogic;
import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.facade.ReportFacade;
import com.smartech.sistemacooperativa.dominio.Aporte;
import com.smartech.sistemacooperativa.dominio.Concepto;
import com.smartech.sistemacooperativa.dominio.Cuota;
import com.smartech.sistemacooperativa.dominio.Deposito;
import com.smartech.sistemacooperativa.dominio.Empleado;
import com.smartech.sistemacooperativa.dominio.HabilitacionCuotas;
import com.smartech.sistemacooperativa.dominio.ITransaccion;
import com.smartech.sistemacooperativa.dominio.MovimientoAdministrativo;
import com.smartech.sistemacooperativa.dominio.Retiro;
import com.smartech.sistemacooperativa.dominio.SolicitudPrestamo;
import com.smartech.sistemacooperativa.dominio.Tarjeta;
import com.smartech.sistemacooperativa.dominio.TipoCuenta;
import com.smartech.sistemacooperativa.dominio.TipoPago;
import com.smartech.sistemacooperativa.dominio.Transferencia;
import com.smartech.sistemacooperativa.dominio.TransferenciaCuenta;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import com.smartech.sistemacooperativa.util.interfaces.IObservable;
import com.smartech.sistemacooperativa.util.interfaces.IObserver;
import com.smartech.sistemacooperativa.util.reporting.ReportUtil;
import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import org.pentaho.reporting.engine.classic.core.parameters.ParameterDefinitionEntry;

/**
 *
 * @author Smartech
 */
public class IFrmConsultarPagos extends javax.swing.JInternalFrame implements IObserver, IObservable {

    private List<IObserver> lstObservers = new ArrayList<>();

    private List<Concepto> lstConceptos = new ArrayList<>();
    private List<ITransaccion> lstTransacciones = new ArrayList<>();
    private List<TipoCuenta> lstTiposCuenta = new ArrayList<>();
    private TableRowSorter trsTransacciones = new TableRowSorter();

    public IFrmConsultarPagos() {
        initComponents();

        this.bgpPagos.add(this.rdbEfectuados);
        this.bgpPagos.add(this.rdbHabilitados);
        this.rdbHabilitados.setSelected(true);

        this.btnImprimirComprobante.setVisible(false);

        this.dtpSinceDate.setDate(new Date());
        this.dtpToDate.setDate(new Date());

        this.updateLstConceptos();
        this.updateCmbConceptos();
        this.updateLstTiposCuenta();
        this.updateCmbTiposCuenta();

        this.update();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgpPagos = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblPagos = new javax.swing.JTable();
        rdbEfectuados = new javax.swing.JRadioButton();
        rdbHabilitados = new javax.swing.JRadioButton();
        jLabel1 = new javax.swing.JLabel();
        cmbConceptos = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        txtSocio = new javax.swing.JTextField();
        btnPagos = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        dtpSinceDate = new org.jdesktop.swingx.JXDatePicker();
        jLabel4 = new javax.swing.JLabel();
        dtpToDate = new org.jdesktop.swingx.JXDatePicker();
        btnActualizar = new javax.swing.JButton();
        btnImprimirComprobante = new javax.swing.JButton();
        lblTipoCuenta = new javax.swing.JLabel();
        cmbTipoCuenta = new javax.swing.JComboBox<>();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Pagos");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Pagos"));

        tblPagos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblPagos);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 612, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 228, Short.MAX_VALUE)
        );

        rdbEfectuados.setText("Efectuados");
        rdbEfectuados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbEfectuadosActionPerformed(evt);
            }
        });

        rdbHabilitados.setText("Habilitados");
        rdbHabilitados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbHabilitadosActionPerformed(evt);
            }
        });

        jLabel1.setText("Concepto:");

        cmbConceptos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbConceptosActionPerformed(evt);
            }
        });

        jLabel2.setText("Socio:");

        txtSocio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSocioKeyReleased(evt);
            }
        });

        btnPagos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/money-bag.png"))); // NOI18N
        btnPagos.setText("Pagar");
        btnPagos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPagosActionPerformed(evt);
            }
        });

        jLabel3.setText("Desde:");

        dtpSinceDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dtpSinceDateActionPerformed(evt);
            }
        });

        jLabel4.setText("Hasta:");

        dtpToDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dtpToDateActionPerformed(evt);
            }
        });

        btnActualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/actualizacion.png"))); // NOI18N
        btnActualizar.setText("Actualizar");
        btnActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarActionPerformed(evt);
            }
        });

        btnImprimirComprobante.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/printer24x24.png"))); // NOI18N
        btnImprimirComprobante.setText("Imprimir Comprobante");
        btnImprimirComprobante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirComprobanteActionPerformed(evt);
            }
        });

        lblTipoCuenta.setText("Tipo de cuenta:");

        cmbTipoCuenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTipoCuentaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtSocio)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cmbConceptos, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(rdbEfectuados)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdbHabilitados)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(dtpSinceDate, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(dtpToDate, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnActualizar))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnImprimirComprobante)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnPagos))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblTipoCuenta)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbTipoCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cmbConceptos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rdbEfectuados)
                    .addComponent(rdbHabilitados))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTipoCuenta)
                    .addComponent(cmbTipoCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(dtpSinceDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(dtpToDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnActualizar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPagos)
                    .addComponent(btnImprimirComprobante))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void rdbHabilitadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdbHabilitadosActionPerformed
        this.update();
        this.updateButtons();
        try {
            this.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(IFrmConsultarPagos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_rdbHabilitadosActionPerformed

    private void rdbEfectuadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdbEfectuadosActionPerformed
        this.update();
        this.updateButtons();
        try {
            this.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(IFrmConsultarPagos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_rdbEfectuadosActionPerformed

    private void btnPagosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPagosActionPerformed
        boolean ingresarTarjeta, generarPago = false;
        TipoPago objTipoPago = null;

        int index = this.tblPagos.getSelectedRow();

        if (index != -1 && PrincipalJRibbon.getInstance().getObjCaja() != null && PrincipalJRibbon.getInstance().getObjUsuarioLogeado().getObjTipoUsuario().isAccesocaja()) {
            index = this.tblPagos.convertRowIndexToModel(index);
            ITransaccion objTransaccion = this.lstTransacciones.get(index);
            ingresarTarjeta = this.ingresarTarjeta(objTransaccion);

            Concepto objConcepto = this.lstConceptos.get(this.cmbConceptos.getSelectedIndex());
            if (ingresarTarjeta) {
                DlgIngresarTarjeta dlgIngresarTarjeta = new DlgIngresarTarjeta(PrincipalJRibbon.getInstance().getFrame(), true, objConcepto.getId() == Concepto.CONCEPTO_RETIROS_CUENTA || objConcepto.getId() == Concepto.CONCEPTO_CUOTAS, objConcepto.getId() == Concepto.CONCEPTO_RETIROS_CUENTA);
                SwingUtil.centerOnScreen(dlgIngresarTarjeta);
                dlgIngresarTarjeta.setVisible(true);
                Tarjeta objTarjeta = dlgIngresarTarjeta.getObjTarjeta();

                if (objTarjeta == null) {
                    generarPago = false;
                    JOptionPane.showMessageDialog(this, "No se autentifico la Tarjeta", "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    if (objTransaccion instanceof Retiro) {
                        Retiro objRetiro = (Retiro) objTransaccion;
                        if (objRetiro.getObjCuenta().verificarSocio(objTarjeta.getObjSocio())) {
                            generarPago = PagosProcessLogic.getInstance().permiteSocioPagoTransaccion(objTransaccion, objTarjeta.getObjSocio());
                        } else {
                            JOptionPane.showMessageDialog(this, "El Socio no coincide con la transaccion", "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    if (objTransaccion instanceof Transferencia) {
                        Transferencia objTransferencia = (Transferencia) objTransaccion;
                        if (objTransferencia instanceof TransferenciaCuenta) {
                            TransferenciaCuenta objTransferenciaCuenta = (TransferenciaCuenta) objTransferencia;
                            if (objTransferenciaCuenta.getObjCuentaOrigen().verificarSocio(objTarjeta.getObjSocio())) {
                                generarPago = PagosProcessLogic.getInstance().permiteSocioPagoTransaccion(objTransaccion, objTarjeta.getObjSocio());
                            } else {
                                JOptionPane.showMessageDialog(this, "El Socio no coincide con la transaccion", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    }
                }
            } else {
                generarPago = true;
            }

            if (generarPago) {
                if (objConcepto.getId() != Concepto.CONCEPTO_RETIROS_CUENTA) {
                    DlgSeleccionarTipoPago dlgSeleccionarTipoPago = new DlgSeleccionarTipoPago(PrincipalJRibbon.getInstance().getFrame(), true);
                    SwingUtil.centerOnScreen(dlgSeleccionarTipoPago);
                    dlgSeleccionarTipoPago.setVisible(true);
                    objTipoPago = dlgSeleccionarTipoPago.getObjTipoPagoSeleccionado();

                    if (objTipoPago != null) {
                        int answer = JOptionPane.showConfirmDialog(this, "Pago por efectuar mediante " + objTipoPago.getNombre() + "\nDesea continuar?", "Pagos", JOptionPane.INFORMATION_MESSAGE);

                        if (answer == JOptionPane.YES_OPTION) {
//                            if (objTipoPago.getId() == 1) {
                            generarPago = true;
//                            }
                        } else {
                            generarPago = false;
                        }
                    } else {
                        generarPago = false;
                    }
                } else {
                    generarPago = true;
                }
            }

            if (generarPago) {
                BigDecimal monto = objTransaccion.getMonto();
                boolean result = true;
                if (!(objTransaccion instanceof Retiro)) {
                    DlgRecibidor dlgRecibidor = new DlgRecibidor(PrincipalJRibbon.getInstance().getFrame(), true, monto);
                    SwingUtil.centerOnScreen(dlgRecibidor);
                    dlgRecibidor.setVisible(true);
                    objTipoPago = QueryFacade.getInstance().getTipoPago(1);
                    result = dlgRecibidor.isEfectuado();
                } else {
                    objTipoPago = QueryFacade.getInstance().getTipoPago(1);
                }
                if (result) {
                    String mensaje = PagosProcessLogic.getInstance().generarPago(objTransaccion, objTipoPago, PrincipalJRibbon.getInstance().getObjUsuarioLogeado(), PrincipalJRibbon.getInstance().getObjCaja(), (Empleado) PrincipalJRibbon.getInstance().getObjPersona(), PrincipalJRibbon.getInstance().getObjEstablecimiento());

                    if (!mensaje.contains("Error")) {
                        this.update();
                        this.notifyObservers();
                        int answer = JOptionPane.showConfirmDialog(this, mensaje + "\nDesea imprimir el comprobante?", "Solicitud", JOptionPane.YES_NO_OPTION);
                        if (answer == JOptionPane.YES_OPTION) {
                            this.imprimirComprobante(objTransaccion, false);
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, mensaje, "Error", JOptionPane.ERROR_MESSAGE);
                    }

                } else {
                    JOptionPane.showMessageDialog(this, "No se efectuo la recepcion del efectivo", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(this, "Error al procesar el pago", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un pago", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnPagosActionPerformed

    private boolean ingresarTarjeta(ITransaccion objTransacion) {
        return (objTransacion instanceof Retiro || objTransacion instanceof Transferencia);
    }

    private void dtpSinceDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dtpSinceDateActionPerformed
        this.update();
        try {
            this.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(IFrmConsultarPagos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_dtpSinceDateActionPerformed

    private void cmbConceptosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbConceptosActionPerformed
        this.update();
        this.updateButtons();
        try {
            this.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(IFrmConsultarPagos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_cmbConceptosActionPerformed

    private void dtpToDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dtpToDateActionPerformed
        this.update();
        try {
            this.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(IFrmConsultarPagos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_dtpToDateActionPerformed

    private void btnActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarActionPerformed
        this.update();
        try {
            this.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(IFrmConsultarPagos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnActualizarActionPerformed

    private void btnImprimirComprobanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirComprobanteActionPerformed
        int index = this.tblPagos.getSelectedRow();

        try {
            if (index != -1) {
                index = this.tblPagos.convertRowIndexToModel(index);
                ITransaccion objTransaccion = this.lstTransacciones.get(index);
                this.imprimirComprobante(objTransaccion, true);
            } else {
                JOptionPane.showMessageDialog(this, "Seleccione un pago", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnImprimirComprobanteActionPerformed

    private void txtSocioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSocioKeyReleased
        SwingUtil.textFilter(evt, this.tblPagos, this, this.txtSocio, this.trsTransacciones);
    }//GEN-LAST:event_txtSocioKeyReleased

    private void cmbTipoCuentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTipoCuentaActionPerformed
        this.update();
        this.updateButtons();
        try {
            this.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(IFrmConsultarPagos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_cmbTipoCuentaActionPerformed

    //<editor-fold defaultstate="collapsed" desc="Variables">
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bgpPagos;
    private javax.swing.JButton btnActualizar;
    private javax.swing.JButton btnImprimirComprobante;
    private javax.swing.JButton btnPagos;
    private javax.swing.JComboBox<String> cmbConceptos;
    private javax.swing.JComboBox<String> cmbTipoCuenta;
    private org.jdesktop.swingx.JXDatePicker dtpSinceDate;
    private org.jdesktop.swingx.JXDatePicker dtpToDate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblTipoCuenta;
    private javax.swing.JRadioButton rdbEfectuados;
    private javax.swing.JRadioButton rdbHabilitados;
    private javax.swing.JTable tblPagos;
    private javax.swing.JTextField txtSocio;
    // End of variables declaration//GEN-END:variables
    //</editor-fold>

    private void imprimirComprobante(ITransaccion objTransaccion, boolean mostrarReporte) {
        try {
            Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> reportData = ReportFacade.getInstance().imprimirComprobante(objTransaccion, PrincipalJRibbon.getInstance().getObjEstablecimiento());
            if (mostrarReporte) {
                if (objTransaccion instanceof Aporte) {
                    ReportUtil.showReport(PrincipalJRibbon.getInstance(), new URL("file:rpt_cooperativa_comprobante_aporte.prpt"), ReportUtil.getDataOnly(reportData), ReportUtil.getParametersOnly(reportData));
                } else if (objTransaccion instanceof Deposito) {
                    ReportUtil.showReport(PrincipalJRibbon.getInstance(), new URL("file:rpt_cooperativa_comprobante_deposito.prpt"), ReportUtil.getDataOnly(reportData), ReportUtil.getParametersOnly(reportData));
                } else if (objTransaccion instanceof Retiro) {
                    ReportUtil.showReport(PrincipalJRibbon.getInstance(), new URL("file:rpt_cooperativa_comprobante_retiro.prpt"), ReportUtil.getDataOnly(reportData), ReportUtil.getParametersOnly(reportData));
                } else if (objTransaccion instanceof TransferenciaCuenta) {
                    ReportUtil.showReport(PrincipalJRibbon.getInstance(), new URL("file:rpt_cooperativa_comprobante_transferencia.prpt"), ReportUtil.getDataOnly(reportData), ReportUtil.getParametersOnly(reportData));
                } else if (objTransaccion instanceof HabilitacionCuotas) {
                    ReportUtil.showReport(PrincipalJRibbon.getInstance(), new URL("file:rpt_cooperativa_comprobante_cuota.prpt"), ReportUtil.getDataOnly(reportData), ReportUtil.getParametersOnly(reportData));
                } else if (objTransaccion instanceof MovimientoAdministrativo) {
                    ReportUtil.showReport(PrincipalJRibbon.getInstance(), new URL("file:rpt_cooperativa_comprobante_movimientoadministrativo.prpt"), ReportUtil.getDataOnly(reportData), ReportUtil.getParametersOnly(reportData));
                } else if (objTransaccion instanceof SolicitudPrestamo) {
                    ReportUtil.showReport(PrincipalJRibbon.getInstance(), new URL("file:rpt_cooperativa_comprobante_solicitud_prestamo.prpt"), ReportUtil.getDataOnly(reportData), ReportUtil.getParametersOnly(reportData));
                }
            } else //ReportUtil.printReport(new URL("file:imp_factura.prpt"), this.convert(objImpresionFactura), ConfigFacade.getTipoImpresion());
             if (objTransaccion instanceof Aporte) {
                    //ReportUtil.printReport(new URL("file:rpt_cooperativa_comprobante_aporte.prpt"), Report, title);
                    ReportUtil.showReport(PrincipalJRibbon.getInstance(), new URL("file:rpt_cooperativa_comprobante_aporte.prpt"), ReportUtil.getDataOnly(reportData), ReportUtil.getParametersOnly(reportData));
                } else if (objTransaccion instanceof Deposito) {
                    ReportUtil.showReport(PrincipalJRibbon.getInstance(), new URL("file:rpt_cooperativa_comprobante_deposito.prpt"), ReportUtil.getDataOnly(reportData), ReportUtil.getParametersOnly(reportData));
                } else if (objTransaccion instanceof Retiro) {
                    ReportUtil.showReport(PrincipalJRibbon.getInstance(), new URL("file:rpt_cooperativa_comprobante_retiro.prpt"), ReportUtil.getDataOnly(reportData), ReportUtil.getParametersOnly(reportData));
                } else if (objTransaccion instanceof TransferenciaCuenta) {
                    ReportUtil.showReport(PrincipalJRibbon.getInstance(), new URL("file:rpt_cooperativa_comprobante_transferencia.prpt"), ReportUtil.getDataOnly(reportData), ReportUtil.getParametersOnly(reportData));
                } else if (objTransaccion instanceof HabilitacionCuotas) {
                    ReportUtil.showReport(PrincipalJRibbon.getInstance(), new URL("file:rpt_cooperativa_comprobante_cuota.prpt"), ReportUtil.getDataOnly(reportData), ReportUtil.getParametersOnly(reportData));
                } else if (objTransaccion instanceof MovimientoAdministrativo) {
                    ReportUtil.showReport(PrincipalJRibbon.getInstance(), new URL("file:rpt_cooperativa_comprobante_movimientoadministrativo.prpt"), ReportUtil.getDataOnly(reportData), ReportUtil.getParametersOnly(reportData));
                } else if (objTransaccion instanceof SolicitudPrestamo) {
                    ReportUtil.showReport(PrincipalJRibbon.getInstance(), new URL("file:rpt_cooperativa_comprobante_solicitud_prestamo.prpt"), ReportUtil.getDataOnly(reportData), ReportUtil.getParametersOnly(reportData));
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateButtons() {
        this.btnPagos.setVisible(this.rdbHabilitados.isSelected() && this.lstConceptos.get(this.cmbConceptos.getSelectedIndex()).getId() != Concepto.CONCEPTO_TRANSFERENCIAS);
        this.btnImprimirComprobante.setVisible(this.rdbEfectuados.isSelected() || this.lstConceptos.get(this.cmbConceptos.getSelectedIndex()).getId() == Concepto.CONCEPTO_TRANSFERENCIAS);
    }

    private void updateLstTransacciones() {
        this.lstTransacciones = QueryFacade.getInstance().getAllTransacciones(this.lstConceptos.get(this.cmbConceptos.getSelectedIndex()), this.dtpSinceDate.getDate(), this.dtpToDate.getDate(), this.rdbHabilitados.isSelected() ? ITransaccion.HABILITADO : ITransaccion.PAGADO);
    }

    private void updateLstConceptos() {
        this.lstConceptos = QueryFacade.getInstance().getAllConceptosOnly();
    }

    private void updateLstTiposCuenta() {
        this.lstTiposCuenta = QueryFacade.getInstance().getAllTiposCuenta(false);
    }

    private void updateCmbConceptos() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (Concepto objConcepto : this.lstConceptos) {
            comboBoxModel.addElement(objConcepto.getNombre());
        }

        this.cmbConceptos.setModel(comboBoxModel);
    }

    private void updateCmbTiposCuenta() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (TipoCuenta objTipoCuenta : this.lstTiposCuenta) {
            comboBoxModel.addElement(objTipoCuenta.getNombre());
        }

        this.cmbTipoCuenta.setModel(comboBoxModel);
    }

    private void updateTblPagos() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        int concepto = this.lstConceptos.get(this.cmbConceptos.getSelectedIndex()).getId();
        if (concepto == Concepto.CONCEPTO_APORTES) {
            tableModel.addColumn("Socio");
            tableModel.addColumn("DNI Socio");
            tableModel.addColumn("Modo");
        }
        if (concepto == Concepto.CONCEPTO_DEPOSITOS) {
            tableModel.addColumn("N° de Cuenta");
            tableModel.addColumn("DNI Depositante");
        }
        if (concepto == Concepto.CONCEPTO_TRANSFERENCIAS) {
            tableModel.addColumn("Cuenta origen");
            tableModel.addColumn("Cuenta destino");
        }
        if (concepto == Concepto.CONCEPTO_RETIROS_CUENTA) {
            tableModel.addColumn("N° de Cuenta");
            //tableModel.addColumn("DNI Acreedor");
        }
        if (concepto == Concepto.CONCEPTO_CUOTAS || concepto == Concepto.CONCEPTO_SOLICITUD_PRESTAMO) {
            tableModel.addColumn("Socio");
            tableModel.addColumn("DNI Socio");
        }

        if (concepto == Concepto.CONCEPTO_RETIROS_SOCIO) {
            tableModel.addColumn("Socio");
            tableModel.addColumn("DNI Socio");
            tableModel.addColumn("Estado socio");
        }

        if (concepto == Concepto.CONCEPTO_EGRESO_CAJA_ADMINISTRATIVO || concepto == Concepto.CONCEPTO_INGRESO_CAJA_ADMINISTRATIVO) {
            tableModel.addColumn("Empleado");
            tableModel.addColumn("Descripcion");
        }

        tableModel.addColumn("Concepto");
        tableModel.addColumn("Monto");
        tableModel.addColumn("Moneda");

        tableModel.addColumn("Fecha");

        List<Object[]> lstRows = new ArrayList<>();
        Object[] row;

        String moneda;
        switch (this.lstConceptos.get(this.cmbConceptos.getSelectedIndex()).getId()) {
            case 1:
                Aporte objAporte;
                for (ITransaccion objTransaccion : this.lstTransacciones) {
                    objAporte = (Aporte) objTransaccion;
                    moneda = "PEN";

                    row = new Object[]{
                        objAporte.getObjTarjeta().getObjSocio().getNombreCompleto(),
                        objAporte.getObjTarjeta().getObjSocio().getDocumentoIdentidad(),
                        objAporte.isRetiro() ? "RETIRO" : "ABONO",
                        this.lstConceptos.get(this.cmbConceptos.getSelectedIndex()).getNombre(),
                        objAporte.getMonto().toPlainString(),
                        moneda,
                        DateUtil.getRegularDate(DateUtil.getDate(objAporte.getFechaRegistro()))
                    };
                    lstRows.add(row);
                }
                break;
            case 2:
                Deposito objDeposito;
                this.filterTransacciones(this.lstTransacciones, this.lstTiposCuenta.get(this.cmbTipoCuenta.getSelectedIndex()));
                for (ITransaccion objTransaccion : this.lstTransacciones) {
                    objDeposito = (Deposito) objTransaccion;

                    moneda = objDeposito.getObjCuenta().getObjTipoCuenta().getObjMoneda().getAbreviatura();

                    row = new Object[]{
                        objDeposito.getObjCuenta().getCodigo(),
                        objDeposito.getDniDepositante(),
                        this.lstConceptos.get(this.cmbConceptos.getSelectedIndex()).getNombre(),
                        objDeposito.getMonto().toPlainString(),
                        moneda,
                        DateUtil.getRegularDate(DateUtil.getDate(objDeposito.getFechaRegistro()))
                    };
                    lstRows.add(row);
                }
                break;
            case Concepto.CONCEPTO_TRANSFERENCIAS:
                TransferenciaCuenta objTransferenciaCuenta;
                this.filterTransacciones(this.lstTransacciones, this.lstTiposCuenta.get(this.cmbTipoCuenta.getSelectedIndex()));
                for (ITransaccion objTransaccion : this.lstTransacciones) {
                    objTransferenciaCuenta = (TransferenciaCuenta) objTransaccion;

                    row = new Object[]{
                        objTransferenciaCuenta.getObjCuentaOrigen().getCodigo(),
                        objTransferenciaCuenta.getObjCuentaDestino().getCodigo(),
                        this.lstConceptos.get(this.cmbConceptos.getSelectedIndex()).getNombre(),
                        objTransferenciaCuenta.getMonto().toPlainString(),
                        objTransferenciaCuenta.getObjCuentaOrigen().getObjTipoCuenta().getObjMoneda().getAbreviatura(),
                        DateUtil.getRegularDate(DateUtil.getDate(objTransferenciaCuenta.getFechaRegistro()))
                    };
                    lstRows.add(row);
                }
                break;
            case 4:
                Retiro objRetiro;
                this.filterTransacciones(this.lstTransacciones, this.lstTiposCuenta.get(this.cmbTipoCuenta.getSelectedIndex()));
                for (ITransaccion objTransaccion : this.lstTransacciones) {
                    objRetiro = (Retiro) objTransaccion;

                    moneda = objRetiro.getObjCuenta().getObjTipoCuenta().getObjMoneda().getAbreviatura();

                    row = new Object[]{
                        objRetiro.getObjCuenta().getCodigo(),
                        //objRetiro.getAcreedor(),
                        this.lstConceptos.get(this.cmbConceptos.getSelectedIndex()).getNombre(),
                        objRetiro.getMonto().toPlainString(),
                        moneda,
                        DateUtil.getRegularDate(DateUtil.getDate(objRetiro.getFechaRegistro()))
                    };
                    lstRows.add(row);
                }
                break;
            case 5:
                HabilitacionCuotas objHabilitacionCuotas;
                for (ITransaccion objTransaccion : this.lstTransacciones) {
                    objHabilitacionCuotas = (HabilitacionCuotas) objTransaccion;

                    moneda = objHabilitacionCuotas.getLstCuotas().get(0).getObjPrestamo().getObjSolicitudPrestamo().getObjTipoPrestamo().getObjMoneda().getAbreviatura();

                    row = new Object[]{
                        objHabilitacionCuotas.getLstCuotas().get(0).getObjPrestamo().getObjSolicitudPrestamo().getObjSocioPrestatario().getNombreCompleto(),
                        objHabilitacionCuotas.getLstCuotas().get(0).getObjPrestamo().getObjSolicitudPrestamo().getObjSocioPrestatario().getDocumentoIdentidad(),
                        this.lstConceptos.get(this.cmbConceptos.getSelectedIndex()).getNombre(),
                        objHabilitacionCuotas.getMonto().toPlainString(),
                        moneda,
                        DateUtil.getRegularDate(DateUtil.getDate(objHabilitacionCuotas.getFechaRegistro()))
                    };
                    lstRows.add(row);
                }
                break;
            case Concepto.CONCEPTO_RETIROS_SOCIO:
                for (ITransaccion objTransaccion : this.lstTransacciones) {
                    objAporte = (Aporte) objTransaccion;
                    moneda = "PEN";

                    row = new Object[]{
                        objAporte.getObjTarjeta().getObjSocio().getNombreCompleto(),
                        objAporte.getObjTarjeta().getObjSocio().getDocumentoIdentidad(),
                        objAporte.isRetiro() ? "RETIRO" : "INGRESO",
                        this.lstConceptos.get(this.cmbConceptos.getSelectedIndex()).getNombre(),
                        objAporte.getMonto().toPlainString(),
                        moneda,
                        DateUtil.getRegularDate(DateUtil.getDate(objAporte.getFechaRegistro()))
                    };
                    lstRows.add(row);
                }
                break;
            case Concepto.CONCEPTO_INGRESO_CAJA_ADMINISTRATIVO:
                for (ITransaccion objTransaccion : this.lstTransacciones) {
                    moneda = "PEN";
                    MovimientoAdministrativo objMovimientoAdministrativo = (MovimientoAdministrativo) objTransaccion;

                    row = new Object[]{
                        objMovimientoAdministrativo.getObjEmpleado().getNombreCompleto(),
                        objMovimientoAdministrativo.getDescripcion(),
                        this.lstConceptos.get(this.cmbConceptos.getSelectedIndex()).getNombre(),
                        objMovimientoAdministrativo.getMonto().toPlainString(),
                        moneda,
                        DateUtil.getRegularDate(DateUtil.getDate(objMovimientoAdministrativo.getFechaRegistro()))
                    };
                    lstRows.add(row);
                }
                break;

            case Concepto.CONCEPTO_EGRESO_CAJA_ADMINISTRATIVO:
                for (ITransaccion objTransaccion : this.lstTransacciones) {
                    moneda = "PEN";
                    MovimientoAdministrativo objMovimientoAdministrativo = (MovimientoAdministrativo) objTransaccion;

                    row = new Object[]{
                        objMovimientoAdministrativo.getObjEmpleado().getNombreCompleto(),
                        objMovimientoAdministrativo.getDescripcion(),
                        this.lstConceptos.get(this.cmbConceptos.getSelectedIndex()).getNombre(),
                        objMovimientoAdministrativo.getMonto().toPlainString(),
                        moneda,
                        DateUtil.getRegularDate(DateUtil.getDate(objMovimientoAdministrativo.getFechaRegistro()))
                    };
                    lstRows.add(row);
                }
                break;

            case Concepto.CONCEPTO_SOLICITUD_PRESTAMO:
                for (ITransaccion objTransaccion : this.lstTransacciones) {
                    moneda = "PEN";
                    SolicitudPrestamo objSolicitudPrestamo = (SolicitudPrestamo) objTransaccion;

                    row = new Object[]{
                        objSolicitudPrestamo.getObjSocioPrestatario().getNombreCompleto(),
                        objSolicitudPrestamo.getObjSocioPrestatario().getDocumentoIdentidad(),
                        this.lstConceptos.get(this.cmbConceptos.getSelectedIndex()).getNombre(),
                        objSolicitudPrestamo.getMontoAprobado().toPlainString(),
                        moneda,
                        DateUtil.getRegularDate(DateUtil.getDate(objSolicitudPrestamo.getFechaRegistro()))
                    };
                    lstRows.add(row);
                }
                break;
        }

        for (Object[] rowObject : lstRows) {
            tableModel.addRow(rowObject);
        }

        this.tblPagos.setModel(tableModel);
        this.trsTransacciones.setModel(tableModel);
        this.tblPagos.setRowSorter(this.trsTransacciones);
    }

    private void update() {
        this.updateLstTransacciones();
        int concepto = this.lstConceptos.get(this.cmbConceptos.getSelectedIndex()).getId();
        if (concepto == Concepto.CONCEPTO_DEPOSITOS || concepto == Concepto.CONCEPTO_RETIROS_CUENTA || concepto == Concepto.CONCEPTO_TRANSFERENCIAS) {
            this.lblTipoCuenta.setVisible(true);
            this.cmbTipoCuenta.setVisible(true);
        } else {
            this.lblTipoCuenta.setVisible(false);
            this.cmbTipoCuenta.setVisible(false);
        }

        pack();
        validate();

        this.updateTblPagos();
    }

    private void filterTransacciones(List<ITransaccion> lstTransacciones, TipoCuenta objTipoCuenta) {
        List<ITransaccion> lstTransaccionesToReplace = new ArrayList<>();
        for (ITransaccion objITransaccion : lstTransacciones) {
            if (objITransaccion instanceof Deposito) {
                Deposito objDeposito = (Deposito) objITransaccion;
                if (objDeposito.getObjCuenta().getObjTipoCuenta().getId() == objTipoCuenta.getId()) {
                    lstTransaccionesToReplace.add(objITransaccion);
                }
            } else if (objITransaccion instanceof Retiro) {
                Retiro objRetiro = (Retiro) objITransaccion;
                if (objRetiro.getObjCuenta().getObjTipoCuenta().getId() == objTipoCuenta.getId()) {
                    lstTransaccionesToReplace.add(objITransaccion);
                }
            } else if (objITransaccion instanceof Transferencia) {
                Transferencia objTransferencia = (Transferencia) objITransaccion;
                if (objTransferencia instanceof TransferenciaCuenta) {
                    TransferenciaCuenta objTransferenciaCuenta = (TransferenciaCuenta) objTransferencia;
                    if (objTransferenciaCuenta.getObjCuentaOrigen().getObjTipoCuenta().getId() == objTipoCuenta.getId()) {
                        lstTransaccionesToReplace.add(objITransaccion);
                    }
                }
            }
        }
        lstTransacciones.clear();
        lstTransacciones.addAll(lstTransaccionesToReplace);
    }

    @Override
    public void update(IObservable objIObservable) {
        this.update();
    }

    @Override
    public void addObserver(IObserver objIObserver) {
        this.lstObservers.add(objIObserver);
    }

    @Override
    public List<IObserver> getObserverList() {
        return this.lstObservers;
    }

    @Override
    public void notifyObservers() {
        for (IObserver observer : this.lstObservers) {
            observer.update(this);
        }
    }
}
