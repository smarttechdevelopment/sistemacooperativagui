package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.facade.ReportFacade;
import com.smartech.sistemacooperativa.bll.process.PrestamosProcessLogic;
import com.smartech.sistemacooperativa.dominio.SolicitudPrestamo;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import com.smartech.sistemacooperativa.util.interfaces.IObservable;
import com.smartech.sistemacooperativa.util.interfaces.IObserver;
import com.smartech.sistemacooperativa.util.reporting.ReportUtil;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import org.pentaho.reporting.engine.classic.core.parameters.ParameterDefinitionEntry;
import org.pentaho.reporting.engine.classic.core.parameters.PlainParameter;

/**
 *
 * @author Smartech
 */
public class IFrmConsultarSolicitudesPrestamo extends javax.swing.JInternalFrame implements IObserver {

    private PrincipalJRibbon principalJRibbon;
    private List<SolicitudPrestamo> lstSolicitudesPrestamo = new ArrayList<>();
    private TableRowSorter trsSolicitudesPrestamo = new TableRowSorter();

    public IFrmConsultarSolicitudesPrestamo(PrincipalJRibbon principalJRibbon) {
        initComponents();

        this.principalJRibbon = principalJRibbon;
        this.updateLstSolicitudesPendientes();
        this.updateTblSolicitudesPendientes();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtSocio = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblSolicitudesPendientes = new javax.swing.JTable();
        btnActualizarPendientes = new javax.swing.JButton();
        btnEliminarPendientesPago = new javax.swing.JButton();
        btnNuevaSolicitudIngreso = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Solicitudes de Prestamo");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Solicitudes Pendientes"));

        jLabel1.setText("Buscar por socio:");

        txtSocio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSocioKeyReleased(evt);
            }
        });

        tblSolicitudesPendientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblSolicitudesPendientes);

        btnActualizarPendientes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/actualizacion.png"))); // NOI18N
        btnActualizarPendientes.setText("Actualizar");
        btnActualizarPendientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarPendientesActionPerformed(evt);
            }
        });

        btnEliminarPendientesPago.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/basura.png"))); // NOI18N
        btnEliminarPendientesPago.setText("Eliminar");
        btnEliminarPendientesPago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarPendientesPagoActionPerformed(evt);
            }
        });

        btnNuevaSolicitudIngreso.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/nuevo.png"))); // NOI18N
        btnNuevaSolicitudIngreso.setText("Nuevo");
        btnNuevaSolicitudIngreso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevaSolicitudIngresoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtSocio))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 692, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(btnEliminarPendientesPago)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnActualizarPendientes)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnNuevaSolicitudIngreso)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 221, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnNuevaSolicitudIngreso, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnEliminarPendientesPago, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnActualizarPendientes))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(4, 4, 4))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnActualizarPendientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarPendientesActionPerformed
        int index = this.tblSolicitudesPendientes.getSelectedRow();

        if (index != -1) {
            index = this.tblSolicitudesPendientes.convertRowIndexToModel(index);
            SolicitudPrestamo objSolicitudPrestamo = this.lstSolicitudesPrestamo.get(index);

            IFrmRegistroSolicitudPrestamo frmRegistroSolicitudPrestamo = new IFrmRegistroSolicitudPrestamo(this.principalJRibbon, objSolicitudPrestamo);
            frmRegistroSolicitudPrestamo.addObserver(this);

            this.principalJRibbon.getDesktopPane().add(frmRegistroSolicitudPrestamo);
            frmRegistroSolicitudPrestamo.setVisible(true);

            try {
                frmRegistroSolicitudPrestamo.setMaximum(true);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione una Solicitud", "Solicitud", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnActualizarPendientesActionPerformed

    private void btnNuevaSolicitudIngresoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevaSolicitudIngresoActionPerformed
        IFrmRegistroSolicitudPrestamo frmRegistroSolicitudPrestamo = new IFrmRegistroSolicitudPrestamo(this.principalJRibbon);
        frmRegistroSolicitudPrestamo.addObserver(this);

        this.principalJRibbon.getDesktopPane().add(frmRegistroSolicitudPrestamo);
        frmRegistroSolicitudPrestamo.setVisible(true);

        try {
            frmRegistroSolicitudPrestamo.setMaximum(true);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }//GEN-LAST:event_btnNuevaSolicitudIngresoActionPerformed

    private void btnEliminarPendientesPagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarPendientesPagoActionPerformed
        int index = this.tblSolicitudesPendientes.getSelectedRow();
        
        if(index != -1){
            index = this.tblSolicitudesPendientes.convertRowIndexToModel(index);
            int answer = JOptionPane.showConfirmDialog(this, "Desea eliminar la solicitud?\nEsta operacion no podra ser cancelada posteriormente", "Solicitud", JOptionPane.YES_NO_OPTION);
            if(answer == JOptionPane.YES_OPTION){
                String message = PrestamosProcessLogic.getInstance().eliminarSolicitud(this.lstSolicitudesPrestamo.get(index));
                
                JOptionPane.showMessageDialog(this, message, "Solicitud", message.contains("Error") ? JOptionPane.ERROR_MESSAGE : JOptionPane.INFORMATION_MESSAGE);
            }
        }else{
            JOptionPane.showMessageDialog(this, "Seleccione una Solicitud", "Solicitud", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnEliminarPendientesPagoActionPerformed

    private void txtSocioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSocioKeyReleased
        SwingUtil.textFilter(evt, this.tblSolicitudesPendientes, this, this.txtSocio, this.trsSolicitudesPrestamo);
    }//GEN-LAST:event_txtSocioKeyReleased

    //<editor-fold defaultstate="collapsed" desc="Variables">
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActualizarPendientes;
    private javax.swing.JButton btnEliminarPendientesPago;
    private javax.swing.JButton btnNuevaSolicitudIngreso;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblSolicitudesPendientes;
    private javax.swing.JTextField txtSocio;
    // End of variables declaration//GEN-END:variables
//</editor-fold>

    private void updateLstSolicitudesPendientes() {
        this.lstSolicitudesPrestamo = QueryFacade.getInstance().getAllSolicitudesPrestamosPorAprobar();
    }

    private void updateTblSolicitudesPendientes() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tableModel.addColumn("Persona");
        tableModel.addColumn("DNI");
        tableModel.addColumn("Fecha de Solicitud");

        for (SolicitudPrestamo objSolicitudPrestamo : this.lstSolicitudesPrestamo) {
            tableModel.addRow(new Object[]{
                objSolicitudPrestamo.getObjSocioPrestatario().getNombreCompleto(),
                objSolicitudPrestamo.getObjSocioPrestatario().getDocumentoIdentidad(),
                DateUtil.getRegularDate(DateUtil.getDate(objSolicitudPrestamo.getFechaRegistro()))
            });
        }

        this.tblSolicitudesPendientes.setModel(tableModel);
        this.trsSolicitudesPrestamo.setModel(tableModel);
        this.tblSolicitudesPendientes.setRowSorter(this.trsSolicitudesPrestamo);
    }

    @Override
    public void update(IObservable objIObservable) {
        this.updateLstSolicitudesPendientes();
        this.updateTblSolicitudesPendientes();
    }
}
