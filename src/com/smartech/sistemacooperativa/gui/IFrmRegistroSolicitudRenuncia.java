package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.process.PrestamosProcessLogic;
import com.smartech.sistemacooperativa.bll.process.RenunciaSocioProcessLogic;
import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.DetalleDocumentoSolicitud;
import com.smartech.sistemacooperativa.dominio.Documento;
import com.smartech.sistemacooperativa.dominio.Empleado;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.SolicitudRetiroSocio;
import com.smartech.sistemacooperativa.dominio.TipoDocumentoIdentidad;
import com.smartech.sistemacooperativa.dominio.TipoSolicitud;
import com.smartech.sistemacooperativa.gui.util.AWTUtil;
import com.smartech.sistemacooperativa.gui.util.MyFingerprintVerification;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Smartech
 */
public class IFrmRegistroSolicitudRenuncia extends javax.swing.JInternalFrame {

    private static final int OPCION_COMBO_DOCUMENTOIDENTIDAD = 0;
    private static final int OPCION_COMBO_RUC = 1;
    private static final int OPCION_COMBO_TARJETA = 2;

    private Socio objSocio = null;
    private SolicitudRetiroSocio objSolicitudRetiroSocio = null;

    private Map<Documento, File> mapArchivos = new HashMap<>();
    private List<DetalleDocumentoSolicitud> lstDetalleDocumentoSolicitud = new ArrayList<>();

    public IFrmRegistroSolicitudRenuncia(PrincipalJRibbon principalJRibbon) {
        initComponents();
        AWTUtil.setUpperCase();
        this.setDefaultConfiguration();

        pack();
    }

    private void setDefaultConfiguration() {
        this.setCmbModoBuscarSocio();
        this.setTextInputBuscarSocio();
        this.setTextButtonIngresarTarjeta();

        this.btnIngresarTarjeta.setEnabled(true);

        this.txtEstadoSocio.setEditable(false);
        this.txtFechaIngresoSocio.setEditable(false);
        this.txtSaldoAportes.setEditable(false);
        this.txtSaldoAhorro.setEditable(false);
        this.txtaNotificacion.setEditable(false);

        this.pnlGarantiasPendientes.setVisible(false);
        this.pnlSocios.setVisible(false);
        this.txtTotalDescuentos.setEditable(false);

        this.btnAprobarDocumento.setEnabled(false);
        this.btnDesaprobarDocumento.setEnabled(false);
        this.btnCargarDocumento.setEnabled(false);
        this.btnGuardarSolicitudRenuncia.setEnabled(false);

        this.txtBuscarSocio.requestFocusInWindow();
    }

    private void setTextInputBuscarSocio() {
        if (this.cmbModoBuscarSocio.getSelectedIndex() < 0) {
            this.cmbModoBuscarSocio.setSelectedIndex(OPCION_COMBO_DOCUMENTOIDENTIDAD);
        } else {
            int selectedIndex = this.cmbModoBuscarSocio.getSelectedIndex();

            if (selectedIndex == OPCION_COMBO_TARJETA) {
                this.txtBuscarSocio.setText("");
                this.txtBuscarSocio.setEditable(false);
            } else {
                this.txtBuscarSocio.setEditable(true);
                if (selectedIndex == OPCION_COMBO_DOCUMENTOIDENTIDAD) {
                    SwingUtil.setDocumentoIdentidadInput(new TipoDocumentoIdentidad(), this.txtBuscarSocio);
                } else if (selectedIndex == OPCION_COMBO_RUC) {
                    SwingUtil.setRUCInput(this.txtBuscarSocio);
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane6 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cmbModoBuscarSocio = new javax.swing.JComboBox<>();
        txtBuscarSocio = new javax.swing.JTextField();
        btnIngresarTarjeta = new javax.swing.JButton();
        pnlSocios = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblSociosRelacionados = new javax.swing.JTable();
        btnAprobarSocio = new javax.swing.JButton();
        btnDesaprobarSocio = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtEstadoSocio = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtSaldoAportes = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblCuentas = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        txtSaldoAhorro = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtFechaIngresoSocio = new javax.swing.JTextField();
        jScrollPane5 = new javax.swing.JScrollPane();
        txtaNotificacion = new javax.swing.JTextArea();
        btnGuardarSolicitudRenuncia = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        pnlGarantiasPendientes = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblGarantìasPendientes = new javax.swing.JTable();
        txtTotalDescuentos = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        btnAprobarDocumento = new javax.swing.JButton();
        btnDesaprobarDocumento = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblDocumentos = new javax.swing.JTable();
        btnCargarDocumento = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setTitle("Registrar Renuncia");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Buscar socio"));

        jLabel1.setText("Buscar:");

        cmbModoBuscarSocio.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Selecciona" }));
        cmbModoBuscarSocio.setRequestFocusEnabled(false);
        cmbModoBuscarSocio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbModoBuscarSocioActionPerformed(evt);
            }
        });

        btnIngresarTarjeta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/credit-card.png"))); // NOI18N
        btnIngresarTarjeta.setText("Ingresar tarjeta");
        btnIngresarTarjeta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIngresarTarjetaActionPerformed(evt);
            }
        });

        pnlSocios.setBorder(javax.swing.BorderFactory.createTitledBorder("Socios relacionados a cuentas"));

        tblSociosRelacionados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblSociosRelacionados);

        btnAprobarSocio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_checked.png"))); // NOI18N
        btnAprobarSocio.setText("Aprobar");
        btnAprobarSocio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAprobarSocioActionPerformed(evt);
            }
        });

        btnDesaprobarSocio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_cancel.png"))); // NOI18N
        btnDesaprobarSocio.setText("Desaprobar");
        btnDesaprobarSocio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDesaprobarSocioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlSociosLayout = new javax.swing.GroupLayout(pnlSocios);
        pnlSocios.setLayout(pnlSociosLayout);
        pnlSociosLayout.setHorizontalGroup(
            pnlSociosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSociosLayout.createSequentialGroup()
                .addComponent(jScrollPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlSociosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnDesaprobarSocio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAprobarSocio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );
        pnlSociosLayout.setVerticalGroup(
            pnlSociosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE)
            .addGroup(pnlSociosLayout.createSequentialGroup()
                .addComponent(btnAprobarSocio)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnDesaprobarSocio))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos de socio"));

        jLabel2.setText("Estado:");

        jLabel3.setText("Saldo de aportes:");

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Cuentas"));
        jPanel4.setMinimumSize(new java.awt.Dimension(100, 100));
        jPanel4.setPreferredSize(new java.awt.Dimension(700, 23));

        tblCuentas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(tblCuentas);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
        );

        jLabel4.setText("Saldo de ahorro:");

        jLabel5.setText("Fecha de ingreso:");

        txtaNotificacion.setColumns(20);
        txtaNotificacion.setRows(5);
        jScrollPane5.setViewportView(txtaNotificacion);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtEstadoSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtFechaIngresoSocio))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtSaldoAportes, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtSaldoAhorro, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 248, Short.MAX_VALUE))
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, 794, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtEstadoSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2)
                            .addComponent(jLabel5)
                            .addComponent(txtFechaIngresoSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtSaldoAportes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel4)
                                .addComponent(txtSaldoAhorro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, 113, Short.MAX_VALUE))
        );

        btnGuardarSolicitudRenuncia.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_save.png"))); // NOI18N
        btnGuardarSolicitudRenuncia.setText("Guardar");
        btnGuardarSolicitudRenuncia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarSolicitudRenunciaActionPerformed(evt);
            }
        });

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder("Requisitos de Cooperativa"));

        pnlGarantiasPendientes.setBorder(javax.swing.BorderFactory.createTitledBorder("Garantìas pendientes"));
        pnlGarantiasPendientes.setMinimumSize(new java.awt.Dimension(400, 100));
        pnlGarantiasPendientes.setName(""); // NOI18N

        tblGarantìasPendientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(tblGarantìasPendientes);

        jLabel6.setText("Total de descuentos:");

        javax.swing.GroupLayout pnlGarantiasPendientesLayout = new javax.swing.GroupLayout(pnlGarantiasPendientes);
        pnlGarantiasPendientes.setLayout(pnlGarantiasPendientesLayout);
        pnlGarantiasPendientesLayout.setHorizontalGroup(
            pnlGarantiasPendientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlGarantiasPendientesLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(pnlGarantiasPendientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pnlGarantiasPendientesLayout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTotalDescuentos, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 315, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        pnlGarantiasPendientesLayout.setVerticalGroup(
            pnlGarantiasPendientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlGarantiasPendientesLayout.createSequentialGroup()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlGarantiasPendientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTotalDescuentos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)))
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder("Documentos"));
        jPanel6.setMinimumSize(new java.awt.Dimension(400, 100));

        btnAprobarDocumento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_checked.png"))); // NOI18N
        btnAprobarDocumento.setText("Aprobar Documento");
        btnAprobarDocumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAprobarDocumentoActionPerformed(evt);
            }
        });

        btnDesaprobarDocumento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_cancel.png"))); // NOI18N
        btnDesaprobarDocumento.setText("Desaprobar Documento");
        btnDesaprobarDocumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDesaprobarDocumentoActionPerformed(evt);
            }
        });

        tblDocumentos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane4.setViewportView(tblDocumentos);

        btnCargarDocumento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_upload.png"))); // NOI18N
        btnCargarDocumento.setText("Cargar Documento");
        btnCargarDocumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCargarDocumentoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnDesaprobarDocumento)
                    .addComponent(btnAprobarDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCargarDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(btnAprobarDocumento)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDesaprobarDocumento)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCargarDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlGarantiasPendientes, javax.swing.GroupLayout.PREFERRED_SIZE, 349, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(51, 51, 51))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlGarantiasPendientes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 799, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(btnGuardarSolicitudRenuncia))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cmbModoBuscarSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 291, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtBuscarSocio)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnIngresarTarjeta))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(pnlSocios, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBuscarSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnIngresarTarjeta)
                    .addComponent(cmbModoBuscarSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlSocios, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnGuardarSolicitudRenuncia))
        );

        jScrollPane6.setViewportView(jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane6)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane6)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private SolicitudRetiroSocio crearNuevaSolicitudRetiroSocio(Socio objSocioSolicitante) {
        TipoSolicitud objTipoSolicitud = QueryFacade.getInstance().getTipoSolicitud(3, PrincipalJRibbon.getInstance().getObjEstablecimiento());
        return new SolicitudRetiroSocio(
                0,
                "",
                false,
                true,
                objSocioSolicitante,
                RenunciaSocioProcessLogic.getInstance().getEstadoActividadSocio(objSocioSolicitante),
                DateUtil.currentTimestamp(),
                null,
                PrincipalJRibbon.getInstance().getObjUsuarioLogeado(),
                objTipoSolicitud,
                objTipoSolicitud.generarDetalleDocumentoSolicitud(PrincipalJRibbon.getInstance().getObjUsuarioLogeado()),
                objTipoSolicitud.generarAprobaciones(),
                RenunciaSocioProcessLogic.getInstance().generarDetalleSociosSolicitudRetiro(this.objSocio, PrincipalJRibbon.getInstance().getObjUsuarioLogeado()));
    }

    private void btnIngresarTarjetaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIngresarTarjetaActionPerformed
        int selectedIndex = this.cmbModoBuscarSocio.getSelectedIndex();

        this.cmbModoBuscarSocio.setEnabled(false);

        if (selectedIndex != OPCION_COMBO_TARJETA) {

            if (SwingUtil.hasBlankInputs(this.txtBuscarSocio) || (selectedIndex == OPCION_COMBO_DOCUMENTOIDENTIDAD && this.txtBuscarSocio.getText().length() < 8) || (selectedIndex == this.OPCION_COMBO_RUC && this.txtBuscarSocio.getText().length() < 10)) {
                JOptionPane.showMessageDialog(this, "Datos imcompletos", "Información", JOptionPane.INFORMATION_MESSAGE);
                this.cmbModoBuscarSocio.setEnabled(true);
                return;
            }

            if (selectedIndex == OPCION_COMBO_DOCUMENTOIDENTIDAD) {
                this.objSocio = QueryFacade.getInstance().getSocioByDocumentoIdentidad(this.txtBuscarSocio.getText(), false);
            } else if (selectedIndex == OPCION_COMBO_RUC) {
                this.objSocio = QueryFacade.getInstance().getSocioByRUC(this.txtBuscarSocio.getText(), false);
            }

            if (this.objSocio != null) {
                MyFingerprintVerification myFingerprintVerification = new MyFingerprintVerification(PrincipalJRibbon.getInstance().getFrame(), true, this.objSocio.getObjUsuario());
                SwingUtil.centerOnScreen(myFingerprintVerification);
                myFingerprintVerification.setVisible(true);

                if (myFingerprintVerification.isMatched()) {

                    String disponibilidad = PrestamosProcessLogic.getInstance().verificarDisponibilidadGarante(this.objSocio);

                    if (disponibilidad.contains("Disponible")) { // Verifica si no es fiador de algún préstamo

                        boolean continuar = false;
//                        boolean esSocioActivo = CuentasProcessLogic.getInstance().comprobarEstadoActividadSocio(this.objSocio);
                        if (RenunciaSocioProcessLogic.getInstance().tienecuentasActivas(this.objSocio)) { // Verifica si tiene cuentas activas
//                            if (esSocioActivo) {
//                                JOptionPane.showMessageDialog(this, "Tiene cuentas activas. Anule todas las cuentas donde sea miembro antes de proceder con la renuncia.", "Error", JOptionPane.ERROR_MESSAGE);
//                                return;
//                            } else {
//                                //JOptionPane.showMessageDialog(this, "Tiene cuentas activas. No podrá retirar el saldo de su(s) cuenta(s) a menos que cumpla todos sus aportes.", "Error", JOptionPane.ERROR_MESSAGE);
//                                int respuesta = JOptionPane.showConfirmDialog(this, "Tiene cuentas activas. El saldo de su(s) cuentas se transferiran a sus aportes. ¿Desea continuar?.", "Advertencia", JOptionPane.WARNING_MESSAGE);
//                                if (respuesta != JOptionPane.YES_OPTION) {
//                                    return;
//                                }
//                            }
                            JOptionPane.showMessageDialog(this, "Tiene cuentas activas. Anule todas las cuentas donde sea miembro antes de proceder con la renuncia.", "Información", JOptionPane.INFORMATION_MESSAGE);
                        } else {
                            continuar = true;
                        }
                        if (continuar) {
                            continuar = false;
                            if (RenunciaSocioProcessLogic.getInstance().tienePrestamosPendientes(this.objSocio)) { // Verifica si tiene préstamos pendientes
                                //                            if (esSocioActivo) {
                                //                                int respuesta = JOptionPane.showConfirmDialog(this, "Necesita cancelar todos los préstamos pendientes. De lo contrario se descontará de sus aportes como socio", "Advertencia", JOptionPane.WARNING_MESSAGE);
                                //                                if (respuesta != JOptionPane.YES_OPTION) {
                                //                                    return;
                                //                                }
                                //                            } else {
                                //                                int respuesta = JOptionPane.showConfirmDialog(this, "El total de su(s) prestamo(s) se descontará de sus aportes como socio", "Advertencia", JOptionPane.WARNING_MESSAGE);
                                //                                if (respuesta != JOptionPane.YES_OPTION) {
                                //                                    return;
                                //                                }
                                //                            }
                                JOptionPane.showMessageDialog(this, "Necesita cancelar todos los préstamos pendientes.", "Información", JOptionPane.INFORMATION_MESSAGE);
                            } else {
                                continuar = true;
                            }
                        }

                        if (continuar) {
                            // Verificar multas y obligaciones pendientes del socio (Decidir si descuenta de aportes o si los paga antes de procesder con la renuncia)
                            // Consultar si existe una solicitud pendiente
                            this.objSolicitudRetiroSocio = QueryFacade.getInstance().getLastSolicitudRetiroSocio(this.objSocio);

                            if (this.objSolicitudRetiroSocio == null) {
                                this.objSolicitudRetiroSocio = this.crearNuevaSolicitudRetiroSocio(this.objSocio);
                            } else if (this.objSolicitudRetiroSocio.isAprobado() && this.objSolicitudRetiroSocio.isEstado()) {
                                JOptionPane.showMessageDialog(this, "Solicitud de Renuncia ya ha sido aceptada.", "Información", JOptionPane.INFORMATION_MESSAGE);
                                continuar = false;
                            }

                            if (continuar) {
                                //this.updateTblSociosRelacionados();
                                this.updateTblCuentas();
                                this.updateDatosSocio();
                                if (this.objSolicitudRetiroSocio.getLstDetalleDocumentoSolicitud().size() > 0) {
                                    this.updateTblDocumentos();
                                    this.activarDocumentos(true);
                                } else {
                                    JOptionPane.showMessageDialog(this, "No existen documentos", "Información", JOptionPane.INFORMATION_MESSAGE);
                                }
                                this.btnGuardarSolicitudRenuncia.setEnabled(true);
                            }
                        }

                    } else {
                        JOptionPane.showMessageDialog(this, disponibilidad + " Necesita cambiar su estado de garante", "Información", JOptionPane.INFORMATION_MESSAGE);
                    }

                } else {
                    this.activarDocumentos(false);
                }
            } else {
                JOptionPane.showMessageDialog(this, "El socio identificado con " + this.txtBuscarSocio.getText() + " no existe", "Error", JOptionPane.ERROR_MESSAGE);
            }

        } else {
            DlgIngresarTarjeta objDlgIngresarTarjeta = new DlgIngresarTarjeta(PrincipalJRibbon.getInstance().getFrame(), true, true, true);
            objDlgIngresarTarjeta.setModal(true);
            SwingUtil.centerOnScreen(objDlgIngresarTarjeta);
            objDlgIngresarTarjeta.setVisible(true);

            if (objDlgIngresarTarjeta.getObjTarjeta() != null) {
                this.objSocio = objDlgIngresarTarjeta.getObjTarjeta().getObjSocio();

                String disponibilidad = PrestamosProcessLogic.getInstance().verificarDisponibilidadGarante(this.objSocio);

                if (disponibilidad.contains("Disponible")) { // Verifica si no es fiador de algún préstamo

                    boolean continuar = false;
//                    boolean esSocioActivo = CuentasProcessLogic.getInstance().comprobarEstadoActividadSocio(this.objSocio);
                    if (RenunciaSocioProcessLogic.getInstance().tienecuentasActivas(this.objSocio)) { // Verifica si tiene cuentas activas
//                        if (esSocioActivo) {
//                            JOptionPane.showMessageDialog(this, "Tiene cuentas activas. Anule todas las cuentas donde sea miembro antes de proceder con la renuncia.", "Error", JOptionPane.ERROR_MESSAGE);
//                            return;
//                        } else {
//                            //JOptionPane.showMessageDialog(this, "Tiene cuentas activas. No podrá retirar el saldo de su(s) cuenta(s) a menos que cumpla todos sus aportes.", "Error", JOptionPane.ERROR_MESSAGE);
//                            int respuesta = JOptionPane.showConfirmDialog(this, "Tiene cuentas activas. El saldo de su(s) cuentas se transferiran a sus aportes. ¿Desea continuar?.", "Advertencia", JOptionPane.WARNING_MESSAGE);
//                            if (respuesta != JOptionPane.YES_OPTION) {
//                                return;
//                            }
//                        }
                        JOptionPane.showMessageDialog(this, "Tiene cuentas activas. Anule todas las cuentas donde sea miembro antes de proceder con la renuncia.", "Información", JOptionPane.INFORMATION_MESSAGE);
                    } else {
                        continuar = true;
                    }

                    if (continuar) {
                        continuar = false;
                        if (RenunciaSocioProcessLogic.getInstance().tienePrestamosPendientes(this.objSocio)) { // Verifica si tiene préstamos pendientes
                            //                        if (esSocioActivo) {
                            //                            int respuesta = JOptionPane.showConfirmDialog(this, "Necesita cancelar todos los préstamos pendientes. De lo contrario se descontará de sus aportes como socio", "Advertencia", JOptionPane.WARNING_MESSAGE);
                            //                            if (respuesta != JOptionPane.YES_OPTION) {
                            //                                return;
                            //                            }
                            //                        } else {
                            //                            int respuesta = JOptionPane.showConfirmDialog(this, "El total de su(s) prestamo(s) se descontará de sus aportes como socio", "Advertencia", JOptionPane.WARNING_MESSAGE);
                            //                            if (respuesta != JOptionPane.YES_OPTION) {
                            //                                return;
                            //                            }
                            //                        }
                            JOptionPane.showMessageDialog(this, "Necesita cancelar todos los préstamos pendientes.", "Información", JOptionPane.INFORMATION_MESSAGE);
                        } else {
                            continuar = true;
                        }
                    }

                    if (continuar) {
                        // Verificar multas y obligaciones pendientes del socio (Decidir si descuenta de aportes o si los paga antes de procesder con la renuncia)
                        // Verificar si existe una solicitud pendiente
                        this.objSolicitudRetiroSocio = QueryFacade.getInstance().getLastSolicitudRetiroSocio(this.objSocio);

                        if (this.objSolicitudRetiroSocio == null) {
                            this.objSolicitudRetiroSocio = this.crearNuevaSolicitudRetiroSocio(this.objSocio);
                        } else if (this.objSolicitudRetiroSocio.isAprobado() && this.objSolicitudRetiroSocio.isEstado()) {
                            JOptionPane.showMessageDialog(this, "Solicitud de Renuncia ya ha aceptada.", "Información", JOptionPane.INFORMATION_MESSAGE);
                            continuar = false;
                        }

                        if (continuar) {
                            //this.updateTblSociosRelacionados();
                            this.updateTblCuentas();
                            this.updateDatosSocio();

                            if (this.objSolicitudRetiroSocio.getLstDetalleDocumentoSolicitud().size() > 0) {
                                this.updateTblDocumentos();
                                this.activarDocumentos(true);
                            } else {
                                JOptionPane.showMessageDialog(this, "No se encontraron documentos", "Información", JOptionPane.INFORMATION_MESSAGE);
                            }
                            this.btnGuardarSolicitudRenuncia.setEnabled(true);
                        }
                    }

                } else {
                    JOptionPane.showMessageDialog(this, disponibilidad + " Necesita cambiar su estado de garante", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(this, "Comprobación fallida", "Error", JOptionPane.ERROR_MESSAGE);
                this.activarDocumentos(false);
            }
        }

        this.updateButtons();
        this.cmbModoBuscarSocio.setEnabled(true);
    }//GEN-LAST:event_btnIngresarTarjetaActionPerformed

    private void cmbModoBuscarSocioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbModoBuscarSocioActionPerformed
        this.setTextInputBuscarSocio();
        this.setTextButtonIngresarTarjeta();
    }//GEN-LAST:event_cmbModoBuscarSocioActionPerformed

    private void btnDesaprobarDocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDesaprobarDocumentoActionPerformed
        // mensaje no tiene permiso para modificar el docuemento
        int selectedRow = this.tblDocumentos.getSelectedRow();
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, "Selecciona un documento", "Información", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        if (this.objSolicitudRetiroSocio != null) {
            //TipoSolicitud.updateAprobacion(PrincipalJRibbon.getInstance().getObjUsuarioLogeado().getObjTipoUsuario(), this.objSolicitudRetiroSocio.getLstAprobaciones(), false);
            this.objSolicitudRetiroSocio.getLstDetalleDocumentoSolicitud().get(selectedRow).setAprobado(true);
            this.updateTblDocumentos();

        } else {
            System.out.println("IFrmRegistroSolicitudRenuncia.btnAprobarSocioActionPerformed(): this.objSolicitudRetiroSocio == null");
        }
    }//GEN-LAST:event_btnDesaprobarDocumentoActionPerformed

    private void btnAprobarDocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAprobarDocumentoActionPerformed
        // mensaje no tiene permiso para modificar el docuemento
        int selectedRow = this.tblDocumentos.getSelectedRow();
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, "Selecciona un documento", "Información", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        if (this.objSolicitudRetiroSocio != null) {
            //TipoSolicitud.updateAprobacion(PrincipalJRibbon.getInstance().getObjUsuarioLogeado().getObjTipoUsuario(), this.objSolicitudRetiroSocio.getLstAprobaciones(), true);
            this.objSolicitudRetiroSocio.getLstDetalleDocumentoSolicitud().get(selectedRow).setAprobado(true);
            this.updateTblDocumentos();
        } else {
            System.out.println("IFrmRegistroSolicitudRenuncia.btnAprobarSocioActionPerformed(): this.objSolicitudRetiroSocio == null");
        }
    }//GEN-LAST:event_btnAprobarDocumentoActionPerformed

    private void btnGuardarSolicitudRenunciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarSolicitudRenunciaActionPerformed
        boolean result = false;

        if (this.objSolicitudRetiroSocio != null || this.objSolicitudRetiroSocio.getId() == 0) {
            int respuesta = JOptionPane.showConfirmDialog(this, "Confirmar Renuncia de socio: " + this.objSocio.getNombreCompleto() + "(" + this.objSocio.getDocumentoIdentidad() + ")", "Socio", JOptionPane.INFORMATION_MESSAGE);

            if (respuesta == JOptionPane.YES_OPTION) {
                result = RenunciaSocioProcessLogic.getInstance().guardarSolicitudRetiroSocio(this.objSolicitudRetiroSocio, this.mapArchivos, PrincipalJRibbon.getInstance().getObjCaja(), (Empleado) PrincipalJRibbon.getInstance().getObjPersona());
                if (result) {
//                    if (objSolicitudRetiroSocio.getObjSocio().isActivo()) {
//                        JOptionPane.showMessageDialog(this, "Error. Estado de Socio: ACTIVO", "Error", JOptionPane.ERROR_MESSAGE);
//                    } else {
//                        JOptionPane.showMessageDialog(this, "Renuncia registrada. Estado de Socio: INACTIVO", "Información", JOptionPane.INFORMATION_MESSAGE);
//                    }
                    JOptionPane.showMessageDialog(this, "Solicitud registrada correctamente.", "Informacion", JOptionPane.INFORMATION_MESSAGE);
                    this.dispose();
                } else {
                    JOptionPane.showMessageDialog(this, "Error en renuncia de Socio", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        } else {
            System.out.println("IFrmRegistroSolicitudRenuncia.btnGuardarSolicitudRenunciaActionPerformed(): this.objSolicitudRetiroSocio = null");
        }
    }//GEN-LAST:event_btnGuardarSolicitudRenunciaActionPerformed

    private void btnAprobarSocioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAprobarSocioActionPerformed
//        int selectedRow = this.tblSociosRelacionados.getSelectedRow();
//        if (selectedRow < 0) {
//            JOptionPane.showMessageDialog(this, "Selecciona un socio", "Información", JOptionPane.INFORMATION_MESSAGE);
//            return;
//        }
//        if (this.objSolicitudRetiroSocio != null) {
//            MyFingerprintVerification myFingerprintVerification = new MyFingerprintVerification(PrincipalJRibbon.getInstance().getFrame(), true, this.objSolicitudRetiroSocio.getLstDetalleSolicitudRetiroSocios().get(selectedRow).getObjSocio().getObjUsuario());
//            SwingUtil.centerOnScreen(myFingerprintVerification);
//            myFingerprintVerification.setVisible(true);
//
//            if (myFingerprintVerification.isMatched()) {
//                this.objSolicitudRetiroSocio.getLstDetalleSolicitudRetiroSocios().get(selectedRow).setAprueba(true);
//            } else {
//                this.objSolicitudRetiroSocio.getLstDetalleSolicitudRetiroSocios().get(selectedRow).setAprueba(false);
//            }
//
//            this.updateTblSociosRelacionados();
//        } else {
//            System.out.println("IFrmRegistroSolicitudRenuncia.btnAprobarSocioActionPerformed(): this.objSolicitudRetiroSocio = null");
//        }
    }//GEN-LAST:event_btnAprobarSocioActionPerformed

    private void btnDesaprobarSocioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDesaprobarSocioActionPerformed
//        int selectedRow = this.tblSociosRelacionados.getSelectedRow();
//        if (selectedRow < 0) {
//            JOptionPane.showMessageDialog(this, "Selecciona un socio", "Información", JOptionPane.INFORMATION_MESSAGE);
//            return;
//        }
//        if (this.objSolicitudRetiroSocio != null) {
//            this.objSolicitudRetiroSocio.getLstDetalleSolicitudRetiroSocios().get(selectedRow).setAprueba(false);
//            this.updateTblSociosRelacionados();
//        } else {
//            System.out.println("IFrmRegistroSolicitudRenuncia.btnAprobarSocioActionPerformed(): this.objSolicitudRetiroSocio = null");
//        }
    }//GEN-LAST:event_btnDesaprobarSocioActionPerformed

    private void btnCargarDocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCargarDocumentoActionPerformed
        int index = this.tblDocumentos.getSelectedRow();

        if (index != -1) {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("PDF Documents", "pdf"));
            fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Word Documents", "docx", "doc"));
            fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Images", "jpg", "png"));
            fileChooser.setAcceptAllFileFilterUsed(true);
            int result = fileChooser.showOpenDialog(this);
            if (result == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fileChooser.getSelectedFile();
                System.out.println("Selected file: " + selectedFile.getAbsolutePath());
                this.mapArchivos.put(this.objSolicitudRetiroSocio.getLstDetalleDocumentoSolicitud().get(index).getObjDocumento(), selectedFile);
                JOptionPane.showMessageDialog(this, "Documento cargado con exito.", "Documento", JOptionPane.INFORMATION_MESSAGE);
                this.updateTblDocumentos();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un Documento", "Documentos", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnCargarDocumentoActionPerformed

    private void updateButtons() {
        this.btnCargarDocumento.setEnabled(this.objSolicitudRetiroSocio != null && !this.objSolicitudRetiroSocio.isAprobado());
    }

    private void setTextButtonIngresarTarjeta() {
        String resourceImage = "";

        if (this.cmbModoBuscarSocio.getSelectedIndex() < -1) {
            this.cmbModoBuscarSocio.setSelectedIndex(OPCION_COMBO_DOCUMENTOIDENTIDAD);
        }
        switch (this.cmbModoBuscarSocio.getSelectedIndex()) {
            case OPCION_COMBO_DOCUMENTOIDENTIDAD:
                resourceImage = "/com/smartech/sistemacooperativa/gui/images/huella_24x24.png";
                this.btnIngresarTarjeta.setText("Comprobar huella");
                break;
            case OPCION_COMBO_RUC:
                resourceImage = "/com/smartech/sistemacooperativa/gui/images/huella_24x24.png";
                this.btnIngresarTarjeta.setText("Comprobar huella");
                break;
            case OPCION_COMBO_TARJETA:
                resourceImage = "/com/smartech/sistemacooperativa/gui/images/tarjeta_24x24.png";        
                this.btnIngresarTarjeta.setText("Ingresar tarjeta");
                break;
            default:
                break;
        }
        
        this.btnIngresarTarjeta.setIcon(new ImageIcon(getClass().getResource(resourceImage)));
        this.btnIngresarTarjeta.validate();
//        PrincipalJRibbon.getInstance().revalidate();
//        validate();

//        pack();
//        PrincipalJRibbon.getInstance().pack();

    }

    private void setCmbModoBuscarSocio() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        comboBoxModel.addElement("Documento de identidad de Socio");
        comboBoxModel.addElement("RUC de Empresa");
        comboBoxModel.addElement("Tarjeta de Socio");

        this.cmbModoBuscarSocio.setModel(comboBoxModel);
    }

    private void activarDocumentos(boolean activar) {
        this.btnAprobarDocumento.setEnabled(activar);
        this.btnDesaprobarDocumento.setEnabled(activar);
    }

    private void updateDatosSocio() {
        if (this.objSocio != null) {
            this.txtEstadoSocio.setText(RenunciaSocioProcessLogic.getInstance().getEstadoActividadSocio(this.objSocio));
            this.txtFechaIngresoSocio.setText(DateUtil.getRegularDateTime(DateUtil.getDate(this.objSocio.getFechaRegistro())));
            BigDecimal saldoAportes = QueryFacade.getInstance().getSaldoAportesSocio(this.objSocio);
            this.txtSaldoAportes.setText(saldoAportes != null ? saldoAportes.toPlainString() : BigDecimal.ZERO.toPlainString());
            BigDecimal saldoAhorros = QueryFacade.getInstance().getSaldoAhorroSocio(this.objSocio);
            this.txtSaldoAhorro.setText(saldoAhorros != null ? saldoAhorros.toPlainString() : BigDecimal.ZERO.toPlainString());
            this.txtaNotificacion.setText(PrestamosProcessLogic.getInstance().verificarDisponibilidadGarante(this.objSocio));
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAprobarDocumento;
    private javax.swing.JButton btnAprobarSocio;
    private javax.swing.JButton btnCargarDocumento;
    private javax.swing.JButton btnDesaprobarDocumento;
    private javax.swing.JButton btnDesaprobarSocio;
    private javax.swing.JButton btnGuardarSolicitudRenuncia;
    private javax.swing.JButton btnIngresarTarjeta;
    private javax.swing.JComboBox<String> cmbModoBuscarSocio;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JPanel pnlGarantiasPendientes;
    private javax.swing.JPanel pnlSocios;
    private javax.swing.JTable tblCuentas;
    private javax.swing.JTable tblDocumentos;
    private javax.swing.JTable tblGarantìasPendientes;
    private javax.swing.JTable tblSociosRelacionados;
    private javax.swing.JTextField txtBuscarSocio;
    private javax.swing.JTextField txtEstadoSocio;
    private javax.swing.JTextField txtFechaIngresoSocio;
    private javax.swing.JTextField txtSaldoAhorro;
    private javax.swing.JTextField txtSaldoAportes;
    private javax.swing.JTextField txtTotalDescuentos;
    private javax.swing.JTextArea txtaNotificacion;
    // End of variables declaration//GEN-END:variables

    private void updateTblCuentas() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        if (this.objSocio != null) {
            tableModel.addColumn("Codigo");
            tableModel.addColumn("Saldo");
            tableModel.addColumn("Fecha de Registro");
            tableModel.addColumn("Fecha de Modificación");
            tableModel.addColumn("Tipo de cuenta");
            tableModel.addColumn("Tasa de interés");
            tableModel.addColumn("Activo");

            String activo;
            List<Cuenta> lstCuentas = this.objSocio.getLstCuentas();
            for (Cuenta objCuenta : lstCuentas) {

                if (objCuenta.isEstado()) {
                    activo = objCuenta.isActivo() ? "ACTIVA" : "INACTIVA";

                    tableModel.addRow(new Object[]{
                        objCuenta.getCodigo(),
                        objCuenta.getSaldo(),
                        objCuenta.getFecharegistro(),
                        objCuenta.getFechamodificacion(),
                        objCuenta.getObjTipoCuenta().getNombre(),
                        objCuenta.getObjTasaInteres().getNombre(),
                        activo
                    });
                }
            }
        }

        this.tblCuentas.setModel(tableModel);
    }

//    private void updateTblSociosRelacionados() {
//        DefaultTableModel tableModel = new DefaultTableModel() {
//            @Override
//            public boolean isCellEditable(int row, int column) {
//                return false;
//            }
//        };
//
//        if (this.objSolicitudRetiroSocio != null) {
//            if (this.objSolicitudRetiroSocio.getLstDetalleSolicitudRetiroSocios() != null) {
//                tableModel.addColumn("Codigo");
//                tableModel.addColumn("Nombres");
//                tableModel.addColumn("Apellido paterno");
//                tableModel.addColumn("Apellido materno");
//                tableModel.addColumn("DNI");
//                tableModel.addColumn("RUC");
//                tableModel.addColumn("Sexo");
//                tableModel.addColumn("Estado");
//                tableModel.addColumn("Aprueba");
//
//                List<DetalleSolicitudRetiroSocio> lstDetalleSolicitudRetiroSocios = this.objSolicitudRetiroSocio.getLstDetalleSolicitudRetiroSocios();
//                String activo, sexo, aprueba;
//                for (DetalleSolicitudRetiroSocio objDetalleSolicitudRetiroSocio : lstDetalleSolicitudRetiroSocios) {
//                    Socio objSocioRelacionado = objDetalleSolicitudRetiroSocio.getObjSocio();
//                    activo = objSocioRelacionado.isActivo() ? "ACTIVO" : "INACTIVO";
//                    sexo = objSocioRelacionado.isSexo() ? "MASCULINO" : "FEMENINO";
//                    aprueba = objDetalleSolicitudRetiroSocio.isAprueba() ? "APRUEBA" : "NO APRUEBA";
//                    tableModel.addRow(new Object[]{
//                        objSocioRelacionado.getCodigo(),
//                        objSocioRelacionado.getNombre(),
//                        objSocioRelacionado.getApellidoPaterno(),
//                        objSocioRelacionado.getApellidoMaterno(),
//                        objSocioRelacionado.getDocumentoIdentidad(),
//                        objSocioRelacionado.getRUC(),
//                        sexo,
//                        activo,
//                        aprueba,
//                        CuentasProcessLogic.getInstance().comprobarEstadoActividadSocio(objDetalleSolicitudRetiroSocio.getObjSocio()) ? aprueba : "APROBADO"
//                    });
//                }
//
//            } else {
//                System.out.println("IFrmRegistroSolicitudRenuncia.updateTblSociosRelacionados(): this.objSolicitudRetiroSocio..getLstDetalleSolicitudRetiroSocios() = null");
//            }
//        } else {
//            System.out.println("IFrmRegistroSolicitudRenuncia.updateTblSociosRelacionados(): this.objSolicitudRetiroSocio = null");
//        }
//
//        this.tblSociosRelacionados.setModel(tableModel);
//    }
    private void updateTblDocumentos() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tableModel.addColumn("Documento");
        tableModel.addColumn("Estado");
        tableModel.addColumn("Archivo");

        if (this.objSolicitudRetiroSocio != null) {
            for (DetalleDocumentoSolicitud objDetalleDocumentoSolicitud : this.objSolicitudRetiroSocio.getLstDetalleDocumentoSolicitud()) {
                tableModel.addRow(new Object[]{
                    objDetalleDocumentoSolicitud.getObjDocumento().getNombre(),
                    objDetalleDocumentoSolicitud.isAprobado() ? "Aprobado" : "No Aprobado",
                    objDetalleDocumentoSolicitud.getRepresentacion() != null ? "Guardado" : this.mapArchivos.containsKey(objDetalleDocumentoSolicitud.getObjDocumento()) ? "Cargado" : "Sin Archivo"
                });
            }
        } else {
            System.out.println("IFrmRegistroSolicitudRenuncia.updateTblDocumentos(): this.objSolicitudRetiroSocio = null");
        }

        this.tblDocumentos.setModel(tableModel);
    }
}
