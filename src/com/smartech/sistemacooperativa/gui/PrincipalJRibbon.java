package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.facade.SecurityFacade;
import com.smartech.sistemacooperativa.bll.process.CajaProcessLogic;
import com.smartech.sistemacooperativa.bll.process.CuentasProcessLogic;
import com.smartech.sistemacooperativa.dal.DAOAccesoSistema;
import com.smartech.sistemacooperativa.dominio.Caja;
import com.smartech.sistemacooperativa.dominio.Concepto;
import com.smartech.sistemacooperativa.dominio.Persona;
import java.awt.Dimension;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import org.pushingpixels.flamingo.api.common.icon.ImageWrapperResizableIcon;
import org.pushingpixels.flamingo.api.ribbon.JRibbonFrame;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenu;
import com.smartech.sistemacooperativa.dominio.Empleado;
import com.smartech.sistemacooperativa.dominio.Establecimiento;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.TipoUsuario;
import com.smartech.sistemacooperativa.dominio.Usuario;
import com.smartech.sistemacooperativa.gui.reports.DlgReporteCrediticio;
import com.smartech.sistemacooperativa.gui.reports.DlgReporteEstadoCuenta;
import com.smartech.sistemacooperativa.gui.reports.DlgSimuladorCreditoPrestamo;
import com.smartech.sistemacooperativa.gui.reports.IFrmReporteSocios;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;
import javax.swing.JLabel;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.ribbon.JRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.JRibbonComponent;
import org.pushingpixels.flamingo.api.ribbon.RibbonElementPriority;
import org.pushingpixels.flamingo.api.ribbon.RibbonTask;
import org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizePolicies;
import com.smartech.sistemacooperativa.util.interfaces.IContentFrame;
import com.smartech.sistemacooperativa.util.interfaces.IObservable;
import com.smartech.sistemacooperativa.util.interfaces.IObserver;
import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javax.swing.JComponent;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import org.pushingpixels.flamingo.api.ribbon.AbstractRibbonBand;

/**
 *
 * @author Smartech
 */
public class PrincipalJRibbon extends JRibbonFrame implements IContentFrame {

    private List<Usuario> lstUsuarios = new ArrayList<>();
    private List<Persona> lstPersonas = new ArrayList<>();

    public List<Usuario> getLstUsuarios() {
        return lstUsuarios;
    }

    public void setLstUsuarios(List<Usuario> lstUsuarios) {
        this.lstUsuarios = lstUsuarios;
    }

    public List<Persona> getLstPersonas() {
        return lstPersonas;
    }

    public void setLstPersonas(List<Persona> lstPersonas) {
        this.lstPersonas = lstPersonas;
    }

    private static JDesktopPane desktopPane = new JDesktopPane();
    private static PrincipalJRibbon instance = null;
    private Persona objPersona;
    private Establecimiento objEstablecimiento = null;
    private Caja objCaja = null;
    private Usuario objUsuarioLogeado = null;
    private JLabel txtEstablecimiento = new JLabel();

    private static String ACCESO_SOLICITUD_INGRESO_SOCIO = "SolicitudIngresoSocio";
    private static String ACCESO_SOLICITUD_PRESTAMO = "SolicitudPrestamo";
    private static String ACCESO_PRESTAMOS = "Prestamos";
    private static String ACCESO_RETIROS = "Retiros";
    private static String ACCESO_DEPOSITOS = "Depositos";
    private static String ACCESO_TRANSFERENCIAS = "Transferencias";
    private static String ACCESO_APORTES = "Aportes";
    private static String ACCESO_PAGOS = "Pagos";
    private static String ACCESO_REGISTRO_CUENTA_SOCIO = "Cuentas";
    private static String ACCESO_ACTIVAR_TARJETA = "ActivarTarjeta";
    private static String ACCESO_CAMBIAR_CLAVE_TARJETA = "CambiarClaveTarjeta";
    private static String ACCESO_REPORTE_SOCIOS = "ReporteSocios";
    private static String ACCESO_SOLICITUD_RENUNCIA_SOCIO = "SolicitudRenunciaSocio";
    private static String ACCESO_ACTUALIZAR_SOCIO = "ActualizarSocio";
    private static String ACCESO_REPORTE_ESTADO_CUENTA = "ReporteEstadoCuenta";
    private static String ACCESO_TIPO_CAMBIO = "TipoCambio";
    private static String ACCESO_PRESTAMOS_ACTIVOS = "PrestamosActivos";
    private static String ACCESO_EMPLEADOS = "Empleados";
    private static String ACCESO_REPORTE_CREDITICIO = "ReporteCrediticio";
    private static String ACCESO_TIPO_INTERES = "TipoInteres";
    private static String ACCESO_SIMULADOR_CREDITICIO = "SimuladorCrediticio";
    private static String ACCESO_INHABILITAR_SOCIOS = "InhabilitarSocios";
    private static String ACCESO_ACTIVAR_SOCIOS = "ReingresoSocios";
    private static String ACCESO_MOVIMIENTO_ADMINISTRATIVO = "MovimientoAdministrativo";

    private PrincipalJRibbon() {
        super();
        this.getContentPane().add(desktopPane);
    }

    public static PrincipalJRibbon getInstance() {
        if (instance == null) {
            instance = new PrincipalJRibbon();
        }
        return instance;
    }

    public JDesktopPane getDesktopPane() {
        return desktopPane;
    }

    public void setDesktopPane(JDesktopPane desktopPane) {
        this.desktopPane = desktopPane;
    }

    @Override
    public JFrame getFrame() {
        return this;
    }

    public void setObjPersona(Persona objPersona) {
        this.objPersona = objPersona;
    }

    public Persona getObjPersona() {
        return this.objPersona;
    }

    public Usuario getObjUsuarioLogeado() {
        return objUsuarioLogeado;
    }

    public void setObjUsuarioLogeado(Usuario objUsuarioLogeado) {
        this.objUsuarioLogeado = objUsuarioLogeado;
    }

    public Establecimiento getObjEstablecimiento() {
        return objEstablecimiento;
    }

    public void setObjEstablecimiento(Establecimiento objEstablecimiento) {
        this.objEstablecimiento = objEstablecimiento;
    }

    public Caja getObjCaja() {
        return objCaja;
    }

    public void setObjCaja(Caja objCaja) {
        this.objCaja = objCaja;
    }

    public JLabel getTxtEstablecimiento() {
        return txtEstablecimiento;
    }

    public void setTxtEstablecimiento(JLabel txtEstablecimiento) {
        this.txtEstablecimiento = txtEstablecimiento;
    }

    public void setTxtEstablecimientoText(String text) {
        if (this.txtEstablecimiento != null) {
            this.txtEstablecimiento.setText(text);
        }
    }

    public static void main() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                final PrincipalJRibbon principalJRibbon = getInstance();
                principalJRibbon.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
                //final JDesktopPane desktopPane = principalJRibbon.getDesktopPane();
                principalJRibbon.addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosing(WindowEvent e) {
                        if (PrincipalJRibbon.getInstance().getObjUsuarioLogeado().getObjTipoUsuario().isAccesocaja()) {
                            System.out.println("estado de caja " + PrincipalJRibbon.getInstance().getObjCaja().getCodigo() + ": " + PrincipalJRibbon.getInstance().getObjCaja().isAbierto());
                            if (!PrincipalJRibbon.getInstance().getObjCaja().isAbierto()) {
                                principalJRibbon.setDefaultCloseOperation(EXIT_ON_CLOSE);
                                PrincipalJRibbon.getInstance().dispose();
                            } else {
                                JOptionPane.showMessageDialog(PrincipalJRibbon.getInstance().getFrame(), "Necesita cerrar caja", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        } else {
                            principalJRibbon.setDefaultCloseOperation(EXIT_ON_CLOSE);
                            PrincipalJRibbon.getInstance().dispose();
                        }
                        //super.windowClosing(e); //To change body of generated methods, choose Tools | Templates.
                    }
                });
                Empleado objEmpleado = null;
                Socio objSocio = null;
                TipoUsuario objTipoUsuario = null;

                if (instance.getObjPersona() instanceof Socio) {
                    objSocio = (Socio) instance.getObjPersona();
                    objTipoUsuario = objSocio.getObjUsuario().getObjTipoUsuario();
                } else if (instance.getObjPersona() instanceof Empleado) {
                    objEmpleado = (Empleado) instance.getObjPersona();
                    objTipoUsuario = objEmpleado.getObjUsuario().getObjTipoUsuario();
                }

                if (objTipoUsuario != null) {
                    boolean result;
                    String mensaje;
                    mensaje = CuentasProcessLogic.getInstance().actualizarInteresesCuentas(PrincipalJRibbon.getInstance().getObjUsuarioLogeado());
                    result = SecurityFacade.getInstance().registrarAccesoSistema(PrincipalJRibbon.getInstance().getObjUsuarioLogeado());
                    if (result) {
                        if (!mensaje.trim().isEmpty()) {
                            if (mensaje.toLowerCase().contains("error")) {
                                JOptionPane.showMessageDialog(PrincipalJRibbon.getInstance().getFrame(), mensaje, "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    } else if (PrincipalJRibbon.getInstance().getObjUsuarioLogeado().getObjTipoUsuario().getId() != TipoUsuario.TIPO_SOCIO) {
                        JOptionPane.showMessageDialog(PrincipalJRibbon.getInstance().getFrame(), "No pudo registrar su acceso", "Error", JOptionPane.ERROR_MESSAGE);
                        principalJRibbon.setDefaultCloseOperation(EXIT_ON_CLOSE);
                        PrincipalJRibbon.getInstance().dispose();
                    }

                    //<editor-fold defaultstate="collapsed" desc="Tab Socios">
                    //<editor-fold defaultstate="collapsed" desc="Band Socios">
                    JRibbonBand bandSocios = new JRibbonBand("Socios", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/config.png"), new Dimension(443, 443)));
                    bandSocios.setResizePolicies((List) Arrays.asList(new CoreRibbonResizePolicies.None(bandSocios.getControlPanel())));

                    //<editor-fold defaultstate="collapsed" desc="Btn Solicitud Ingreso">
                    JCommandButton btnConsultarSolicitudesSocios = new JCommandButton("Solicitud de Ingreso", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/user_lapiz.png"), new Dimension(128, 128)));
                    btnConsultarSolicitudesSocios.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            IFrmConsultarSolicitudesIngreso frmConsultarSolicitudes = new IFrmConsultarSolicitudesIngreso(principalJRibbon);
                            principalJRibbon.getDesktopPane().add(frmConsultarSolicitudes);
                            frmConsultarSolicitudes.setVisible(true);
                            try {
                                frmConsultarSolicitudes.setMaximum(true);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    });
                    //</editor-fold> Fin Btn Solicitud Ingreso

                    //<editor-fold defaultstate="collapsed" desc="Btn Retiro Solicitud Socio">
                    JCommandButton btnRegistrarSolicitudesRenunciaSocios = new JCommandButton("Solicitud de Renuncia", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/user_renuncia.png"), new Dimension(128, 128)));
                    btnRegistrarSolicitudesRenunciaSocios.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            IFrmRegistroSolicitudRenuncia frmRegistroSolicitudRenuncia = new IFrmRegistroSolicitudRenuncia(principalJRibbon);
                            principalJRibbon.getDesktopPane().add(frmRegistroSolicitudRenuncia);
                            frmRegistroSolicitudRenuncia.setVisible(true);
                            try {
                                frmRegistroSolicitudRenuncia.setMaximum(true);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    });
                    //</editor-fold> Fin Btn Solicitud Retiro Socio

                    //<editor-fold defaultstate="collapsed" desc="Btn Consultar Socios">
                    JCommandButton btnConsultarSocios = new JCommandButton("Actualizar Datos", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/user_conf.png"), new Dimension(128, 128)));
                    btnConsultarSocios.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            IFrmConsultarSocios frmConsultarSocios = new IFrmConsultarSocios();
                            principalJRibbon.getDesktopPane().add(frmConsultarSocios);
                            frmConsultarSocios.setVisible(true);
                            try {
                                frmConsultarSocios.setMaximum(true);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    });
                    //</editor-fold>
                    if (objTipoUsuario.verificarAcceso(ACCESO_SOLICITUD_INGRESO_SOCIO)) {
                        bandSocios.addCommandButton(btnConsultarSolicitudesSocios, RibbonElementPriority.TOP);
                    }
                    if (objTipoUsuario.verificarAcceso(ACCESO_SOLICITUD_RENUNCIA_SOCIO)) {
                        bandSocios.addCommandButton(btnRegistrarSolicitudesRenunciaSocios, RibbonElementPriority.TOP);
                    }
                    if (objTipoUsuario.verificarAcceso(ACCESO_ACTUALIZAR_SOCIO)) {
                        bandSocios.addCommandButton(btnConsultarSocios, RibbonElementPriority.TOP);
                    }
                    //</editor-fold> Fin Band Socios

                    //<editor-fold defaultstate="collapsed" desc="Band Cuentas">
                    JRibbonBand bandCuentas = new JRibbonBand("Cuentas", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/config.png"), new Dimension(443, 443)));
                    bandCuentas.setResizePolicies((List) Arrays.asList(new CoreRibbonResizePolicies.None(bandCuentas.getControlPanel())));

                    //<editor-fold defaultstate="collapsed" desc="Btn Cuenta Socio">
                    JCommandButton btnCuentaSocio = new JCommandButton("Cuentas", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/cuenta.png"), new Dimension(128, 128)));
                    btnCuentaSocio.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            IFrmConsultarCuentas frmConsultarCuentas = new IFrmConsultarCuentas();
                            PrincipalJRibbon.setObserverToObservables(frmConsultarCuentas);
                            principalJRibbon.getDesktopPane().add(frmConsultarCuentas);
                            frmConsultarCuentas.setVisible(true);
                            try {
                                frmConsultarCuentas.setMaximum(true);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    });
                    //</editor-fold>

                    if (objTipoUsuario.verificarAcceso(ACCESO_REGISTRO_CUENTA_SOCIO)) {
                        bandCuentas.addCommandButton(btnCuentaSocio, RibbonElementPriority.TOP);
                    }
                    //</editor-fold> Fin Band Cuentas

                    //<editor-fold defaultstate="collapsed" desc="Band Tarjetas">
                    JRibbonBand bandTarjetas = new JRibbonBand("Tarjetas", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/config.png"), new Dimension(443, 443)));
                    bandTarjetas.setResizePolicies((List) Arrays.asList(new CoreRibbonResizePolicies.None(bandTarjetas.getControlPanel())));

                    //<editor-fold defaultstate="collapsed" desc="Btn Activar Tarjeta">
                    JCommandButton btnActivarTarjeta = new JCommandButton("Activar", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/tarjeta_24x24.png"), new Dimension(128, 128)));
                    btnActivarTarjeta.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            DlgCambiarClaveTarjeta dlgCambiarClaveTarjeta = new DlgCambiarClaveTarjeta(principalJRibbon.getFrame(), true, DlgCambiarClaveTarjeta.ACTIVAR_TARJETA);
                            SwingUtil.centerOnScreen(dlgCambiarClaveTarjeta);
                            dlgCambiarClaveTarjeta.setVisible(true);
                        }
                    });
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="Btn Cambiar Clave Tarjeta">
                    JCommandButton btnCambiarClaveTarjeta = new JCommandButton("Cambiar Clave", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/password_2.png"), new Dimension(128, 128)));
                    btnCambiarClaveTarjeta.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            DlgCambiarClaveTarjeta dlgCambiarClaveTarjeta = new DlgCambiarClaveTarjeta(principalJRibbon.getFrame(), true, DlgCambiarClaveTarjeta.CAMBIAR_CLAVE);
                            SwingUtil.centerOnScreen(dlgCambiarClaveTarjeta);
                            dlgCambiarClaveTarjeta.setVisible(true);
                        }
                    });
                    //</editor-fold>

                    if (objTipoUsuario.verificarAcceso(ACCESO_ACTIVAR_TARJETA)) {
                        bandTarjetas.addCommandButton(btnActivarTarjeta, RibbonElementPriority.TOP);
                    }
                    if (objTipoUsuario.verificarAcceso(ACCESO_CAMBIAR_CLAVE_TARJETA)) {
                        bandTarjetas.addCommandButton(btnCambiarClaveTarjeta, RibbonElementPriority.TOP);
                    }
                    //</editor-fold> Fin Band Tarjetas

                    List<AbstractRibbonBand> lstBandsSocios = new ArrayList<>();

                    lstBandsSocios.add(getBandEstablecimiento());
                    if (objTipoUsuario.verificarAcceso(ACCESO_SOLICITUD_INGRESO_SOCIO) || objTipoUsuario.verificarAcceso(ACCESO_SOLICITUD_RENUNCIA_SOCIO) || objTipoUsuario.verificarAcceso(ACCESO_ACTUALIZAR_SOCIO)) {
                        lstBandsSocios.add(bandSocios);
                    }
                    if (objTipoUsuario.verificarAcceso(ACCESO_REGISTRO_CUENTA_SOCIO)) {
                        lstBandsSocios.add(bandCuentas);
                    }
                    if (objTipoUsuario.verificarAcceso(ACCESO_ACTIVAR_TARJETA) || objTipoUsuario.verificarAcceso(ACCESO_CAMBIAR_CLAVE_TARJETA)) {
                        lstBandsSocios.add(bandTarjetas);
                    }

                    RibbonTask ribbonTaskSocios = new RibbonTask("Socios", lstBandsSocios.toArray(new AbstractRibbonBand[lstBandsSocios.size()]));
                    //</editor-fold> Fin Tab Socios

                    //<editor-fold defaultstate="collapsed" desc="Tab Operaciones">
                    //<editor-fold defaultstate="collapsed" desc="Band Transacciones">
                    JRibbonBand bandTransacciones = new JRibbonBand("Transacciones", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/config.png"), new Dimension(443, 443)));
                    bandTransacciones.setResizePolicies((List) Arrays.asList(new CoreRibbonResizePolicies.None(bandTransacciones.getControlPanel())));

                    //<editor-fold defaultstate="collapsed" desc="Btn Retiros">
                    JCommandButton btnRetiros = new JCommandButton("Retiros", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/atm.png"), new Dimension(128, 128)));

                    btnRetiros.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if (PrincipalJRibbon.getInstance().getObjCaja().isAbierto()) {
                                IFrmConsultarMovimientos frmConsultaMovimientos = new IFrmConsultarMovimientos(Concepto.CONCEPTO_RETIROS_CUENTA);
                                PrincipalJRibbon.setObserverToObservables(frmConsultaMovimientos);
                                PrincipalJRibbon.setObservers(frmConsultaMovimientos);
                                PrincipalJRibbon.getInstance().getDesktopPane().add(frmConsultaMovimientos);
                                frmConsultaMovimientos.setVisible(true);
                                try {
                                    frmConsultaMovimientos.setMaximum(true);
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            } else {
                                JOptionPane.showMessageDialog(PrincipalJRibbon.getInstance().getDesktopPane(), "La caja está cerrada. Realice la apertura de caja", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    });
                    //</editor-fold> Fin Btn Retiros

                    //<editor-fold defaultstate="collapsed" desc="Btn Depositos">
                    JCommandButton btnDepositos = new JCommandButton("Depositos", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/money-bag.png"), new Dimension(128, 128)));
                    btnDepositos.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if (PrincipalJRibbon.getInstance().getObjCaja().isAbierto()) {
                                IFrmConsultarMovimientos frmConsultaMovimientos = new IFrmConsultarMovimientos(Concepto.CONCEPTO_DEPOSITOS);
                                PrincipalJRibbon.setObserverToObservables(frmConsultaMovimientos);
                                PrincipalJRibbon.setObservers(frmConsultaMovimientos);
                                PrincipalJRibbon.getInstance().getDesktopPane().add(frmConsultaMovimientos);
                                frmConsultaMovimientos.setVisible(true);
                                try {
                                    frmConsultaMovimientos.setMaximum(true);
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            } else {
                                JOptionPane.showMessageDialog(PrincipalJRibbon.getInstance().getDesktopPane(), "La caja está cerrada. Realice la apertura de caja", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    });
                    //</editor-fold> Fin Btn Depositos

                    //<editor-fold defaultstate="collapsed" desc="Btn Aportes Fondo Mortuorio">
                    JCommandButton btnAportesFondoMortuorio = new JCommandButton("Fondo Mortuorio", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/fondo_mortuorio.png"), new Dimension(128, 128)));
                    btnAportesFondoMortuorio.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            IFrmConsultarDepositosFondoMortuorio frmConsultarAportesFondoMortuorio = new IFrmConsultarDepositosFondoMortuorio();
                            PrincipalJRibbon.getInstance().getDesktopPane().add(frmConsultarAportesFondoMortuorio);
                            frmConsultarAportesFondoMortuorio.setVisible(true);
                            try {
                                frmConsultarAportesFondoMortuorio.setMaximum(true);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    });
                    //</editor-fold> Fin Btn Aportes Fondo Mortuorio

                    //<editor-fold defaultstate="collapsed" desc="Btn Aportes">
                    JCommandButton btnAportes = new JCommandButton("Aportes", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/notes.png"), new Dimension(128, 128)));
                    btnAportes.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            IFrmConsultarAportes frmConsultarAportes = new IFrmConsultarAportes();
                            PrincipalJRibbon.getInstance().getDesktopPane().add(frmConsultarAportes);
                            frmConsultarAportes.setVisible(true);
                            try {
                                frmConsultarAportes.setMaximum(true);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    });
                    //</editor-fold> Fin Btn Aportes

                    //<editor-fold defaultstate="collapsed" desc="Btn Transferencias">
                    JCommandButton btnTransferencias = new JCommandButton("Transferencias", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/transferencia_24x24.png"), new Dimension(128, 128)));
                    btnTransferencias.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            IFrmConsultarTransferencias frmConsultarTransferencias = new IFrmConsultarTransferencias();
                            PrincipalJRibbon.getInstance().getDesktopPane().add(frmConsultarTransferencias);
                            PrincipalJRibbon.setObserverToObservables(frmConsultarTransferencias);
                            frmConsultarTransferencias.setVisible(true);
                            try {
                                frmConsultarTransferencias.setMaximum(true);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    });
                    //</editor-fold> Fin Btn Transferencias

                    //<editor-fold defaultstate="collapsed" desc="Btn Pagos">
                    JCommandButton btnPagos = new JCommandButton("Pagos", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/get-money.png"), new Dimension(128, 128)));

                    btnPagos.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            IFrmConsultarPagos frmConsultarPagos = new IFrmConsultarPagos();
                            PrincipalJRibbon.setObservers(frmConsultarPagos);
                            PrincipalJRibbon.setObserverToObservables(frmConsultarPagos);
                            PrincipalJRibbon.getInstance().getDesktopPane().add(frmConsultarPagos);
                            frmConsultarPagos.setVisible(true);
                            try {
                                frmConsultarPagos.setMaximum(true);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    });
                    //</editor-fold> Fin Btn Pagos

                    if (objTipoUsuario.verificarAcceso(ACCESO_RETIROS)) {
                        bandTransacciones.addCommandButton(btnRetiros, RibbonElementPriority.TOP);
                    }
                    if (objTipoUsuario.verificarAcceso(ACCESO_DEPOSITOS)) {
                        bandTransacciones.addCommandButton(btnDepositos, RibbonElementPriority.TOP);
                    }
                    if (objTipoUsuario.verificarAcceso(ACCESO_APORTES)) {
                        bandTransacciones.addCommandButton(btnAportes, RibbonElementPriority.TOP);
                        bandTransacciones.addCommandButton(btnAportesFondoMortuorio, RibbonElementPriority.TOP);
                    }
                    if (objTipoUsuario.verificarAcceso(ACCESO_TRANSFERENCIAS)) {
                        bandTransacciones.addCommandButton(btnTransferencias, RibbonElementPriority.TOP);
                    }
                    if (objTipoUsuario.verificarAcceso(ACCESO_PAGOS)) {
                        bandTransacciones.addCommandButton(btnPagos, RibbonElementPriority.TOP);
                    }
                    //</editor-fold> Fin Band Transacciones

                    //<editor-fold defaultstate="collapsed" desc="Band Movimientos Administrativos">
                    JRibbonBand bandMovimientosAdministrativos = new JRibbonBand("Movimientos Administrativos", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/config.png"), new Dimension(443, 443)));
                    bandMovimientosAdministrativos.setResizePolicies((List) Arrays.asList(new CoreRibbonResizePolicies.None(bandMovimientosAdministrativos.getControlPanel())));
                    
                    //<editor-fold defaultstate="collapsed" desc="Btn Movimientos Administrativos">
                    JCommandButton btnMovimientosAdministrativos = new JCommandButton("Movimientos Administrativos", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/exchange1.png"), new Dimension(128, 128)));

                    btnMovimientosAdministrativos.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if (PrincipalJRibbon.getInstance().getObjCaja().isAbierto()) {
                                IFrmConsultarMovimientosAdministrativos frmConsultarMovimientosAdministrativos = new IFrmConsultarMovimientosAdministrativos();
                                PrincipalJRibbon.setObserverToObservables(frmConsultarMovimientosAdministrativos);
                                PrincipalJRibbon.setObservers(frmConsultarMovimientosAdministrativos);
                                PrincipalJRibbon.getInstance().getDesktopPane().add(frmConsultarMovimientosAdministrativos);
                                frmConsultarMovimientosAdministrativos.setVisible(true);
                                try {
                                    frmConsultarMovimientosAdministrativos.setMaximum(true);
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            } else {
                                JOptionPane.showMessageDialog(PrincipalJRibbon.getInstance().getDesktopPane(), "La caja está cerrada. Realice la apertura de caja", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    });
                    //</editor-fold>
                    
                    if (objTipoUsuario.verificarAcceso(ACCESO_MOVIMIENTO_ADMINISTRATIVO)) {
                        bandMovimientosAdministrativos.addCommandButton(btnMovimientosAdministrativos, RibbonElementPriority.TOP);
                    }
                    //</editor-fold> Fin Band Movimientos Administrativos

                    //<editor-fold defaultstate="collapsed" desc="Band Cajas">
                    JRibbonBand bandCaja = new JRibbonBand("Caja", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/config.png"), new Dimension(443, 443)));
                    bandCaja.setResizePolicies((List) Arrays.asList(new CoreRibbonResizePolicies.None(bandCaja.getControlPanel())));

                    //<editor-fold defaultstate="collapsed" desc="Btn Abrir y Cerrar caja">
                    JCommandButton btnAbrirCaja = new JCommandButton("Abrir Caja", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/open.png"), new Dimension(128, 128)));

                    JCommandButton btnCerrarCaja = new JCommandButton("Cerrar Caja", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/close.png"), new Dimension(128, 128)));

                    btnAbrirCaja.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            boolean result = CajaProcessLogic.getInstance().abrirCaja(PrincipalJRibbon.getInstance().getObjCaja(), (Empleado) PrincipalJRibbon.getInstance().getObjPersona());
                            if (result) {
                                btnAbrirCaja.setEnabled(false);
                                btnCerrarCaja.setEnabled(true);
                                JOptionPane.showMessageDialog(PrincipalJRibbon.getInstance().getDesktopPane(), "Apertura de caja realizada", "Caja", JOptionPane.INFORMATION_MESSAGE);
                            } else {
                                btnAbrirCaja.setEnabled(true);
                                btnCerrarCaja.setEnabled(false);
                                JOptionPane.showMessageDialog(PrincipalJRibbon.getInstance().getDesktopPane(), "Error en apertura de caja. Comuníquese con el admnistrador.", "Caja", JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    });

                    btnCerrarCaja.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            boolean result = CajaProcessLogic.getInstance().cerrarCaja(PrincipalJRibbon.getInstance().getObjCaja(), (Empleado) PrincipalJRibbon.getInstance().getObjPersona());
                            if (result) {

                                if (PrincipalJRibbon.getInstance().getObjCaja().isAbierto()) {
                                    JOptionPane.showMessageDialog(PrincipalJRibbon.getInstance().getDesktopPane(), "La caja ha sido bloqueda", "Advertencia", JOptionPane.WARNING_MESSAGE);
                                    principalJRibbon.setDefaultCloseOperation(EXIT_ON_CLOSE);
                                    PrincipalJRibbon.getInstance().dispose();
                                } else {
                                    btnAbrirCaja.setEnabled(true);
                                    btnCerrarCaja.setEnabled(false);
                                    JOptionPane.showMessageDialog(PrincipalJRibbon.getInstance().getDesktopPane(), "Cierre de caja realizado correctamente", "Caja", JOptionPane.INFORMATION_MESSAGE);
                                }

                            } else {
                                JOptionPane.showMessageDialog(PrincipalJRibbon.getInstance().getDesktopPane(), "Error en cierre de caja", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    });
                    //</editor-fold>

                    if (objTipoUsuario.isAccesocaja()) {
                        bandCaja.addCommandButton(btnAbrirCaja, RibbonElementPriority.TOP);
                        bandCaja.addCommandButton(btnCerrarCaja, RibbonElementPriority.TOP);
                        btnAbrirCaja.setEnabled(false);
                        btnCerrarCaja.setEnabled(true);
                    }
                    //</editor-fold> Fin Band Cajas

                    List<AbstractRibbonBand> lstBandsOperaciones = new ArrayList<>();
                    lstBandsOperaciones.add(getBandEstablecimiento());
                    if (objTipoUsuario.verificarAcceso(ACCESO_RETIROS) || objTipoUsuario.verificarAcceso(ACCESO_DEPOSITOS) || objTipoUsuario.verificarAcceso(ACCESO_TRANSFERENCIAS) || objTipoUsuario.verificarAcceso(ACCESO_PAGOS)) {
                        lstBandsOperaciones.add(bandTransacciones);
                    }
                    if(objTipoUsuario.verificarAcceso(ACCESO_MOVIMIENTO_ADMINISTRATIVO)){
                        lstBandsOperaciones.add(bandMovimientosAdministrativos);
                    }
                    if (objTipoUsuario.isAccesocaja()) {
                        lstBandsOperaciones.add(bandCaja);
                    }

                    RibbonTask ribbonTaskOperaciones = new RibbonTask("Operaciones", lstBandsOperaciones.toArray(new AbstractRibbonBand[lstBandsOperaciones.size()]));

                    //</editor-fold> Fin Tab Operaciones
                    
                    //<editor-fold defaultstate="collapsed" desc="Tab Prestamos">                    
                    //<editor-fold defaultstate="collapsed" desc="Band Prestamos">
                    JRibbonBand bandPrestamos = new JRibbonBand("Prestamos", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/config.png"), new Dimension(443, 443)));
                    bandPrestamos.setResizePolicies((List) Arrays.asList(new CoreRibbonResizePolicies.None(bandPrestamos.getControlPanel())));

                    //<editor-fold defaultstate="collapsed" desc="Btn Solicitud Prestamo">
                    JCommandButton btnConsultarSolicitudesPrestamo = new JCommandButton("Solicitud de Prestamo", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/loan.png"), new Dimension(128, 128)));
                    btnConsultarSolicitudesPrestamo.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            IFrmConsultarSolicitudesPrestamo frmConsultarSolicitudesPrestamo = new IFrmConsultarSolicitudesPrestamo(principalJRibbon);
                            principalJRibbon.getDesktopPane().add(frmConsultarSolicitudesPrestamo);
                            frmConsultarSolicitudesPrestamo.setVisible(true);
                            try {
                                frmConsultarSolicitudesPrestamo.setMaximum(true);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    });

                    if (objTipoUsuario.verificarAcceso(ACCESO_SOLICITUD_PRESTAMO)) {
                        bandPrestamos.addCommandButton(btnConsultarSolicitudesPrestamo, RibbonElementPriority.TOP);
                    }
                    //</editor-fold> Fin Btn Solicitud Prestamo

                    //<editor-fold defaultstate="collapsed" desc="Btn Prestamos Activos">
                    JCommandButton btnPrestamosActivos = new JCommandButton("Prestamos Activos", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/prestamo_activo.png"), new Dimension(128, 128)));
                    btnPrestamosActivos.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            IFrmPrestamosActivos frmPrestamosActivos = new IFrmPrestamosActivos();
                            principalJRibbon.getDesktopPane().add(frmPrestamosActivos);
                            frmPrestamosActivos.setVisible(true);
                            try {
                                frmPrestamosActivos.setMaximum(true);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    });

                    if (objTipoUsuario.verificarAcceso(ACCESO_PRESTAMOS_ACTIVOS)) {
                        bandPrestamos.addCommandButton(btnPrestamosActivos, RibbonElementPriority.TOP);
                    }
                    //</editor-fold> Fin Btn Prestamos Activos

                    //<editor-fold defaultstate="collapsed" desc="Btn Simulador Crediticio">
                    JCommandButton btnSimuladorCrediticio = new JCommandButton("Simulador Crediticio", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/calculator.png"), new Dimension(128, 128)));
                    btnSimuladorCrediticio.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            DlgSimuladorCreditoPrestamo dlgSimuladorCreditoPrestamo = new DlgSimuladorCreditoPrestamo(instance.getFrame(), true);
                            SwingUtil.centerOnScreen(dlgSimuladorCreditoPrestamo);
                            dlgSimuladorCreditoPrestamo.setVisible(true);
                        }
                    });

                    if (objTipoUsuario.verificarAcceso(ACCESO_SIMULADOR_CREDITICIO)) {
                        bandPrestamos.addCommandButton(btnSimuladorCrediticio, RibbonElementPriority.TOP);
                    }
                    //</editor-fold>

                    //</editor-fold> Fin Band Prestamos                      
                    List<AbstractRibbonBand> lstBandsPrestamos = new ArrayList<>();

                    lstBandsPrestamos.add(getBandEstablecimiento());
                    if (objTipoUsuario.verificarAcceso(ACCESO_SOLICITUD_PRESTAMO) || objTipoUsuario.verificarAcceso(ACCESO_PRESTAMOS) || objTipoUsuario.verificarAcceso(ACCESO_PRESTAMOS_ACTIVOS) || objTipoUsuario.verificarAcceso(ACCESO_SIMULADOR_CREDITICIO)) {
                        lstBandsPrestamos.add(bandPrestamos);
                    }

                    RibbonTask ribbonTaskPrestamos = new RibbonTask("Prestamos", lstBandsPrestamos.toArray(new AbstractRibbonBand[lstBandsPrestamos.size()]));
                    //</editor-fold> Fin Tab Prestamos

                    //<editor-fold defaultstate="collapsed" desc="Tab Reportes">
                    //<editor-fold defaultstate="collapsed" desc="Band Reportes Cuentas">
                    JRibbonBand bandReportesCuentas = new JRibbonBand("Cuentas", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/config.png"), new Dimension(443, 443)));
                    bandReportesCuentas.setResizePolicies((List) Arrays.asList(new CoreRibbonResizePolicies.None(bandReportesCuentas.getControlPanel())));

                    //<editor-fold defaultstate="collapsed" desc="Btn Reporte Estado Cuenta">
                    JCommandButton btnReporteEstadoCuenta = new JCommandButton("Estado de Cuenta", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/estado_cuenta.png"), new Dimension(128, 128)));
                    btnReporteEstadoCuenta.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            DlgReporteEstadoCuenta dlgReporteEstadoCuenta = new DlgReporteEstadoCuenta(instance.getFrame(), true);
                            SwingUtil.centerOnScreen(dlgReporteEstadoCuenta);
                            dlgReporteEstadoCuenta.setVisible(true);
                        }
                    });

                    if (objTipoUsuario.verificarAcceso(ACCESO_REPORTE_ESTADO_CUENTA)) {
                        bandReportesCuentas.addCommandButton(btnReporteEstadoCuenta, RibbonElementPriority.TOP);
                    }
                    //</editor-fold> Fin Btn Reporte Estado Cuenta

                    //<editor-fold defaultstate="collapsed" desc="Btn Reporte Crediticio">
                    JCommandButton btnReporteCrediticio = new JCommandButton("Reporte Crediticio", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/reporte-cuentas.png"), new Dimension(128, 128)));
                    btnReporteCrediticio.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            DlgReporteCrediticio dlgReporteCrediticio = new DlgReporteCrediticio(instance.getFrame(), true);
                            SwingUtil.centerOnScreen(dlgReporteCrediticio);
                            dlgReporteCrediticio.setVisible(true);
                        }
                    });

                    if (objTipoUsuario.verificarAcceso(ACCESO_REPORTE_CREDITICIO)) {
                        bandReportesCuentas.addCommandButton(btnReporteCrediticio, RibbonElementPriority.TOP);
                    }
                    //</editor-fold> Fin Btn Reporte Crediticio

                    //</editor-fold> Fin Band Reportes Cuentas
                    //<editor-fold defaultstate="collapsed" desc="Band Reporte Socios">
                    JRibbonBand bandReporteSocios = new JRibbonBand("Socios", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/config.png"), new Dimension(443, 443)));
                    bandReporteSocios.setResizePolicies((List) Arrays.asList(new CoreRibbonResizePolicies.None(bandReporteSocios.getControlPanel())));

                    //<editor-fold defaultstate="collapsed" desc="Btn Reporte General">
                    JCommandButton btnReporteSociosGeneral = new JCommandButton("General", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/group.png"), new Dimension(128, 128)));
                    btnReporteSociosGeneral.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            IFrmReporteSocios frmReporteSocios = new IFrmReporteSocios(principalJRibbon, true);
                            principalJRibbon.getDesktopPane().add(frmReporteSocios);
                            frmReporteSocios.setVisible(true);
                            try {
                                frmReporteSocios.setMaximum(true);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    });

                    if (objTipoUsuario.verificarAcceso(ACCESO_REPORTE_SOCIOS)) {
                        bandReporteSocios.addCommandButton(btnReporteSociosGeneral, RibbonElementPriority.TOP);
                    }

                    //</editor-fold> Fin Btn Reporte General
                    //<editor-fold defaultstate="collapsed" desc="Btn Reporte Individual">
                    JCommandButton btnReporteSociosIndividual = new JCommandButton("Individual", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/single.png"), new Dimension(128, 128)));
                    btnReporteSociosIndividual.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            IFrmReporteSocios frmReporteSocios = new IFrmReporteSocios(principalJRibbon, false);
                            principalJRibbon.getDesktopPane().add(frmReporteSocios);
                            frmReporteSocios.setVisible(true);
                            try {
                                frmReporteSocios.setMaximum(true);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    });

                    if (objTipoUsuario.verificarAcceso(ACCESO_REPORTE_SOCIOS)) {
                        bandReporteSocios.addCommandButton(btnReporteSociosIndividual, RibbonElementPriority.TOP);
                    }

                    //</editor-fold> Fin Btn Reporte Individual                    
                    //</editor-fold> Fin Band Reporte Socios
                    List<AbstractRibbonBand> lstBandsReportes = new ArrayList<>();

                    lstBandsReportes.add(getBandEstablecimiento());
                    if (objTipoUsuario.verificarAcceso(ACCESO_REPORTE_ESTADO_CUENTA)) {
                        lstBandsReportes.add(bandReportesCuentas);
                    }

                    if (objTipoUsuario.verificarAcceso(ACCESO_REPORTE_SOCIOS)) {
                        lstBandsReportes.add(bandReporteSocios);
                    }

                    RibbonTask ribbonTaskReportes = new RibbonTask("Reportes", lstBandsReportes.toArray(new AbstractRibbonBand[lstBandsReportes.size()]));
                    //</editor-fold> Fin Tab Reportes

                    //<editor-fold defaultstate="collapsed" desc="Tab Configuracion General">
                    //<editor-fold defaultstate="collapsed" desc="Band Configuracion">
                    JRibbonBand bandConfiguracion = new JRibbonBand("Configuracion", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/config.png"), new Dimension(443, 443)));
                    bandConfiguracion.setResizePolicies((List) Arrays.asList(new CoreRibbonResizePolicies.None(bandConfiguracion.getControlPanel())));

                    //<editor-fold defaultstate="collapsed" desc="Btn Tipos de Cambio">     
                    JCommandButton btnTipoCambio = new JCommandButton("Tipo de Cambio", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/change.png"), new Dimension(128, 128)));
                    btnTipoCambio.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            DlgTipoCambio dlgTipoCambio = new DlgTipoCambio(instance.getFrame(), true);
                            SwingUtil.centerOnScreen(dlgTipoCambio);
                            dlgTipoCambio.setVisible(true);
                        }
                    });

                    if (objTipoUsuario.verificarAcceso(ACCESO_TIPO_CAMBIO)) {
                        bandConfiguracion.addCommandButton(btnTipoCambio, RibbonElementPriority.TOP);
                    }
                    //</editor-fold> Fin Btn Tipos de Cambio

                    //<editor-fold defaultstate="collapsed" desc="Btn Empleados">
                    JCommandButton btnEmpleados = new JCommandButton("Empleados", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/team.png"), new Dimension(128, 128)));
                    btnEmpleados.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            IFrmConsultarEmpleados frmConsultarEmpleados = new IFrmConsultarEmpleados();
                            PrincipalJRibbon.setObserverToObservables(frmConsultarEmpleados);
                            PrincipalJRibbon.getInstance().getDesktopPane().add(frmConsultarEmpleados);
                            frmConsultarEmpleados.setVisible(true);
                            try {
                                frmConsultarEmpleados.setMaximum(true);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    });

                    if (objTipoUsuario.verificarAcceso(ACCESO_EMPLEADOS)) {
                        bandConfiguracion.addCommandButton(btnEmpleados, RibbonElementPriority.TOP);
                    }
                    //</editor-fold> Fin Btn Empleados

                    //<editor-fold defaultstate="collapsed" desc="Btn Tipos de Interes">
                    JCommandButton btnTiposInteres = new JCommandButton("Tipos de Interes", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/percentage.png"), new Dimension(128, 128)));
                    btnTiposInteres.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            IFrmConsultarTiposInteres frmConsultarTiposInteres = new IFrmConsultarTiposInteres();
                            PrincipalJRibbon.setObserverToObservables(frmConsultarTiposInteres);
                            PrincipalJRibbon.getInstance().getDesktopPane().add(frmConsultarTiposInteres);
                            frmConsultarTiposInteres.setVisible(true);
                            try {
                                frmConsultarTiposInteres.setMaximum(true);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    });

                    if (objTipoUsuario.verificarAcceso(ACCESO_TIPO_INTERES)) {
                        bandConfiguracion.addCommandButton(btnTiposInteres, RibbonElementPriority.TOP);
                    }
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="Btn Inhabilitar Personas">
                    JCommandButton btnInhabilitarPersonas = new JCommandButton("Inhabilitar Socios", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/user_inhabilitar.png"), new Dimension(128, 128)));
                    btnInhabilitarPersonas.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            IFrmInhabilitarPersonas frmInhabilitarPersonas = new IFrmInhabilitarPersonas();
                            PrincipalJRibbon.setObserverToObservables(frmInhabilitarPersonas);
                            PrincipalJRibbon.getInstance().getDesktopPane().add(frmInhabilitarPersonas);
                            frmInhabilitarPersonas.setVisible(true);
                        }
                    });

                    if (objTipoUsuario.verificarAcceso(ACCESO_INHABILITAR_SOCIOS)) {
                        bandConfiguracion.addCommandButton(btnInhabilitarPersonas, RibbonElementPriority.TOP);
                    }
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="Btn Activar Socios">
                    JCommandButton btnActivarSocios = new JCommandButton("Activar Socios", ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/usuarios.png"), new Dimension(128, 128)));
                    btnActivarSocios.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            IFrmReingresoSocio frmReingresoSocio = new IFrmReingresoSocio();
                            //PrincipalJRibbon.setObserverToObservables(frmReingresoSocio);
                            PrincipalJRibbon.getInstance().getDesktopPane().add(frmReingresoSocio);
                            frmReingresoSocio.setVisible(true);
                        }
                    });

                    if (objTipoUsuario.verificarAcceso(ACCESO_ACTIVAR_SOCIOS)) {
                        bandConfiguracion.addCommandButton(btnActivarSocios, RibbonElementPriority.TOP);
                    }
                    //</editor-fold>

                    List<AbstractRibbonBand> lstBandsConfiguracion = new ArrayList<>();

                    lstBandsConfiguracion.add(getBandEstablecimiento());
                    if (objTipoUsuario.verificarAcceso(ACCESO_TIPO_CAMBIO) || objTipoUsuario.verificarAcceso(ACCESO_EMPLEADOS) || objTipoUsuario.verificarAcceso(ACCESO_TIPO_INTERES)) {
                        lstBandsConfiguracion.add(bandConfiguracion);
                    }

                    RibbonTask ribbonTaskConfiguracion = new RibbonTask("Configuracion General", lstBandsConfiguracion.toArray(new AbstractRibbonBand[lstBandsConfiguracion.size()]));
                    //</editor-fold> Fin Band Configuracion

                    //</editor-fold> Fin Tab Configuracion General
                    principalJRibbon.getRibbon().addTask(ribbonTaskOperaciones);
                    principalJRibbon.getRibbon().addTask(ribbonTaskSocios);
                    principalJRibbon.getRibbon().addTask(ribbonTaskPrestamos);
                    principalJRibbon.getRibbon().addTask(ribbonTaskReportes);
                    principalJRibbon.getRibbon().addTask(ribbonTaskConfiguracion);
                }

                principalJRibbon.setApplicationIcon(ImageWrapperResizableIcon.getIcon(principalJRibbon.getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/icon.png"), new Dimension(231, 231)));
                principalJRibbon.setTitle("Sistema Cooperativa - X3M Software");
                principalJRibbon.getRibbon().setApplicationMenu(new RibbonApplicationMenu());

                principalJRibbon.setVisible(true);
                principalJRibbon.pack();
                principalJRibbon.setBounds(0, 0, 640, 480);
                principalJRibbon.setExtendedState(principalJRibbon.getExtendedState() | javax.swing.JFrame.MAXIMIZED_BOTH);
            }
        });
    }

    @Override
    public JComponent getContentContainer() {
        return desktopPane;
    }

    public static JRibbonBand getBandEstablecimiento() {
        JRibbonBand bandEstablecimiento = new JRibbonBand("Establecimiento", ImageWrapperResizableIcon.getIcon(PrincipalJRibbon.getInstance().getClass().getResourceAsStream("/com/smartech/sistemacooperativa/gui/images/config.png"), new Dimension(443, 443)));
        bandEstablecimiento.setResizePolicies((List) Arrays.asList(new CoreRibbonResizePolicies.None(bandEstablecimiento.getControlPanel())));

        //JRibbonComponent ribbonComponentEstablecimiento;
        if (instance.getObjEstablecimiento() != null) {
            JRibbonComponent ribbonComponentEstablecimiento = new JRibbonComponent(null, "Establecimiento: " + instance.getObjEstablecimiento().getNombre(), new JLabel());
            bandEstablecimiento.addRibbonComponent(ribbonComponentEstablecimiento);
            JRibbonComponent ribbonComponentUsuario = new JRibbonComponent(null, "Usuario: ", new JLabel(instance.getObjPersona().getNombreCompleto() + "     Tipo: " + instance.getObjUsuarioLogeado().getObjTipoUsuario().getNombre()));
            bandEstablecimiento.addRibbonComponent(ribbonComponentUsuario);
            if (instance.getObjUsuarioLogeado().getObjTipoUsuario().isAccesocaja()) {
                JRibbonComponent ribbonComponentCaja = new JRibbonComponent(null, "Caja: " + instance.getObjCaja().getCodigo(), new JLabel());
                bandEstablecimiento.addRibbonComponent(ribbonComponentCaja);
            }
        } else {
            //ribbonComponentEstablecimiento = new JRibbonComponent(null, "Empresa: ", instance.getTxtEstablecimiento());
        }

        return bandEstablecimiento;
    }

    public static void setObserverToObservables(Component observer, Component... lstObservableComponents) {
        if (observer instanceof IObserver) {
            if (lstObservableComponents.length > 0) {
                for (Component component : lstObservableComponents) {
                    if (component instanceof IObservable) {
                        ((IObservable) component).addObserver((IObserver) observer);
                    }
                }
            } else {
                List<Component> lstPrincipalJRibbonComponents = Arrays.asList(instance.getDesktopPane().getComponents());
                for (Component component : lstPrincipalJRibbonComponents) {
                    if (component instanceof IObservable) {
                        ((IObservable) component).addObserver((IObserver) observer);
                    }
                }
            }
        }
    }

    public static void setObservers(Component observable, Component... lstObserverComponents) {
        //List<JInternalFrame> lstJInternalFrames = Arrays.asList(instance.getDesktopPane().getAllFrames());

        if (observable instanceof IObservable) {
            if (lstObserverComponents.length > 0) {
                for (Component component : lstObserverComponents) {
                    if (component instanceof IObserver) {
                        ((IObservable) observable).addObserver((IObserver) component);
                    }
                }
            } else {
                List<Component> lstPrincipalJRibbonComponents = Arrays.asList(instance.getDesktopPane().getComponents());
                for (Component component : lstPrincipalJRibbonComponents) {
                    if (component instanceof IObserver) {
                        ((IObservable) observable).addObserver((IObserver) component);
                    }
                }
            }
        }
    }

}
