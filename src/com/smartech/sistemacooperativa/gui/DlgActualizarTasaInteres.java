package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.dominio.TasaInteres;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import java.math.BigDecimal;

/**
 *
 * @author Smartech
 */
public class DlgActualizarTasaInteres extends javax.swing.JDialog {

    private TasaInteres objTasaInteres = null;

    public TasaInteres getObjTasaInteres() {
        return objTasaInteres;
    }

    public void setObjTasaInteres(TasaInteres objTasaInteres) {
        this.objTasaInteres = objTasaInteres;
    }

    public DlgActualizarTasaInteres(java.awt.Frame parent, boolean modal, TasaInteres objTasaInteres) {
        super(parent, modal);
        initComponents();
        this.setConfiguration();

        this.objTasaInteres = objTasaInteres;
        this.txtNombreTasaInteres.setText(objTasaInteres.getNombre());
        if (this.objTasaInteres.getObjTipoInteres().isPlazoFijo()) {
            this.lblTasa.setText("Monto Plazo Fijo:");
            this.txtTasa.setText(objTasaInteres.getMontoPlazoFijo().toPlainString());
            this.lblTea.setVisible(false);
            this.txtTea.setVisible(false);
        } else {
            this.txtTasa.setText(objTasaInteres.getTasa().toPlainString());
        }
        this.txtTea.setText(objTasaInteres.getTea().toPlainString());
        this.txtTim.setText(objTasaInteres.getTim().toPlainString());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtNombreTasaInteres = new javax.swing.JTextField();
        lblTea = new javax.swing.JLabel();
        txtTea = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtTim = new javax.swing.JTextField();
        lblTasa = new javax.swing.JLabel();
        txtTasa = new javax.swing.JTextField();
        btnAceptar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Tasa de Interes");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Tasa de Interes"));

        jLabel1.setText("Nombre:");

        lblTea.setText("Tasa efectiva anual:");

        jLabel3.setText("Tasa de interes moratorio:");

        lblTasa.setText("Tasa:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblTasa)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTasa, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(lblTea)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTea, javax.swing.GroupLayout.DEFAULT_SIZE, 131, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTim, javax.swing.GroupLayout.DEFAULT_SIZE, 131, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNombreTasaInteres)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtNombreTasaInteres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTea, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(txtTim, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTasa)
                    .addComponent(txtTasa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnAceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_checked.png"))); // NOI18N
        btnAceptar.setText("Aceptar");
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAceptar)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAceptar)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
        if (this.objTasaInteres.getObjTipoInteres().isPlazoFijo() ? !SwingUtil.hasBlankInputs(this.txtNombreTasaInteres, this.txtTasa, this.txtTim) : !SwingUtil.hasBlankInputs(this.txtNombreTasaInteres, this.txtTasa, this.txtTea, this.txtTim)) {
            this.objTasaInteres.setNombre(this.txtNombreTasaInteres.getText().trim());
            if (this.objTasaInteres.getObjTipoInteres().isPlazoFijo()) {
                this.objTasaInteres.setMontoPlazoFijo(new BigDecimal(this.txtTasa.getText().trim()));
            }
            this.objTasaInteres.setTea(new BigDecimal(this.txtTea.getText().trim()));
            this.objTasaInteres.setTim(new BigDecimal(this.txtTim.getText().trim()));

            this.dispose();
        }
    }//GEN-LAST:event_btnAceptarActionPerformed

    private void setConfiguration() {
        SwingUtil.setPositiveNumericInput(this.txtTasa, this.txtTea, this.txtTim);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblTasa;
    private javax.swing.JLabel lblTea;
    private javax.swing.JTextField txtNombreTasaInteres;
    private javax.swing.JTextField txtTasa;
    private javax.swing.JTextField txtTea;
    private javax.swing.JTextField txtTim;
    // End of variables declaration//GEN-END:variables
}
