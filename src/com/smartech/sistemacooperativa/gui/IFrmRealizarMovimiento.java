package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.process.CuentasProcessLogic;
import com.smartech.sistemacooperativa.bll.process.RetirosProcessLogic;
import com.smartech.sistemacooperativa.dominio.Concepto;
import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.TipoDocumentoIdentidad;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.util.interfaces.IObservable;
import com.smartech.sistemacooperativa.util.interfaces.IObserver;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Smartech
 */
public class IFrmRealizarMovimiento extends javax.swing.JInternalFrame implements IObserver {

    private IObserver objObserver;

    public IObserver getObjObserver() {
        return objObserver;
    }

    public void setObjObserver(IObserver objObserver) {
        this.objObserver = objObserver;
    }

    //public static boolean opened = false;
    private Concepto objConcepto = null;

    public Concepto getObjConcepto() {
        return objConcepto;
    }

    public static int COMBO_INDEX_DOCUMENTO_IDENTIDAD_INDISTINTO = 1;
    public static int COMBO_INDEX_NUMERO_CUENTA = 0; // default

    private Cuenta objCuenta = null;
    private Socio objSocio = null;

    private boolean permitirBuscarEnTxtBusarSocio = true;
    private boolean cuentaSeleccionada = false;

    private List<TipoDocumentoIdentidad> lstTiposDocumentoIdentidad = new ArrayList<>();

    public IFrmRealizarMovimiento(Concepto objConcepto) {
        initComponents();

        this.lstTiposDocumentoIdentidad = QueryFacade.getInstance().getAllTiposDocumentoIdentidad();
        this.objConcepto = objConcepto;

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        String newtitle;
        newtitle = getObjConcepto().getNombre() + " de " + dateFormat.format(date);
        this.setTitle(newtitle);

        this.updateCmbModo();

        SwingUtil.setAccountNumberInput(this.txtBuscar);

        this.updateInterfaz();
        pack();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grpbusquedasocio = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtBuscar = new javax.swing.JTextField();
        btnRealizarMovimiento = new javax.swing.JButton();
        cmbModoBuscar = new javax.swing.JComboBox<>();
        btnCancelar = new javax.swing.JButton();
        pnlSocios = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblSocios = new javax.swing.JTable();
        btnVerificarSocio = new javax.swing.JButton();
        btnIngresarTarjeta = new javax.swing.JButton();
        pnlCuentas = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblCuentas = new javax.swing.JTable();
        btnSeleccionaCuenta = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Habilitados");
        setMinimumSize(new java.awt.Dimension(548, 572));

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("Buscar:");

        txtBuscar.setMinimumSize(new java.awt.Dimension(80, 20));
        txtBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarKeyReleased(evt);
            }
        });

        btnRealizarMovimiento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_save.png"))); // NOI18N
        btnRealizarMovimiento.setText("Realizar Movimiento");
        btnRealizarMovimiento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRealizarMovimientoActionPerformed(evt);
            }
        });

        cmbModoBuscar.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione" }));
        cmbModoBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbModoBuscarActionPerformed(evt);
            }
        });

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_cancel.png"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        pnlSocios.setBorder(javax.swing.BorderFactory.createTitledBorder("Descripcion de Socios"));

        tblSocios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblSocios);

        btnVerificarSocio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_checked.png"))); // NOI18N
        btnVerificarSocio.setText("Comprobar Socio");
        btnVerificarSocio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerificarSocioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlSociosLayout = new javax.swing.GroupLayout(pnlSocios);
        pnlSocios.setLayout(pnlSociosLayout);
        pnlSociosLayout.setHorizontalGroup(
            pnlSociosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSociosLayout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 527, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnVerificarSocio)
                .addContainerGap())
        );
        pnlSociosLayout.setVerticalGroup(
            pnlSociosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSociosLayout.createSequentialGroup()
                .addContainerGap(46, Short.MAX_VALUE)
                .addComponent(btnVerificarSocio)
                .addGap(42, 42, 42))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );

        btnIngresarTarjeta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/credit-card.png"))); // NOI18N
        btnIngresarTarjeta.setText("Ingresar tarjeta");
        btnIngresarTarjeta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIngresarTarjetaActionPerformed(evt);
            }
        });

        pnlCuentas.setBorder(javax.swing.BorderFactory.createTitledBorder("Descripcion de Cuentas "));

        tblCuentas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(tblCuentas);

        btnSeleccionaCuenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_checked.png"))); // NOI18N
        btnSeleccionaCuenta.setText("Seleccionar cuenta");
        btnSeleccionaCuenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeleccionaCuentaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlCuentasLayout = new javax.swing.GroupLayout(pnlCuentas);
        pnlCuentas.setLayout(pnlCuentasLayout);
        pnlCuentasLayout.setHorizontalGroup(
            pnlCuentasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCuentasLayout.createSequentialGroup()
                .addComponent(jScrollPane2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSeleccionaCuenta))
        );
        pnlCuentasLayout.setVerticalGroup(
            pnlCuentasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(pnlCuentasLayout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(btnSeleccionaCuenta)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlCuentas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnRealizarMovimiento))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(cmbModoBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBuscar, javax.swing.GroupLayout.DEFAULT_SIZE, 255, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnIngresarTarjeta, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(pnlSocios, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(cmbModoBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnIngresarTarjeta))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlCuentas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlSocios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnRealizarMovimiento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnVerificarSocioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerificarSocioActionPerformed
        System.out.println(this.objCuenta != null ? "(verificar)Exite cuenta" : "(verificar)No existe cuenta");
        System.out.println(RetirosProcessLogic.getInstance().getObjCuentaSeleccionada() != null ? "(verificar)Exite cuenta retiro" : "(verificar)No existe cuenta retiro");
        int selectedRow = this.tblSocios.getSelectedRow();
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, "Seleccione un socio", "Información", JOptionPane.INFORMATION_MESSAGE);
        } else if (this.objCuenta.getLstSocios().get(selectedRow).isActivo()) {
            DlgIngresarTarjeta objDlgIngresarTarjeta = new DlgIngresarTarjeta(PrincipalJRibbon.getInstance().getFrame(), true, this.getObjConcepto().getId() == Concepto.CONCEPTO_RETIROS_CUENTA, this.getObjConcepto().getId() == Concepto.CONCEPTO_RETIROS_CUENTA);
            objDlgIngresarTarjeta.setModal(true);
            SwingUtil.centerOnScreen(objDlgIngresarTarjeta);
            objDlgIngresarTarjeta.setVisible(true);

            if (objDlgIngresarTarjeta.getObjTarjeta() != null) {
                Socio objSocioVerificado = objDlgIngresarTarjeta.getObjTarjeta().getObjSocio();
                System.out.println("Socio verificar: " + objSocioVerificado.getId());
                RetirosProcessLogic.getInstance().actualizarAprobacionSocio(objSocioVerificado, true);
            } else {
                RetirosProcessLogic.getInstance().actualizarAprobacionSocio(this.objCuenta.getLstSocios().get(selectedRow), false);
            }
            this.updateTblSociosHabilitados();
        } else {
            JOptionPane.showMessageDialog(this, "El socio está inactivo", "Error", JOptionPane.ERROR_MESSAGE);
        }

        this.updateInterfaz();
    }//GEN-LAST:event_btnVerificarSocioActionPerformed

    private String listarCuentas() {
        String message = "";
        boolean realizaBusqueda = false;

        if (this.txtBuscar.isEnabled()) {
            if (this.cmbModoBuscar.getSelectedIndex() == COMBO_INDEX_NUMERO_CUENTA) {
                if (this.txtBuscar.getText().trim().length() == Cuenta.DIGITOS_CODIGO) {
                    realizaBusqueda = true;
                    this.objCuenta = QueryFacade.getInstance().getCuentaHabilitada(this.txtBuscar.getText());
                }
            } else if (this.cmbModoBuscar.getSelectedIndex() == COMBO_INDEX_DOCUMENTO_IDENTIDAD_INDISTINTO) {
                if (this.objSocio != null) {
                    if (!this.objSocio.getDocumentoIdentidad().equals(this.txtBuscar.getText()) && TipoDocumentoIdentidad.existeLongitud(this.txtBuscar.getText().trim().length(), this.lstTiposDocumentoIdentidad)) {
                        realizaBusqueda = true;
                        this.objSocio = QueryFacade.getInstance().getSocioByDocumentoIdentidad(this.txtBuscar.getText(), false);
                    }
                } else if (TipoDocumentoIdentidad.existeLongitud(this.txtBuscar.getText().trim().length(), this.lstTiposDocumentoIdentidad)) {
                    realizaBusqueda = true;
                    this.objSocio = QueryFacade.getInstance().getSocioByDocumentoIdentidad(this.txtBuscar.getText(), false);
                }
            } else if (this.txtBuscar.getText().trim().length() == this.lstTiposDocumentoIdentidad.get(this.cmbModoBuscar.getSelectedIndex() - 1).getLongitud()) {
                realizaBusqueda = true;
                this.objSocio = QueryFacade.getInstance().getSocioByDocumentoIdentidad(this.txtBuscar.getText().trim(), this.lstTiposDocumentoIdentidad.get(this.cmbModoBuscar.getSelectedIndex() - 1), false);
            }

            if (realizaBusqueda) {
                if (this.cmbModoBuscar.getSelectedIndex() == COMBO_INDEX_NUMERO_CUENTA) {
                    if (this.objCuenta == null) {
                        message = "No existe cuenta";
                    }
                } else if (this.objSocio == null) {
                    message = "No existe socio";
                } else if (!this.objSocio.isEstado() || !this.objSocio.isActivo()) {
                    message = "El socio ha sido dado de baja";
                }
            }

        } else if (this.objSocio != null) {
            if (this.objSocio.isActivo() && this.objSocio.isEstado()) {
                if (this.objSocio.getLstCuentas().isEmpty()) {
                    message = "No tiene cuentas activas";
                }
            } else {
                message = "No está activo";
            }
        } else {
            message = "No existe socio";
        }

        return message;
    }

    private void btnRealizarMovimientoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRealizarMovimientoActionPerformed
        String message;
        boolean permiteRealizarMovimiento = false;

        if (PrincipalJRibbon.getInstance().getObjCaja().isAbierto()) {

            if (this.getObjConcepto().getId() == Concepto.CONCEPTO_DEPOSITOS) {
                // Solo es necesario que la cuenta exista
                if (this.objCuenta == null) {
                    if (this.objSocio != null) {
                        if (this.objSocio.getLstCuentas().isEmpty()) {
                            message = "No tiene cuentas activas";
                            JOptionPane.showMessageDialog(this, message, "Información", JOptionPane.INFORMATION_MESSAGE);
                        }
                    } else {
                        message = "Busque un socio o una cuenta";
                        JOptionPane.showMessageDialog(this, message, "Información", JOptionPane.INFORMATION_MESSAGE);
                    }
                } else {
                    permiteRealizarMovimiento = CuentasProcessLogic.getInstance().verificarSociosCuenta(this.objCuenta);
                    if (!permiteRealizarMovimiento) {
                        message = "El socio está inactivo";
                        JOptionPane.showMessageDialog(this, message, "Información", JOptionPane.INFORMATION_MESSAGE);
                    }
                }
            } else if (this.getObjConcepto().getId() == Concepto.CONCEPTO_RETIROS_CUENTA) {
                //Es necesario verificar la aprobación de los socios
                if (RetirosProcessLogic.getInstance().verificarAprobacionSocios()) {
                    permiteRealizarMovimiento = true;
                } else {
                    message = "Confirme la aprobación de los socios";
                    JOptionPane.showMessageDialog(this, message, "Información", JOptionPane.INFORMATION_MESSAGE);
                }

            }

            if (permiteRealizarMovimiento) {
                if (this.objCuenta.isEstado() && this.objCuenta.isActivo()) {
                    DlgSolicitarMovimiento objDlgSolicitarMovimiento = new DlgSolicitarMovimiento(PrincipalJRibbon.getInstance().getFrame(), true, this.getObjConcepto(), this.objCuenta);
                    PrincipalJRibbon.setObservers(objDlgSolicitarMovimiento);
                    SwingUtil.centerOnScreen(objDlgSolicitarMovimiento);
                    objDlgSolicitarMovimiento.setVisible(true);
                } else {
                    this.objCuenta = null;
                    this.objSocio = null;
                    this.cuentaSeleccionada = false;
                    JOptionPane.showMessageDialog(this, "La cuenta está deshabilitada", "Información", JOptionPane.INFORMATION_MESSAGE);
                }
                this.updateTblCuentasHabilitadas();
                this.updateTblSociosHabilitados();
                this.updateInterfaz();
            }
        } else {
            JOptionPane.showMessageDialog(this, "La caja está cerrada. Realice la apertura de caja", "Error", JOptionPane.ERROR_MESSAGE);
        }


    }//GEN-LAST:event_btnRealizarMovimientoActionPerformed

    private void txtBuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyReleased
        String message;

        // Buscar cuentas de socio
        message = listarCuentas(); // Busca según los componentes radioButton y Tarjeta ingresada. Retorna un mensaje cuando realiza una búsqueda y no existe socio.
        // No realiza la búsqueda cuando ha sido encontrado un socio recientemente y los datos como DNI o código son los mismos
        // No realiza la búsqueda mientras no complete la cantidad de caracteres según la opción seleccionada
        //Actualiza tabla con las cuentas
        System.out.println(this.objCuenta != null ? "Exite cuenta" : "No existe cuenta");
        System.out.println(this.objSocio != null ? "Existe socio" : "No existe socio");
        this.updateTblCuentasHabilitadas();

        if (this.objCuenta != null) {
            CuentasProcessLogic.getInstance().updateSociosCuenta(this.objCuenta);
        }

        // Muestra mensaje
        if (this.permitirBuscarEnTxtBusarSocio && message.length() > 0) {
            this.updateTblCuentasHabilitadas();
            JOptionPane.showMessageDialog(this, message, "Información", JOptionPane.INFORMATION_MESSAGE);
            this.permitirBuscarEnTxtBusarSocio = false;
        } else {
            this.permitirBuscarEnTxtBusarSocio = true;
        }

    }//GEN-LAST:event_txtBuscarKeyReleased

    private void cmbModoBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbModoBuscarActionPerformed
        this.txtBuscar.setText("");
        this.txtBuscar.setEnabled(true);
        if (this.cmbModoBuscar.getSelectedIndex() == COMBO_INDEX_NUMERO_CUENTA) {
            SwingUtil.setAccountNumberInput(this.txtBuscar);
        } else if (this.cmbModoBuscar.getSelectedIndex() == COMBO_INDEX_DOCUMENTO_IDENTIDAD_INDISTINTO) {
            SwingUtil.setDocumentoIdentidadInput(new TipoDocumentoIdentidad(), this.txtBuscar);
        } else {
            SwingUtil.setDocumentoIdentidadInput(this.lstTiposDocumentoIdentidad.get(this.cmbModoBuscar.getSelectedIndex() - 1), this.txtBuscar);
        }

        if (!this.cuentaSeleccionada) {
            this.objSocio = null;
            this.objCuenta = null;
            ((DefaultTableModel) this.tblCuentas.getModel()).setRowCount(0);
            ((DefaultTableModel) this.tblSocios.getModel()).setRowCount(0);
        }
    }//GEN-LAST:event_cmbModoBuscarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        //opened = false;
        this.dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnIngresarTarjetaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIngresarTarjetaActionPerformed
        this.txtBuscar.setEditable(false);
        this.cmbModoBuscar.setEnabled(false);

        SwingUtil.clear(this.txtBuscar);
        this.objCuenta = null;

//        DlgIngresarTarjeta objDlgIngresarTarjeta = new DlgIngresarTarjeta(PrincipalJRibbon.getInstance().getFrame(), true, this.getObjConcepto().getId() == Concepto.CONCEPTO_RETIROS, this.getObjConcepto().getId() == Concepto.CONCEPTO_RETIROS);
        DlgIngresarTarjeta objDlgIngresarTarjeta = new DlgIngresarTarjeta(PrincipalJRibbon.getInstance().getFrame(), true, false, false);
        objDlgIngresarTarjeta.setModal(true);
        SwingUtil.centerOnScreen(objDlgIngresarTarjeta);
        objDlgIngresarTarjeta.setVisible(true);

        if (objDlgIngresarTarjeta.getObjTarjeta() != null) {
            this.objSocio = objDlgIngresarTarjeta.getObjTarjeta().getObjSocio();
            if(this.objSocio != null && !this.objSocio.isEstado()){
                this.objSocio = null;
                JOptionPane.showMessageDialog(this, "El socio ha sido dado de baja","Error",JOptionPane.ERROR_MESSAGE);
            }
            this.updateTblCuentasHabilitadas();
        }

        this.updateInterfaz();
        this.updateTblCuentasHabilitadas();
    }//GEN-LAST:event_btnIngresarTarjetaActionPerformed

    private void btnSeleccionaCuentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeleccionaCuentaActionPerformed
        int selectedRow = this.tblCuentas.getSelectedRow();
        if (!this.cuentaSeleccionada) {
            if (this.objCuenta == null) {
                if (selectedRow < 0) {
                    if (this.objSocio != null || this.objCuenta != null) {
                        JOptionPane.showMessageDialog(this, "Elige una cuenta", "Información", JOptionPane.INFORMATION_MESSAGE);
                    } else if (this.objConcepto.getId() == Concepto.CONCEPTO_DEPOSITOS) {
                        JOptionPane.showMessageDialog(this, "Busque un socio o una cuenta", "Información", JOptionPane.INFORMATION_MESSAGE);
                    } else if (this.objConcepto.getId() == Concepto.CONCEPTO_RETIROS_CUENTA) {
                        JOptionPane.showMessageDialog(this, "Busque una cuenta", "Información", JOptionPane.INFORMATION_MESSAGE);
                    }
                } else if (this.objSocio != null) {
                    if (this.objSocio.getLstCuentas().size() > 0) {
                        this.objCuenta = this.objSocio.getLstCuentas().get(selectedRow);
                        this.setLstSociosEnCuenta(this.objCuenta);
                        CuentasProcessLogic.getInstance().updateSociosCuenta(this.objCuenta);
                        this.cuentaSeleccionada = true;
                    } else {
                        JOptionPane.showMessageDialog(this, "El socio no cuenta con cuentas disponibles", "Información", JOptionPane.INFORMATION_MESSAGE);
                    }
                }
            } else {
                if (this.objConcepto.getId() == Concepto.CONCEPTO_RETIROS_CUENTA) {
                    RetirosProcessLogic.getInstance().generarSocios(this.objCuenta);
                }
                CuentasProcessLogic.getInstance().updateSociosCuenta(this.objCuenta);
                this.cuentaSeleccionada = true;
            }
        } else {
            this.objCuenta = null;
            this.objSocio = null;
            this.cuentaSeleccionada = false;
        }

        this.updateTblCuentasHabilitadas();
        this.updateTblSociosHabilitados();
        this.updateInterfaz();
    }//GEN-LAST:event_btnSeleccionaCuentaActionPerformed

    private void setLstSociosEnCuenta(Cuenta objCuentaSeleccionada) {
        if (objCuentaSeleccionada != null) {
            if (objCuentaSeleccionada.getLstSocios() != null) {
                objCuentaSeleccionada.setLstSocios(QueryFacade.getInstance().getAllSociosOnly(objCuentaSeleccionada, false));
                RetirosProcessLogic.getInstance().generarSocios(objCuentaSeleccionada);
            }
        }
    }

    private void updateInterfaz() {
//        System.out.println(this.objCuenta != null ? "(update)Exite cuenta" : "(update)No existe cuenta");
//        System.out.println(this.objSocio != null ? "(update)Existe socio" : "(update)No existe socio");

        this.btnSeleccionaCuenta.setText(this.objCuenta != null ? "Cancelar selección" : "Seleccionar cuenta");

        this.txtBuscar.setEditable(this.objCuenta == null);

        this.cmbModoBuscar.setEnabled(this.objCuenta == null);
        if (this.cuentaSeleccionada) {
            //this.cmbModoBuscar.setSelectedIndex(COMBO_INDEX_NUMERO_CUENTA);
            ((DefaultComboBoxModel) this.cmbModoBuscar.getModel()).setSelectedItem("Número de Cuenta");
        }
        if (this.cmbModoBuscar.getSelectedIndex() == COMBO_INDEX_NUMERO_CUENTA) {
            this.txtBuscar.setText(this.objCuenta != null ? this.objCuenta.getCodigo() : "");
        } else {
            this.txtBuscar.setText(this.objSocio != null ? this.objSocio.getDocumentoIdentidad() : "");
        }

        this.btnIngresarTarjeta.setEnabled(this.objCuenta == null);

        if (this.objCuenta == null) {
            this.tblCuentas.clearSelection();
        }

        this.btnVerificarSocio.setVisible(this.objConcepto.getId() == Concepto.CONCEPTO_RETIROS_CUENTA);
        this.btnVerificarSocio.setEnabled(this.objCuenta != null);

        if (this.objConcepto.getId() == Concepto.CONCEPTO_DEPOSITOS) {
            this.btnRealizarMovimiento.setEnabled(this.objCuenta != null && CuentasProcessLogic.getInstance().sociosVerificados(this.objCuenta));
        } else if (this.objConcepto.getId() == Concepto.CONCEPTO_RETIROS_CUENTA) {
            this.btnRealizarMovimiento.setEnabled(this.objCuenta != null && RetirosProcessLogic.getInstance().verificarAprobacionSocios());
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnIngresarTarjeta;
    private javax.swing.JButton btnRealizarMovimiento;
    private javax.swing.JButton btnSeleccionaCuenta;
    private javax.swing.JButton btnVerificarSocio;
    private javax.swing.JComboBox<String> cmbModoBuscar;
    private javax.swing.ButtonGroup grpbusquedasocio;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPanel pnlCuentas;
    private javax.swing.JPanel pnlSocios;
    private javax.swing.JTable tblCuentas;
    private javax.swing.JTable tblSocios;
    private javax.swing.JTextField txtBuscar;
    // End of variables declaration//GEN-END:variables

    private void updateTblCuentasHabilitadas() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tableModel.addColumn("Codigo");
        tableModel.addColumn("Saldo");
        tableModel.addColumn("Fecha de Registro");
        tableModel.addColumn("Fecha de Modificación");
        tableModel.addColumn("Tipo de cuenta");
        tableModel.addColumn("Tasa de interés");
        tableModel.addColumn("Activo");

        List<Cuenta> lstCuentas = new ArrayList<>();
        if (this.objSocio != null) {
            lstCuentas = this.objSocio.getLstCuentas();
        } else if (this.objCuenta != null) {
            Cuenta objCuentaSeleccionada = this.objCuenta;
            lstCuentas.add(objCuentaSeleccionada);
        }

        int size = lstCuentas.size();
//        boolean removed;
        for (int i = 0; i < size; i++) {
            Cuenta objCuentaHabilitada = lstCuentas.get(i);

//            if (objCuentaHabilitada.isEstado()) {
            tableModel.addRow(new Object[]{
                objCuentaHabilitada.getCodigo(),
                objCuentaHabilitada.getSaldo(),
                objCuentaHabilitada.getFecharegistro(),
                objCuentaHabilitada.getFechamodificacion(),
                objCuentaHabilitada.getObjTipoCuenta().getNombre(),
                objCuentaHabilitada.getObjTasaInteres().getNombre(),
                objCuentaHabilitada.isActivo() ? "ACTIVA" : "INACTIVA"
            });
//            }else{
//                removed = lstCuentas.remove(objCuentaHabilitada);
//                if (removed){
//                    size--;
//                    i--;
//                }
//            }
        }

        this.tblCuentas.setModel(tableModel);
    }

    private void updateTblSociosHabilitados() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        if (this.objCuenta != null) {
            tableModel.addColumn("Codigo");
            tableModel.addColumn("Nombres");
            tableModel.addColumn("Apellido paterno");
            tableModel.addColumn("Apellido materno");
            tableModel.addColumn("DNI");
            tableModel.addColumn("RUC");
            tableModel.addColumn("Sexo");
            tableModel.addColumn("Estado");
            tableModel.addColumn("Aprobado");

            String activo, sexo;
            for (Socio objSocioAprobacion : this.objCuenta.getLstSocios()) {

                activo = objSocioAprobacion.isActivo() ? "ACTIVO" : "INACTIVO";
                sexo = objSocioAprobacion.isSexo() ? "MASCULINO" : "FEMENINO";

                tableModel.addRow(new Object[]{
                    objSocioAprobacion.getCodigo(),
                    objSocioAprobacion.getNombre(),
                    objSocioAprobacion.getApellidoPaterno(),
                    objSocioAprobacion.getApellidoMaterno(),
                    objSocioAprobacion.getDocumentoIdentidad(),
                    objSocioAprobacion.getRUC(),
                    sexo,
                    activo,
                    //this.objConcepto.getId() == Concepto.CONCEPTO_RETIROS ? RetirosProcessLogic.getInstance().verificarAprobacionSocio(objSocioAprobacion) ? "APROBADO" : "NO APROBADO" : "APROBADO"
                    objSocioAprobacion.isActivo() ? this.objConcepto.getId() == Concepto.CONCEPTO_RETIROS_CUENTA ? RetirosProcessLogic.getInstance().verificarAprobacionSocio(objSocioAprobacion) ? "APROBADO" : "DESAPROBADO" : "APROBADO" : "DESAPROBADO"
                });
            }
        }

        this.tblSocios.setModel(tableModel);
    }

    private void updateCmbModo() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        comboBoxModel.addElement("Número de Cuenta");
        if (this.getObjConcepto().getId() == Concepto.CONCEPTO_DEPOSITOS) {
            for (TipoDocumentoIdentidad objTipoDocumentoIdentidad : this.lstTiposDocumentoIdentidad) {
                comboBoxModel.addElement(objTipoDocumentoIdentidad.getNombre());
            }
            comboBoxModel.addElement("Indistinto");
            COMBO_INDEX_DOCUMENTO_IDENTIDAD_INDISTINTO = this.lstTiposDocumentoIdentidad.size() + 1;
        }

        this.cmbModoBuscar.setModel(comboBoxModel);
    }

    private void update() {
        this.listarCuentas();
        this.updateTblSociosHabilitados();
        this.updateTblCuentasHabilitadas();
        this.updateInterfaz();
    }

    @Override
    public void update(IObservable objIObservable) {
        this.update();
    }

}
