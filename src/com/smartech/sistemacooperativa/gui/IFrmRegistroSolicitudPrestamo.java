package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.facade.ConfigFacade;
import com.smartech.sistemacooperativa.bll.process.PrestamosProcessLogic;
import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.facade.ReportFacade;
import com.smartech.sistemacooperativa.dominio.Aprobacion;
import com.smartech.sistemacooperativa.dominio.DetalleDocumentoSolicitud;
import com.smartech.sistemacooperativa.dominio.DetalleSolicitudPrestamoSocio;
import com.smartech.sistemacooperativa.dominio.Documento;
import com.smartech.sistemacooperativa.dominio.Prestamo;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.SolicitudPrestamo;
import com.smartech.sistemacooperativa.dominio.TasaInteres;
import com.smartech.sistemacooperativa.dominio.TipoInteres;
import com.smartech.sistemacooperativa.dominio.TipoPrestamo;
import com.smartech.sistemacooperativa.dominio.TipoSolicitud;
import com.smartech.sistemacooperativa.gui.util.AWTUtil;
import com.smartech.sistemacooperativa.gui.util.MyFingerprintVerification;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import com.smartech.sistemacooperativa.util.interfaces.IObservable;
import com.smartech.sistemacooperativa.util.interfaces.IObserver;
import com.smartech.sistemacooperativa.util.reporting.ReportUtil;
import java.awt.Color;
import java.io.File;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.pentaho.reporting.engine.classic.core.parameters.ParameterDefinitionEntry;

/**
 *
 * @author Smartech
 */
public class IFrmRegistroSolicitudPrestamo extends javax.swing.JInternalFrame implements IObservable {

    private PrincipalJRibbon principalJRibbon;
    private List<IObserver> lstIObservers = new ArrayList<>();

    private Socio objSocio = null;
    private SolicitudPrestamo objSolicitudPrestamo = null;
    //Tipo solicitud de prestamo = 2
    private TipoSolicitud objTipoSolicitud = QueryFacade.getInstance().getTipoSolicitud(2, PrincipalJRibbon.getInstance().getObjEstablecimiento());
    private List<DetalleDocumentoSolicitud> lstDetalleDocumentoSolicitud = new ArrayList<>();
    private List<TipoInteres> lstTiposInteres = new ArrayList<>();
    private List<TasaInteres> lstTasasInteres = new ArrayList<>();
    private List<TipoPrestamo> lstTiposPrestamo = new ArrayList<>();
    private List<Aprobacion> lstAprobaciones = new ArrayList<>();

    private List<Socio> lstGarantes = new ArrayList<>();

    private Map<Documento, File> mapArchivos = new HashMap<>();

    private boolean verificado = false;

    public IFrmRegistroSolicitudPrestamo(PrincipalJRibbon principalJRibbon) {
        initComponents();
        AWTUtil.setUpperCase();
        this.principalJRibbon = principalJRibbon;

        this.setConfiguration();

        this.lstDetalleDocumentoSolicitud = this.objTipoSolicitud.generarDetalleDocumentoSolicitud(this.principalJRibbon.getObjUsuarioLogeado());
        this.lstAprobaciones = this.objTipoSolicitud.generarAprobaciones();

        this.txtFechaSolicitud.setText(DateUtil.getRegularDate(new Date()));
        this.txtNumeroSolicitud.setText(PrestamosProcessLogic.getInstance().generarCodigoSolicitudPrestamo());

        this.updateLblEstadoSocio();

        this.updateTblDocumentos();
        this.updateTblAprobaciones();

        this.updateLstTiposInteres();
        this.updateCmbTiposInteres();
        this.updateCmbTasasInteres();
        this.updateLstTiposPrestamo();
        this.updateCmbTiposPrestamo();
    }

    public IFrmRegistroSolicitudPrestamo(PrincipalJRibbon principalJRibbon, SolicitudPrestamo objSolicitudPrestamo) {
        initComponents();
        this.principalJRibbon = principalJRibbon;
        this.objSolicitudPrestamo = objSolicitudPrestamo;

        this.btnBuscarSocio.setEnabled(false);

        this.setConfiguration();

        this.objSocio = objSolicitudPrestamo.getObjSocioPrestatario();

        this.objTipoSolicitud = this.objSolicitudPrestamo.getObjTipoSolicitud();
        this.lstDetalleDocumentoSolicitud = this.objSolicitudPrestamo.getLstDetalleDocumentoSolicitud();
        this.lstAprobaciones = this.objSolicitudPrestamo.getLstAprobaciones();

        this.txtMontoSolicitado.setEditable(false);

        this.txtNumeroSolicitud.setText(objSolicitudPrestamo.getCodigo());
        this.txtCuotas.setText(String.valueOf(objSolicitudPrestamo.getCuotas()));
        this.txtFechaSolicitud.setText(DateUtil.getRegularDate(DateUtil.getDate(this.objSolicitudPrestamo.getFechaRegistro())));
        this.txtSaldoAportes.setText(PrestamosProcessLogic.getInstance().calcularSaldoAportes(this.objSocio).toPlainString());
        this.txtMontoSolicitado.setText(this.objSolicitudPrestamo.getMontoSolicitado().toPlainString());
        this.txtMontoAprobado.setText(this.objSolicitudPrestamo.getMontoAprobado().equals(BigDecimal.ZERO) ? "" : this.objSolicitudPrestamo.getMontoAprobado().toPlainString());
        this.txtOtrosIngresosSocio.setText(this.objSolicitudPrestamo.getOtrosIngresos().toPlainString());
        this.txtNivelGastosSocio.setText(this.objSolicitudPrestamo.getNivelGastos().toPlainString());
        this.txtMotivo.setText(this.objSolicitudPrestamo.getMotivo());
        this.txtMotivoDesaprobacion.setText(this.objSolicitudPrestamo.getMotivoDesaprobacion());

        this.lstGarantes = this.objSolicitudPrestamo.getLstSociosGarantes();
        this.updateTblFiadores();
        this.txtMontoDisponible.setText(PrestamosProcessLogic.getInstance().calcularMontoDisponiblePrestamo(this.objSocio, this.tblGarantes.getModel()).toPlainString());

        this.updateLblEstadoSocio();

        this.updateCamposSocio();
        this.updateTblDocumentos();
        this.updateTblAprobaciones();

        this.updateLstTiposInteres();
        this.updateCmbTiposInteres();
        this.cmbTiposInteres.setSelectedItem(this.objSolicitudPrestamo.getObjTasaInteres().getObjTipoInteres().getNombre());
        this.updateCmbTasasInteres();
        this.updateLstTiposPrestamo();
        this.updateCmbTiposPrestamo();
        this.cmbTiposPrestamo.setSelectedItem(this.objSolicitudPrestamo.getObjTipoPrestamo().getNombre());
    }

    private void setConfiguration() {
        SwingUtil.setPositiveNumericInput(this.txtNivelIngresosSocio, this.txtOtrosIngresosSocio, this.txtNivelGastosSocio, this.txtMontoSolicitado, this.txtMontoAprobado, this.txtCuotas);
        SwingUtil.setPhoneInput(this.txtCuotas);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel6 = new javax.swing.JPanel();
        jLabel34 = new javax.swing.JLabel();
        txtNumeroSolicitud = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        txtFechaSolicitud = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        btnBuscarSocio = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        txtNombreSocio = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtDniSocio = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        txtRucSocio = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        txtApellidosSocio = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        txtSexoSocio = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        txtEstadoCivilSocio = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        txtDireccionSocio = new javax.swing.JLabel();
        txtUbigeoSocio = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        txtTelefonoCasaSocio = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        txtCelularSocio = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        txtNivelIngresosSocio = new javax.swing.JTextField();
        txtOtrosIngresosSocio = new javax.swing.JTextField();
        txtNivelGastosSocio = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        btnVerificarHuellas = new javax.swing.JButton();
        lblEstadoSocio = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnAgregarFiador = new javax.swing.JButton();
        btnEliminarFiador = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblGarantes = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtMotivo = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtMontoSolicitado = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtCuotas = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtSaldoAportes = new javax.swing.JLabel();
        txtMontoDisponible = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtMotivoDesaprobacion = new javax.swing.JTextField();
        txtMontoAprobado = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        cmbTiposInteres = new javax.swing.JComboBox<>();
        jLabel11 = new javax.swing.JLabel();
        cmbTasasInteres = new javax.swing.JComboBox<>();
        jLabel13 = new javax.swing.JLabel();
        cmbTiposPrestamo = new javax.swing.JComboBox<>();
        pnlDocumentos = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblDocumentos = new javax.swing.JTable();
        btnAprobar = new javax.swing.JButton();
        btnDeshabilitar = new javax.swing.JButton();
        btnCargarDocumento = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblAprobaciones = new javax.swing.JTable();
        btnAprobarSolicitud = new javax.swing.JButton();
        btnDesaprobarSolicitud = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Solicitud de Prestamo");

        jLabel34.setText("Numero de Solicitud:");

        txtNumeroSolicitud.setText("-");

        jLabel37.setText("Fecha de Solicitud:");

        txtFechaSolicitud.setText("-");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Socio"));

        btnBuscarSocio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/lupa.png"))); // NOI18N
        btnBuscarSocio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarSocioActionPerformed(evt);
            }
        });

        jLabel10.setText("Nombres:");

        txtNombreSocio.setText("-");
        txtNombreSocio.setMinimumSize(new java.awt.Dimension(4, 200));

        jLabel12.setText("DNI:");

        txtDniSocio.setText("-");

        jLabel14.setText("RUC:");

        txtRucSocio.setText("-");

        jLabel16.setText("Apellidos:");

        txtApellidosSocio.setText("-");

        jLabel18.setText("Sexo:");

        txtSexoSocio.setText("-");

        jLabel1.setText("Estado civil:");

        txtEstadoCivilSocio.setText("-");

        jLabel20.setText("Direccion:");

        jLabel21.setText("Ubigeo:");

        txtDireccionSocio.setText("-");

        txtUbigeoSocio.setText("-");

        jLabel24.setText("Telefono Casa:");

        txtTelefonoCasaSocio.setText("-");

        jLabel26.setText("Celular:");

        txtCelularSocio.setText("-");

        jLabel28.setText("Nivel de Ingresos:");

        jLabel30.setText("Otros Ingresos:");

        jLabel32.setText("Nivel de Gastos:");

        txtNivelIngresosSocio.setEditable(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel16, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel20, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtApellidosSocio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(txtNombreSocio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGap(4, 4, 4)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel18)
                                            .addComponent(jLabel12))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtSexoSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtDniSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtDireccionSocio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(txtUbigeoSocio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel24)
                                            .addComponent(jLabel26))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtTelefonoCasaSocio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(txtCelularSocio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtEstadoCivilSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtRucSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(btnBuscarSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(8, 8, 8))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(44, 44, 44)
                                .addComponent(txtNivelIngresosSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel30)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtOtrosIngresosSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel32)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtNivelGastosSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel28)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnBuscarSocio)
                    .addComponent(txtRucSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDniSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtNombreSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtSexoSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtEstadoCivilSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtApellidosSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
                    .addComponent(txtDireccionSocio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtTelefonoCasaSocio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCelularSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel21)
                        .addComponent(txtUbigeoSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel28)
                    .addComponent(jLabel30)
                    .addComponent(jLabel32)
                    .addComponent(txtNivelIngresosSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtOtrosIngresosSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNivelGastosSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Verificacion Biometrica"));

        btnVerificarHuellas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/huella_41x41.png"))); // NOI18N
        btnVerificarHuellas.setText("Comprobar Huella");
        btnVerificarHuellas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerificarHuellasActionPerformed(evt);
            }
        });

        lblEstadoSocio.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblEstadoSocio.setText("jLabel15");
        lblEstadoSocio.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnVerificarHuellas))
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblEstadoSocio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnVerificarHuellas)
                .addGap(18, 18, 18)
                .addComponent(lblEstadoSocio)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Garantes"));

        btnAgregarFiador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_add.png"))); // NOI18N
        btnAgregarFiador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarFiadorActionPerformed(evt);
            }
        });

        btnEliminarFiador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/basura.png"))); // NOI18N
        btnEliminarFiador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarFiadorActionPerformed(evt);
            }
        });

        tblGarantes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(tblGarantes);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnEliminarFiador, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAgregarFiador, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(btnAgregarFiador)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEliminarFiador)
                .addGap(0, 66, Short.MAX_VALUE))
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Informacion Economica"));

        jLabel3.setText("Motivo:");

        jLabel4.setText("Saldo en cuenta de aportes:");

        jLabel5.setText("Monto disponible para préstamo:");

        jLabel6.setText("Monto Solicitado:");

        jLabel7.setText("Plazo (cuotas):");

        jLabel8.setText("Monto aprobado:");

        txtSaldoAportes.setText("-");

        txtMontoDisponible.setText("-");

        jLabel2.setText("Motivo de desaprobacion:");

        jLabel9.setText("Tipo de Interes:");

        cmbTiposInteres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTiposInteresActionPerformed(evt);
            }
        });

        jLabel11.setText("Tasa de Interes:");

        jLabel13.setText("Tipo de Prestamo:");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtMotivo))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtMotivoDesaprobacion))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtSaldoAportes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtMontoDisponible, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtMontoSolicitado)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCuotas))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtMontoAprobado)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbTiposPrestamo, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbTiposInteres, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbTasasInteres, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtMotivo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel6)
                    .addComponent(txtMontoSolicitado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSaldoAportes)
                    .addComponent(jLabel5)
                    .addComponent(txtMontoDisponible)
                    .addComponent(jLabel7)
                    .addComponent(txtCuotas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtMontoAprobado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(cmbTiposInteres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(cmbTasasInteres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13)
                    .addComponent(cmbTiposPrestamo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtMotivoDesaprobacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pnlDocumentos.setBorder(javax.swing.BorderFactory.createTitledBorder("Documentos"));

        tblDocumentos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(tblDocumentos);

        btnAprobar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_checked.png"))); // NOI18N
        btnAprobar.setText("Aprobar Documento");
        btnAprobar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAprobarActionPerformed(evt);
            }
        });

        btnDeshabilitar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_cancel.png"))); // NOI18N
        btnDeshabilitar.setText("Desaprobar Documento");
        btnDeshabilitar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeshabilitarActionPerformed(evt);
            }
        });

        btnCargarDocumento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_upload.png"))); // NOI18N
        btnCargarDocumento.setText("Cargar Documento");
        btnCargarDocumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCargarDocumentoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlDocumentosLayout = new javax.swing.GroupLayout(pnlDocumentos);
        pnlDocumentos.setLayout(pnlDocumentosLayout);
        pnlDocumentosLayout.setHorizontalGroup(
            pnlDocumentosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDocumentosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDocumentosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnDeshabilitar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAprobar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnCargarDocumento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlDocumentosLayout.setVerticalGroup(
            pnlDocumentosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDocumentosLayout.createSequentialGroup()
                .addComponent(btnAprobar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDeshabilitar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCargarDocumento)
                .addContainerGap(90, Short.MAX_VALUE))
            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Aprobaciones"));

        tblAprobaciones.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane4.setViewportView(tblAprobaciones);

        btnAprobarSolicitud.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_checked.png"))); // NOI18N
        btnAprobarSolicitud.setText("Aprobar Solicitud");
        btnAprobarSolicitud.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAprobarSolicitudActionPerformed(evt);
            }
        });

        btnDesaprobarSolicitud.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_cancel.png"))); // NOI18N
        btnDesaprobarSolicitud.setText("Desaprobar Solicitud");
        btnDesaprobarSolicitud.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDesaprobarSolicitudActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnDesaprobarSolicitud, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAprobarSolicitud, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(btnAprobarSolicitud)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDesaprobarSolicitud)
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );

        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_save.png"))); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel34)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNumeroSolicitud, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(539, 539, 539)
                        .addComponent(jLabel37)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtFechaSolicitud, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 67, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addComponent(pnlDocumentos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnGuardar, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel34)
                    .addComponent(txtNumeroSolicitud)
                    .addComponent(txtFechaSolicitud)
                    .addComponent(jLabel37))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlDocumentos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnGuardar)
                .addContainerGap())
        );

        jScrollPane1.setViewportView(jPanel6);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 803, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAgregarFiadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarFiadorActionPerformed
        if (this.objSocio != null) {
            DlgBuscarSocio dlgBuscarSocio = new DlgBuscarSocio(this.principalJRibbon.getFrame(), true, true);
            SwingUtil.centerOnScreen(dlgBuscarSocio);
            dlgBuscarSocio.setVisible(true);

            Socio objSocioGarante = dlgBuscarSocio.getObjSocioSeleccionado();
            if (objSocioGarante != null) {
                if (PrestamosProcessLogic.getInstance().verificarGarante(this.objSocio, objSocioGarante, this.lstGarantes)) {
                    String message = PrestamosProcessLogic.getInstance().verificarDisponibilidadGarante(objSocioGarante);
                    if (message.contains("Disponible")) {
                        this.lstGarantes.add(objSocioGarante);
                        this.updateTblFiadores();
                        this.txtMontoDisponible.setText(PrestamosProcessLogic.getInstance().calcularMontoDisponiblePrestamo(this.objSocio, this.tblGarantes.getModel()).toPlainString());
                    }

                    JOptionPane.showMessageDialog(this, message, "Garante", message.contains("Disponible") ? JOptionPane.INFORMATION_MESSAGE : JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this, "No se puede agregar al Garante\nRevise los datos", "Garante", JOptionPane.ERROR_MESSAGE);
                }
            }
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un Socio", "Socio", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnAgregarFiadorActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        if (SwingUtil.hasBlankInputs(this.txtNivelIngresosSocio, this.txtOtrosIngresosSocio, this.txtNivelGastosSocio, this.txtMontoSolicitado, this.txtCuotas)) {
            JOptionPane.showMessageDialog(this, "Error en el registro\n Revise los campos", "Error", JOptionPane.ERROR_MESSAGE);
        } else if (this.verificado) {
            String message = "";
            Prestamo objPrestamo = null;
            Map<String, Prestamo> resultMap = null;

            int confirmar = JOptionPane.showConfirmDialog(this, "¿Desea confirmar solicitud?", "Solicitud de préstamo", JOptionPane.YES_NO_OPTION);
            if (confirmar != JOptionPane.YES_OPTION) {
                return;
            }

            if (this.objSolicitudPrestamo == null || this.objSolicitudPrestamo.getId() == 0) {
                List<DetalleSolicitudPrestamoSocio> lstDetalleSolicitudPrestamoSocio = PrestamosProcessLogic.getInstance().getAllDetalleSolicitudPrestamoSociosFiadores(this.lstGarantes, this.tblGarantes.getModel());
                lstDetalleSolicitudPrestamoSocio.add(PrestamosProcessLogic.getInstance().getDetalleSolicitudPrestamoSocio(this.objSocio, new BigDecimal(this.txtNivelIngresosSocio.getText().trim()), new BigDecimal(this.txtOtrosIngresosSocio.getText().trim()), new BigDecimal(this.txtNivelGastosSocio.getText().trim())));

                resultMap = PrestamosProcessLogic.getInstance().generarSolicitud(
                        this.txtNumeroSolicitud.getText().trim(),
                        Integer.parseInt(this.txtCuotas.getText()),
                        this.txtMotivo.getText().trim(),
                        new BigDecimal(this.txtOtrosIngresosSocio.getText().trim()),
                        new BigDecimal(this.txtNivelGastosSocio.getText().trim()),
                        new BigDecimal(this.txtMontoDisponible.getText().trim()),
                        new BigDecimal(this.txtMontoSolicitado.getText().trim()),
                        this.txtMontoAprobado.getText().trim().isEmpty() ? BigDecimal.ZERO : new BigDecimal(this.txtMontoAprobado.getText().trim()),
                        this.objSocio,
                        PrincipalJRibbon.getInstance().getObjUsuarioLogeado(),
                        this.objTipoSolicitud,
                        this.lstTiposInteres.get(this.cmbTiposInteres.getSelectedIndex()).getLstTasasInteres().get(this.cmbTasasInteres.getSelectedIndex()),
                        this.lstTiposPrestamo.get(this.cmbTiposPrestamo.getSelectedIndex()),
                        lstDetalleSolicitudPrestamoSocio,
                        this.lstDetalleDocumentoSolicitud,
                        this.lstAprobaciones,
                        this.mapArchivos);
            } else {
                List<DetalleSolicitudPrestamoSocio> lstDetalleSolicitudPrestamoSocio = PrestamosProcessLogic.getInstance().getAllDetalleSolicitudPrestamoSociosFiadores(this.lstGarantes, this.tblGarantes.getModel());
                lstDetalleSolicitudPrestamoSocio.add(PrestamosProcessLogic.getInstance().getDetalleSolicitudPrestamoSocio(this.objSocio, new BigDecimal(this.txtNivelIngresosSocio.getText().trim()), new BigDecimal(this.txtOtrosIngresosSocio.getText().trim()), new BigDecimal(this.txtNivelGastosSocio.getText().trim())));

                resultMap = PrestamosProcessLogic.getInstance().actualizarSolicitud(
                        this.objSolicitudPrestamo,
                        Integer.parseInt(this.txtCuotas.getText().trim()),
                        this.txtMotivo.getText().trim(),
                        this.txtMotivoDesaprobacion.getText().trim(),
                        new BigDecimal(this.txtOtrosIngresosSocio.getText().trim()),
                        new BigDecimal(this.txtNivelGastosSocio.getText().trim()),
                        new BigDecimal(this.txtMontoDisponible.getText().trim()),
                        new BigDecimal(this.txtMontoSolicitado.getText().trim()),
                        new BigDecimal(this.txtMontoAprobado.getText().trim()),
                        PrincipalJRibbon.getInstance().getObjUsuarioLogeado(),
                        this.objTipoSolicitud,
                        this.lstTiposInteres.get(this.cmbTiposInteres.getSelectedIndex()).getLstTasasInteres().get(this.cmbTasasInteres.getSelectedIndex()),
                        this.lstTiposPrestamo.get(this.cmbTiposPrestamo.getSelectedIndex()),
                        lstDetalleSolicitudPrestamoSocio,
                        this.lstDetalleDocumentoSolicitud,
                        this.lstAprobaciones,
                        this.mapArchivos);
            }

            if (resultMap != null) {
                for (Map.Entry<String, Prestamo> entry : resultMap.entrySet()) {
                    message = entry.getKey();
                    objPrestamo = entry.getValue();
                }

                JOptionPane.showMessageDialog(this, message, "Solicitud", message.contains("Error") ? JOptionPane.ERROR_MESSAGE : JOptionPane.INFORMATION_MESSAGE);

                if (/*message.contains("generado") && */objPrestamo != null) {
                    int answer = JOptionPane.showConfirmDialog(this, "¿Desea ajustar el Plan de Pagos?", "Plan de Pagos", JOptionPane.YES_NO_OPTION);
                    if (answer == JOptionPane.YES_OPTION) {
                        DlgAjustarCuotas dlgAjustarCuotas = new DlgAjustarCuotas(PrincipalJRibbon.getInstance().getFrame(), true, objPrestamo);
                        SwingUtil.centerOnScreen(dlgAjustarCuotas);
                        dlgAjustarCuotas.setVisible(true);
                        objPrestamo = dlgAjustarCuotas.getObjNuevoPrestamo();
                    }
                    answer = JOptionPane.showConfirmDialog(this, "¿Desea imprimir la solicitud?", "Solicitud", JOptionPane.YES_NO_OPTION);
                    if (answer == JOptionPane.YES_OPTION) {
                        this.imprimirFichaSolicitudPrestamo(objPrestamo, false);
                    }
                }

                if (!message.contains("Error")) {
                    this.notifyObservers();
                    this.dispose();
                }
            }
        } else {
            JOptionPane.showMessageDialog(this, "El Socio no ha sido verificado.", "Fiador", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnEliminarFiadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarFiadorActionPerformed
        int index = this.tblGarantes.getSelectedRow();

        if (index != -1) {
            this.lstGarantes.remove(index);
            this.txtMontoDisponible.setText(PrestamosProcessLogic.getInstance().calcularMontoDisponiblePrestamo(this.objSocio, this.tblGarantes.getModel()).toPlainString());
            this.updateTblFiadores();
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un Fiador", "Fiador", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnEliminarFiadorActionPerformed

    private void btnBuscarSocioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarSocioActionPerformed
        DlgBuscarSocio dlgBuscarSocio = new DlgBuscarSocio(this.principalJRibbon.getFrame(), true, true);
        SwingUtil.centerOnScreen(dlgBuscarSocio);
        dlgBuscarSocio.setVisible(true);
        Socio objSocioSeleccionado = dlgBuscarSocio.getObjSocioSeleccionado();
        if (objSocioSeleccionado != null) {
            if (PrestamosProcessLogic.getInstance().verificarSocio(objSocioSeleccionado, this.lstGarantes)) {
                this.objSocio = objSocioSeleccionado;
                this.updateCamposSocio();
                this.txtSaldoAportes.setText(PrestamosProcessLogic.getInstance().calcularSaldoAportes(this.objSocio).toPlainString());
                this.txtMontoDisponible.setText(PrestamosProcessLogic.getInstance().calcularMontoDisponiblePrestamo(this.objSocio, this.tblGarantes.getModel()).toPlainString());
            } else {
                JOptionPane.showMessageDialog(this, "No se puede agregar al Socio\nRevise los datos", "Socio", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btnBuscarSocioActionPerformed

    private void cmbTiposInteresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTiposInteresActionPerformed
        this.updateLstTasasInteres();
        this.updateCmbTasasInteres();
    }//GEN-LAST:event_cmbTiposInteresActionPerformed

    private void btnVerificarHuellasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerificarHuellasActionPerformed
        if (this.objSocio != null) {
            MyFingerprintVerification myFingerprintVerification = new MyFingerprintVerification(PrincipalJRibbon.getInstance().getFrame(), true, this.objSocio.getObjUsuario());
            SwingUtil.centerOnScreen(myFingerprintVerification);
            myFingerprintVerification.setVisible(true);

            this.verificado = myFingerprintVerification.isMatched();
            this.updateLblEstadoSocio();

            if (this.verificado) {
                JOptionPane.showMessageDialog(this, "Socio autenticado", "Socio", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, "No se autentico al Socio", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un Socio", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnVerificarHuellasActionPerformed

    private void btnAprobarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAprobarActionPerformed
        int index = this.tblDocumentos.getSelectedRow();
        if (index != -1) {
            this.lstDetalleDocumentoSolicitud.get(index).setAprobado(true);
            this.lstDetalleDocumentoSolicitud.get(index).setFechaEntrega(DateUtil.currentTimestamp());
            this.updateTblDocumentos();
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un Documento", "Documentos", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnAprobarActionPerformed

    private void btnDeshabilitarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeshabilitarActionPerformed
        int index = this.tblDocumentos.getSelectedRow();
        if (index != -1) {
            this.lstDetalleDocumentoSolicitud.get(index).setAprobado(false);
            this.lstDetalleDocumentoSolicitud.get(index).setFechaEntrega(null);
            this.updateTblDocumentos();
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un Documento", "Documentos", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnDeshabilitarActionPerformed

    private void btnDesaprobarSolicitudActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDesaprobarSolicitudActionPerformed
        TipoSolicitud.updateAprobacion(PrincipalJRibbon.getInstance().getObjUsuarioLogeado().getObjTipoUsuario(), this.lstAprobaciones, false);
        this.updateTblAprobaciones();
    }//GEN-LAST:event_btnDesaprobarSolicitudActionPerformed

    private void btnAprobarSolicitudActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAprobarSolicitudActionPerformed
        TipoSolicitud.updateAprobacion(PrincipalJRibbon.getInstance().getObjUsuarioLogeado().getObjTipoUsuario(), this.lstAprobaciones, true);
        this.updateTblAprobaciones();
    }//GEN-LAST:event_btnAprobarSolicitudActionPerformed

    private void btnCargarDocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCargarDocumentoActionPerformed
        int index = this.tblDocumentos.getSelectedRow();

        if (index != -1) {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("PDF Documents", "pdf"));
            fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Word Documents", "docx", "doc"));
            fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Images", "jpg", "png"));
            fileChooser.setAcceptAllFileFilterUsed(true);
            int result = fileChooser.showOpenDialog(this);
            if (result == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fileChooser.getSelectedFile();
                System.out.println("Selected file: " + selectedFile.getAbsolutePath());
                this.mapArchivos.put(this.lstDetalleDocumentoSolicitud.get(index).getObjDocumento(), selectedFile);
                JOptionPane.showMessageDialog(this, "Documento cargado con exito.", "Documento", JOptionPane.INFORMATION_MESSAGE);
                this.updateTblDocumentos();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un Documento", "Documentos", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnCargarDocumentoActionPerformed

    //<editor-fold defaultstate="collapsed" desc="Variables">
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregarFiador;
    private javax.swing.JButton btnAprobar;
    private javax.swing.JButton btnAprobarSolicitud;
    private javax.swing.JButton btnBuscarSocio;
    private javax.swing.JButton btnCargarDocumento;
    private javax.swing.JButton btnDesaprobarSolicitud;
    private javax.swing.JButton btnDeshabilitar;
    private javax.swing.JButton btnEliminarFiador;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnVerificarHuellas;
    private javax.swing.JComboBox<String> cmbTasasInteres;
    private javax.swing.JComboBox<String> cmbTiposInteres;
    private javax.swing.JComboBox<String> cmbTiposPrestamo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JLabel lblEstadoSocio;
    private javax.swing.JPanel pnlDocumentos;
    private javax.swing.JTable tblAprobaciones;
    private javax.swing.JTable tblDocumentos;
    private javax.swing.JTable tblGarantes;
    private javax.swing.JLabel txtApellidosSocio;
    private javax.swing.JLabel txtCelularSocio;
    private javax.swing.JTextField txtCuotas;
    private javax.swing.JLabel txtDireccionSocio;
    private javax.swing.JLabel txtDniSocio;
    private javax.swing.JLabel txtEstadoCivilSocio;
    private javax.swing.JLabel txtFechaSolicitud;
    private javax.swing.JTextField txtMontoAprobado;
    private javax.swing.JLabel txtMontoDisponible;
    private javax.swing.JTextField txtMontoSolicitado;
    private javax.swing.JTextField txtMotivo;
    private javax.swing.JTextField txtMotivoDesaprobacion;
    private javax.swing.JTextField txtNivelGastosSocio;
    private javax.swing.JTextField txtNivelIngresosSocio;
    private javax.swing.JLabel txtNombreSocio;
    private javax.swing.JLabel txtNumeroSolicitud;
    private javax.swing.JTextField txtOtrosIngresosSocio;
    private javax.swing.JLabel txtRucSocio;
    private javax.swing.JLabel txtSaldoAportes;
    private javax.swing.JLabel txtSexoSocio;
    private javax.swing.JLabel txtTelefonoCasaSocio;
    private javax.swing.JLabel txtUbigeoSocio;
    // End of variables declaration//GEN-END:variables
//</editor-fold>

    private void imprimirFichaSolicitudPrestamo(Prestamo objPrestamo, boolean mostrarReporte) {
        try {
            Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> reportData = ReportFacade.getInstance().getFichaSolicitudPrestamo(objPrestamo);
            if (mostrarReporte) {
                ReportUtil.showReport(PrincipalJRibbon.getInstance(), new URL("file:rpt_cooperativa_solicitud_prestamo.prpt"), ReportUtil.getDataOnly(reportData), ReportUtil.getParametersOnly(reportData));

            } else //ReportUtil.printReport(new URL("file:imp_factura.prpt"), this.convert(objImpresionFactura), ConfigFacade.getTipoImpresion());
            {
                //ReportUtil.printReport(new URL("file:rpt_cooperativa_comprobante_aporte.prpt"), Report, title);
                ReportUtil.showReport(PrincipalJRibbon.getInstance(), new URL("file:rpt_cooperativa_solicitud_prestamo.prpt"), ReportUtil.getDataOnly(reportData), ReportUtil.getParametersOnly(reportData));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateLblEstadoSocio() {
        this.lblEstadoSocio.setHorizontalAlignment(SwingConstants.CENTER);
        if (this.verificado) {
            this.lblEstadoSocio.setText("Verificado");
            this.lblEstadoSocio.setForeground(Color.decode("#A4F879"));
        } else {
            this.lblEstadoSocio.setText("No Verificado");
            this.lblEstadoSocio.setForeground(Color.decode("#E77E7E"));
        }
    }

    private void updateLstTiposInteres() {
        this.lstTiposInteres = QueryFacade.getInstance().getAllTiposInteres();
    }

    private void updateCmbTiposInteres() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (TipoInteres objTipoInteres : this.lstTiposInteres) {
            comboBoxModel.addElement(objTipoInteres.getNombre());
        }

        this.cmbTiposInteres.setModel(comboBoxModel);
        
        this.updateLstTasasInteres();
    }

    private void updateLstTasasInteres() {
        TipoInteres objTipoInteres = this.lstTiposInteres.get(this.cmbTiposInteres.getSelectedIndex());
        for (TasaInteres objTasaInteres : objTipoInteres.getLstTasasInteres()) {
            if (objTasaInteres.isEstado()) {
                this.lstTasasInteres.add(objTasaInteres);
            }
        }
    }

    private void updateCmbTasasInteres() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (TasaInteres objTasaInteres : this.lstTasasInteres) {
            comboBoxModel.addElement(objTasaInteres.getNombre());
        }

        this.cmbTasasInteres.setModel(comboBoxModel);
    }

    private void updateLstTiposPrestamo() {
        this.lstTiposPrestamo = QueryFacade.getInstance().getAllTiposPrestamo();
    }

    private void updateCmbTiposPrestamo() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (TipoPrestamo objTipoPrestamo : this.lstTiposPrestamo) {
            comboBoxModel.addElement(objTipoPrestamo.getNombre());
        }

        this.cmbTiposPrestamo.setModel(comboBoxModel);
    }

    private void updateCamposSocio() {
        this.txtNombreSocio.setText(this.objSocio.getNombre());
        this.txtApellidosSocio.setText(this.objSocio.getApellidoPaterno() + " " + this.objSocio.getApellidoMaterno());
        this.txtDniSocio.setText(this.objSocio.getDocumentoIdentidad());
        this.txtRucSocio.setText(this.objSocio.getRUC());
        this.txtSexoSocio.setText(this.objSocio.isSexo() ? "Masculino" : "Femenino");
        this.txtEstadoCivilSocio.setText(this.objSocio.getObjEstadoCivil().getNombre());
        this.txtDireccionSocio.setText(this.objSocio.getDireccion());
        this.txtUbigeoSocio.setText(this.objSocio.getObjDistrito().getUbigeo());
        this.txtTelefonoCasaSocio.setText(this.objSocio.getTelefono());
        this.txtCelularSocio.setText(this.objSocio.getCelular());
        this.txtNivelIngresosSocio.setText(this.objSocio.getIngresoMensual().toPlainString());
    }

    private void updateTblDocumentos() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tableModel.addColumn("Documento");
        tableModel.addColumn("Estado");
        tableModel.addColumn("Archivo");

        for (DetalleDocumentoSolicitud objDetalleDocumentoSolicitud : this.lstDetalleDocumentoSolicitud) {
            tableModel.addRow(new Object[]{
                objDetalleDocumentoSolicitud.getObjDocumento().getNombre(),
                objDetalleDocumentoSolicitud.isAprobado() ? "Aprobado" : "No Aprobado",
                objDetalleDocumentoSolicitud.getRepresentacion() != null ? "Guardado" : this.mapArchivos.containsKey(objDetalleDocumentoSolicitud.getObjDocumento()) ? "Cargado" : "Sin Archivo"
            });
        }

        this.tblDocumentos.setModel(tableModel);
    }

    private void updateTblFiadores() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return column > 6;
            }
        };
        tableModel.addColumn("Nombres y Apellidos");
        tableModel.addColumn("DNI");
        tableModel.addColumn("Sexo");
        tableModel.addColumn("Direccion");
        tableModel.addColumn("Ingreso Mensual");
        tableModel.addColumn("Saldo en Cuenta de Aportes");
        tableModel.addColumn("Deuda Pendiente de Pago");
        tableModel.addColumn("Ingresos");
        tableModel.addColumn("Egresos");
        tableModel.addColumn("Saldo Disponible para Avalar");

        for (Socio objSocioGarante : this.lstGarantes) {
            tableModel.addRow(new Object[]{
                objSocioGarante.getNombreCompleto(),
                objSocioGarante.getDocumentoIdentidad(),
                objSocioGarante.isSexo() ? "Masculino" : "Femenino",
                objSocioGarante.getDireccion(),
                objSocioGarante.getIngresoMensual().toPlainString(),
                PrestamosProcessLogic.getInstance().calcularSaldoAportes(objSocioGarante),
                PrestamosProcessLogic.getInstance().calcularDeudaSocio(objSocioGarante),
                objSocioGarante.getIngresoMensual().toPlainString(),
                BigDecimal.ZERO.toPlainString(),
                BigDecimal.ZERO.toPlainString()
            });
        }

        this.tblGarantes.setModel(tableModel);
    }

    private void updateTblAprobaciones() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tableModel.addColumn("Usuario");
        tableModel.addColumn("Estado");

        for (Aprobacion objAprobacion : this.lstAprobaciones) {
            tableModel.addRow(new Object[]{
                objAprobacion.getObjTipoUsuario().getNombre(),
                objAprobacion.isAprobado() ? "Aprobado" : "No Aprobado"
            });
        }

        this.tblAprobaciones.setModel(tableModel);
    }

    @Override
    public void addObserver(IObserver objIObserver) {
        this.lstIObservers.add(objIObserver);
    }

    @Override
    public List<IObserver> getObserverList() {
        return this.lstIObservers;
    }

    @Override
    public void notifyObservers() {
        for (IObserver objIObserver : this.lstIObservers) {
            objIObserver.update(this);
        }
    }
}
