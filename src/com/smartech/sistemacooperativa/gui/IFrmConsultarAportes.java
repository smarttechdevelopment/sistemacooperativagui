package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.process.AportesProcessLogic;
import com.smartech.sistemacooperativa.dominio.Aporte;
import com.smartech.sistemacooperativa.dominio.Permiso;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import com.smartech.sistemacooperativa.util.interfaces.IObservable;
import com.smartech.sistemacooperativa.util.interfaces.IObserver;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Smartech
 */
public class IFrmConsultarAportes extends javax.swing.JInternalFrame implements IObserver {

    private List<Aporte> lstAportes = new ArrayList<>();
    private TableRowSorter<TableModel> trsAportes = new TableRowSorter<>();

    public IFrmConsultarAportes() {
        initComponents();
        this.setConfiguration();

        this.bgpPagos.add(this.rdbEfectuados);
        this.bgpPagos.add(this.rdbHabilitados);
        this.rdbHabilitados.setSelected(true);

        this.dtpSinceDate.setDate(new Date());
        this.dtpToDate.setDate(new Date());

        this.update();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgpPagos = new javax.swing.ButtonGroup();
        jLabel3 = new javax.swing.JLabel();
        dtpSinceDate = new org.jdesktop.swingx.JXDatePicker();
        jLabel4 = new javax.swing.JLabel();
        dtpToDate = new org.jdesktop.swingx.JXDatePicker();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblAportes = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        txtSocio = new javax.swing.JTextField();
        rdbEfectuados = new javax.swing.JRadioButton();
        rdbHabilitados = new javax.swing.JRadioButton();
        btnNuevo = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnActualizar = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Aportes");

        jLabel3.setText("Desde:");

        dtpSinceDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dtpSinceDateActionPerformed(evt);
            }
        });

        jLabel4.setText("Hasta:");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Aportes"));

        tblAportes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblAportes);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 700, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
        );

        jLabel2.setText("Socio:");

        txtSocio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSocioKeyReleased(evt);
            }
        });

        rdbEfectuados.setText("Efectuados");
        rdbEfectuados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbEfectuadosActionPerformed(evt);
            }
        });

        rdbHabilitados.setText("Habilitados");
        rdbHabilitados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbHabilitadosActionPerformed(evt);
            }
        });

        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/nuevo.png"))); // NOI18N
        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/basura.png"))); // NOI18N
        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnActualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/actualizacion.png"))); // NOI18N
        btnActualizar.setText("Actualizar");
        btnActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(dtpSinceDate, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(dtpToDate, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(rdbEfectuados)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdbHabilitados)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnActualizar))
                            .addComponent(txtSocio)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnEliminar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnNuevo)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(dtpSinceDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(dtpToDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rdbEfectuados)
                    .addComponent(rdbHabilitados)
                    .addComponent(btnActualizar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNuevo)
                    .addComponent(btnEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void dtpSinceDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dtpSinceDateActionPerformed
        this.update();
    }//GEN-LAST:event_dtpSinceDateActionPerformed

    private void rdbEfectuadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdbEfectuadosActionPerformed
        this.update();
        this.updateBtnEliminar();
    }//GEN-LAST:event_rdbEfectuadosActionPerformed

    private void rdbHabilitadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdbHabilitadosActionPerformed
        this.update();
        this.updateBtnEliminar();
    }//GEN-LAST:event_rdbHabilitadosActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        if(PrincipalJRibbon.getInstance().getObjCaja().isAbierto()){
            DlgRegistrarAporte dlgRegistrarAporte = new DlgRegistrarAporte(PrincipalJRibbon.getInstance().getFrame(), true);
            SwingUtil.centerOnScreen(dlgRegistrarAporte);
            dlgRegistrarAporte.addObserver(this);
            dlgRegistrarAporte.setVisible(true);
        }else{
            JOptionPane.showMessageDialog(this, "No ha abierto caja", "Información", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarActionPerformed
        this.update();
    }//GEN-LAST:event_btnActualizarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        if (this.rdbEfectuados.isSelected()) {
            JOptionPane.showMessageDialog(this, "Aporte ya pagado", "Aportes", JOptionPane.ERROR_MESSAGE);
        } else {
            int index = this.tblAportes.getSelectedRow();

            if (index != -1) {
                index = this.tblAportes.convertRowIndexToModel(index);
                boolean result = AportesProcessLogic.getInstance().eliminarAporte(this.lstAportes.get(index));                
                if(result){
                    JOptionPane.showMessageDialog(this, "Seleccione un Aporte", "Aportes", JOptionPane.ERROR_MESSAGE);
                }else{
                    JOptionPane.showMessageDialog(this, "Error en el registro", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(this, "Seleccione un Aporte", "Aportes", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void txtSocioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSocioKeyReleased
        SwingUtil.textFilter(evt, this.tblAportes, this, this.txtSocio, this.trsAportes);
    }//GEN-LAST:event_txtSocioKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bgpPagos;
    private javax.swing.JButton btnActualizar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnNuevo;
    private org.jdesktop.swingx.JXDatePicker dtpSinceDate;
    private org.jdesktop.swingx.JXDatePicker dtpToDate;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JRadioButton rdbEfectuados;
    private javax.swing.JRadioButton rdbHabilitados;
    private javax.swing.JTable tblAportes;
    private javax.swing.JTextField txtSocio;
    // End of variables declaration//GEN-END:variables

    private void setConfiguration() {
        this.btnNuevo.setVisible(PrincipalJRibbon.getInstance().getObjUsuarioLogeado().verificarPermiso(Permiso.PERMISO_APORTES, Permiso.NIVEL_ESCRITURA));
        this.btnEliminar.setVisible(PrincipalJRibbon.getInstance().getObjUsuarioLogeado().verificarPermiso(Permiso.PERMISO_APORTES, Permiso.NIVEL_ELIMINACION));
    }

    private void updateBtnEliminar() {
        this.btnEliminar.setVisible(this.rdbHabilitados.isSelected());
    }

    private void update() {
        this.lstAportes = this.rdbHabilitados.isSelected() ? QueryFacade.getInstance().getAllAportesHabilitados(this.dtpSinceDate.getDate(), this.dtpToDate.getDate()) : QueryFacade.getInstance().getAllAportesPagados(this.dtpSinceDate.getDate(), this.dtpToDate.getDate());
        this.updateTblAportes();
    }

    private void updateTblAportes() {
        if (PrincipalJRibbon.getInstance().getObjUsuarioLogeado().verificarPermiso(Permiso.PERMISO_APORTES, Permiso.NIVEL_LECTURA)) {
            DefaultTableModel tableModel = new DefaultTableModel() {
                @Override
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            tableModel.addColumn("Socio");
            tableModel.addColumn("DNI");
            tableModel.addColumn("Monto");
            tableModel.addColumn("Fecha");
            tableModel.addColumn("Estado");

            for (Aporte objAporte : this.lstAportes) {
                tableModel.addRow(new Object[]{
                    objAporte.getObjTarjeta().getObjSocio().getNombreCompleto(),
                    objAporte.getObjTarjeta().getObjSocio().getDocumentoIdentidad(),
                    objAporte.getMonto(),
                    DateUtil.getRegularDate(DateUtil.getDate(objAporte.getFechaRegistro())),
                    objAporte.isPagado() ? "PAGADO" : "PENDIENTE"
                });
            }

            this.tblAportes.setModel(tableModel);
            this.trsAportes.setModel(tableModel);
            this.tblAportes.setRowSorter(this.trsAportes);
        }
    }

    @Override
    public void update(IObservable objIObservable) {
        this.update();
    }
}
