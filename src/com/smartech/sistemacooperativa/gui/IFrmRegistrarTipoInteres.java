package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.process.TiposInteresProcessLogic;
import com.smartech.sistemacooperativa.dominio.TasaInteres;
import com.smartech.sistemacooperativa.dominio.TipoInteres;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import com.smartech.sistemacooperativa.util.interfaces.IObservable;
import com.smartech.sistemacooperativa.util.interfaces.IObserver;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Smartech
 */
public class IFrmRegistrarTipoInteres extends javax.swing.JInternalFrame implements IObservable {

    private List<IObserver> lstIObservers = new ArrayList<>();

    private TipoInteres objTipoInteres = null;
    private List<TasaInteres> lstTasasInteres = new ArrayList<>();
    private final List<TasaInteres> lstTasasInteresBackup = new ArrayList<>();

    public IFrmRegistrarTipoInteres() {
        initComponents();
        this.setConfiguration();
        this.txtTasa.setVisible(false);
        this.lblTasa.setVisible(false);
        this.btnActualizarTasaInteres.setVisible(false);
        this.btnActivarTasaInteres.setVisible(false);
        this.btnQuitarTasaInteres.setVisible(false);
        this.updateTblTasasInteres();
    }

    public IFrmRegistrarTipoInteres(TipoInteres objTipoInteres) {
        initComponents();
        this.setConfiguration();

        this.objTipoInteres = objTipoInteres;
        this.lstTasasInteres = objTipoInteres.getLstTasasInteres();
        for (TasaInteres objTasaInteres : objTipoInteres.getLstTasasInteres()) {
            this.lstTasasInteresBackup.add(new TasaInteres(
                    objTasaInteres.getId(),
                    objTasaInteres.getNombre(),
                    objTasaInteres.isEstado(),
                    objTasaInteres.getTasa(),
                    objTasaInteres.getTea(),
                    objTasaInteres.getTim(),
                    objTasaInteres.getMontoPlazoFijo(),
                    objTasaInteres.getFechaRegistro(),
                    objTasaInteres.getFechaModificacion(),
                    objTasaInteres.getObjTipoInteres()));
        }

        this.txtNombreTipoInteres.setText(objTipoInteres.getNombre());
        this.txtPlazos.setText(String.valueOf(objTipoInteres.getDiasPlazos()));

        this.chkPlazoFijo.setSelected(this.objTipoInteres.isPlazoFijo());
        this.chkPlazoFijo.setEnabled(false);
        if (this.objTipoInteres.isPlazoFijo()) {
            this.txtTea.setVisible(false);
            this.lblTasa.setText("Monto Plazo Fijo:");
            this.txtTasa.setVisible(true);
        } else {
            this.txtTasa.setVisible(false);
            this.lblTasa.setVisible(false);
        }
        this.updateTblTasasInteres();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel3 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtNombreTipoInteres = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtPlazos = new javax.swing.JTextField();
        chkPlazoFijo = new javax.swing.JCheckBox();
        jPanel2 = new javax.swing.JPanel();
        lblTea = new javax.swing.JLabel();
        txtTea = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtTim = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblTasasInteres = new javax.swing.JTable();
        btnAgregarTasaInteres = new javax.swing.JButton();
        btnQuitarTasaInteres = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        txtNombreTasaInteres = new javax.swing.JTextField();
        btnActivarTasaInteres = new javax.swing.JButton();
        btnActualizarTasaInteres = new javax.swing.JButton();
        lblTasa = new javax.swing.JLabel();
        txtTasa = new javax.swing.JTextField();
        btnGuardar = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setResizable(true);
        setTitle("Registro de Tipo de Interes");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Tipo de Interes"));

        jLabel1.setText("Nombre:");

        jLabel4.setText("Dias de Plazo:");

        chkPlazoFijo.setText("Plazo Fijo");
        chkPlazoFijo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkPlazoFijoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtNombreTipoInteres)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txtPlazos, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(chkPlazoFijo)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtNombreTipoInteres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtPlazos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chkPlazoFijo))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Tasas de Interes"));

        lblTea.setText("Tasa efectiva anual:");

        jLabel3.setText("Tasa de interes moratorio:");

        tblTasasInteres.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblTasasInteres);

        btnAgregarTasaInteres.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_add.png"))); // NOI18N
        btnAgregarTasaInteres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarTasaInteresActionPerformed(evt);
            }
        });

        btnQuitarTasaInteres.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/basura.png"))); // NOI18N
        btnQuitarTasaInteres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQuitarTasaInteresActionPerformed(evt);
            }
        });

        jLabel5.setText("Nombre:");

        btnActivarTasaInteres.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_checked.png"))); // NOI18N
        btnActivarTasaInteres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActivarTasaInteresActionPerformed(evt);
            }
        });

        btnActualizarTasaInteres.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/actualizacion.png"))); // NOI18N
        btnActualizarTasaInteres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarTasaInteresActionPerformed(evt);
            }
        });

        lblTasa.setText("Tasa:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(lblTasa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtTasa, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(418, 418, 418)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnAgregarTasaInteres, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnActivarTasaInteres, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(btnQuitarTasaInteres, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(btnActualizarTasaInteres, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNombreTasaInteres))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(lblTea)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTea, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTim, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtNombreTasaInteres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTasa)
                    .addComponent(txtTasa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTea, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtTim, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnAgregarTasaInteres, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnActivarTasaInteres)
                        .addGap(1, 1, 1)
                        .addComponent(btnActualizarTasaInteres, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnQuitarTasaInteres)
                        .addContainerGap(122, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
        );

        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_save.png"))); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnGuardar)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnGuardar)
                .addContainerGap())
        );

        jScrollPane2.setViewportView(jPanel3);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        String message = "";

        if (this.objTipoInteres == null) {
            this.objTipoInteres = new TipoInteres(
                    0,
                    this.txtNombreTipoInteres.getText().trim(),
                    Integer.parseInt(this.txtPlazos.getText().trim()),
                    this.chkPlazoFijo.isSelected(),
                    true,
                    DateUtil.currentTimestamp(),
                    null,
                    this.lstTasasInteres);

            message = TiposInteresProcessLogic.getInstance().registrarTipoInteres(this.objTipoInteres);
        } else {
            this.objTipoInteres.setNombre(this.txtNombreTipoInteres.getText().trim());
            this.objTipoInteres.setDiasPlazos(Integer.parseInt(this.txtPlazos.getText().trim()));
            this.objTipoInteres.setFechaModificacion(DateUtil.currentTimestamp());
            this.objTipoInteres.setLstTasasInteres(this.lstTasasInteres);

            message = TiposInteresProcessLogic.getInstance().actualizarTipoInteres(this.objTipoInteres, this.lstTasasInteresBackup);
        }

        if (!message.isEmpty()) {
            if (message.contains("Error")) {
                JOptionPane.showMessageDialog(this, message, "Tipo de Interes", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, message, "Tipo de Interes", JOptionPane.INFORMATION_MESSAGE);
                this.notifyObservers();
                this.dispose();
            }
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnAgregarTasaInteresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarTasaInteresActionPerformed
        boolean plazoFijo = this.objTipoInteres == null ? this.chkPlazoFijo.isSelected() : this.objTipoInteres.isPlazoFijo();

        if (plazoFijo ? SwingUtil.hasBlankInputs(this.txtNombreTasaInteres, this.txtTasa, this.txtTim) : SwingUtil.hasBlankInputs(this.txtNombreTasaInteres, this.txtTea, this.txtTim)) {
            JOptionPane.showMessageDialog(this, "Datos Incorrectos. Revise los datos ingresados", "Tasa de Interes", JOptionPane.ERROR_MESSAGE);
        } else {

            TasaInteres objTasaInteres = new TasaInteres(
                    0,
                    this.txtNombreTasaInteres.getText().trim(),
                    true,
                    plazoFijo ? BigDecimal.ZERO : BigDecimal.ZERO,
                    plazoFijo ? BigDecimal.ZERO : new BigDecimal(this.txtTea.getText().trim()),
                    new BigDecimal(this.txtTim.getText().trim()),
                    plazoFijo ? new BigDecimal(this.txtTasa.getText().trim()) : BigDecimal.ZERO,
                    DateUtil.currentTimestamp(),
                    null,
                    this.objTipoInteres);

            if (TiposInteresProcessLogic.getInstance().existsTasaInteres(objTasaInteres, this.lstTasasInteres) == null) {
                this.lstTasasInteres.add(objTasaInteres);
            } else {
                JOptionPane.showMessageDialog(this, "Tasa de Interes existente", "Error", JOptionPane.ERROR_MESSAGE);
            }
            this.updateTblTasasInteres();
        }
    }//GEN-LAST:event_btnAgregarTasaInteresActionPerformed

    private void btnQuitarTasaInteresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQuitarTasaInteresActionPerformed
        int index = this.tblTasasInteres.getSelectedRow();

        if (index != -1) {
            int respuesta = JOptionPane.showConfirmDialog(this, "Está seguro de querer eliminar el Ingreso?", "Tasa de Interes", JOptionPane.WARNING_MESSAGE);

            if (respuesta == JOptionPane.YES_OPTION) {
                this.lstTasasInteres.get(index).setEstado(false);
                this.updateTblTasasInteres();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione una Tasa de Interes", "Tasa de Interes", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnQuitarTasaInteresActionPerformed

    private void btnActivarTasaInteresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActivarTasaInteresActionPerformed
        int index = this.tblTasasInteres.getSelectedRow();

        if (index != -1) {
            int respuesta = JOptionPane.showConfirmDialog(this, "Está seguro de querer activar el Ingreso?", "Tasa de Interes", JOptionPane.WARNING_MESSAGE);

            if (respuesta == JOptionPane.YES_OPTION) {
                this.lstTasasInteres.get(index).setEstado(true);
                this.updateTblTasasInteres();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione una Tasa de Interes", "Tasa de Interes", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnActivarTasaInteresActionPerformed

    private void btnActualizarTasaInteresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarTasaInteresActionPerformed
        int index = this.tblTasasInteres.getSelectedRow();

        if (index != -1) {
            DlgActualizarTasaInteres dlgActualizarTasaInteres = new DlgActualizarTasaInteres(PrincipalJRibbon.getInstance().getFrame(), true, this.lstTasasInteres.get(index));
            SwingUtil.centerOnScreen(dlgActualizarTasaInteres);
            dlgActualizarTasaInteres.setVisible(true);
            this.lstTasasInteres.set(index, dlgActualizarTasaInteres.getObjTasaInteres());
            this.updateTblTasasInteres();
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione una Tasa de Interes", "Tasa de Interes", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnActualizarTasaInteresActionPerformed

    private void chkPlazoFijoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkPlazoFijoActionPerformed
        if (this.chkPlazoFijo.isSelected()) {
            this.lblTea.setVisible(false);
            this.txtTea.setVisible(false);
            this.lblTasa.setText("Monto Plazo Fijo:");
            this.txtTasa.setVisible(true);
            this.lblTasa.setVisible(true);
        } else {
            this.txtTea.setVisible(true);
            this.lblTea.setVisible(true);
            this.lblTasa.setText("Tasa:");
            this.txtTasa.setVisible(false);
            this.lblTasa.setVisible(false);
        }
        pack();
        revalidate();
        this.updateTblTasasInteres();
    }//GEN-LAST:event_chkPlazoFijoActionPerformed

    private void setConfiguration() {
        SwingUtil.setPositiveNumericInput(this.txtTasa, this.txtTea, this.txtTim);
        SwingUtil.setPhoneInput(this.txtPlazos);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActivarTasaInteres;
    private javax.swing.JButton btnActualizarTasaInteres;
    private javax.swing.JButton btnAgregarTasaInteres;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnQuitarTasaInteres;
    private javax.swing.JCheckBox chkPlazoFijo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblTasa;
    private javax.swing.JLabel lblTea;
    private javax.swing.JTable tblTasasInteres;
    private javax.swing.JTextField txtNombreTasaInteres;
    private javax.swing.JTextField txtNombreTipoInteres;
    private javax.swing.JTextField txtPlazos;
    private javax.swing.JTextField txtTasa;
    private javax.swing.JTextField txtTea;
    private javax.swing.JTextField txtTim;
    // End of variables declaration//GEN-END:variables

    private void updateTblTasasInteres() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tableModel.addColumn("Nombre");
        if (this.chkPlazoFijo.isSelected()) {
            tableModel.addColumn("Monto Plazo Fijo");
        } else {
            tableModel.addColumn("Tasa Efectiva Anual");
        }
        tableModel.addColumn("Tasa de Interes Moratorio");
        tableModel.addColumn("Estado");

        for (TasaInteres objTasaInteres : this.lstTasasInteres) {
            tableModel.addRow(new Object[]{
                objTasaInteres.getNombre(),
                this.chkPlazoFijo.isSelected() ? objTasaInteres.getMontoPlazoFijo().toPlainString() : objTasaInteres.getTea().toPlainString(),
                objTasaInteres.getTim().toPlainString(),
                objTasaInteres.isEstado() ? "Activo" : "Eliminado"
            });
        }

        this.tblTasasInteres.setModel(tableModel);
    }

    @Override
    public void addObserver(IObserver objIObserver) {
        this.lstIObservers.add(objIObserver);
    }

    @Override
    public List<IObserver> getObserverList() {
        return this.lstIObservers;
    }

    @Override
    public void notifyObservers() {
        for (IObserver objIObserver : this.lstIObservers) {
            objIObserver.update(this);
        }
    }
}
