package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.dominio.Prestamo;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.TipoDocumentoIdentidad;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Smartech
 */
public class IFrmPrestamosActivos extends javax.swing.JInternalFrame {

    private List<Prestamo> lstPrestamos = new ArrayList<>();
    private Socio objSocio = null;

    public static int COMBO_INDEX_DOCUMENTO_IDENTIDAD_INDISTINTO = 0;

    private boolean permitirBuscarEnTxtBusarSocio = true;

    private List<TipoDocumentoIdentidad> lstTiposDocumentoIdentidad = new ArrayList<>();

    public IFrmPrestamosActivos() {
        initComponents();

        this.lstTiposDocumentoIdentidad = QueryFacade.getInstance().getAllTiposDocumentoIdentidad();
        this.updateCmbModo();
        this.setCmbModo();
        this.updateCamposSocio();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        cmbModoBuscaSocio = new javax.swing.JComboBox<>();
        txtBuscarSocio = new javax.swing.JTextField();
        btnIngresarTarjeta = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblPrestamos = new javax.swing.JTable();
        btnDetallePrestamo = new javax.swing.JButton();
        cmbRoles = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        cmbEstados = new javax.swing.JComboBox<>();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtNombreSocio = new javax.swing.JTextField();
        txtDNISocio = new javax.swing.JTextField();
        txtRUCSocio = new javax.swing.JTextField();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Consultar Prestamos");

        jLabel1.setText("Buscar:");

        cmbModoBuscaSocio.setMinimumSize(new java.awt.Dimension(280, 20));
        cmbModoBuscaSocio.setPreferredSize(new java.awt.Dimension(280, 20));
        cmbModoBuscaSocio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbModoBuscaSocioActionPerformed(evt);
            }
        });

        txtBuscarSocio.setMinimumSize(new java.awt.Dimension(80, 20));
        txtBuscarSocio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarSocioKeyReleased(evt);
            }
        });

        btnIngresarTarjeta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/credit-card.png"))); // NOI18N
        btnIngresarTarjeta.setText("Ingresar Tarjeta");
        btnIngresarTarjeta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIngresarTarjetaActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Prestamos"));

        tblPrestamos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblPrestamos);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
        );

        btnDetallePrestamo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/n_eye.png"))); // NOI18N
        btnDetallePrestamo.setText("Ver Detalle");
        btnDetallePrestamo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDetallePrestamoActionPerformed(evt);
            }
        });

        cmbRoles.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Prestatario", "Garante" }));
        cmbRoles.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbRolesActionPerformed(evt);
            }
        });

        jLabel2.setText("Rol:");

        jLabel3.setText("Estado:");

        cmbEstados.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Todos", "Pagado", "Pendiente" }));
        cmbEstados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbEstadosActionPerformed(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Socio"));

        jLabel4.setText("Nombre:");

        jLabel5.setText("DNI:");

        jLabel6.setText("RUC:");

        txtNombreSocio.setEditable(false);

        txtDNISocio.setEditable(false);

        txtRUCSocio.setEditable(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtNombreSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtDNISocio, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtRUCSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtNombreSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(txtDNISocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(txtRUCSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbModoBuscaSocio, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBuscarSocio, javax.swing.GroupLayout.DEFAULT_SIZE, 234, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnIngresarTarjeta)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbRoles, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbEstados, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnDetallePrestamo))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cmbModoBuscaSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBuscarSocio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnIngresarTarjeta)
                    .addComponent(cmbRoles, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(cmbEstados, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDetallePrestamo)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmbModoBuscaSocioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbModoBuscaSocioActionPerformed
        this.setCmbModo();
    }//GEN-LAST:event_cmbModoBuscaSocioActionPerformed

    private void txtBuscarSocioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarSocioKeyReleased
        String message;

        message = getSocio();

        this.updateCamposSocio();
        this.updateLstPrestamos();
        this.updateTblPrestamos();

        if (this.permitirBuscarEnTxtBusarSocio && message.length() > 0) {
            this.lstPrestamos.clear();
            this.updateTblPrestamos();
            JOptionPane.showMessageDialog(this, message, "Información", JOptionPane.INFORMATION_MESSAGE);
            this.permitirBuscarEnTxtBusarSocio = false;
        } else {
            this.permitirBuscarEnTxtBusarSocio = true;
        }
    }//GEN-LAST:event_txtBuscarSocioKeyReleased

    private void btnIngresarTarjetaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIngresarTarjetaActionPerformed
        this.txtBuscarSocio.setText("Con tarjeta");
        this.cmbModoBuscaSocio.setEnabled(false);

        DlgIngresarTarjeta objDlgIngresarTarjeta = new DlgIngresarTarjeta(PrincipalJRibbon.getInstance().getFrame(), true, false, false);
        objDlgIngresarTarjeta.setModal(true);
        SwingUtil.centerOnScreen(objDlgIngresarTarjeta);
        objDlgIngresarTarjeta.setVisible(true);

        if (objDlgIngresarTarjeta.getObjTarjeta() != null) {
            this.objSocio = objDlgIngresarTarjeta.getObjTarjeta().getObjSocio();
            this.updateCamposSocio();
            this.updateLstPrestamos();
            this.updateTblPrestamos();
        } else {
            this.txtBuscarSocio.setText("");
            this.cmbModoBuscaSocio.setEnabled(true);
        }
    }//GEN-LAST:event_btnIngresarTarjetaActionPerformed

    private void cmbRolesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbRolesActionPerformed
        this.updateLstPrestamos();
        this.updateTblPrestamos();
    }//GEN-LAST:event_cmbRolesActionPerformed

    private void cmbEstadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbEstadosActionPerformed
        this.updateLstPrestamos();
        this.updateTblPrestamos();
    }//GEN-LAST:event_cmbEstadosActionPerformed

    private void btnDetallePrestamoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDetallePrestamoActionPerformed
        int index = this.tblPrestamos.getSelectedRow();

        if (index != -1) {
            DlgDetallePrestamo dlgDetallePrestamo = new DlgDetallePrestamo(PrincipalJRibbon.getInstance().getFrame(), true, this.lstPrestamos.get(index));
            SwingUtil.centerOnScreen(dlgDetallePrestamo);
            dlgDetallePrestamo.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un Prestamo", "Prestamo", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnDetallePrestamoActionPerformed

    private void updateCamposSocio() {
        if (this.objSocio == null) {
            this.txtNombreSocio.setText("-");
            this.txtDNISocio.setText("-");
            this.txtRUCSocio.setText("-");
        } else {
            this.txtNombreSocio.setText(this.objSocio.getNombreCompleto());
            this.txtDNISocio.setText(this.objSocio.getDocumentoIdentidad());
            this.txtRUCSocio.setText(this.objSocio.getRUC() == null ? "-" : this.objSocio.getRUC());
        }
    }

    private void setCmbModo() {
        this.txtBuscarSocio.setText("");
        this.txtBuscarSocio.setEnabled(true);
        if (this.cmbModoBuscaSocio.getSelectedIndex() == COMBO_INDEX_DOCUMENTO_IDENTIDAD_INDISTINTO) {
            SwingUtil.setDocumentoIdentidadInput(new TipoDocumentoIdentidad(), this.txtBuscarSocio);
        } else {
            SwingUtil.setDocumentoIdentidadInput(this.lstTiposDocumentoIdentidad.get(this.cmbModoBuscaSocio.getSelectedIndex()), this.txtBuscarSocio);
        }
    }

    private void updateLstPrestamos() {
        if (this.objSocio != null) {
            if (this.getCmbRolesValue().equals("Prestatario")) {
                if (this.getCmbEstadosValue().equals("Todos")) {
                    this.lstPrestamos = QueryFacade.getInstance().getAllPrestamos(this.objSocio, true);
                    this.lstPrestamos.addAll(QueryFacade.getInstance().getAllPrestamos(this.objSocio, false));
                } else {
                    this.lstPrestamos = QueryFacade.getInstance().getAllPrestamos(this.objSocio, this.getCmbEstadosValue().equals("Pagado"));
                }
            } else if (this.getCmbEstadosValue().equals("Todos")) {
                this.lstPrestamos = QueryFacade.getInstance().getAllPrestamosAsGarante(this.objSocio, true);
                this.lstPrestamos.addAll(QueryFacade.getInstance().getAllPrestamosAsGarante(this.objSocio, false));
            } else {
                this.lstPrestamos = QueryFacade.getInstance().getAllPrestamosAsGarante(this.objSocio, this.getCmbEstadosValue().equals("Pagado"));
            }
        } else {
            this.lstPrestamos.clear();
        }
    }

    private String getCmbRolesValue() {
        return (String) this.cmbRoles.getSelectedItem();
    }

    private String getCmbEstadosValue() {
        return (String) this.cmbEstados.getSelectedItem();
    }

    private String getSocio() {
        String message = "";
        boolean realizaBusqueda = false;

        if (this.txtBuscarSocio.isEnabled()) {
            if (this.cmbModoBuscaSocio.getSelectedIndex() == COMBO_INDEX_DOCUMENTO_IDENTIDAD_INDISTINTO) {
                if (this.objSocio != null) {
                    if (!this.objSocio.getDocumentoIdentidad().equals(this.txtBuscarSocio.getText()) && TipoDocumentoIdentidad.existeLongitud(this.txtBuscarSocio.getText().trim().length(), this.lstTiposDocumentoIdentidad)) {
                        realizaBusqueda = true;
                        this.objSocio = QueryFacade.getInstance().getSocioByDocumentoIdentidad(this.txtBuscarSocio.getText(), false);
                    }
                } else if (TipoDocumentoIdentidad.existeLongitud(this.txtBuscarSocio.getText().trim().length(), this.lstTiposDocumentoIdentidad)) {
                    realizaBusqueda = true;
                    this.objSocio = QueryFacade.getInstance().getSocioByDocumentoIdentidad(this.txtBuscarSocio.getText(), false);
                }
            } else if (this.txtBuscarSocio.getText().trim().length() == this.lstTiposDocumentoIdentidad.get(this.cmbModoBuscaSocio.getSelectedIndex()).getLongitud()) {
                realizaBusqueda = true;
                this.objSocio = QueryFacade.getInstance().getSocioByDocumentoIdentidad(this.txtBuscarSocio.getText().trim(), this.lstTiposDocumentoIdentidad.get(this.cmbModoBuscaSocio.getSelectedIndex()), false);
            }

            if (realizaBusqueda) {
                if (this.objSocio == null) {
                    message = "No existe socio";
                }
            } else {
                this.objSocio = null;
            }
        } else {
            message = "No existe socio";
        }

        return message;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDetallePrestamo;
    private javax.swing.JButton btnIngresarTarjeta;
    private javax.swing.JComboBox<String> cmbEstados;
    private javax.swing.JComboBox<String> cmbModoBuscaSocio;
    private javax.swing.JComboBox<String> cmbRoles;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblPrestamos;
    private javax.swing.JTextField txtBuscarSocio;
    private javax.swing.JTextField txtDNISocio;
    private javax.swing.JTextField txtNombreSocio;
    private javax.swing.JTextField txtRUCSocio;
    // End of variables declaration//GEN-END:variables

    private void updateTblPrestamos() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tableModel.addColumn("Prestatario");
        tableModel.addColumn("Fecha Prestamo");
        tableModel.addColumn("Cuotas");
        tableModel.addColumn("Monto");

        for (Prestamo objPrestamo : this.lstPrestamos) {
            tableModel.addRow(new Object[]{
                objPrestamo.getObjSolicitudPrestamo().getObjSocioPrestatario().getNombreCompleto(),
                DateUtil.getRegularDate(DateUtil.getDate(objPrestamo.getFechaRegistro())),
                objPrestamo.getLstCuotas().size(),
                objPrestamo.getMonto().toPlainString()
            });
        }

        this.tblPrestamos.setModel(tableModel);
    }

    private void updateCmbModo() {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        for (TipoDocumentoIdentidad objTipoDocumentoIdentidad : this.lstTiposDocumentoIdentidad) {
            comboBoxModel.addElement(objTipoDocumentoIdentidad.getNombre());
        }
        comboBoxModel.addElement("Indistinto");
        COMBO_INDEX_DOCUMENTO_IDENTIDAD_INDISTINTO = this.lstTiposDocumentoIdentidad.size();

        this.cmbModoBuscaSocio.setModel(comboBoxModel);
    }
}
