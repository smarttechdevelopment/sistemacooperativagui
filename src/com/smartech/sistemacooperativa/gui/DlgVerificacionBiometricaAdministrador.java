package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.dominio.TipoUsuario;
import com.smartech.sistemacooperativa.dominio.Usuario;
import com.smartech.sistemacooperativa.gui.util.MyFingerprintVerification;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Smartech
 */
public class DlgVerificacionBiometricaAdministrador extends javax.swing.JDialog {

    private List<TipoUsuario> lstTipoUsuarios = new ArrayList<>();
    private Map<TipoUsuario, Boolean> mapResultado = new HashMap<>();
    private Map<TipoUsuario, List<Usuario>> mapUsuario = new HashMap<>();

    private boolean result = false;

    public DlgVerificacionBiometricaAdministrador(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();

        TipoUsuario objTipoUsuario1 = QueryFacade.getInstance().getTipoUsuario(2);
        this.mapUsuario.put(objTipoUsuario1, QueryFacade.getInstance().getAllUsuarios(objTipoUsuario1));
        this.mapResultado.put(objTipoUsuario1, false);
        TipoUsuario objTipoUsuario2 = QueryFacade.getInstance().getTipoUsuario(3);
        this.mapUsuario.put(objTipoUsuario2, QueryFacade.getInstance().getAllUsuarios(objTipoUsuario2));
        this.mapResultado.put(objTipoUsuario2, false);

        this.lstTipoUsuarios.add(objTipoUsuario1);
        this.lstTipoUsuarios.add(objTipoUsuario2);

        this.updateTblTiposUsuario();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblTiposUsuario = new javax.swing.JTable();
        btnVerificarHuella = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Verificacion Biometrica Administrativa");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Verificacion Biometrica"));
        jPanel1.setToolTipText("");

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Tipos de Usuario"));

        tblTiposUsuario.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblTiposUsuario);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        btnVerificarHuella.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/gui/images/huella_41x41.png"))); // NOI18N
        btnVerificarHuella.setText("Verificar");
        btnVerificarHuella.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerificarHuellaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(10, 10, 10)
                .addComponent(btnVerificarHuella)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnVerificarHuella)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnVerificarHuellaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerificarHuellaActionPerformed
        int index = this.tblTiposUsuario.getSelectedRow();

        if (index != -1) {
            TipoUsuario objTipoUsuario = this.lstTipoUsuarios.get(index);
            MyFingerprintVerification myFingerprintVerification = new MyFingerprintVerification(PrincipalJRibbon.getInstance().getFrame(), true, this.mapUsuario.get(objTipoUsuario));
            SwingUtil.centerOnScreen(myFingerprintVerification);
            myFingerprintVerification.setVisible(true);

            this.updateMapResultado(objTipoUsuario, myFingerprintVerification.isMatched());

            this.updateTblTiposUsuario();

            if (this.verifyMapResultado()) {
                JOptionPane.showMessageDialog(this, "Verificacion Correcta", "Verificacion", JOptionPane.INFORMATION_MESSAGE);
                this.result = true;
                this.dispose();
            } else {
                this.result = false;
            }
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un Tipo de Usuario", "Tipo de Usuario", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnVerificarHuellaActionPerformed

    public boolean isResult() {
        return result;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnVerificarHuella;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblTiposUsuario;
    // End of variables declaration//GEN-END:variables

    private void updateMapResultado(TipoUsuario objTipoUsuario, boolean result) {
        for (Map.Entry<TipoUsuario, Boolean> entry : this.mapResultado.entrySet()) {
            if (entry.getKey().getId() == objTipoUsuario.getId()) {
                entry.setValue(result);
            }
        }
    }

    private boolean verifyMapResultado() {
        boolean resultMap = true;

        for (Map.Entry<TipoUsuario, Boolean> entry : this.mapResultado.entrySet()) {
            if (!entry.getValue()) {
                resultMap = false;
                break;
            }
        }

        return resultMap;
    }

    private void updateTblTiposUsuario() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tableModel.addColumn("Tipo de Usuario");
        tableModel.addColumn("Estado");

        for (TipoUsuario objTipoUsuario : this.lstTipoUsuarios) {
            tableModel.addRow(new Object[]{
                objTipoUsuario.getNombre(),
                this.mapResultado.get(objTipoUsuario) ? "Verificado" : "No Verificado"
            });
        }

        this.tblTiposUsuario.setModel(tableModel);
    }
}
