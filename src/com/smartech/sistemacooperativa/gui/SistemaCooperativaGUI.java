package com.smartech.sistemacooperativa.gui;

import com.smartech.sistemacooperativa.bll.facade.ConfigFacade;
import com.smartech.sistemacooperativa.bll.facade.SecurityFacade;
import com.smartech.sistemacooperativa.dal.DAOEstablecimiento;
import com.smartech.sistemacooperativa.dominio.Empleado;
import com.smartech.sistemacooperativa.gui.util.MyLoginService;
import com.smartech.sistemacooperativa.gui.util.SwingUtil;
import java.awt.Component;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import org.jdesktop.swingx.JXLoginPane;
import org.pentaho.reporting.engine.classic.core.ClassicEngineBoot;
import org.pushingpixels.substance.api.SubstanceLookAndFeel;

/**
 *
 * @author Usuario
 */
public class SistemaCooperativaGUI {

    private static int ID_ESTABLECIMIENTO_DEFAULT = 1;

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                System.setProperty("sun.awt.noerasebackground", "true");
                SubstanceLookAndFeel.setSkin("org.pushingpixels.substance.api.skin.BusinessBlackSteelSkin");

                JFrame.setDefaultLookAndFeelDecorated(true);
                JDialog.setDefaultLookAndFeelDecorated(true);

                //AWTUtil.setUpperCase();
                //ConnectionManager.setDatabase("pgsql");
                JXLoginPane pane = new JXLoginPane();
                pane.setLoginService(new MyLoginService());
                JXLoginPane.Status status = JXLoginPane.showLoginDialog(null, pane);
                System.out.println("" + status);

                if (status.compareTo(JXLoginPane.Status.NOT_STARTED) == 0) {
                    System.out.println("not started");
                    System.exit(0);
                }
                if (status.compareTo(JXLoginPane.Status.SUCCEEDED) == 0) {
                    if (SecurityFacade.getInstance().verifyActivo(PrincipalJRibbon.getInstance().getLstUsuarios())) {
                        ClassicEngineBoot.getInstance().start();
                        ConfigFacade.autoConfig(); // Configuracion de dominio..lk{ñ-ñ{-{´p+´'0
                        if (PrincipalJRibbon.getInstance().getLstPersonas().get(0) instanceof Empleado) {
                            DlgSeleccionarEstablecimiento dlgSeleccionarEstablecimiento = new DlgSeleccionarEstablecimiento(PrincipalJRibbon.getInstance().getFrame(), true);
                            SwingUtil.centerOnScreen(dlgSeleccionarEstablecimiento);
                            dlgSeleccionarEstablecimiento.setVisible(true);
                        } else {
                            PrincipalJRibbon.getInstance().setObjEstablecimiento(DAOEstablecimiento.getInstance().getEstablecimiento(ID_ESTABLECIMIENTO_DEFAULT));
                            PrincipalJRibbon.getInstance().setTxtEstablecimiento(new JLabel());
                            PrincipalJRibbon.getInstance().setTxtEstablecimientoText(PrincipalJRibbon.getInstance().getObjEstablecimiento().getNombre());
                            PrincipalJRibbon.getInstance().main();
                        }
                    } else {
                        JOptionPane.showMessageDialog(PrincipalJRibbon.getInstance().getFrame(), "El Usuario se encuentra Inactivo", "Usuario", JOptionPane.ERROR_MESSAGE);
                        PrincipalJRibbon.getInstance().dispose();
                    }
                }
            }
        });
    }
}
